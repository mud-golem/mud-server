Instructions for setting up the backend of mud-golem
=====================================================

The backend of mud-golem is written in Java with Quarkus.
Access to SSL certificates is expected.


Step 1
=====================================================
Update system and install dependencies

# apt update
# apt upgrade
# apt install git screen openjdk-11-jre-headless mariadb-server



Step 2
=====================================================
Setting up the database;

execute the setup:
# mysql_secure_installation
    -> set root password: n
    -> remove anonymous users: y
    -> disallow root login remotely: y
    -> Remove test database and access to it: y
    -> Reload privilege tables now: y


Set up DB User:
enter Mariadb console:
# mariadb
enter with password replaced by your password:
#    CREATE USER 'admin'@'%' IDENTIFIED BY 'password';
#    GRANT ALL PRIVILEGES ON *.* TO 'admin'@'%';
#    FLUSH PRIVILEGES;
#    quit



Step 3
=====================================================
Create an non-root user.

Create a user that is not accessible by ssh.
# adduser --disabled-login mud-golem

Change to the new user
# su mud-golem

And enter his home folder
# cd ~


Step 4
=====================================================
Download backend repo.

# git clone https://bitbucket.org/mud-golem/mud-server.git

enter the repo
# cd mud-server



Step 5
=====================================================
Configure backend.

Some values must be configured, before the application is compiled.
The required config file is under 'mud-server/src/main/resources/application.properties'
Explanations for the settings are in the file itself.
!!!CONFIGURE SSL HERE!!!



create the Database used by mud-golem:
# mariadb --user=admin --password=yourpassword < mud-server/productive-mudgolem-server.sql


Step 6
=====================================================
Build backend

make mvnw file executable
# chmod a+x mvnw

compile the application
# ./mvnw -DskipTests -Dquarkus-profile=generate-code package



Step 7
=====================================================
Move required files to final directory.
Here "/home/mud-golem/mud" is used.

Copy the jar file
# cp mud-server/target/mud-server-0.0.1-runner.jar mud/

Copy start and stop scripts
# cp mud-server/start_mud.sh mud/
# cp mud-server/stop_mud.sh mud/

make scripts executable
# chmod a+x mud/start_mud.sh mud/stop_mud.sh

Create the credential files.
These must be placed in the same folder as the .jar file.

dbpass.txt:
First line of file is the password for the database
example:
password123

smtp.txt:
First line is the password for the mailserver
second line is the mailaddress from which to send the emails.
Third line is the base url of the frontend

example:
password123
some@one.de
mud.golem.de

Step 8
=====================================================
Control the server.

use the scripts start_mud.sh and stop_mud.sh to start and stop the server. It will run in a screen session
