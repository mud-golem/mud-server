package Security;

import ChatManaging.CoreChatManager;
import Exceptions.AccessDeniedException;
import Exceptions.ElementNotFoundException;
import Model.SecurityModel.MudUser;
import Model.SecurityModel.ResetPasswordObject;
import Model.SecurityModel.UserRegistrationObject;
import Processing.AuthProviderImpl;
import ServerInterfaces.AuthProvider;
import io.quarkus.runtime.Startup;
import org.apache.commons.codec.digest.DigestUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.wildfly.common.Assert;

import javax.inject.Inject;
import javax.websocket.ContainerProvider;
import javax.websocket.Session;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static io.smallrye.common.constraint.Assert.assertFalse;
import static io.smallrye.common.constraint.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class MudSecurityManagerTest {
    @Inject
    MudSecurityManager mudSecurityManager;

    @BeforeEach
    void initMocks(){
        mudSecurityManager = new MudSecurityManager();
        mudSecurityManager.initialize();
    }

    @Test
    public void verifyUserLoginAndGetSecurityKeyTest() throws ElementNotFoundException {
        AuthProviderImpl tester = mock(AuthProviderImpl.class);
        when(tester.validateUserCredentials("test",DigestUtils.sha256Hex("Golem1!?")))
                .thenReturn(true);
        mudSecurityManager.setAuthProvider(tester);
        try {
            String test = mudSecurityManager
                    .verifyUserLoginAndGetSecurityKey("test", DigestUtils.sha256Hex("Golem1!?"));
            Assert.assertNotNull(test);
            String test2 = mudSecurityManager.
                    verifyUserLoginAndGetSecurityKey("test", DigestUtils.sha256Hex("Golem1!"));
            assertNull(test2);
        } catch (Exception e){

        }
    }

    @Test
    public void getSecurityKeyTest() throws ElementNotFoundException {
        AuthProviderImpl authProvider = mock(AuthProviderImpl.class);
        when(authProvider.validateUserCredentials("test",DigestUtils.sha256Hex("Golem1!?")))
                .thenReturn(true);
        mudSecurityManager.setAuthProvider(authProvider);
        try {
            String test = mudSecurityManager
                    .verifyUserLoginAndGetSecurityKey("test", DigestUtils.sha256Hex("Golem1!?"));
            Assert.assertNotNull(test);
        } catch (Exception e){

        }
    }

    @Test
    public void setCurrentUserFromSecurityKey() throws ElementNotFoundException {
        AuthProviderImpl authProvider = mock(AuthProviderImpl.class);
        when(authProvider.validateUserCredentials("test",DigestUtils.sha256Hex("Golem1!?")))
                .thenReturn(true);
        mudSecurityManager.setAuthProvider(authProvider);
        try {
            String test = mudSecurityManager
                    .verifyUserLoginAndGetSecurityKey("test", DigestUtils.sha256Hex("Golem1!?"));
            Assert.assertNotNull(test);
            mudSecurityManager.setCurrentUserFromSecurityKey(test);
            assertTrue(mudSecurityManager.getCurrentUser().getUserName().equals("test"));
        } catch (Exception e){

        }
    }

    @Test
    public void clearCurrentUserTest() throws ElementNotFoundException {
        AuthProviderImpl authProvider = mock(AuthProviderImpl.class);
        when(authProvider.validateUserCredentials("test",DigestUtils.sha256Hex("Golem1!?")))
                .thenReturn(true);
        mudSecurityManager.setAuthProvider(authProvider);
        try {
            String test = mudSecurityManager
                    .verifyUserLoginAndGetSecurityKey("test", DigestUtils.sha256Hex("Golem1!?"));
            Assert.assertNotNull(test);
            mudSecurityManager.setCurrentUserFromSecurityKey(test);
            assertTrue(mudSecurityManager.getCurrentUser().getUserName().equals("test"));
            mudSecurityManager.clearCurrentUser();
            assertNull(mudSecurityManager.getCurrentUser());
        } catch (Exception e){

        }
    }

    @Test
    public void decryptUserTest() throws AccessDeniedException {
        String key = "AAAADObvniPNm%2B%2BfQgCSivbrldyY4mEgWmLStuZvO3%2BpwDgyKcjbzp%2BLdT6iRZFFbuwh%2FJiDeP2dHDhUjR36S" +
                "BddmkWx%2BgtvvlMLCgFyxcewqt0BqaPGfO1MR4iBomHRXh%2BQyo2ui70W48KP0HOZAr4gNYCDL4NTbPvgoObs8Q%3D%3D";
        MudUser mudUser = mudSecurityManager.decryptUser(key);
        assertNotNull(mudUser);
        assertTrue(mudUser.getUserName().equals("test"));
        assertTrue(mudUser.getSha256().equals(DigestUtils.sha256Hex("Golem1!?")));
    }

    @Test
    public void encryptRegistrationObjectTest(){
        UserRegistrationObject obj =
                new UserRegistrationObject("test","test","test@gmail.com");
        String enc = mudSecurityManager.encryptRegistrationObject(obj);
        assertNotNull(enc);
        assertFalse(enc.equals(""));
    }

    @Test
    public void decryptRegistrationObjectTest() throws AccessDeniedException {
        UserRegistrationObject obj =
                new UserRegistrationObject("test","test","test@gmail.com");
        String enc = mudSecurityManager.encryptRegistrationObject(obj);
        UserRegistrationObject dec = mudSecurityManager.decryptRegistrationObject(enc);
        assertTrue(dec.getMailAddress().equals("test@gmail.com"));
        assertTrue(dec.getPassword().equals("test"));
        assertTrue(dec.getUserName().equals("test"));
    }

    @Test
    public void encryptResetPasswordObject(){
        ResetPasswordObject resetPasswordObject =
                new ResetPasswordObject("test@gmail.com","test");
        String enc = mudSecurityManager.encryptResetPasswordObject(resetPasswordObject);
        assertNotNull(enc);
        assertFalse(enc.equals(""));
    }

    @Test
    public void decryptResetPasswordObject() throws AccessDeniedException {
        ResetPasswordObject resetPasswordObject =
                new ResetPasswordObject("test@gmail.com","test");
        String enc = mudSecurityManager.encryptResetPasswordObject(resetPasswordObject);
        ResetPasswordObject dec = mudSecurityManager.decryptResetPasswordObject(enc);
        assertTrue(dec.mailAddress.equals("test@gmail.com"));
        assertTrue(dec.username.equals("test"));
    }

    @Test
    public void isUserInDungeonTest() throws AccessDeniedException {
        CoreChatManager coreChatManager = mock(CoreChatManager.class);
        Map<String, Map<String, Session>> test = new ConcurrentHashMap<>();
        Map<String, Session> test2 = new ConcurrentHashMap<>();
        Session session = mock(Session.class);
        test2.put("test",session);
        test.put("test",test2);
        when(coreChatManager.getDungeonPlayerMap()).thenReturn(test);
        MudUser mudUser = MudUser.newUser("test","test");
        mudSecurityManager.initialize();
        mudSecurityManager.coreChatManager = coreChatManager;
        mudSecurityManager.setCurrentUser(mudUser);
        assertTrue(mudSecurityManager.isUserInDungeon("test"));
    }

    @Test
    public void isUserMasterInDungeonTest(){
        CoreChatManager coreChatManager = mock(CoreChatManager.class);
        when(coreChatManager.isMasterInDungeon("test","test"))
            .thenReturn(true);
        MudUser mudUser = MudUser.newUser("test","test");
        mudSecurityManager.initialize();
        mudSecurityManager.coreChatManager = coreChatManager;
        mudSecurityManager.setCurrentUser(mudUser);
        assertTrue(mudSecurityManager.isUserMasterInDungeon("test"));
    }
}
