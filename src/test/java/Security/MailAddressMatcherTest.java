package Security;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

public class MailAddressMatcherTest {

    @Test
    public void validateMailAddressTest(){
        Assert.assertTrue(MailAddressMatcher.validateMailAddress("test@test.de"));
        Assert.assertFalse(MailAddressMatcher.validateMailAddress("test.de"));
        Assert.assertFalse(MailAddressMatcher.validateMailAddress("test@test"));
        Assert.assertFalse(MailAddressMatcher.validateMailAddress("test"));
    }
}
