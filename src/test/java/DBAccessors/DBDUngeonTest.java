package DBAccessors;

import DataBase.*;
import DataBase.DBDungeonQueryImpl;
import Exceptions.ElementNotFoundException;
import Model.Base.*;
import Model.CommunicationModel.APIDungeon;
import com.fasterxml.jackson.databind.node.ArrayNode;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class DBDUngeonTest {

    DBConnectorTest dbConnectorTest;

    DBDungeonCreationImpl dbDungeonCreation;

    DBDungeonQueryImpl dbDungeonQuerry;

    DBDungeonEditingImpl dbDungeonEditing;

    DBRoomEditingImpl dbRoomEditing;

    DBRoomCreationImpl dbRoomCreation;

    DBRoomQueryImpl dbRoomQuery;

    @BeforeEach
    public void setup(){

        dbConnectorTest = new DBConnectorTest();

        dbDungeonCreation = new DBDungeonCreationImpl();
        dbDungeonCreation.setDbConnector(dbConnectorTest);

        dbDungeonQuerry = new DBDungeonQueryImpl();
        dbDungeonQuerry.setDbConnector(dbConnectorTest);

        dbDungeonEditing = new DBDungeonEditingImpl();
        dbDungeonEditing.setDbConnector(dbConnectorTest);

        dbRoomEditing = new DBRoomEditingImpl();
        dbRoomEditing.setDbConnector(dbConnectorTest);

        dbRoomCreation = new DBRoomCreationImpl();
        dbRoomCreation.setDbConnector(dbConnectorTest);

        dbRoomQuery = new DBRoomQueryImpl();
        dbRoomQuery.setDbConnector(dbConnectorTest);

    }

    //region descriptionTest(10000 Chars)
    String testDescription = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. " +
            "At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur " +
            "sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. " +
            "Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, " +
            "sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. " +
            "Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui " +
            "blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet " +
            "dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in " +
            "hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis " +
            "dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Lorem ipsum dolor sit amet, consectetuer " +
            "adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut " +
            "aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis. At vero eos et accusam et justo " +
            "duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt " +
            "ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. " +
            "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, At accusam aliquyam diam diam dolore dolores duo eirmod eos erat, et nonumy sed tempor et et invidunt justo labore Stet clita ea et gubergren, " +
            "kasd magna no rebum. sanctus sea sed takimata ut vero voluptua. est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore " +
            "et dolore magna aliquyam erat. Consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores " +
            "et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et " +
            "dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor " +
            "sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita " +
            "kasd gubergren, no sea takimata sanctus. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos " +
            "et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod" +
            " tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. " +
            "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet c" +
            "lita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros " +
            "et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut " +
            "laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate " +
            "velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor " +
            "cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat " +
            "volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum " +
            "dolore eu feugiat nulla facilisis. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam " +
            "nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. " +
            "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, At accusam aliquyam diam diam dolore dolores duo eirmod eos erat, et nonumy sed tempor et et invidunt justo labore Stet clita ea et gubergren, kasd magna no rebum. sanctus sea sed " +
            "takimata ut vero voluptua. est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat. Consetetur sadipscing elitr, sed diam nonumy " +
            "eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor " +
            "sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus " +
            "est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. " +
            "Stet clita kasd gubergren, no sea takimata sanctus. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo " +
            "dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed " +
            "diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor " +
            "invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Duis autem vel eum iriure dolor in " +
            "hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. " +
            "Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut " +
            "aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum " +
            "zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed " +
            "diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis au";

    //endregionTest

    //region Dungeon declarations

    String lobbyDescr = "The Lobby Descr";

    String unknownActionResponse = "Unknown Test Action Response";

    String dungeonMasterUserName = "Unknown Dungeon Master UserName";

    Dungeon testDungeon1 = new Dungeon("testDungeon1", testDescription, lobbyDescr,
            -1, unknownActionResponse, dungeonMasterUserName, true, false,
            new ArrayList<DungeonRoom>(), new ArrayList<String>(), new ArrayList<String>(), new ArrayList<String>(),
            new ArrayList<String>(), new ArrayList<PlayerAvatar>());
    Dungeon testDungeon2 = new Dungeon("testDungeon2", testDescription, lobbyDescr,
            -1, unknownActionResponse, dungeonMasterUserName, true, false,
            new ArrayList<DungeonRoom>(), new ArrayList<String>(), new ArrayList<String>(), new ArrayList<String>(),
            new ArrayList<String>(), new ArrayList<PlayerAvatar>());
    Dungeon testDungeon3 = new Dungeon("testDungeon3", testDescription, lobbyDescr,
            -1, unknownActionResponse, dungeonMasterUserName, true, false,
            new ArrayList<DungeonRoom>(), new ArrayList<String>(), new ArrayList<String>(), new ArrayList<String>(),
            new ArrayList<String>(), new ArrayList<PlayerAvatar>());
    Dungeon testDungeon4 = new Dungeon("testDungeon4", testDescription, lobbyDescr,
            -1, unknownActionResponse, dungeonMasterUserName, true, false,
            new ArrayList<DungeonRoom>(), new ArrayList<String>(), new ArrayList<String>(), new ArrayList<String>(),
            new ArrayList<String>(), new ArrayList<PlayerAvatar>());
    //endregion

    //region Class declaration

    String raceDescription = "This is a test-race";

    String raceDescriptionModifier = "This is a test descriptionmodifier";

    int raceHpModifier = 50;

    AvatarRace testRace1 = new AvatarRace(-1, "TestRace1", raceDescription, raceDescriptionModifier, raceHpModifier, true);
    AvatarRace testRace2 = new AvatarRace(-1, "TestRace2", raceDescription, raceDescriptionModifier, raceHpModifier, true);
    AvatarRace testRace3 = new AvatarRace(-1, "TestRace3", raceDescription, raceDescriptionModifier, raceHpModifier, true);
    AvatarRace testRace4 = new AvatarRace(-1, "TestRace4", raceDescription, raceDescriptionModifier, raceHpModifier, true);

    long idTestRace1;
    long idTestRace2;
    long idTestRace3;
    long idTestRace4;

    //endregion

    //region Race declaration

    String classDescription = "This is a test-class";

    String classDescriptionModifier = "This is a test descriptionmodifier";

    int classHpModifier = 59;

    AvatarClass testClass1 = new AvatarClass(-1, "TestRace1", classDescription, classDescriptionModifier, classHpModifier, true);
    AvatarClass testClass2 = new AvatarClass(-1, "TestRace2", classDescription, classDescriptionModifier, classHpModifier, true);
    AvatarClass testClass3 = new AvatarClass(-1, "TestRace3", classDescription, classDescriptionModifier, classHpModifier, true);
    AvatarClass testClass4 = new AvatarClass(-1, "TestRace4", classDescription, classDescriptionModifier, classHpModifier, true);

    long idTestClass1;
    long idTestClass2;
    long idTestClass3;
    long idTestClass4;
    //endregion

    //region Room declaration

    static List<InventoryEntry> testInventory = new ArrayList<>();

    static final ObjectTemplate testObjectTemplateApple = new ObjectTemplate(-1, "testApple", "This is an test Apple", true);

    static final ObjectTemplate testObjectTemplateBanana = new ObjectTemplate(-1, "testApple", "This is an test Apple", true);

    static final InventoryEntry testInvEntryA = new InventoryEntry(testObjectTemplateApple, 16);

    static final InventoryEntry testInvEntryB = new InventoryEntry(testObjectTemplateBanana, 16);

    static {
        testInventory.add(testInvEntryA);
        testInventory.add(testInvEntryB);
    }


    String testRoomDescription = "This is an testDungeonRoom!";

    DungeonRoom testRoom = new DungeonRoom("testRoom1", -1, testRoomDescription, -1, -1, -1, -1, testInventory, new ArrayList<CustomAction>());

    long idTestDungeonRoom1;
    long idTestDungeonRoom2;
    long idTestDungeonRoom3;
    long idTestDungeonRoom4;


    //endregion

    @Test
    public void createDungeonTest(){
        try {
            dbDungeonCreation.createDungeon(testDungeon1);
            idTestRace1 = createNewRace(testDungeon1.getUniqueName(), testRace1);
            idTestClass1 = createNewClass(testDungeon1.getUniqueName(), testClass1);
            idTestDungeonRoom1 = createNewDungeonRoom(testDungeon1.getUniqueName(), testRoom);

            dbDungeonCreation.createDungeon(testDungeon2);
            idTestRace2 = createNewRace(testDungeon2.getUniqueName(), testRace2);
            idTestClass2 = createNewClass(testDungeon2.getUniqueName(), testClass2);
            idTestDungeonRoom2 = createNewDungeonRoom(testDungeon2.getUniqueName(), testRoom);

            dbDungeonCreation.createDungeon(testDungeon3);
            idTestRace3 = createNewRace(testDungeon3.getUniqueName(), testRace3);
            idTestClass3 = createNewClass(testDungeon3.getUniqueName(), testClass3);
            idTestDungeonRoom3 = createNewDungeonRoom(testDungeon3.getUniqueName(), testRoom);


            dbDungeonCreation.createDungeon(testDungeon4);
            idTestRace4 = createNewRace(testDungeon4.getUniqueName(), testRace4);
            idTestClass4 = createNewClass(testDungeon4.getUniqueName(), testClass4);
            idTestDungeonRoom4 = createNewDungeonRoom(testDungeon4.getUniqueName(), testRoom);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getAllDungeons() throws ElementNotFoundException {
        List<APIDungeon> allDungeons = dbDungeonQuerry.getAllAPIDungeons();

        checkAvatarClasses(allDungeons);
        checkAvatarRaces(allDungeons);

        int i = 1;
        for (APIDungeon dungeon : allDungeons) {
            Assert.assertEquals(dungeon.uniqueName, "testDungeon" + Integer.toString(i));
            Assert.assertEquals(dungeon.dungeonDescription, testDescription);
            Assert.assertEquals(dungeon.lobbyDescription, lobbyDescr);
            Assert.assertEquals(dungeon.unknownActionResponse, unknownActionResponse);
            Assert.assertEquals(dungeon.dungeonMasterUserName, dungeonMasterUserName);
            i++;
        }

    }

    public void checkAvatarClasses(List<APIDungeon> allDungeons){
        allDungeons.forEach( dungeon -> {
            AtomicInteger iRaces = new AtomicInteger(1);
            try {
                dbDungeonQuerry.getAvatarClasses(dungeon.uniqueName).forEach(avatarClass -> {
                    Assert.assertEquals(avatarClass.getDisplayName(), "testClass" + Integer.toString(iRaces.getAndIncrement()));
                    Assert.assertEquals(avatarClass.getDescription(), raceDescription);
                    Assert.assertEquals(avatarClass.getDescriptionModifier(), raceDescriptionModifier);
                    Assert.assertEquals(avatarClass.getHpModifier(), raceHpModifier);
                });
            } catch (ElementNotFoundException e) {
                e.printStackTrace();
            }
        });
    }

    public void checkAvatarRaces(List<APIDungeon> allDungeons){
        allDungeons.forEach( dungeon -> {

            AtomicInteger iClasses = new AtomicInteger(1);
            try {
                dbDungeonQuerry.getAvatarRaces(dungeon.uniqueName).forEach(avatarRace -> {
                    Assert.assertEquals(avatarRace.getDisplayName(), "testRace" + Integer.toString(iClasses.getAndIncrement()));
                    Assert.assertEquals(avatarRace.getDescription(), classDescription);
                    Assert.assertEquals(avatarRace.getDescriptionModifier(), classDescriptionModifier);
                    Assert.assertEquals(avatarRace.getHpModifier(), classHpModifier);
                });
            } catch (ElementNotFoundException e) {
                e.printStackTrace();
            }
        });
    }

    public void checkAllRooms (List<APIDungeon> allDungeons){
        allDungeons.forEach( dungeon -> {

            try {
                dbRoomQuery.getRooms(dungeon.uniqueName).forEach(room -> {
                    Assert.assertEquals(room.getDescription(), testRoom.getDescription());
                    Assert.assertEquals(room.getDisplayName(), testRoom.getDisplayName());

                    room.getInventory().forEach(inventoryEntry -> {
                        Assert.assertEquals(testInvEntryA.getFullDescription(), inventoryEntry.getFullDescription());
                        Assert.assertEquals(testInvEntryA.getCount(), inventoryEntry.getCount());
                        Assert.assertEquals(testInvEntryA.getTemplate().getFullDescription(), inventoryEntry.getTemplate().getFullDescription());
                        Assert.assertEquals(testInvEntryA.getTemplate().getDisplayName(), inventoryEntry.getTemplate().getDisplayName());
                    });
                });
            } catch (ElementNotFoundException e) {
                e.printStackTrace();
            }
        });
    }

    public long createNewRace(String dungeonName, AvatarRace avatarRace){
        try {
            return dbDungeonEditing.addNewAvatarRace(dungeonName, avatarRace);
        } catch (ElementNotFoundException e){
            e.printStackTrace();
            return -1;
        }
    }

    public long createNewClass(String dungeonName, AvatarClass avatarClass){
        try {
            return dbDungeonEditing.addNewAvatarClass(dungeonName, avatarClass);
        } catch (ElementNotFoundException e){
            e.printStackTrace();
            return -1;
        }
    }

    public long createNewDungeonRoom(String dungeonName, DungeonRoom dungeonRoom){
        try {
            return dbRoomCreation.addNewRoom(dungeonName, dungeonRoom);
        } catch (ElementNotFoundException e){
            e.printStackTrace();
            return -1;
        }
    }

    @Test
    public void deleteTestDungeon(){
        dbConnectorTest.executeSQL("DELETE FROM dungeon");
        dbConnectorTest.closeConn();
    }

    @Test
    public void deleteTestClasses(){
        dbConnectorTest.executeSQL("DELETE FROM classes");
        dbConnectorTest.closeConn();
    }

    @Test
    public void deleteTestRaces(){
        dbConnectorTest.executeSQL("DELETE FROM races");
        dbConnectorTest.closeConn();
    }

    @Test
    public void deleteTestRooms(){
        dbConnectorTest.executeSQL("DELETE FROM dungeonRooms");
        dbConnectorTest.closeConn();
    }

}
