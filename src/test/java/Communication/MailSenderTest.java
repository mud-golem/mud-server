package Communication;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.wildfly.common.Assert;


import static org.hamcrest.core.Is.is;
import static org.hamcrest.MatcherAssert.*;
import static org.junit.Assert.assertTrue;

public class MailSenderTest {

    MailSender mailSender;

    @BeforeEach
    public void setUp(){
        mailSender = new MailSender();
    }

    @Test
    public void sendChangePasswordMailTest(){
        try {
            mailSender.sendChangePasswordMail(null, null);
        }catch(Exception e){
            assertTrue(e.getClass()==IllegalArgumentException.class);
            assertThat(e.getMessage(),is("key can't be null or empty"));
        }
        try{
            mailSender.sendChangePasswordMail("",null);
        }catch(Exception e){
            assertTrue(e.getClass()==IllegalArgumentException.class);
            assertThat(e.getMessage(),is("key can't be null or empty"));
        }
        try{
            mailSender.sendChangePasswordMail("abc",null);
        }catch(Exception e){
            assertTrue(e.getClass()==IllegalArgumentException.class);
            assertThat(e.getMessage(),is("enter a valid mailAddress"));
        }
        try{
            mailSender.sendChangePasswordMail("","noreply@mud-golem.com");
        }catch(Exception e){
            assertTrue(e.getClass()==IllegalArgumentException.class);
            assertThat(e.getMessage(),is("key can't be null or empty"));
        }
        try{
            mailSender.sendChangePasswordMail("abc","noreply@mud-golem");
        }catch(Exception e){
            assertTrue(e.getClass()==IllegalArgumentException.class);
            assertThat(e.getMessage(),is("enter a valid mailAddress"));
        }try{
            mailSender.sendChangePasswordMail("abc","noreply@mud-golem.com");
        }catch(Exception e){

        }
    }

    @Test
    public void sendRegistrationMailTest(){
        try {
            mailSender.sendRegistrationMail(null, null);
        }catch(Exception e){
            assertTrue(e.getClass()==IllegalArgumentException.class);
            assertThat(e.getMessage(),is("key can't be null or empty"));
        }
        try{
            mailSender.sendRegistrationMail("",null);
        }catch(Exception e){
            assertTrue(e.getClass()==IllegalArgumentException.class);
            assertThat(e.getMessage(),is("key can't be null or empty"));
        }
        try{
            mailSender.sendRegistrationMail("abc",null);
        }catch(Exception e){
            assertTrue(e.getClass()==IllegalArgumentException.class);
            assertThat(e.getMessage(),is("enter a valid mailAddress"));
        }
        try{
            mailSender.sendRegistrationMail("","noreply@mud-golem.com");
        }catch(Exception e){
            assertTrue(e.getClass()==IllegalArgumentException.class);
            assertThat(e.getMessage(),is("key can't be null or empty"));
        }
        try{
            mailSender.sendRegistrationMail("abc","noreply@mud-golem");
        }catch(Exception e){
            assertTrue(e.getClass()==IllegalArgumentException.class);
            assertThat(e.getMessage(),is("enter a valid mailAddress"));
        }try{
            mailSender.sendRegistrationMail("abc","noreply@mud-golem.com");
        }catch(Exception e){

        }
    }
}
