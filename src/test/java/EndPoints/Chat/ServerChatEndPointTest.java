package EndPoints.Chat;

import Model.CommunicationModel.Message;
import Model.CommunicationModel.MessageType;
import io.quarkus.test.common.http.TestHTTPResource;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.websocket.*;
import java.net.URI;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;

@QuarkusTest
public class ServerChatEndPointTest {

    private static final LinkedBlockingDeque<String> MESSAGES =
            new LinkedBlockingDeque<>();

    @TestHTTPResource("/server/chat")
    URI uri;

    @Test
    public void testWebSocketChat() throws Exception{
        Message message = new Message();
        try(Session session = ContainerProvider.getWebSocketContainer()
                .connectToServer(Client.class,uri)){
            Assertions.assertEquals("CONNECT",MESSAGES.poll(10, TimeUnit.SECONDS));
        }
    }

    @ClientEndpoint
    public static class Client{

        @OnOpen
        public void open(Session session){
            MESSAGES.add("CONNECT");
            session.getAsyncRemote().sendText("_ready_");
        }

        @OnMessage
        void message(String msg){
            MESSAGES.add(msg);
        }
    }
}
