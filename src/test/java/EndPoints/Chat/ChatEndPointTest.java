package EndPoints.Chat;

import ChatManaging.CoreChatManager;
import Model.CommunicationModel.Message;
import Model.CommunicationModel.MessageType;
import io.quarkus.test.common.http.TestHTTPResource;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.websocket.*;
import java.net.URI;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;

@QuarkusTest
public class ChatEndPointTest {

    private static final LinkedBlockingDeque<String> MESSAGES =
            new LinkedBlockingDeque<>();

    @TestHTTPResource("/chat/Oblivion/AAAADL3eQ8uSpcseXnUZR1iN9lxK7ThBB8yQzhlmANhhOeyFG0fHItoe66zzp84DsYofxlpu1vKXC%2B3tYZjSRfewTZwyreEk9qvosxEkOaqtx%2B6FWobeS%2BHBR%2BcpJLtB2X2COS1DKxSTvfOApRufVOxPKGO%2BfwwrcZfIkhW36g%3D%3D")
    URI uri;

    @Test
    public void testWebSocketChat() throws Exception{
        Message message = new Message();
        message.content = "Hey";
        message.type = MessageType.RoomChatMessage;
        try(Session session = ContainerProvider.getWebSocketContainer()
            .connectToServer(Client.class,uri)){
            Assertions.assertEquals("CONNECT",MESSAGES.poll(10, TimeUnit.SECONDS));
        }
    }

    @ClientEndpoint
    public static class Client{

        @OnOpen
        public void open(Session session){
            MESSAGES.add("CONNECT");
            session.getAsyncRemote().sendText("_ready_");
        }

        @OnMessage
        void message(String msg){
            MESSAGES.add(msg);
        }
    }
}
