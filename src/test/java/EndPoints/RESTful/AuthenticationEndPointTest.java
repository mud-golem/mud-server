package EndPoints.RESTful;

import Model.CommunicationModel.LogInForm;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;
import io.vertx.core.json.JsonObject;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.core.Is.is;

@QuarkusTest
public class AuthenticationEndPointTest {

    @Test
    public void loginTest() throws JsonProcessingException {
        LogInForm test = new LogInForm();
        test.password = "Golem1!?"; test.username = "test";

        ObjectMapper mapper = new ObjectMapper();

        given()
                .body(mapper.writeValueAsString(test))
                .contentType(ContentType.JSON)
                .when()
                .post("/login")
                .then()
                .statusCode(200);

        test.password = "1234";
        given()
                .body(mapper.writeValueAsString(test))
                .contentType(ContentType.JSON)
                .when()
                .post("/login")
                .then()
                .statusCode(401);
    }
}
