package EndPoints.RESTful;

import Model.CommunicationModel.APIDungeonRoom;
import Model.CommunicationModel.APIObjectTemplate;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;

@QuarkusTest
public class RoomEndPointTest {

    @Test
    public void getDungeonRoomsTest(){
        given()
                .when()
                .get("/dungeons/test/rooms")
                .then()
                .statusCode(401);
    }

    @Test
    public void createRoomInDungeonTest() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();

        APIDungeonRoom room = new APIDungeonRoom();
        given()
                .body(mapper.writeValueAsString(room))
                .contentType(ContentType.JSON)
                .when()
                .post("/dungeons/test/rooms")
                .then()
                .statusCode(401);
    }

    @Test
    public void editRoomInDungeonTest() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();

        APIDungeonRoom room = new APIDungeonRoom();
        given()
                .body(mapper.writeValueAsString(room))
                .contentType(ContentType.JSON)
                .when()
                .put("/dungeons/test/rooms/test")
                .then()
                .statusCode(401);
    }

    @Test
    public void getDungeonRoomTest(){
        given()
                .when()
                .get("/dungeons/test/rooms/test")
                .then()
                .statusCode(401);
    }

    @Test
    public void deleteRoomTest(){
        given()
                .when()
                .delete("/dungeons/test/rooms/test")
                .then()
                .statusCode(401);
    }
}
