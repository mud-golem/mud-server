package EndPoints.RESTful;

import Model.CommunicationModel.APIAvatar;
import Model.CommunicationModel.APICustomAction;
import Model.CommunicationModel.CreatedAvatar;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;

@QuarkusTest
public class AvatarEndPointTest {

    @Test
    public void createAvatarTest() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();

        CreatedAvatar avatar = new CreatedAvatar();
        avatar.uniqueName = "test";
        avatar.classID = 1;
        avatar.raceID = 2;
        avatar.userDescription = "test";
        given()
                .body(mapper.writeValueAsString(avatar))
                .contentType(ContentType.JSON)
                .when()
                .post("/dungeons/test/avatars")
                .then()
                .statusCode(401);
    }

    @Test
    public void getAllAvatarsTest(){
        given()
                .when()
                .get("/dungeons/test/avatars")
                .then()
                .statusCode(401);
    }

    @Test
    public void getAvatarTest(){
        given()
                .when()
                .get("/dungeons/test/avatars/test")
                .then()
                .statusCode(401);
    }

    @Test
    public void editAvatarTest() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();

        APIAvatar avatar = new APIAvatar();
        avatar.userDescription = "test";
        given()
                .body(mapper.writeValueAsString(avatar))
                .contentType(ContentType.JSON)
                .when()
                .put("/dungeons/test/avatars/test")
                .then()
                .statusCode(401);
    }

    @Test
    public void deleteCharacterTest(){
        given()
                .when()
                .delete("/dungeons/test/avatars/test")
                .then()
                .statusCode(401);
    }
}
