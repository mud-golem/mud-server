package EndPoints.RESTful;

import Model.CommunicationModel.APIObjectTemplate;
import Model.CommunicationModel.CreatedAvatar;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;

@QuarkusTest
public class ObjectEndPointTest {

    @Test
    public void createNewObjectTest() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();

        APIObjectTemplate objectTemplate = new APIObjectTemplate();
        given()
                .body(mapper.writeValueAsString(objectTemplate))
                .contentType(ContentType.JSON)
                .when()
                .post("/dungeons/test/objects")
                .then()
                .statusCode(401);
    }

    @Test
    public void getAllObjectsTest(){
        given()
                .when()
                .get("/dungeons/test/objects")
                .then()
                .statusCode(401);
    }

    @Test
    public void getObjectTest(){
        given()
                .when()
                .get("/dungeons/test/objects/test")
                .then()
                .statusCode(401);
    }

    @Test
    public void editObjectTest() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();

        APIObjectTemplate objectTemplate = new APIObjectTemplate();
        given()
                .body(mapper.writeValueAsString(objectTemplate))
                .contentType(ContentType.JSON)
                .when()
                .put("/dungeons/test/objects/test")
                .then()
                .statusCode(401);
    }
}
