package EndPoints.RESTful;

import Exceptions.ElementNotFoundException;
import Model.CommunicationModel.APIAction;
import Model.CommunicationModel.APICustomAction;
import Processing.ActionHandlerImpl;
import Security.MudSecurityManager;
import ServerInterfaces.ActionHandler;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.quarkus.test.Mock;
import io.quarkus.test.TestTransaction;
import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;
import io.restassured.http.Header;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import javax.enterprise.inject.Stereotype;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static io.restassured.RestAssured.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@QuarkusTest
public class ActionEndPointTest {

    public static final String KEY = "AAAADL3eQ8uSpcseXnUZR1iN9lxK7ThBB8yQzhlmANhhOeyFG0fHItoe66zzp84DsYofxlpu1vKXC%2B3tYZjSRfewTZwyreEk9qvosxEkOaqtx%2B6FWobeS%2BHBR%2BcpJLtB2X2COS1DKxSTvfOApRufVOxPKGO%2BfwwrcZfIkhW36g%3D%3D";

    @InjectMocks
    MudSecurityManager mudSecurityManager;

   @Test
    public void sendActionTest() throws ElementNotFoundException, JsonProcessingException {

       ObjectMapper mapper = new ObjectMapper();

        APIAction action = new APIAction();
        action.action = "test";
        given()
                .body(mapper.writeValueAsString(action))
                .contentType(ContentType.JSON)
                .when()
                .put("/dungeons/test/action")
                .then()
                .statusCode(401);
    }

    @Test
    public void getAllCustomActionsTest(){
        given()
                .when()
                .get("/dungeons/test/action")
                .then()
                .statusCode(401);
    }

    @Test
    public void addCustomActionTest() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();

        APICustomAction action = new APICustomAction();
        action.command = "test";
        action.name = "test";
        action.response = "test";
        given()
                .body(mapper.writeValueAsString(action))
                .contentType(ContentType.JSON)
                .when()
                .post("/dungeons/test/action")
                .then()
                .statusCode(401);
    }

    @Test
    public void editCustomActionTest() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();

        APICustomAction action = new APICustomAction();
        action.command = "test";
        action.name = "test";
        action.response = "test";
        given()
                .body(mapper.writeValueAsString(action))
                .contentType(ContentType.JSON)
                .when()
                .put("/dungeons/test/action/test")
                .then()
                .statusCode(401);
    }

    @Test
    public void getCustomActionTest(){
        given()
                .when()
                .get("/dungeons/test/action/test")
                .then()
                .statusCode(401);
    }
}
