package EndPoints.RESTful;

import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.Header;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;

@QuarkusTest
public class DungeonEndPointTest {

    @Test
    public void createDungeonTest(){
        given()
                .when()
                .header(new Header("SecurityKey","hdaipugpifb<OVNBüoj"))
                .get("/dungeons")
                .then()
                .statusCode(401);
    }
}
