package ChatManaging;

import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ChatSecurityManagerTest {

    private ChatSecurityManager chatSecurityManager;

    @BeforeEach
    public void setUp(){
        this.chatSecurityManager = new ChatSecurityManager();
    }

    @Test
    public void tryAddAvatarTest(){
        chatSecurityManager.tryAddAvatar("testDungeon","testUser","testAvatar");
        Assert.assertTrue(chatSecurityManager.getAvatarDungeonMap().get("testDungeon") != null);
        Assert.assertTrue(chatSecurityManager.getAvatarDungeonMap().get("testDungeon").get("testAvatar").equals("testUser"));
        Assert.assertTrue(chatSecurityManager.getUserDungeonMap().get("testDungeon").get("testUser").equals("testAvatar"));
        chatSecurityManager.removeEntriesForUser("testDungeon","testUser");

        chatSecurityManager.tryAddAvatar("testDungeon", null, null);
        Assert.assertTrue(chatSecurityManager.getAvatarDungeonMap().get("testDungeon").get("testAvatar") == null);
        Assert.assertTrue(chatSecurityManager.getUserDungeonMap().get("testDungeon").get("testUSer") == null);
    }

    @Test
    public void addUserToRoomMapTest(){
        chatSecurityManager.addToUserRoomMap("testDungeon","testUser","testRoom");
        Assert.assertTrue(chatSecurityManager.getUserRoomMap().get("testDungeon") != null);
        Assert.assertTrue(chatSecurityManager.getUserRoomMap().get("testDungeon").get("testUser").equals("testRoom"));
        chatSecurityManager.removeEntriesForUser("testDungeon","testUser");

        chatSecurityManager.addToUserRoomMap("testDungeon",null,null);
        Assert.assertTrue(chatSecurityManager.getUserRoomMap().get("testDungeon").get("testUser") == null);
    }

    @Test
    public void removeEntriesForUserTest(){
        chatSecurityManager.tryAddAvatar("testDungeon","testUser","testAvatar");
        chatSecurityManager.addToUserRoomMap("testDungeon","testUser","testRoom");
        chatSecurityManager.addEntryForMasterMap("testDungeon","testUser");
        chatSecurityManager.removeEntriesForUser("testDungeon","testUser");

        Assert.assertTrue(chatSecurityManager.getAvatarDungeonMap().get("testDungeon").get("testAvatar") == null);
        Assert.assertTrue(chatSecurityManager.getUserDungeonMap().get("testDungeon").get("testUSer") == null);
        Assert.assertTrue(chatSecurityManager.getUserRoomMap().get("testDungeon").get("testUser") == null);
        Assert.assertTrue(chatSecurityManager.getMasterMap().get("testDungeon") == null);
    }

    @Test
    public void addEntryForMasterMapTest(){
        chatSecurityManager.addEntryForMasterMap("testDungeon","testUser");
        Assert.assertTrue(chatSecurityManager.getMasterMap().get("testDungeon").equals("testUser"));
        chatSecurityManager.removeEntriesForUser("testDungeon","testUser");

        chatSecurityManager.addEntryForMasterMap("testDungeon",null);
        Assert.assertTrue(chatSecurityManager.getMasterMap().get("testDungeon") == null);
    }

    @Test
    public void changeRoomTest(){
        chatSecurityManager.addToUserRoomMap("testDungeon","testUser","testRoom");
        chatSecurityManager.changeRoom("testDungeon","testRoom2","testUser");

        Assert.assertFalse(chatSecurityManager.getUserRoomMap().get("testDungeon").get("testUser").equals("testRoom"));
        Assert.assertTrue(chatSecurityManager.getUserRoomMap().get("testDungeon").get("testUser").equals("testRoom2"));

        chatSecurityManager.changeRoom("testDungeon",null,"testUser");

        Assert.assertFalse(chatSecurityManager.getUserRoomMap().get("testDungeon").get("testUser").equals("testRoom"));
        Assert.assertTrue(chatSecurityManager.getUserRoomMap().get("testDungeon").get("testUser").equals("testRoom2"));

        chatSecurityManager.removeEntriesForUser("testDungeon","testUser");
    }
}
