package ChatManaging;


import org.junit.jupiter.api.Test;
import org.wildfly.common.Assert;

import javax.inject.Inject;
import javax.websocket.ContainerProvider;
import javax.websocket.Session;

import java.net.URI;

public class SessionManagerTest {

    @Inject
    SessionManager sessionManager;

    private String uri = "ws://localhost:1996/server/chat";
    private Session userSession = null;
    private String answer;

    @Test
    public void joinChatTest(){
        try {
            Session session = ContainerProvider.getWebSocketContainer().connectToServer(this, new URI(uri));
            sessionManager.setRoomFirstTime("test","test",1);
            sessionManager.addSessionToManager(session,"test","test");
            Assert.assertTrue(sessionManager.getDungeonPlayerMap().containsKey("test"));
            Assert.assertTrue(sessionManager.getDungeonPlayerMap().get("test").containsKey("test"));
            Assert.assertTrue(sessionManager.getDungeonPlayerMap().get("test").containsValue(session));
            sessionManager.remove("test","test");
        }catch(Exception e){

        }
    }

    @Test
    public void setRoomFirstTime(){
        try{
            Session session = ContainerProvider.getWebSocketContainer().connectToServer(this, new URI(uri));
            sessionManager.setRoomFirstTime("test","test",1);
            sessionManager.addSessionToManager(session,"test","test");
            sessionManager.setRoomFirstTime("test","test",1);
            Assert.assertTrue(sessionManager.getDungeonPlayerMap().containsKey("test"));
            Assert.assertTrue(sessionManager.getDungeonPlayerMap().get("test").containsKey("test"));
            Assert.assertTrue(sessionManager.getDungeonPlayerMap().get("test").containsValue(session));

            Assert.assertTrue(sessionManager.getDungeonRoomPlayerMap().containsKey("test"));
            Assert.assertTrue(sessionManager.getDungeonRoomPlayerMap().get("test").containsKey("test"));
            Assert.assertTrue(sessionManager.getDungeonRoomPlayerMap().get("test").get("test").containsKey("test"));
            Assert.assertTrue(sessionManager.getDungeonRoomPlayerMap().get("test").get("test").get("test") == session);
            Assert.assertTrue(sessionManager.getDungeonRoomPlayerMap().get("test").get("test").containsValue(session));

            sessionManager.remove("test","test");
        }catch(Exception e){

        }
    }

    @Test
    public void changeRoomTest(){
        try{
            Session session = ContainerProvider.getWebSocketContainer().connectToServer(this, new URI(uri));
            sessionManager.setRoomFirstTime("test","test",1);
            sessionManager.addSessionToManager(session,"test","test");
            sessionManager.setRoomFirstTime("test","test",1);
            sessionManager.changeRoom("test",1,2,"test");

            Assert.assertTrue(sessionManager.getDungeonPlayerMap().containsKey("test"));
            Assert.assertTrue(sessionManager.getDungeonPlayerMap().get("test").containsKey("test"));
            Assert.assertTrue(sessionManager.getDungeonPlayerMap().get("test").containsValue(session));

            Assert.assertTrue(sessionManager.getDungeonRoomPlayerMap().containsKey("test"));
            Assert.assertTrue(sessionManager.getDungeonRoomPlayerMap().get("test").containsKey("test"));
            Assert.assertFalse(sessionManager.getDungeonRoomPlayerMap().get("test").get(1).containsKey("test"));
            Assert.assertFalse(sessionManager.getDungeonRoomPlayerMap().get("test").get(1).get("test") == session);
            Assert.assertFalse(sessionManager.getDungeonRoomPlayerMap().get("test").get(1).containsValue(session));

            Assert.assertTrue(sessionManager.getDungeonRoomPlayerMap().containsKey("test"));
            Assert.assertTrue(sessionManager.getDungeonRoomPlayerMap().get("test").containsKey(2));
            Assert.assertTrue(sessionManager.getDungeonRoomPlayerMap().get("test").get(2).containsKey("test"));
            Assert.assertTrue(sessionManager.getDungeonRoomPlayerMap().get("test").get(2).get("test") == session);
            Assert.assertTrue(sessionManager.getDungeonRoomPlayerMap().get("test").get(2).containsValue(session));

            sessionManager.remove("test","test");
        }catch(Exception e){

        }
    }

    @Test
    public void removeTest(){
        try{
            Session session = ContainerProvider.getWebSocketContainer().connectToServer(this, new URI(uri));
            sessionManager.setRoomFirstTime("test","test",1);
            sessionManager.addSessionToManager(session,"test","test");
            sessionManager.remove("test","test");

            Assert.assertFalse(sessionManager.getDungeonPlayerMap().containsKey("test"));
            Assert.assertFalse(sessionManager.getDungeonPlayerMap().get("test").containsKey("test"));
            Assert.assertFalse(sessionManager.getDungeonPlayerMap().get("test").containsValue(session));

            sessionManager.setRoomFirstTime("test","test",1);
            sessionManager.addSessionToManager(session,"test","test");
            sessionManager.setRoomFirstTime("test","test",1);
            sessionManager.remove("test","test");

            Assert.assertFalse(sessionManager.getDungeonPlayerMap().containsKey("test"));
            Assert.assertFalse(sessionManager.getDungeonPlayerMap().get("test").containsKey("test"));
            Assert.assertFalse(sessionManager.getDungeonPlayerMap().get("test").containsValue(session));

            Assert.assertFalse(sessionManager.getDungeonRoomPlayerMap().containsKey("test"));
            Assert.assertFalse(sessionManager.getDungeonRoomPlayerMap().get("test").containsKey(1));
            Assert.assertFalse(sessionManager.getDungeonRoomPlayerMap().get("test").get(1).containsKey("test"));
            Assert.assertFalse(sessionManager.getDungeonRoomPlayerMap().get("test").get(1).get("test") == session);
            Assert.assertFalse(sessionManager.getDungeonRoomPlayerMap().get("test").get(1).containsValue(session));

            sessionManager.setRoomFirstTime("test","test",1);
            sessionManager.addSessionToManager(session,"test","test");
            sessionManager.setRoomFirstTime("test","test",1);
            sessionManager.changeRoom("test",1,2,"test");
            sessionManager.remove("test","test");

            Assert.assertFalse(sessionManager.getDungeonPlayerMap().containsKey("test"));
            Assert.assertFalse(sessionManager.getDungeonPlayerMap().get("test").containsKey("test"));
            Assert.assertFalse(sessionManager.getDungeonPlayerMap().get("test").containsValue(session));

            Assert.assertFalse(sessionManager.getDungeonRoomPlayerMap().containsKey("test"));
            Assert.assertFalse(sessionManager.getDungeonRoomPlayerMap().get("test").containsKey(2));
            Assert.assertFalse(sessionManager.getDungeonRoomPlayerMap().get("test").get(2).containsKey("test"));
            Assert.assertFalse(sessionManager.getDungeonRoomPlayerMap().get("test").get(2).get("test") == session);
            Assert.assertFalse(sessionManager.getDungeonRoomPlayerMap().get("test").get(2).containsValue(session));

        }catch(Exception e){

        }
    }
}