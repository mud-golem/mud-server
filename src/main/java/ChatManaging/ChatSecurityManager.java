package ChatManaging;

import Exceptions.ElementNotFoundException;
import Processing.PlayerQueryImpl;
import ServerInterfaces.PlayerQuery;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@Singleton
/**
 * The ChatSecurityManager manages the rooms, avatars and usernames of dungeons for the Chat
 * It is needed that players cant send messages with any avatarname or from any room they want
 * ©Jan Stöffler
 */
public class ChatSecurityManager {
    //Map that maps from users to their avatarNames: Map<dungeonName,Map<avatarName,userName>>
    private Map<String, Map<String,String>> avatarDungeonMap = new ConcurrentHashMap<>();
    //Map that maps from avatars to their userName: Map<dungeonName,Map<userName,avatarName>>
    private Map<String,Map<String,String>> userDungeonMap = new ConcurrentHashMap<>();

    //Map that maps from userName to their current Room: Map<dungeonName,Map<userName,roomName>>
    private Map<String,Map<String,String>> userRoomMap = new ConcurrentHashMap<>();

    //Map that maps from a dungeonName top its current master: Map<dungeonName,userName>
    private Map<String,String> masterMap = new ConcurrentHashMap<>();

    private PlayerQuery playerQuery;

    @Inject
    public void setPlayerQuery(PlayerQueryImpl playerQuery){
        this.playerQuery = playerQuery;
    }

    @Inject
    SessionManager sessionManager;

    /**
     * @author Jan Stöffler
     * tryAddAvatar tries to add an Avatar to the system
     * @param dungeonName
     * @param userName
     * @param avatarName
     */
    protected void tryAddAvatar(String dungeonName, String userName, String avatarName){
        if(dungeonName != null && userName != null && avatarName != null) {
            if (avatarDungeonMap.get(dungeonName) == null) {
                avatarDungeonMap.put(dungeonName, new ConcurrentHashMap<>());
                avatarDungeonMap.get(dungeonName).put(avatarName, userName);
            } else if (avatarDungeonMap.get(dungeonName).get(avatarName) == null) {
                avatarDungeonMap.get(dungeonName).put(avatarName, userName);
            }
            if (userDungeonMap.get(dungeonName) == null) {
                userDungeonMap.put(dungeonName, new ConcurrentHashMap<>());
                userDungeonMap.get(dungeonName).put(userName, avatarName);
            } else if (userDungeonMap.get(dungeonName).get(avatarName) == null) {
                userDungeonMap.get(dungeonName).put(userName, avatarName);
            }
        }
        //TODO add error handling
    }

    /**
     * @author Jan Stöffler
     * addToUserRoomMap adds an entry for a player in the USerRoomMap, so the System can always tell in which room he is
     * @param dungeonName
     * @param playerName
     * @param roomName
     */
    protected void addToUserRoomMap(String dungeonName, String playerName, String roomName){
        if(dungeonName != null && playerName != null && roomName != null) {
            if (userRoomMap.get(dungeonName) == null) {
                userRoomMap.put(dungeonName, new ConcurrentHashMap<>());
                userRoomMap.get(dungeonName).put(playerName, roomName);
            } else if (userRoomMap.get(dungeonName).get(playerName) == null) {
                userRoomMap.get(dungeonName).put(playerName, roomName);
            }
        }
        //TODO Errorhandling
    }

    /**
     * @author Jan Stöffler
     * removeEntriesForUser() removes all entries of a User in the system
     * @param dungeonName
     * @param playerName
     */
    protected void removeEntriesForUser(String dungeonName, String playerName){
        String avatarName = "";
        //remove from userDungeonMap
        if(userDungeonMap.get(dungeonName)!= null) {
            if(userDungeonMap.get(dungeonName).get(playerName) != null) {
                avatarName = userDungeonMap.get(dungeonName).get(playerName);
                userDungeonMap.get(dungeonName).remove(playerName);
            }
        }
        //remove from avatarDungeonMap
        if(!avatarName.equals("") && avatarDungeonMap.get(dungeonName) != null) {
            if(avatarDungeonMap.get(dungeonName).get(avatarName) != null) {
                avatarDungeonMap.get(dungeonName).remove(avatarName);
            }
        }
        //remove from userRoomMap
        if(userRoomMap.get(dungeonName) != null) {
            if(userRoomMap.get(dungeonName).get(playerName) != null)
            userRoomMap.get(dungeonName).remove(playerName);
        }
        //remove from masterMap if necessary
        if(masterMap.get(dungeonName) != null && masterMap.get(dungeonName).equals(playerName)){
            masterMap.remove(dungeonName);
        }
    }

    /**
     * @author Jan Stöffler
     * addEntryForMasterMap makes a new entry in the masterMap, so the System knows that hes is the master in the
     * specified dungeon
     * @param dungeonName
     * @param playerName
     */
    protected void addEntryForMasterMap(String dungeonName, String playerName){
        if(dungeonName != null && playerName != null) {
            masterMap.put(dungeonName, playerName);
        }
        //TODO Error Handling
    }

    /**
     * @author Jan Stöffler
     * changeRoom changes the room of a player to a new room
     * @param dungeonName
     * @param newRoom
     * @param playerName
     */
    protected void changeRoom(String dungeonName, String newRoom, String playerName){
        if(dungeonName != null && newRoom != null && playerName != null) {
            if (userRoomMap.get(dungeonName) != null) {
                if (userRoomMap.get(dungeonName).get(playerName) != null) {
                    userRoomMap.get(dungeonName).remove(playerName);
                }
            }
            if (userRoomMap.get(dungeonName) == null) {
                userRoomMap.put(dungeonName, new ConcurrentHashMap<>());
                userRoomMap.get(dungeonName).put(playerName, newRoom);
            } else userRoomMap.get(dungeonName).put(playerName, newRoom);
        }
        //TODO Error Handling
    }

    /**
     * @author Jan Stöffler
     * getAllAvatarNamesInSameRoom gets all names of active avatars in the same room as the user who performed
     * this method
     * @param dungeonName
     * @param userName
     * @return List of avatarNames
     */
    protected List<String> getAllAvatarNamesInSameRoom(String dungeonName, String userName) {
        try {
            long roomID = playerQuery.getActualRoomGlobalId(dungeonName, userName);
            List<String> userNames = sessionManager.getDungeonRoomPlayerMap().get(dungeonName).
                    get(roomID)
                    .keySet()
                    .stream()
                    .filter(string -> !userName.equals(string)).collect(Collectors.toList());
            if(masterMap.get(dungeonName) != null) {
                List<String> list = userNames.stream().map(name -> {
                    return userDungeonMap.get(dungeonName).get(name);
                }).collect(Collectors.toList());
                list.add("DM");
                return list;
            }
            return userNames.stream().map(name -> {
                return userDungeonMap.get(dungeonName).get(name);
            }).collect(Collectors.toList());
        } catch (Exception e){
            return Collections.EMPTY_LIST;
        }
    }

    /**
     * @author Jan Stöffler
     * getAllAvatarNamesInDungeon gets all names of activeAvatars in a dungeon
     * @param dungeonName
     * @return List of avatarNames
     */
    protected List<String> getAllAvatarNamesInDungeon(String dungeonName){
        try {
            return new ArrayList<>(userDungeonMap.get(dungeonName).values());
        } catch (Exception e){
            return Collections.emptyList();
        }
    }

    protected Map<String,Map<String,String>> getAvatarDungeonMap(){
        return avatarDungeonMap;
    }

    protected Map<String,Map<String,String>> getUserDungeonMap(){
        return userDungeonMap;
    }

    protected Map<String,Map<String,String>> getUserRoomMap(){
        return userRoomMap;
    }

    protected Map<String,String> getMasterMap(){ return masterMap; }
}
