package ChatManaging;

import Communication.MailSender;
import Exceptions.ElementNotFoundException;
import Processing.DungeonManagementImpl;
import Processing.PlayerEditingImpl;
import ServerInterfaces.DungeonManagement;
import ServerInterfaces.PlayerEditing;
import org.jboss.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.websocket.Session;
import java.io.IOException;
import java.util.List;
import java.util.Map;

@ApplicationScoped
/**
 * The CoreChatManager manages everything concerning the Chat System
 * ©Jan Stöffler
 */
public class CoreChatManager {

    private static final Logger LOG = Logger.getLogger(CoreChatManager.class);

    private PlayerEditing playerEditing;

    private DungeonManagement dungeonManagement;

    @Inject
    public void setDungeonManagement(DungeonManagementImpl dungeonManagement){
        this.dungeonManagement = dungeonManagement;
    }

    @Inject
    public void setPlayerEditing(PlayerEditingImpl playerEditing) {
        this.playerEditing = playerEditing;
    }

    @Inject
    SessionManager sessionManager;

    @Inject
    ChatSecurityManager chatSecurityManager;


    /**
     * @param session
     * @param dungeonName
     * @param playerName
     * @param avatarName
     * @param roomName
     * @author Jan Stöffler
     * joinChat lets a player join the chat-System
     */
    public void joinChat(Session session, String dungeonName, String playerName, String avatarName, long roomID, String roomName) throws ElementNotFoundException {
        try {
            playerEditing.setPlayerIsActive(dungeonName, playerName, true);
            chatSecurityManager.addToUserRoomMap(dungeonName, playerName, roomName);
            chatSecurityManager.tryAddAvatar(dungeonName, playerName, avatarName);
            sessionManager.addSessionToManager(session, dungeonName, playerName);
            sessionManager.setRoomFirstTime(playerName, dungeonName, roomID);
            chatSecurityManager.addToUserRoomMap(dungeonName, playerName, roomName);
        } catch (ElementNotFoundException el) {
            LOG.info(el.toString(), el);
            throw el;
        }
    }

    /**
     * @param dungeonName
     * @param oldRoomID
     * @param newRoomID
     * @param playerName
     * @author Jan Stöffler
     * changeRoom lets a player change the room
     */
    public void changeRoom(String dungeonName, long oldRoomID, long newRoomID, String roomName, String playerName) {
        sessionManager.changeRoom(dungeonName, oldRoomID, newRoomID, playerName);
        chatSecurityManager.changeRoom(dungeonName, roomName, playerName);
    }

    /**
     * @param session
     * @param dungeonName
     * @param playerName
     * @author Jan Stöffler
     * joinAsMaster lets a player join the system as master
     */
    public void joinAsMaster(Session session, String dungeonName, String playerName) throws ElementNotFoundException {
        dungeonManagement.setActualDM(dungeonName,playerName);
        sessionManager.addSessionToManager(session, dungeonName, playerName);
        chatSecurityManager.addEntryForMasterMap(dungeonName, playerName);
    }

    /**
     * @param dungeonName
     * @param playerName
     * @author Jan Stöffler
     * removes all data of a player from the system (logout)
     */
    public void remove(String dungeonName, String playerName) throws ElementNotFoundException, IOException {
        try {
            if(chatSecurityManager.getMasterMap().get(dungeonName) != null){
                if(chatSecurityManager.getMasterMap().get(dungeonName).equals(playerName)){
                    dungeonManagement.removeDM(dungeonName);
                }
            }
            sessionManager.remove(dungeonName, playerName);
            chatSecurityManager.removeEntriesForUser(dungeonName, playerName);
            playerEditing.setPlayerIsActive(dungeonName, playerName, false);
        } catch (ElementNotFoundException | IOException el) {
            LOG.info(el.toString(), el);
            throw el;
        }
    }

    public Map<String, Map<String, String>> getAvatarDungeonMap() {
        return chatSecurityManager.getAvatarDungeonMap();
    }

    public Map<String, Map<String, String>> getUserDungeonMap() {
        return chatSecurityManager.getUserDungeonMap();
    }

    public Map<String, Map<String, String>> getUserRoomMap() {
        return chatSecurityManager.getUserRoomMap();
    }

    public Map<String, String> getMasterMap() {
        return chatSecurityManager.getMasterMap();
    }

    public Map<String, Map<String, Session>> getDungeonPlayerMap() {
        return sessionManager.getDungeonPlayerMap();
    }

    public Map<String, Map<Long, Map<String, Session>>> getDungeonRoomPlayerMap() {
        return sessionManager.getDungeonRoomPlayerMap();
    }

    public List<String> getAvatarNamesInSameRoom(String dungeonName, String userName) {
        return chatSecurityManager.getAllAvatarNamesInSameRoom(dungeonName, userName);
    }

    public List<String> getAllAvatarNamesInDungeon(String dungeonName) {
        return chatSecurityManager.getAllAvatarNamesInDungeon(dungeonName);
    }

    public boolean isMasterInDungeon(String dungeonName, String userName) {
        return (chatSecurityManager.getMasterMap().get(dungeonName).equals(userName));
    }
}
