package ChatManaging;

import javax.inject.Singleton;
import javax.websocket.Session;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Singleton
/**
 * The SessionManager is a Core component of the Chatsystem
 * It manages the Websocket Sessions of the Users which are currently logged in in the Chatsystem
 * It is a Singelton to make sure that the SessionManager is always consistent
 * @author Jan Stöffler
 */
public class SessionManager {
    //ConcurrentHashmaps for constant Runtime
    //Map<dungeonName,Map<playerName,Session of Socket>>
    private Map<String, Map<String, Session>> dungeonPlayerMap = new ConcurrentHashMap<>();
    //Manages all Players, only two Layers,better for whispering
    //Map<dungeonName,<roomID,Map<playerName,Session>>>
    private Map<String, Map<Long, Map<String, Session>>> dungeonRoomPlayerMap = new ConcurrentHashMap<>();
    //Manages all Players in three Layers, better for roomBroadcasts

    /**
     * @param session
     * @param dungeonName
     * @param playerName
     * @author Jan Stöffler
     * A player can join the Chat, when the joinChat method is called
     * It needs to be specified which dungeon the player wants to enter
     */
    protected void addSessionToManager(Session session, String dungeonName, String playerName) {
        //check if no param is null
        if (session != null && dungeonName != null && playerName != null) {
            //is an entry for the dungeon already existing?
            if (dungeonPlayerMap.get(dungeonName) == null) {
                //add playerSession to playerMap
                Map<String, Session> playerMap = new ConcurrentHashMap<>();
                playerMap.put(playerName, session);
                dungeonPlayerMap.put(dungeonName, playerMap);
            } else {
                //add playerSession to playerMap
                dungeonPlayerMap.get(dungeonName).put(playerName, session);
            }
            //new Map for dungeon in dungeonRoomPlayerMap if not existing
            if (dungeonRoomPlayerMap.get(dungeonName) == null) {
                dungeonRoomPlayerMap.put(dungeonName, new ConcurrentHashMap<>());
            }
        } else {
            throw new IllegalArgumentException("null is not allowed as parameter");
        }
    }

    /**
     * @param dungeonName
     * @param oldRoom
     * @param newRoom
     * @param playerName
     * @author Jan Stöffler
     * The method changeRoom() is required to change the Room in the Sessionmanager, after the player
     * has or has been moved into another Room
     */
    protected void changeRoom(String dungeonName, long oldRoom, long newRoom, String playerName) {
        //check if no param is null
        if (dungeonName != null && playerName != null) {
            //get the session of the player
            Session session = dungeonPlayerMap.get(dungeonName).get(playerName);
            //remove player from old room
            dungeonRoomPlayerMap.get(dungeonName).get(oldRoom).remove(playerName);
            if (dungeonRoomPlayerMap.get(dungeonName) == null) {
                //create new Hashmap if none exists
                Map<Long, Map<String, Session>> map = new ConcurrentHashMap<>();
                Map<String, Session> sessionMap = new ConcurrentHashMap<>();
                //add player to new room
                sessionMap.put(playerName, session);
                map.put(newRoom, sessionMap);
                dungeonRoomPlayerMap.put(dungeonName, map);
            } else if (dungeonRoomPlayerMap.get(dungeonName).get(newRoom) != null) {
                //add player to new room
                dungeonRoomPlayerMap.get(dungeonName).get(newRoom).put(playerName, session);
            } else {
                //create new Hashmap if none exists
                Map<String, Session> sessionMap = new ConcurrentHashMap<>();
                //add player to new room
                sessionMap.put(playerName, session);
                dungeonRoomPlayerMap.get(dungeonName).put(newRoom, sessionMap);
            }
        } else {
            throw new IllegalArgumentException("null is not allowed as parameter");
        }
    }

    /**
     * @param playerName
     * @param dungeonName
     * @param roomName
     * @author Jan Stöffler
     * setRoomFirstTime() adds the player to both Hashmaps
     * the player will be added to the initial room of the dungeon in the RoomPlayerDungeonMap
     */
    protected void setRoomFirstTime(String playerName, String dungeonName, long roomName) {
        //check if no param is null
        Session session = dungeonPlayerMap.get(dungeonName).get(playerName);
        if (playerName != null && dungeonName != null) {
            //get session of player
            //create new Hashmap if none exists
            if (dungeonPlayerMap.get(dungeonName).get(roomName) != null) {
                Map<String, Session> playerMap = new ConcurrentHashMap<>();
                playerMap.put(playerName, session);
                Map<Long, Map<String, Session>> roomMap = new ConcurrentHashMap<>();
                roomMap.put(roomName, playerMap);
                dungeonRoomPlayerMap.put(dungeonName, roomMap);
            } else if (dungeonRoomPlayerMap.get(dungeonName).get(roomName) == null) {
                //create new Hashmap if none exists
                Map<String, Session> playerMap = new ConcurrentHashMap<>();
                playerMap.put(playerName, session);
                dungeonRoomPlayerMap.get(dungeonName).put(roomName, playerMap);
            }
            //only add when key not already in Map
            else if (!dungeonRoomPlayerMap.get(dungeonName).get(roomName).containsKey(playerName)) {
                //only add when key not already in Map
                dungeonRoomPlayerMap.get(dungeonName).get(roomName).put(playerName, session);
            }
        } else {
            throw new IllegalArgumentException("null is not allowed as parameter");
        }
    }

    /**
     * @param dungeonName
     * @param playerName
     * @author Jan Stöffler
     * remove() is the method which is called when a player leaves the Chat
     * removes PLayer from both Hashmaps
     */
    protected void remove(String dungeonName, String playerName) throws IOException {
        //check if no param is null
        if (dungeonName != null && playerName != null) {
            if (dungeonPlayerMap.get(dungeonName) != null) {
                //only remove if possible
                dungeonPlayerMap.get(dungeonName).remove(playerName).close();
            }
            if (dungeonRoomPlayerMap.get(dungeonName) != null) {
                //only remove if possible
                dungeonRoomPlayerMap.get(dungeonName)
                        .values()
                        .forEach(map -> {
                    if (map.containsKey(playerName)) {
                        try {
                            map.remove(playerName).close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        } else {
            throw new IllegalArgumentException("null is not allowed as parameter");
        }
    }

    protected Map<String, Map<String, Session>> getDungeonPlayerMap() {
        return dungeonPlayerMap;
    }

    protected Map<String, Map<Long, Map<String, Session>>> getDungeonRoomPlayerMap() {
        return dungeonRoomPlayerMap;
    }
}
