package Model.Base;

import java.util.ArrayList;
import java.util.List;

public class Dungeon {
    private String uniqueName;
    private String dungeonDescription;
    private String lobbyDescription;
    private long startRoomGlobalID;
    private String unknownActionResponse;
    private String dungeonMasterUserName;
    private boolean isJoinable;
    private boolean useWhitelistInsteadOfBlacklist;
    private List<DungeonRoom> dungeonRooms;
    private List<String> whitelistUserNames;
    private List<String> blacklistUserNames;
    private List<String> joinRequestWhenWhitelist;

    private List<String> allowedDMs;
    private List<PlayerAvatar> avatarsInDungeon;

    public Dungeon(String uniqueName, String dungeonDescription, String lobbyDescription, long startRoomGlobalID, String unknownActionResponse, String dungeonMasterUserName,
                   boolean isJoinable, boolean useWhitelistInsteadOfBlacklist, List<DungeonRoom> dungeonRooms, List<String> whitelistUserNames,
                   List<String> blacklistUserNames, List<String> joinRequestWhenWhitelist, List<String> allowedDMs, List<PlayerAvatar> avatarsInDungeon) {
        this.uniqueName = uniqueName;
        this.dungeonDescription = dungeonDescription;
        this.lobbyDescription = lobbyDescription;
        this.startRoomGlobalID = startRoomGlobalID;
        this.unknownActionResponse = unknownActionResponse;
        this.dungeonMasterUserName = dungeonMasterUserName;
        this.isJoinable = isJoinable;
        this.useWhitelistInsteadOfBlacklist = useWhitelistInsteadOfBlacklist;
        this.dungeonRooms = dungeonRooms;
        this.whitelistUserNames = whitelistUserNames;
        this.blacklistUserNames = blacklistUserNames;
        this.joinRequestWhenWhitelist = joinRequestWhenWhitelist;
        this.allowedDMs = allowedDMs;
        this.avatarsInDungeon = avatarsInDungeon;
    }

    public Dungeon() {

    }

//region Accessors

    public String getUniqueName() {
        return uniqueName;
    }

    public void setUniqueName(String uniqueName) {
        this.uniqueName = uniqueName;
    }

    public String getDungeonDescription() {
        return dungeonDescription;
    }

    public void setDungeonDescription(String dungeonDescription) {
        this.dungeonDescription = dungeonDescription;
    }

    public String getLobbyDescription() {
        return lobbyDescription;
    }

    public void setLobbyDescription(String lobbyDescription) {
        this.lobbyDescription = lobbyDescription;
    }

    public long getStartRoomGlobalID() {
        return startRoomGlobalID;
    }

    public void setStartRoomGlobalID(long startRoomGlobalID) {
        this.startRoomGlobalID = startRoomGlobalID;
    }

    public String getUnknownActionResponse() {
        return unknownActionResponse;
    }

    public void setUnknownActionResponse(String unknownActionResponse) {
        this.unknownActionResponse = unknownActionResponse;
    }

    public String getDungeonMasterUserName() {
        return dungeonMasterUserName;
    }

    public void setDungeonMasterUserName(String dungeonMasterUserName) {
        this.dungeonMasterUserName = dungeonMasterUserName;
    }

    public boolean isJoinable() {
        return isJoinable;
    }

    public void setJoinable(boolean joinable) {
        isJoinable = joinable;
    }

    public boolean isUseWhitelistInsteadOfBlacklist() {
        return useWhitelistInsteadOfBlacklist;
    }

    public void setUseWhitelistInsteadOfBlacklist(boolean useWhitelistInsteadOfBlacklist) {
        this.useWhitelistInsteadOfBlacklist = useWhitelistInsteadOfBlacklist;
    }

    public List<String> getWhitelistUserNames() {
        return whitelistUserNames;
    }

    public void setWhitelistUserNames(List<String> whitelistUserNames) {
        this.whitelistUserNames = whitelistUserNames;
    }

    public List<String> getBlacklistUserNames() {
        return blacklistUserNames;
    }

    public void setBlacklistUserNames(List<String> blacklistUserNames) {
        this.blacklistUserNames = blacklistUserNames;
    }

    public List<String> getJoinRequestWhenWhitelist() {
        return joinRequestWhenWhitelist;
    }

    public void setJoinRequestWhenWhitelist(List<String> joinRequestWhenWhitelist) {
        this.joinRequestWhenWhitelist = joinRequestWhenWhitelist;
    }

    public List<String> getAllowedDMs() {
        if (allowedDMs == null) {
            allowedDMs = new ArrayList<>();
        }
        return allowedDMs;
    }

    public void setAllowedDMs(List<String> allowedDMs) {
        this.allowedDMs = allowedDMs;
    }

    public List<PlayerAvatar> getAvatarsInDungeon() {
        return avatarsInDungeon;
    }

    public void setAvatarsInDungeon(List<PlayerAvatar> avatarsInDungeon) {
        this.avatarsInDungeon = avatarsInDungeon;
    }

    //endregion


    //region List Accessors
    public void addUserToWhitelist(String username) {
        whitelistUserNames.add(username);
    }

    public void removeUserFromWhitelist(String username) {
        whitelistUserNames.removeIf(s -> s.equals(username));
    }

    public void addUserToBlacklist(String username) {
        blacklistUserNames.add(username);
    }

    public void removeUserFromBlacklist(String username) {
        blacklistUserNames.removeIf(s -> s.equals(username));
    }

    public void addUserToJoinRequests(String username) {
        joinRequestWhenWhitelist.add(username);
    }

    public void removeUserFromJoinRequests(User user) {
        joinRequestWhenWhitelist.removeIf(s -> s.equals(user));
    }

    public void addUserToAllowedDMs(String username) {
        if (allowedDMs != null) {
            allowedDMs.add(username);
        } else {
            allowedDMs = new ArrayList<>();
            allowedDMs.add(username);
        }
    }

    public void removeUserFromAllowedDMs(User user) {
        allowedDMs.removeIf(s -> s.equals(user));
    }

    public void addPlayerToAvatarsInDungeon(PlayerAvatar user) {
        avatarsInDungeon.add(user);
    }

    public void removePlayerFromAvatarsInDungeon(PlayerAvatar user) {
        avatarsInDungeon.removeIf(s -> s.equals(user));
    }

    public void addRoomToDungeonRooms(DungeonRoom room) {
        if (dungeonRooms != null) {
            dungeonRooms.add(room);
        } else {
            dungeonRooms = new ArrayList<>();
            dungeonRooms.add(room);
        }
    }

    public void removeRoomFromDungeonRooms(DungeonRoom room) {
        dungeonRooms.removeIf(s -> s.equals(room));
    }
    //endregion
}
