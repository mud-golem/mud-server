package Model.Base;

public class AvatarClass {
    private long globalID;
    private String displayName;
    private String description;
    private String descriptionModifier;
    private int hpModifier;
    private boolean enabled;

    public AvatarClass(long globalID, String displayName, String description, String descriptionModifier, int hpModifier, boolean enabled) {
        this.globalID = globalID;
        this.displayName = displayName;
        this.description = description;
        this.descriptionModifier = descriptionModifier;
        this.hpModifier = hpModifier;
        this.enabled = enabled;
    }

    public AvatarClass(long globalID) {
        this.globalID = globalID;
    }

    public AvatarClass(){

    }

    //region Accessors
    public long getGlobalID() {
        return globalID;
    }

    public void setGlobalID(long globalID) {
        this.globalID = globalID;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescriptionModifier() {
        return descriptionModifier;
    }

    public void setDescriptionModifier(String descriptionModifier) {
        this.descriptionModifier = descriptionModifier;
    }

    public int getHpModifier() {
        return hpModifier;
    }

    public void setHpModifier(int hpModifier) {
        this.hpModifier = hpModifier;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }
    //endregion

}
