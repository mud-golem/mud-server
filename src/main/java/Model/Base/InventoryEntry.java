package Model.Base;

import java.util.Objects;

public class InventoryEntry {
    private int count;
    private ObjectTemplate template;

    InventoryEntry() {
    }


    public InventoryEntry(ObjectTemplate t, int count){
        this.template = t;
        this.count = count;
    }


    //region Accessors
    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getName() {
        return this.template.getDisplayName();
    }

    public String getFullDescription() {
        return this.template.getFullDescription();
    }

    public long getTemplateGlobalID() {
        return this.template.getGlobalID();
    }

    public ObjectTemplate getTemplate() {
        return template;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InventoryEntry that = (InventoryEntry) o;
        return count == that.count && template.equals(that.template);
    }

    @Override
    public int hashCode() {
        return Objects.hash(count, template);
    }
    //endregion
}
