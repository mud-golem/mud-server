package Model.Base;

import java.util.ArrayList;
import java.util.List;

public class PlayerAvatar {
    private long globalId;
    private String dungeonName;
    private String name;
    private String userDescription;
    private int currHP;
    private int maxHP;
    private boolean isActive;
    private String ownerUserName;
    private long actualRoomScopedId;
    private List<InventoryEntry> inventory;
    private AvatarRace avatarRace;
    private AvatarClass avatarClass;

    public PlayerAvatar(long globalId, String dungeonName, String name, String userDescription, int currHP, int maxHP,
                        boolean isActive, String ownerUserName, long actualRoomScopedId) {
        this.setGlobalId(globalId);
        this.setDungeonName(dungeonName);
        this.setName(name);
        this.setUserDescription(userDescription);
        this.setCurrHP(currHP);
        this.setMaxHP(maxHP);
        this.setActive(isActive);
        this.setOwnerUserName(ownerUserName);
        this.setActualRoomScopedId(actualRoomScopedId);
        this.inventory = new ArrayList<InventoryEntry>();
    }

    public PlayerAvatar(long globalId, String dungeonName, String name, String userDescription, int currHP, int maxHP,
                        boolean isActive, String ownerUserName, long actualRoomScopedId, List<InventoryEntry> inventory, AvatarRace avatarRace, AvatarClass avatarClass) {
        this.setGlobalId(globalId);
        this.setDungeonName(dungeonName);
        this.setName(name);
        this.setUserDescription(userDescription);
        this.setCurrHP(currHP);
        this.setMaxHP(maxHP);
        this.setActive(isActive);
        this.setOwnerUserName(ownerUserName);
        this.setActualRoomScopedId(actualRoomScopedId);
        this.inventory = inventory;
        this.avatarRace = avatarRace;
        this.avatarClass = avatarClass;
    }

    //region accessors

    public long getGlobalId() {
        return globalId;
    }

    public void setGlobalId(long globalId) {
        this.globalId = globalId;
    }

    public String getDungeonName() {
        return dungeonName;
    }

    public void setDungeonName(String dungeonName) {
        this.dungeonName = dungeonName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUserDescription() {
        return userDescription;
    }

    public void setUserDescription(String userDescription) {
        this.userDescription = userDescription;
    }

    public int getCurrHP() {
        return currHP;
    }

    public void setCurrHP(int currHP) {
        this.currHP = currHP;
    }

    public int getMaxHP() {
        return maxHP;
    }

    public void setMaxHP(int maxHP) {
        this.maxHP = maxHP;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public String getOwnerUserName() {
        return ownerUserName;
    }

    public void setOwnerUserName(String ownerUserName) {
        this.ownerUserName = ownerUserName;
    }

    public long getActualRoomGlobalId() {
        return actualRoomScopedId;
    }

    public void setActualRoomScopedId(long actualRoomScopedId) {
        this.actualRoomScopedId = actualRoomScopedId;
    }

    public List<InventoryEntry> getInventory() {
        return inventory;
    }

    public void setInventory(List<InventoryEntry> inventory) {
        this.inventory = inventory;
    }

    //endregion

    //region List accessors

    /**
     * If the inventory already contains the specified template, then add or remove the specified amount
     * @param objectTemplate
     * @param numberToAdd can be negative
     * @throws IllegalArgumentException
     */
    public void addOrChangeInventoryItemCount(ObjectTemplate objectTemplate, int numberToAdd) throws IllegalArgumentException {
        //try to find an existing inventoryEntry with the specified Template
        InventoryEntry inventoryEntry = inventory.stream().filter(ie -> ie.getTemplateGlobalID() == objectTemplate.getGlobalID()).findFirst().orElseGet(() -> null);

        if (inventoryEntry != null) {

            int newValue = inventoryEntry.getCount() + numberToAdd;

            if (newValue < 0)
                throw new IllegalArgumentException("the resulting value is either negative or an overflow");

            else
                inventoryEntry.setCount(newValue);
        } else {
            if (numberToAdd < 0)
                throw new IllegalArgumentException("the resulting value is either negative or an overflow");

            inventory.add(new InventoryEntry(objectTemplate, numberToAdd));
        }
    }

    public AvatarRace getAvatarRace() {
        return avatarRace;
    }

    public void setAvatarRace(AvatarRace avatarRace) {
        this.avatarRace = avatarRace;
    }

    public AvatarClass getAvatarClass() {
        return avatarClass;
    }

    public void setAvatarClass(AvatarClass avatarClass) {
        this.avatarClass = avatarClass;
    }

//endregion
}

