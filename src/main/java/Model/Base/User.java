package Model.Base;

import java.util.Objects;

public class User {
    private String userName;
    private String email;
    private String sha256;

    public User(String userName, String email, String sha256) {
        this.userName = userName;
        this.email = email;
        this.sha256 = sha256;
    }

    //region Accessors
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSha256() {
        return sha256;
    }

    public void setSha256(String sha256) {
        this.sha256 = sha256;
    }
    //endregion


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(userName, user.userName) &&
                Objects.equals(email, user.email) &&
                Objects.equals(sha256, user.sha256);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userName, email, sha256);
    }


}
