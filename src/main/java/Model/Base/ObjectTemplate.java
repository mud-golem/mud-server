package Model.Base;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class ObjectTemplate {
    private long globalID;
    private String displayName;
    private String fullDescription;
    private boolean pickupable;
    private List<CustomAction> customActionList;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ObjectTemplate that = (ObjectTemplate) o;
        return globalID == that.globalID && pickupable == that.pickupable && displayName.equals(that.displayName) && fullDescription.equals(that.fullDescription);
    }

    @Override
    public int hashCode() {
        return Objects.hash(globalID, displayName, fullDescription, pickupable);
    }

    public ObjectTemplate(long globalID) {
        this.globalID = globalID;
    }

    public ObjectTemplate(long globalID, String displayName, String fullDescription, boolean pickupable, List<CustomAction> customActionList) {
        this.globalID = globalID;
        this.displayName = displayName;
        this.fullDescription = fullDescription;
        this.pickupable = pickupable;
        this.customActionList = customActionList;
    }

    public ObjectTemplate(long globalID, String displayName, String fullDescription, boolean pickupable) {
        this.globalID = globalID;
        this.displayName = displayName;
        this.fullDescription = fullDescription;
        this.pickupable = pickupable;
    }

    public ObjectTemplate() {

    }

    //region Accessors
    public long getGlobalID() {
        return globalID;
    }

    public void setGlobalID(long globalID) {
        this.globalID = globalID;
    }

    public String getFullDescription() {
        return fullDescription;
    }

    public void setFullDescription(String fullDescription) {
        this.fullDescription = fullDescription;
    }

    public boolean isPickupable() {
        return pickupable;
    }

    public void setPickupable(boolean pickupable) {
        this.pickupable = pickupable;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public List<CustomAction> getCustomActionList() {
        return customActionList;
    }

    public void setCustomActionList(List<CustomAction> customActionList) {
        this.customActionList = customActionList;
    }

    //endregion
}
