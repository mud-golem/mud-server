package Model.Base;

import java.util.ArrayList;
import java.util.List;

public class DungeonRoom {
    private String displayName;
    private long globalID;
    private String description;
    private long roomNorthGlobalID;
    private long roomSouthGlobalID;
    private long roomEastGlobalID;
    private long roomWestGlobalID;
    private List<InventoryEntry> inventory;
    private List<CustomAction> customActions;

    public DungeonRoom(){}

    public DungeonRoom(String displayName, long globalID, String description, long roomNorthGlobalID, long roomSouthGlobalID, long roomEastGlobalID, long roomWestGlobalID) {
        this.displayName = displayName;
        this.globalID = globalID;
        this.description = description;
        this.roomNorthGlobalID = roomNorthGlobalID;
        this.roomSouthGlobalID = roomSouthGlobalID;
        this.roomEastGlobalID = roomEastGlobalID;
        this.roomWestGlobalID = roomWestGlobalID;
        this.inventory = new ArrayList<>();
    }

    public DungeonRoom(String displayName, long globalID, String description, long roomNorthGlobalID, long roomSouthGlobalID, long roomEastGlobalID, long roomWestGlobalID, List<InventoryEntry> inventory, List<CustomAction> customActions) {
        this.displayName = displayName;
        this.globalID = globalID;
        this.description = description;
        this.roomNorthGlobalID = roomNorthGlobalID;
        this.roomSouthGlobalID = roomSouthGlobalID;
        this.roomEastGlobalID = roomEastGlobalID;
        this.roomWestGlobalID = roomWestGlobalID;
        this.inventory = inventory;
        this.customActions = customActions;
    }

    public DungeonRoom(String displayName, String description) {
        this.displayName = displayName;
        this.description = description;
        this.globalID = -1;
        this.roomNorthGlobalID = -1;
        this.roomSouthGlobalID = -1;
        this.roomEastGlobalID = -1;
        this.roomWestGlobalID = -1;

    }


    //region Accessors

    public List<CustomAction> getCustomActions() { return customActions; }

    public void setCustomActions(List<CustomAction> customActions) { this.customActions = customActions; }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public long getGlobalID() {
        return globalID;
    }

    public void setGlobalID(long globalID) {
        this.globalID = globalID;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getRoomNorthGlobalID() {
        return roomNorthGlobalID;
    }

    public void setRoomNorthGlobalID(long roomNorthGlobalID) {
        this.roomNorthGlobalID = roomNorthGlobalID;
    }

    public long getRoomSouthGlobalID() {
        return roomSouthGlobalID;
    }

    public void setRoomSouthGlobalID(long roomSouthGlobalID) {
        this.roomSouthGlobalID = roomSouthGlobalID;
    }

    public long getRoomEastGlobalID() {
        return roomEastGlobalID;
    }

    public void setRoomEastGlobalID(long roomEastGlobalID) {
        this.roomEastGlobalID = roomEastGlobalID;
    }

    public long getRoomWestGlobalID() {
        return roomWestGlobalID;
    }

    public void setRoomWestGlobalID(long roomWestGlobalID) {
        this.roomWestGlobalID = roomWestGlobalID;
    }

    public List<InventoryEntry> getInventory() {
        return inventory;
    }

    public void setInventory(List<InventoryEntry> inventory) {
        this.inventory = inventory;
    }

    //endregion

    /**
     * If the inventory already contains the specified template, then add or remove the specified amount
     * @param objectTemplate
     * @param numberToAdd can be negative
     * @throws IllegalArgumentException
     */
    public void addOrChangeInventoryItemCount(ObjectTemplate objectTemplate, int numberToAdd) throws IllegalArgumentException {
        //try to find an existing inventoryEntry with the specified Template
        InventoryEntry inventoryEntry = inventory.stream().filter(ie -> ie.getTemplateGlobalID() ==objectTemplate.getGlobalID())
                .reduce((a, b) -> null).orElseGet(() -> null);

        if (inventoryEntry != null) {

            int newValue = inventoryEntry.getCount() + numberToAdd;

            if (newValue < 0)
                throw new IllegalArgumentException("the resulting value is either negative or an overflow");

            if (newValue == 0)
                inventory.remove(inventoryEntry);

            else
                inventoryEntry.setCount(newValue);
        } else {
            if (numberToAdd < 0)
                throw new IllegalArgumentException("the resulting value is either negative or an overflow");

            inventory.add(new InventoryEntry(objectTemplate, numberToAdd));
        }
    }
}
