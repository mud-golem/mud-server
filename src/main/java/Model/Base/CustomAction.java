package Model.Base;

public class CustomAction {

    private long globalActionId;
    private int priority;
    private String actionName;
    //the command to invoke the action
    private String command;
    private String response;

    public long getGlobalActionId() {
        return globalActionId;
    }

    public void setGlobalActionId(long globalActionId) {

        this.globalActionId = globalActionId;
    }

    public CustomAction(long globalActionId, int priority, String actionName, String command, String response) {
        this.globalActionId = globalActionId;
        this.priority = priority;
        this.actionName = actionName;
        this.command = command;
        this.response = response;
    }

    public CustomAction(){

    }

    public CustomAction(long globalActionId){
        this.globalActionId = globalActionId;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public String getActionName() {
        return actionName;
    }

    public void setActionName(String actionName) {
        this.actionName = actionName;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }
}
