package Model.SecurityModel;

/**
 * The MudUser class is needed to track a UserSession, to log in a User
 * Mainly it is used in the MudSecurityManager
 */
public class MudUser {
    private String userName;
    private String sha256;

    /**
     * newUser() is a static method to create a new User with the following parameters
     * @param userName
     * @param sha256
     * @return a new User
     */
    public static MudUser newUser(String userName, String sha256){
        MudUser temp = new MudUser();
        temp.userName = userName;
        temp.sha256 = sha256;
        return temp;
    }

    public String getSha256(){
        return this.sha256;
    }

    public String getUserName(){
        return this.userName;
    }
}
