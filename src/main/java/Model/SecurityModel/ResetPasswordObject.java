package Model.SecurityModel;

import Model.CommunicationModel.ResetPasswordForm;

import java.util.Date;

/**
 * This Class is the form for Resetting a password
 */
public class ResetPasswordObject {
    public String mailAddress;
    public String username;
    public long timeStamp;

    public ResetPasswordObject(String mailAddress, String username){
        this.mailAddress = mailAddress;
        this.username = username;
        Date date = new Date();
        this.timeStamp = date.getTime();
    }

    public ResetPasswordObject(){}
}
