package Model.SecurityModel;

import java.util.Date;

/**
 * @author Jan Stöffler
 *The UserRegistrationObject Class is used during the registration process
 * it is mainly used in the RegistrationEndPoint and the MudSecurityManager
 */
public class UserRegistrationObject {
    String userName;
    String password;
    String mailAddress;
    long timeStamp;

    public UserRegistrationObject(){

    }

    public UserRegistrationObject(String userName, String password, String mailAddress){
        this.userName = userName;
        this.password = password;
        this.mailAddress = mailAddress;
        Date date = new Date();
        this.timeStamp = date.getTime();
    }

    public UserRegistrationObject(String userName, String password, String mailAddress, long timeStamp){
        this.userName = userName;
        this.password = password;
        this.mailAddress = mailAddress;
        this.timeStamp = timeStamp;
    }


    public String getUserName() {
        return userName;
    }

    public String getPassword() {
        return password;
    }

    public String getMailAddress() {
        return mailAddress;
    }

    public long getTimeStamp(){
        return timeStamp;
    }
}
