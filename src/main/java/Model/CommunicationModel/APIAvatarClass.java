package Model.CommunicationModel;

/**
 * AvatarClass is used as Messageobject between Back- and FrontEnd
 */
public class APIAvatarClass {
    public long globalID;
    public String displayName;
    public String description;
    public float HPModifier;
    public boolean isEnabled;

    public APIAvatarClass(){}

    public APIAvatarClass(String displayName, String description, float HPModifier){
        this.displayName = displayName;
        this.description = description;
        this.HPModifier = HPModifier;
    }

    public APIAvatarClass(long globalID,String displayName, String description, float HPModifier){
        this.globalID = globalID;
        this.displayName = displayName;
        this.description = description;
        this.HPModifier = HPModifier;
    }

    public APIAvatarClass(long globalID,String displayName, String description, float HPModifier, boolean isEnabled){
        this.globalID = globalID;
        this.displayName = displayName;
        this.description = description;
        this.HPModifier = HPModifier;
        this.isEnabled = isEnabled;
    }
}
