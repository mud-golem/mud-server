package Model.CommunicationModel;

/**
 * RegistrationForm is the bodytype provided by the Front-End when a User wants to register himself
 */
public class RegistrationForm {
    public String username;
    public String password;
    public String mailAddress;
}
