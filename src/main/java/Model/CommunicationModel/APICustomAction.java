package Model.CommunicationModel;

import Model.Base.CustomAction;

public class APICustomAction {
    public long globalID;
    public String name;
    public byte priority;
    public String command;
    public String response;


    public APICustomAction() {
    }

    public APICustomAction(CustomAction customAction) {
        globalID = customAction.getGlobalActionId();
        name = customAction.getActionName();
        priority = (byte) customAction.getPriority();
        command = customAction.getCommand();
        response = customAction.getResponse();
    }
}
