package Model.CommunicationModel;

/**
 * CreatedAvatar is a class used for the communication between Front- and Back-End
 * Is used during the process of an avatar creation
 */
public class CreatedAvatar {
    public String uniqueName;
    public String userDescription;
    public long raceID;
    public long classID;
}
