package Model.CommunicationModel;

/**
 * LobbyDungeon is a class used for the communication between Front- and Back-End
 * is used to provide the dungeon entries for the lobby
 */
public class LobbyDungeon {
    public String uniqueName;
    public String lobbyDescription;
    public String dungeonMasterUserName;
    public int activePlayers;
    public boolean allowedJoinAsPlayer;
    public boolean allowedJoinAsDM;
    public boolean classesAndRacesDefined;

    public LobbyDungeon(String uniqueName,
                        String lobbyDescription,
                        String dungeonMasterUserName,
                        int activePlayers,
                        boolean allowedJoinAsPlayer,
                        boolean allowedJoinAsDM,
                        boolean classesAndRacesDefined){

        this.uniqueName = uniqueName;
        this.lobbyDescription = lobbyDescription;
        this.dungeonMasterUserName = dungeonMasterUserName;
        this.activePlayers = activePlayers;
        this.allowedJoinAsPlayer = allowedJoinAsPlayer;
        this.allowedJoinAsDM = allowedJoinAsDM;
        this.classesAndRacesDefined = classesAndRacesDefined;
    }
}
