package Model.CommunicationModel;

import Model.Base.InventoryEntry;

public class APIInventoryEntry {
    public int count;
    public long objectTemplateGlobalID;

    public APIInventoryEntry(){
    }

    public APIInventoryEntry(InventoryEntry ie){
        count = ie.getCount();
        objectTemplateGlobalID = ie.getTemplateGlobalID();
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public long getObjectTemplateGlobalID() {
        return objectTemplateGlobalID;
    }

    public void setObjectTemplateGlobalID(long objectTemplateGlobalID) {
        this.objectTemplateGlobalID = objectTemplateGlobalID;
    }
}
