package Model.CommunicationModel;

/**
 * The MessageType enum provides all different Types of messages that are possible
 */
public enum MessageType {
    DungeonChatMessage,
    RoomChatMessage,
    WhisperChatMessage,

    DungeonSystemMessage,
    RoomSystemMessage,
    WhisperSystemMessage,

    ActionAlertMessage
}
