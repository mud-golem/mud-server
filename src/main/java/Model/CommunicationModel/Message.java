package Model.CommunicationModel;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * The Message class is a class used for the communication between the Front- and Back-End
 * the message class is the object which is used to send messages between WebSockets
 */
public class Message {
    public MessageType type;
    public String sender;
    //username
    public String recipient;
    //DisplayName of room
    public String room;

    public String object;
    public String dungeonName;
    public String actionName; //for DM only
    public byte priority; //For DM only
    public String content;

    /**
     * The method toJson() parses the current Message Object to a jsonString
     * @return a jsonString
     * this method is needed to send the Message Objects via WebSockets
     */
    public String toJson(){
        ObjectMapper mapper = new ObjectMapper();
        try{
            String result = mapper.writeValueAsString(this);
            return result;
        }catch(Exception e){
            return "";
        }
    }
}
