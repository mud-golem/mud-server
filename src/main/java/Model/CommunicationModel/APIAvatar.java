package Model.CommunicationModel;

import Model.Base.InventoryEntry;
import Model.Base.PlayerAvatar;

import java.util.List;
import java.util.stream.Collectors;

public class APIAvatar {
    public long globalID;
    public String name;
    public String userName;
    public String userDescription;
    public int currHP;
    public int maxHP;
    public boolean isActive;
    public long raceGlobalID;
    public long classGlobalID;
    public long roomGlobalID;
    public List<APIInventoryEntry> inventory;

    public APIAvatar() {

    }

    public APIAvatar(PlayerAvatar pa) {
        globalID = pa.getGlobalId();
        name = pa.getName();
        userName = pa.getOwnerUserName();
        userDescription = pa.getUserDescription();
        currHP = pa.getCurrHP();
        maxHP = pa.getMaxHP();
        isActive = pa.isActive();
        raceGlobalID = pa.getAvatarRace().getGlobalID();
        classGlobalID = pa.getAvatarClass().getGlobalID();
        roomGlobalID = pa.getActualRoomGlobalId();
        inventory = pa.getInventory().stream().map(inventoryEntry -> new APIInventoryEntry(inventoryEntry)).collect(Collectors.toList());
    }
}
