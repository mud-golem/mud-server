package Model.CommunicationModel;

import java.util.List;

/**
 * Communication.Dungeon is used as MessageObject between Back- and Front-End
 */
public class APIDungeon {
    public String uniqueName;
    public String dungeonDescription;
    public String lobbyDescription;
    public long startRoomScopedID;
    public String unknownActionResponse;
    public String dungeonMasterUserName;
    public boolean isJoinable;
    public boolean useWhitelistInsteadOfBlacklist;
    public List<String> whitelistUserNames;
    public List<String> blacklistUserNames;
    public List<String> joinRequestUserNames;
    public List<String> DMUserNames;
    public List<Long> actionIDs;

    public APIDungeon(){}

    public APIDungeon(String uniqueName,
                      String dungeonDescription,
                      String lobbyDescription,
                      long startRoomScopedID,
                      String unknownActionResponse,
                      String dungeonMasterUserName,
                      boolean isJoinable,
                      boolean useWhitelistInsteadOfBlacklist,
                      List<String> whitelistUserNames,
                      List<String> blacklistUserNames,
                      List<String> joinRequestUserNames,
                      List<String> DMUserNames,
                      List<Long> actionIDs) {

        this.uniqueName = uniqueName;
        this.dungeonDescription = dungeonDescription;
        this.lobbyDescription = lobbyDescription;
        this.startRoomScopedID = startRoomScopedID;
        this.unknownActionResponse = unknownActionResponse;
        this.dungeonMasterUserName = dungeonMasterUserName;
        this.isJoinable = isJoinable;
        this.useWhitelistInsteadOfBlacklist = useWhitelistInsteadOfBlacklist;
        this.whitelistUserNames = whitelistUserNames;
        this.blacklistUserNames = blacklistUserNames;
        this.joinRequestUserNames = joinRequestUserNames;
        this.DMUserNames = DMUserNames;
        this.actionIDs = actionIDs;
    }
}
