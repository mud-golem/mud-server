package Model.CommunicationModel;

/**
 * AvatarRace is used as MessageObject between Back- and Front-End
 */
public class APIAvatarRace {
    public long globalID;
    public String displayName;
    public String description;
    public float HPModifier;
    public boolean isEnabled;

    public APIAvatarRace(){}

    public APIAvatarRace(String displayName, String description, float HPModifier){
        this.displayName = displayName;
        this.description = description;
        this.HPModifier = HPModifier;
    }
    public APIAvatarRace(long globalID, String displayName, String description, float HPModifier){
        this.globalID = globalID;
        this.displayName = displayName;
        this.description = description;
        this.HPModifier = HPModifier;
    }
    public APIAvatarRace(long globalID, String displayName, String description, float HPModifier, boolean isEnabled){
        this.globalID = globalID;
        this.displayName = displayName;
        this.description = description;
        this.HPModifier = HPModifier;
        this.isEnabled = isEnabled;
    }
}
