package Model.CommunicationModel;

import Model.Base.PlayerAvatar;

public class PlayerAvatarListEntry {
    public long globalID;
    public String name;
    public int currHP;
    public boolean isActive;
    public long roomGlobalID;


    public PlayerAvatarListEntry(){

    }
    public  PlayerAvatarListEntry(PlayerAvatar pa){
        globalID = pa.getGlobalId();
        name = pa.getName();
        currHP = pa.getCurrHP();
        isActive = pa.isActive();
        roomGlobalID = pa.getActualRoomGlobalId();
    }
}
