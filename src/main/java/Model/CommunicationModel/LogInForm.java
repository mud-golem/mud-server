package Model.CommunicationModel;

/**
 * LogInForm is the bodytype sent by the FrontEnd when a User wants to login
 */
public class LogInForm {
    public String username;
    public String password;
}
