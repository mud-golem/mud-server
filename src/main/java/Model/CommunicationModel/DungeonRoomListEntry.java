package Model.CommunicationModel;

/**
 * DungeonRoomListEntry is used as MessageObject between Back- and Front-End
 */
public class DungeonRoomListEntry {
    public String displayName;
    public long scopedID;
    public long roomNorthScopedID;
    public long roomSouthScopedID;
    public long roomEastScopedID;
    public long roomWestScopedID;
}
