package Model.CommunicationModel;

/**
 * CreatedDungeon is a class used for the communication between Front- and Back-End
 * Is used during the process of a dungeon creation
 */
public class CreatedDungeon {
    public String uniqueName;
    public String lobbyDescription;
}
