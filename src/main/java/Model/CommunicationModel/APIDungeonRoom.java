package Model.CommunicationModel;

import Model.Base.DungeonRoom;

import java.util.List;
import java.util.stream.Collectors;

/**
 * DungeonRoom is used as MessageObject between Back- and Front-End
 */
public class APIDungeonRoom {
    public String displayName;
    public long globalID;
    public String description;
    public long roomNorthGlobalID;
    public long roomSouthGlobalID;
    public long roomEastGlobalID;
    public long roomWestGlobalID;
    public List<Long> actionIDs;
    public List<APIInventoryEntry> placedObjects;

    public APIDungeonRoom() {
    }
    public APIDungeonRoom(DungeonRoom room){
        displayName = room.getDisplayName();
        globalID = room.getGlobalID();
        description = room.getDescription();
        roomNorthGlobalID = room.getRoomNorthGlobalID();
        roomSouthGlobalID = room.getRoomSouthGlobalID();
        roomWestGlobalID = room.getRoomWestGlobalID();
        roomEastGlobalID = room.getRoomEastGlobalID();
        actionIDs = room.getCustomActions().stream().map(customAction -> customAction.getGlobalActionId())
                .collect(Collectors.toList());
        placedObjects = room.getInventory().stream().map(inventoryEntry -> new APIInventoryEntry(inventoryEntry))
                .collect(Collectors.toList());
    }
}
