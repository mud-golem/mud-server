package Model.CommunicationModel;

import Model.Base.ObjectTemplate;

import java.util.List;
import java.util.stream.Collectors;

public class APIObjectTemplate {
    public long globalID;
    public String name;
    public String fullDescription;
    public boolean isPickupable;
    public List<Long> actionIDs;

    public APIObjectTemplate() {

    }

    public APIObjectTemplate(ObjectTemplate ot) {
        globalID = ot.getGlobalID();
        name = ot.getDisplayName();
        fullDescription = ot.getFullDescription();
        isPickupable = ot.isPickupable();
        actionIDs = ot.getCustomActionList().stream()
                .map(customAction -> customAction.getGlobalActionId()).collect(Collectors.toList());
    }

}