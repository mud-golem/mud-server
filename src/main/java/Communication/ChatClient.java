package Communication;

import ChatManaging.SessionManager;
import Model.CommunicationModel.Message;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jboss.logging.Logger;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.websocket.*;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;


@Singleton
@ClientEndpoint
/**
 * @author Jan Stöffler
 * The ChatClient can communicate with the Chatsystem but is only internal available
 * There should only be one Instance of the Chatclient to communicate with the Chatsystem
 */
public class ChatClient {

    private static final Logger LOG = Logger.getLogger(ChatClient.class);

    Session session = null;

    //URI of the Websocket of the Chat
    final URI serverUri = new URI("wss://alpha.mud-golem.com:1996/server/chat");

    @Inject
    SessionManager sessionManager;

    public ChatClient() throws URISyntaxException {
        try{
            WebSocketContainer container = ContainerProvider.getWebSocketContainer();
            container.connectToServer(this,serverUri);
        }catch (Exception e){
            LOG.info(e.toString(),e);
        }
    }

    @OnOpen
    public void open(Session session) {
        this.session = session;
    }

    @OnMessage
    void message(String msg) {
    }

    /**
     * @author Jan Stöffler
     * sendMessage send a message from the Server to the Chatsystem
     * @param msg
     */
    public void sendMessage(Message msg) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            session.getAsyncRemote().sendText(mapper.writeValueAsString(msg));
        } catch (JsonProcessingException j) {

        }
    }
}
