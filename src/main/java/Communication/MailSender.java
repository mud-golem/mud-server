package Communication;

import EndPoints.RESTful.RegistrationEndPoint;
import Exceptions.MailErrorException;
import Security.MailAddressMatcher;
import org.jboss.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.*;

@ApplicationScoped
/**
 * The MailSender can send Mails over the Mail-address noreply@mud-golem.com
 * Insert the Password for the account in a smtp.txt file at the root of the project
 * otherwise the Mailflow won't work
 */
public class MailSender {

    private static final Logger LOG = Logger.getLogger(MailSender.class);

    private static final String EMAIL_SUBJECT = "Registration for MUD-Golem.com";

    /**
     * getPassword() reads the password of the Mailaccount form the smtp.txt
     * @return password
     */
    public String getPassword(String file){
        try {
            File smtpFile = new File(file);
            Scanner sc = new Scanner(smtpFile);
            return sc.next();
        } catch (FileNotFoundException e) {
            LOG.info(e.toString(),e);
            return "";
        }
    }

    public String getUserName(String file){
        try {
            String result = "";
            File smtpFile = new File(file);
            Scanner sc = new Scanner(smtpFile);
            sc.next();
            return sc.next();
        } catch (FileNotFoundException e) {
            LOG.info(e.toString(),e);
            return "";
        }
    }

    public String getDomain(String file){
        try {
            String result = "";
            File smtpFile = new File(file);
            Scanner sc = new Scanner(smtpFile);
            sc.next();
            sc.next();
            return sc.next();
        } catch (FileNotFoundException e) {
            LOG.info(e.toString(),e);
            return "";
        }
    }

    /**
     * creates the Properties for the smtp Flow
     * @return Properties prop
     */
    private Properties getProperties(){
        Properties prop = System.getProperties();
        prop.put("mail.smtp.host", "smtp.strato.de");
        prop.put("mail.smtp.auth", "true");
        prop.put("mail.smtp.port", "465");
        prop.put("mail.smtp.starttls.enable", "true");
        prop.put("mail.smtp.ssl.enable","true");
        prop.put("mail.smtp.ssl.checkserveridentity","true");
        return prop;
    }


    /**
     * sendChangePasswordMail() sends a Mail with a link to a User.
     * the USer can change his password with the Link
     * @param key to append to the link
     * @param mailAddress of the User
     */
    public void sendChangePasswordMail(String key, String mailAddress) throws MailErrorException {

        if(key != null && key != "") {

            if(mailAddress != null && MailAddressMatcher.validateMailAddress(mailAddress)) {

                Properties prop = getProperties();

                Session session = Session.getInstance(prop,
                        new Authenticator() {
                            protected PasswordAuthentication getPasswordAuthentication() {
                                return new PasswordAuthentication(getUserName("smtp.txt")
                                        , getPassword("smtp.txt"));
                            }
                        });
                Message msg = new MimeMessage(session);

                try {

                    msg.setFrom(new InternetAddress(getUserName("smtp.txt")));

                    msg.setRecipients(Message.RecipientType.TO,
                            InternetAddress.parse(mailAddress, false));

                    msg.setSubject(EMAIL_SUBJECT);

                    msg.setContent("<h1>Change your password with the following Link</h1></br><a href=\""
                                    + getDomain("smtp.txt")
                                    + "/passwordChange/"
                                    + URLEncoder.encode(key, StandardCharsets.UTF_8.toString())
                                    + "\">Change Password</a>", "text/html");

                    msg.setSentDate(new Date());

                    Transport.send(msg);

                } catch (MessagingException e) {
                    LOG.info(e.toString(), e);
                    throw new MailErrorException();
                } catch (UnsupportedEncodingException u){
                    LOG.info(u.toString(),u);
                    throw new MailErrorException();
                }

            }else{
                throw new IllegalArgumentException("enter a valid mailAddress");
            }

        }else{
            throw new IllegalArgumentException("key can't be null or empty");
        }

    }

    /**
     * sendRegistrationMail() send an E-Mail to the specified mailAddress, the mail contains a Link
     * with the Link a User can be registered.
     * @param key which is appended to the Link
     * @param mailAddress of User
     * @throws  MailErrorException when there is an error with the Mailflow
     */
    public void sendRegistrationMail(String key,String mailAddress) throws MailErrorException {

        if(key != null && key != "") {

            if(mailAddress != null && MailAddressMatcher.validateMailAddress(mailAddress)) {

                Properties prop = getProperties();

                Session session = Session.getInstance(prop,
                        new Authenticator() {
                            protected PasswordAuthentication getPasswordAuthentication() {
                                return new PasswordAuthentication(getUserName("smtp.txt")
                                        ,getPassword("smtp.txt"));
                            }
                        });
                Message msg = new MimeMessage(session);

                try {

                    msg.setFrom(new InternetAddress(getUserName("smtp.txt")));

                    msg.setRecipients(Message.RecipientType.TO,
                            InternetAddress.parse(mailAddress, false));

                    msg.setSubject(EMAIL_SUBJECT);

                    msg.setContent("<h1>Verify your Registration with the following Link</h1></br><a href=\""
                            + getDomain("smtp.txt")
                            + "/mailConfirmed/"
                            + URLEncoder.encode(key, StandardCharsets.UTF_8.toString())
                            + "\"> Register here </a>","text/html");

                    msg.setSentDate(new Date());

                    Transport.send(msg);

                } catch (MessagingException e) {
                    LOG.info(e.toString(),e);
                    throw new MailErrorException();
                }catch (UnsupportedEncodingException e){
                    LOG.info(e.toString(),e);
                }

            }else{
                throw new IllegalArgumentException("enter a valid mailAddress");
            }

        }else{
            throw new IllegalArgumentException("key can't be null or empty");
        }

    }
}
