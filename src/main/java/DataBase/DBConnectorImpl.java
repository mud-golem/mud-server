package DataBase;

import java.io.File;
import java.io.FileNotFoundException;
import java.sql.*;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import DatabaseInterfaces.DBConnector;
import Exceptions.ElementNotFoundException;
import io.quarkus.runtime.Startup;
import org.mariadb.jdbc.*;

import javax.inject.Singleton;

@Singleton
/**
 * @Author Moritz Klaiber
 * This Class implements functions to access the Database.
 * It provides the connecti@onpool, which guarentees that the database accesses are thread safe.
 * Also the datatypes from the Resultsets can be returned after a polishment.
 */
public class DBConnectorImpl implements DBConnector {

    private static final String user = "admin";
    private static final String pwd = giveDBPwd();
    private static final String poolSize = "100";
    private static final MariaDbPoolDataSource pool
            = new MariaDbPoolDataSource("jdbc:mariadb://127.0.0.1:3306/productive-mudgolem-server?user=" + user
            + "&password=" + pwd + "&maxPoolSize=" + poolSize);


    private String userTestDB = "test";
    private String pwdTestDB = giveDBPwd();
    private MariaDbPoolDataSource poolTestDB
            = new MariaDbPoolDataSource("jdbc:mariadb://127.0.0.1:3306/mudgolem-server-test?user=" + userTestDB
            + "&password=" + pwdTestDB + "&maxPoolSize=" + poolSize);

    public boolean useTestDB = false;

    private static String giveDBPwd() {
        try {
            File dbpwdFile = new File("dbpass.txt");
            Scanner sc = new Scanner(dbpwdFile);
            return sc.next();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return "";
        }
    }

    private Connection getConnection() {

        try {
            if (useTestDB == true) {
                return poolTestDB.getConnection();
            } else {
                return pool.getConnection();
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            pool.close();
            return null;
        }
    }

    /**
     * @param sequel
     * @param preparedInjectionValues
     * @return
     * @Author Moritz Klaiber
     * Returns the ResultSet from the Sequel. The Object List contains all values that are required for SQLInjections.
     */
    @Override
    public ResultSet getSQLResults(String sequel, List<Object> preparedInjectionValues) {

        Connection connection = null;
        AtomicInteger counter = new AtomicInteger(1);

        try {
            connection = getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(sequel);
            if (preparedStatement == null) {
                throw new SQLException("Prepared Statement was null.");
            }
            preparedInjectionValues.stream().forEach(value -> {
                try {
                    preparedStatement.setObject(counter.getAndIncrement(), value);
                } catch (SQLException sqlException) {
                    sqlException.printStackTrace();
                }
            });
            return preparedStatement.executeQuery();
        } catch (Exception throwables) {
            throwables.printStackTrace();
            return null;
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * @param sequel
     * @param preparedInjectionValues
     * @throws SQLException
     * @Author Moritz Klaiber
     * This method executes SQL commands that don't require an return value, such as INSERT, UPDATE or DELETE. The Object List contains all values that are required for SQLInjections.
     */
    @Override
    public void executeSQL(String sequel, List<Object> preparedInjectionValues) throws SQLException {

        Connection connection = null;
        AtomicInteger counter = new AtomicInteger(1);

        try {
            connection = getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(sequel);
            preparedInjectionValues.stream().forEach(value -> {
                try {
                    preparedStatement.setObject(counter.getAndIncrement(), value);
                } catch (SQLException sqlException) {
                    sqlException.printStackTrace();
                }
            });
            preparedStatement.executeUpdate();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * @param sequel
     * @param preparedInjectionValues
     * @throws SQLException
     * @Author Moritz Klaiber
     * This method executes SQL commands that INSERT the Object to the table and returns the global id as a long value. The Object List contains all values that are required for SQLInjections.
     */
    @Override
    public long insertSQL(String sequel, List<Object> preparedInjectionValues) throws SQLException {

        Connection connection = null;
        AtomicInteger counter = new AtomicInteger(1);

        try {
            connection = getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(sequel);
            preparedInjectionValues.stream().forEach(value -> {
                try {
                    preparedStatement.setObject(counter.getAndIncrement(), value);
                } catch (SQLException sqlException) {
                    sqlException.printStackTrace();
                }
            });
            PreparedStatement preparedStatement1 = connection.prepareStatement("SELECT LAST_INSERT_ID() as ID;");
            preparedStatement.executeUpdate();
            ResultSet resultSet = preparedStatement1.executeQuery();
            if (resultSet.next()) {
                return getLong(resultSet, "ID");
            } else {
                return -1;
            }

        } catch (ElementNotFoundException e) {
            e.printStackTrace();
            return -1;
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * @param resultSet
     * @param columnName
     * @return
     * @Author Moritz Klaiber
     * Returns an String from the mentioned ResultSet
     */
    @Override
    public String getString(ResultSet resultSet, String columnName) throws ElementNotFoundException {
        try {
            if (resultSet.getString(columnName) != null) {
                return resultSet.getString(columnName);
            } else return "n/a";
        } catch (SQLException sqlException) {
            throw new ElementNotFoundException("Dungeon name not found", columnName, sqlException);
        }
    }

    /**
     * @param resultSet
     * @param columnName
     * @return
     * @Author Moritz Klaiber
     * Returns an Integer from the mentioned ResultSet
     */
    @Override
    public int getInt(ResultSet resultSet, String columnName) throws ElementNotFoundException {
        try {
            return resultSet.getInt(columnName);
        } catch (SQLException sqlException) {
            throw new ElementNotFoundException("Dungeon name not found", columnName, sqlException);
        }
    }

    /**
     * @param resultSet
     * @param columnName
     * @return
     * @Author Moritz Klaiber
     * Returns an Long from the mentioned ResultSet
     */
    @Override
    public long getLong(ResultSet resultSet, String columnName) throws ElementNotFoundException {
        try {
            long resultSetValue = resultSet.getLong(columnName);
            return (resultSetValue == 0 ? -1 : resultSetValue);
        } catch (SQLException sqlException) {
            throw new ElementNotFoundException("Dungeon name not found", columnName, sqlException);
        }
    }

    /**
     * @param resultSet
     * @param columnName
     * @return
     * @Author Moritz Klaiber
     * Returns an Boolean from the mentioned ResultSet.
     */
    @Override
    public boolean getBoolean(ResultSet resultSet, String columnName) throws ElementNotFoundException {
        try {
            int intBool = (int) resultSet.getByte(columnName);
            return (intBool == 1 ? true : false);
        } catch (SQLException sqlException) {
            throw new ElementNotFoundException("Dungeon name not found", columnName, sqlException);
        }
    }
}
