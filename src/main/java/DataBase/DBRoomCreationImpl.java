package DataBase;

import DatabaseInterfaces.DBConnector;
import DatabaseInterfaces.DBRoomCreation;
import Exceptions.ElementNotFoundException;
import Model.Base.DungeonRoom;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@ApplicationScoped
/**
 * This implementation can be used to create a new room
 * @author Moritz Klaiber
 */
public class DBRoomCreationImpl implements DBRoomCreation {

    private DBConnector dbConnector;

    @Inject
    public void setDbConnector(DBConnectorImpl dbConnector) {

        this.dbConnector = dbConnector;
    }

    public void setDbConnector(DBConnector dbConnector) {

        this.dbConnector = dbConnector;
    }

    /**
     * @param dungeonName
     * @param dungeonRoom
     * @return long
     * @throws ElementNotFoundException
     * @Author Moritz Klaiber
     * This method adds a new Room to the Dungeon.
     */
    @Override
    public long addNewRoom(String dungeonName, DungeonRoom dungeonRoom) throws ElementNotFoundException {
        try {
            List<Object> parameterList = new ArrayList<Object>();
            parameterList.add(dungeonRoom.getDisplayName());
            parameterList.add(null);
            parameterList.add(dungeonRoom.getDescription());
            parameterList.add((dungeonRoom.getRoomNorthGlobalID() == -1 ? null : dungeonRoom.getRoomNorthGlobalID()));
            parameterList.add(dungeonRoom.getRoomSouthGlobalID() == -1 ? null : dungeonRoom.getRoomSouthGlobalID());
            parameterList.add(dungeonRoom.getRoomEastGlobalID() == -1 ? null : dungeonRoom.getRoomEastGlobalID());
            parameterList.add(dungeonRoom.getRoomWestGlobalID() == -1 ? null : dungeonRoom.getRoomWestGlobalID());
            parameterList.add(dungeonName);

            long roomId = dbConnector.insertSQL("INSERT INTO dungeonRooms VALUES(?, ?, ?, ?, ?, ?, ?, ?)", parameterList);

            parameterList.clear();

            return roomId;


        } catch (SQLException e) {
            ElementNotFoundException elementNotFoundException = new ElementNotFoundException("Dungeon name not found", dungeonName, e);
            throw elementNotFoundException;
        }
    }

}
