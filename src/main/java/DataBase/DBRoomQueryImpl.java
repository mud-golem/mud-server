package DataBase;

import DatabaseInterfaces.DBConnector;
import DatabaseInterfaces.DBRoomQuery;
import Exceptions.ElementNotFoundException;
import Model.Base.*;
import Model.CommunicationModel.APIDungeon;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.List;

@ApplicationScoped
/**
 * This implementation can be used to get all information about the rooms from a dungeon.
 * @author Moritz Klaiber
 */
public class DBRoomQueryImpl implements DBRoomQuery {

    private DBConnector dbConnector;

    @Inject
    public void setDbConnector(DBConnectorImpl dbConnector) {

        this.dbConnector = dbConnector;
    }

    public void setDbConnector(DBConnector dbConnector) {

        this.dbConnector = dbConnector;
    }


    /**
     * @param dungeonName
     * @return List<DungeonRoom>
     * @Author Moritz Klaiber
     * returns all rooms from one dungeon
     */
    @Override
    public List<DungeonRoom> getRooms(String dungeonName) throws ElementNotFoundException {
        List<Object> parameterList = new ArrayList<Object>();
        parameterList.add(dungeonName);
        System.out.println(dungeonName);
        List<DungeonRoom> ret = new ArrayList<DungeonRoom>();
        ResultSet rs = dbConnector.getSQLResults("SELECT * FROM dungeonRooms WHERE dungeonName = ?", parameterList);
        try {
            while (rs.next()) {
                parameterList.clear();
                parameterList.add(dbConnector.getLong(rs, "globalId"));
                ResultSet rsInventory = dbConnector.getSQLResults("SELECT * FROM inventoryEntries ie, objectTemplates ot WHERE ie.globalDungeonRoomId = ? AND ie.globalTemplateId = ot.globalId", parameterList);
                ResultSet rsCusAct = dbConnector.getSQLResults("SELECT * FROM customActions ca, hasCustomAction hca WHERE ca.globalId = hca.customActionId AND hca.dungeonRoomGlobalId = ?", parameterList);
                List<InventoryEntry> inventoryEntryList = new ArrayList<InventoryEntry>();
                List<CustomAction> customActionList;
                while (rsInventory.next()) {
                    parameterList.clear();
                    parameterList.add(dbConnector.getLong(rsInventory, "globalTemplateId"));
                    System.out.println("Global Template ID: " + dbConnector.getLong(rsInventory, "globalTemplateId"));
                    ResultSet rsCAInv = dbConnector.getSQLResults("SELECT * FROM customActions ca, hasCustomAction hca WHERE ca.globalId = hca.customActionId AND hca.templateGlobalId = ?", parameterList);
                    List<CustomAction> customActions = new ArrayList<>();
                    while (rsCAInv.next()) {
                        System.out.println("Custom Action gefunden: " + dbConnector.getString(rsCAInv, "actionName") + " für Objekt " + dbConnector.getLong(rsInventory, "globalTemplateId"));
                        customActions.add(new CustomAction(dbConnector.getLong(rsCAInv, "globalId"), dbConnector.getInt(rsCAInv, "priority"), dbConnector.getString(rsCAInv, "actionName"),
                                dbConnector.getString(rsCAInv, "command"), dbConnector.getString(rsCAInv, "response")));
                    }
                    inventoryEntryList.add(new InventoryEntry(new ObjectTemplate(dbConnector.getLong(rsInventory, "globalTemplateId"),
                            dbConnector.getString(rsInventory, "displayName"),
                            dbConnector.getString(rsInventory, "fullDescription"),
                            dbConnector.getBoolean(rsInventory, "pickupable"),
                            customActions),
                            dbConnector.getInt(rsInventory, "count")));
                }
                customActionList = new ArrayList<>();
                while (rsCusAct.next()) {
                    customActionList.add(new CustomAction(dbConnector.getLong(rsCusAct, "globalId"), dbConnector.getInt(rsCusAct, "priority"), dbConnector.getString(rsCusAct, "actionName"),
                            dbConnector.getString(rsCusAct, "command"), dbConnector.getString(rsCusAct, "response")));
                }
                System.out.println("Room: " + dbConnector.getString(rs, "displayName"));
                ret.add(new DungeonRoom(dbConnector.getString(rs, "displayName"),
                        dbConnector.getLong(rs, "globalId"),
                        dbConnector.getString(rs, "description"),
                        dbConnector.getLong(rs, "roomNorthGlobalID"),
                        dbConnector.getLong(rs, "roomSouthGlobalID"),
                        dbConnector.getLong(rs, "roomEastGlobalID"),
                        dbConnector.getLong(rs, "roomWestGlobalID"),
                        inventoryEntryList, customActionList));

            }

        } catch (Exception e) {
            throw new ElementNotFoundException("No room with this id ", dungeonName, e);
        }
        return ret;
    }

    /**
     * @param dungeonName
     * @return DungeonRoom
     * @Author Moritz Klaiber
     * returns one room from one dungeon
     */
    @Override
    public DungeonRoom getDungeonRoom(String dungeonName, long globalId) throws ElementNotFoundException {
        List<Object> parameterList = new ArrayList<Object>();
        parameterList.add(globalId);

        try {
            ResultSet rs = dbConnector.getSQLResults("SELECT * FROM dungeonRooms WHERE globalId = ?", parameterList);
            parameterList.clear();
            parameterList.add(globalId);
            ResultSet rsInventory = dbConnector.getSQLResults("SELECT * FROM inventoryEntries ie, objectTemplates ot WHERE ie.globalDungeonRoomId = ? AND ie.globalTemplateId = ot.globalId", parameterList);
            ResultSet rsCusAct = dbConnector.getSQLResults("SELECT * FROM customActions ca, hasCustomAction hca WHERE ca.globalId = hca.customActionId AND hca.dungeonRoomGlobalId = ?", parameterList);
            List<InventoryEntry> inventoryEntryList = new ArrayList<InventoryEntry>();
            List<CustomAction> customActionList = new ArrayList<>();
            if (rs.next()) {
                while (rsInventory.next()) {
                    parameterList.clear();
                    parameterList.add(dbConnector.getLong(rsInventory, "globalTemplateId"));
                    ResultSet rsCAInv = dbConnector.getSQLResults("SELECT * FROM customActions ca, hasCustomAction hca WHERE ca.globalId = hca.customActionId AND hca.templateGlobalId = ?", parameterList);
                    List<CustomAction> customActions = new ArrayList<>();
                    while (rsCAInv.next()) {
                        System.out.println("Custom Action gefunden: " + dbConnector.getString(rsCAInv, "actionName") + " für Objekt " + dbConnector.getLong(rsInventory, "globalTemplateId"));
                        customActions.add(new CustomAction(dbConnector.getLong(rsCAInv, "globalId"), dbConnector.getInt(rsCAInv, "priority"), dbConnector.getString(rsCAInv, "actionName"),
                                dbConnector.getString(rsCAInv, "command"), dbConnector.getString(rsCAInv, "response")));
                    }
                    inventoryEntryList.add(new InventoryEntry(new ObjectTemplate(
                            dbConnector.getLong(rsInventory, "globalTemplateId"),
                            dbConnector.getString(rsInventory, "displayName"),
                            dbConnector.getString(rsInventory, "fullDescription"),
                            dbConnector.getBoolean(rsInventory, "pickupable"),
                            customActions),
                            dbConnector.getInt(rsInventory, "count")));
                }
                while (rsCusAct.next()) {
                    System.out.println("Cusrtom Action gefunden: " + dbConnector.getString(rsCusAct, "actionName") + " auf " + dbConnector.getString(rs, "displayName"));
                    customActionList.add(new CustomAction(dbConnector.getLong(rsCusAct, "globalId"), dbConnector.getInt(rsCusAct, "priority"), dbConnector.getString(rsCusAct, "actionName"),
                            dbConnector.getString(rsCusAct, "command"), dbConnector.getString(rsCusAct, "response")));
                }
                return new DungeonRoom(
                        dbConnector.getString(rs, "displayName"),
                        dbConnector.getLong(rs, "globalId"),
                        dbConnector.getString(rs, "description"),
                        dbConnector.getLong(rs, "roomNorthGlobalID"),
                        dbConnector.getLong(rs, "roomSouthGlobalID"),
                        dbConnector.getLong(rs, "roomEastGlobalID"),
                        dbConnector.getLong(rs, "roomWestGlobalID"),
                        inventoryEntryList, customActionList);
            } else {
                throw new ElementNotFoundException("No room with this id ", dungeonName);
            }

        } catch (SQLException e) {
            throw new ElementNotFoundException("No room with this id ", dungeonName, e);
        }
    }

    /**
     * Returns all usernames, that are present in the actual room
     *
     * @param dungeonName
     * @param roomGlobalId
     * @return
     * @throws ElementNotFoundException
     * @author Moritz Klaiber
     */
    @Override
    public List<String> getAllAvatarNamesInRoom(String dungeonName, long roomGlobalId) throws ElementNotFoundException {
        List<String> allAvatarNamesFromRoom = new ArrayList<>();
        try {
            List<Object> parameterList = new ArrayList<Object>();
            parameterList.add(dungeonName);
            parameterList.add(roomGlobalId);

            ResultSet rs = dbConnector.getSQLResults("SELECT name FROM playerAvatars WHERE dungeonName = ? AND actualDungeonRoomGlobalId = ?", parameterList);
            while (rs.next()) {
                allAvatarNamesFromRoom.add(dbConnector.getString(rs, "name"));
            }

        } catch (SQLException e) {
            throw new ElementNotFoundException("No room with this id ", roomGlobalId + "", e);
        }
        return allAvatarNamesFromRoom;
    }

}
