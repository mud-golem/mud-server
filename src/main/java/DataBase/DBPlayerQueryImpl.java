package DataBase;

import DatabaseInterfaces.DBConnector;
import DatabaseInterfaces.DBPlayerQuery;
import Exceptions.ElementNotFoundException;
import Model.Base.*;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@ApplicationScoped
/**
 * This implementation can be used to get all information about avatars their specific dungeons.
 * @author Moritz Klaiber
 */
public class DBPlayerQueryImpl implements DBPlayerQuery {

    private DBConnector dbConnector;

    @Inject
    public void setDbConnector(DBConnectorImpl dbConnector) {

        this.dbConnector = dbConnector;
    }

    public void setDbConnector(DBConnector dbConnector) {

        this.dbConnector = dbConnector;
    }

    /**
     * returns the full avatar with race, class and whole inventory. Returns an ElementNotFoundException when one Element was not found.
     *
     * @param dungeonName
     * @param globalAvatarId
     * @return PlayerAvatar
     * @throws ElementNotFoundException
     */
    @Override
    public PlayerAvatar getPlayerAvatar(String dungeonName, long globalAvatarId) throws ElementNotFoundException {
        try {
            List<Object> parameterList = new ArrayList<Object>();
            parameterList.add(dungeonName);
            parameterList.add(globalAvatarId);

            AvatarClass avatarClass;
            AvatarRace avatarRace;
            List<InventoryEntry> inventory = new ArrayList<InventoryEntry>();


            ResultSet rsPlayerAvatar = dbConnector.getSQLResults("SELECT * FROM playerAvatars WHERE dungeonName = ? AND globalId = ?", parameterList);
            if (rsPlayerAvatar.next()) {
                parameterList.clear();
                parameterList.add(globalAvatarId);
                ResultSet rsInventory = dbConnector.getSQLResults("SELECT * FROM inventoryEntries ie, objectTemplates ot WHERE ie.globalPlayerAvatarId = ? AND ie.globalTemplateId = ot.globalId", parameterList);
                while (rsInventory.next()) {
                    parameterList.clear();
                    parameterList.add(dbConnector.getLong(rsInventory, "globalTemplateId"));
                    ResultSet rsCAInv = dbConnector.getSQLResults("SELECT * FROM customActions ca, hasCustomAction hca WHERE ca.globalId = hca.customActionId AND hca.templateGlobalId = ?", parameterList);
                    List<CustomAction> customActions = new ArrayList<>();
                    while (rsCAInv.next()) {
                        customActions.add(new CustomAction(dbConnector.getLong(rsCAInv, "globalId"), dbConnector.getInt(rsCAInv, "priority"), dbConnector.getString(rsCAInv, "actionName"),
                                dbConnector.getString(rsCAInv, "command"), dbConnector.getString(rsCAInv, "response")));
                    }
                    inventory.add(new InventoryEntry(new ObjectTemplate(
                            dbConnector.getLong(rsInventory, "globalTemplateId"),
                            dbConnector.getString(rsInventory, "displayName"),
                            dbConnector.getString(rsInventory, "fullDescription"),
                            dbConnector.getBoolean(rsInventory, "pickupable"),
                            customActions),
                            dbConnector.getInt(rsInventory, "count")));
                }

                parameterList.clear();
                parameterList.add(dbConnector.getString(rsPlayerAvatar, "classGlobalId"));
                ResultSet rsAvatarClass = dbConnector.getSQLResults("SELECT * FROM classes WHERE globalId = ?", parameterList);
                if (rsAvatarClass.next()) {
                    avatarClass = new AvatarClass(dbConnector.getLong(rsAvatarClass, "globalId"),
                            dbConnector.getString(rsAvatarClass, "displayName"),
                            dbConnector.getString(rsAvatarClass, "description"),
                            dbConnector.getString(rsAvatarClass, "descriptionModifier"),
                            dbConnector.getInt(rsAvatarClass, "hpModifier"),
                            dbConnector.getBoolean(rsAvatarClass, "enabled"));
                } else {
                    throw new ElementNotFoundException("No avatarclass found for PlayerCharacter ", dungeonName);
                }

                parameterList.clear();
                parameterList.add(dbConnector.getString(rsPlayerAvatar, "raceGlobalId"));
                ResultSet rsAvatarRaces = dbConnector.getSQLResults("SELECT * FROM races WHERE globalId = ?", parameterList);
                if (rsAvatarRaces.next()) {
                    avatarRace = new AvatarRace(dbConnector.getLong(rsAvatarRaces, "globalId"),
                            dbConnector.getString(rsAvatarRaces, "displayName"),
                            dbConnector.getString(rsAvatarRaces, "description"),
                            dbConnector.getString(rsAvatarRaces, "descriptionModifier"),
                            dbConnector.getInt(rsAvatarRaces, "hpModifier"),
                            dbConnector.getBoolean(rsAvatarRaces, "enabled"));
                } else {
                    throw new ElementNotFoundException("No avatarrace found for PlayerCharacter ", dungeonName);
                }

                return new PlayerAvatar(dbConnector.getLong(rsPlayerAvatar, "globalId"),
                        dbConnector.getString(rsPlayerAvatar, "dungeonName"),
                        dbConnector.getString(rsPlayerAvatar, "name"),
                        dbConnector.getString(rsPlayerAvatar, "userDescription"),

                        dbConnector.getInt(rsPlayerAvatar, "currHP"),
                        dbConnector.getInt(rsPlayerAvatar, "maxHP"),

                        dbConnector.getBoolean(rsPlayerAvatar, "isActive"),

                        dbConnector.getString(rsPlayerAvatar, "owner"),
                        dbConnector.getLong(rsPlayerAvatar, "actualDungeonRoomGlobalId"),

                        inventory, avatarRace, avatarClass);

            } else {
                throw new ElementNotFoundException("Player Avatar not Found ", dungeonName);
            }

        } catch (SQLException throwables) {
            ElementNotFoundException elementNotFoundException = new ElementNotFoundException("Dungeon not found ", dungeonName + "", throwables);
            elementNotFoundException.setStackTrace(throwables.getStackTrace());
            throw elementNotFoundException;
        }
    }

    /**
     * returns the full avatar with race, class and whole inventory. Returns an ElementNotFoundException when one Element was not found.
     *
     * @param dungeonName
     * @param userName
     * @return PlayerAvatar
     * @throws ElementNotFoundException
     */
    @Override
    public PlayerAvatar getPlayerAvatar(String dungeonName, String userName) throws ElementNotFoundException {
        try {
            List<Object> parameterList = new ArrayList<Object>();
            parameterList.add(dungeonName);
            parameterList.add(userName);

            AvatarClass avatarClass;
            AvatarRace avatarRace;
            List<InventoryEntry> inventory = new ArrayList<InventoryEntry>();


            ResultSet rsPlayerAvatar = dbConnector.getSQLResults("SELECT * FROM playerAvatars WHERE dungeonName = ? AND owner = ?", parameterList);
            if (rsPlayerAvatar.next()) {
                parameterList.clear();
                parameterList.add(dbConnector.getLong(rsPlayerAvatar, "globalId"));
                ResultSet rsInventory = dbConnector.getSQLResults("SELECT * FROM inventoryEntries ie, objectTemplates ot WHERE ie.globalPlayerAvatarId = ? AND ie.globalTemplateId = ot.globalId", parameterList);
                while (rsInventory.next()) {
                    parameterList.clear();
                    parameterList.add(dbConnector.getLong(rsInventory, "globalTemplateId"));
                    ResultSet rsCAInv = dbConnector.getSQLResults("SELECT * FROM customActions ca, hasCustomAction hca WHERE ca.globalId = hca.customActionId AND hca.templateGlobalId = ?", parameterList);
                    List<CustomAction> customActions = new ArrayList<>();
                    while (rsCAInv.next()) {
                        customActions.add(new CustomAction(dbConnector.getLong(rsCAInv, "globalId"), dbConnector.getInt(rsCAInv, "priority"), dbConnector.getString(rsCAInv, "actionName"),
                                dbConnector.getString(rsCAInv, "command"), dbConnector.getString(rsCAInv, "response")));
                    }
                    inventory.add(new InventoryEntry(new ObjectTemplate(
                            dbConnector.getLong(rsInventory, "globalTemplateId"),
                            dbConnector.getString(rsInventory, "displayName"),
                            dbConnector.getString(rsInventory, "fullDescription"),
                            dbConnector.getBoolean(rsInventory, "pickupable"),
                            customActions),
                            dbConnector.getInt(rsInventory, "count")));
                }

                parameterList.clear();
                parameterList.add(dbConnector.getString(rsPlayerAvatar, "classGlobalId"));
                ResultSet rsAvatarClass = dbConnector.getSQLResults("SELECT * FROM classes WHERE globalId = ?", parameterList);
                if (rsAvatarClass.next()) {
                    avatarClass = new AvatarClass(dbConnector.getLong(rsAvatarClass, "globalId"),
                            dbConnector.getString(rsAvatarClass, "displayName"),
                            dbConnector.getString(rsAvatarClass, "description"),
                            dbConnector.getString(rsAvatarClass, "descriptionModifier"),
                            dbConnector.getInt(rsAvatarClass, "hpModifier"),
                            dbConnector.getBoolean(rsAvatarClass, "enabled"));
                } else {
                    throw new ElementNotFoundException("No avatarclass found for PlayerCharacter ", dungeonName);
                }

                parameterList.clear();
                parameterList.add(dbConnector.getString(rsPlayerAvatar, "raceGlobalId"));
                ResultSet rsAvatarRaces = dbConnector.getSQLResults("SELECT * FROM races WHERE globalId = ?", parameterList);
                if (rsAvatarRaces.next()) {
                    avatarRace = new AvatarRace(dbConnector.getLong(rsAvatarRaces, "globalId"),
                            dbConnector.getString(rsAvatarRaces, "displayName"),
                            dbConnector.getString(rsAvatarRaces, "description"),
                            dbConnector.getString(rsAvatarRaces, "descriptionModifier"),
                            dbConnector.getInt(rsAvatarRaces, "hpModifier"),
                            dbConnector.getBoolean(rsAvatarRaces, "enabled"));
                } else {
                    throw new ElementNotFoundException("No avatarrace found for PlayerCharacter ", dungeonName);
                }

                return new PlayerAvatar(dbConnector.getLong(rsPlayerAvatar, "globalId"),
                        dbConnector.getString(rsPlayerAvatar, "dungeonName"),
                        dbConnector.getString(rsPlayerAvatar, "name"),
                        dbConnector.getString(rsPlayerAvatar, "userDescription"),

                        dbConnector.getInt(rsPlayerAvatar, "currHP"),
                        dbConnector.getInt(rsPlayerAvatar, "maxHP"),

                        dbConnector.getBoolean(rsPlayerAvatar, "isActive"),

                        dbConnector.getString(rsPlayerAvatar, "owner"),
                        dbConnector.getLong(rsPlayerAvatar, "actualDungeonRoomGlobalId"),

                        inventory, avatarRace, avatarClass);

            } else {
                throw new ElementNotFoundException("Player Avatar not Found ", dungeonName);
            }

        } catch (SQLException throwables) {
            ElementNotFoundException elementNotFoundException = new ElementNotFoundException("Dungeon not found", dungeonName + "", throwables);
            elementNotFoundException.setStackTrace(throwables.getStackTrace());
            throw elementNotFoundException;
        }
    }

    /**
     * returns the full avatar with race, class and whole inventory. Returns an ElementNotFoundException when one Element was not found.
     *
     * @param dungeonName
     * @param avatarName
     * @return PlayerAvatar
     * @throws ElementNotFoundException
     */

    @Override
    public PlayerAvatar getPlayerAvatarWithAvatarName(String dungeonName, String avatarName) throws ElementNotFoundException {
        try {
            List<Object> parameterList = new ArrayList<Object>();
            parameterList.add(dungeonName);
            parameterList.add(avatarName);

            AvatarClass avatarClass;
            AvatarRace avatarRace;
            List<InventoryEntry> inventory = new ArrayList<InventoryEntry>();


            ResultSet rsPlayerAvatar = dbConnector.getSQLResults("SELECT * FROM playerAvatars WHERE dungeonName = ? AND name = ?", parameterList);
            if (rsPlayerAvatar.next()) {
                parameterList.clear();
                parameterList.add(dbConnector.getLong(rsPlayerAvatar, "globalId"));
                ResultSet rsInventory = dbConnector.getSQLResults("SELECT * FROM inventoryEntries ie, objectTemplates ot WHERE ie.globalPlayerAvatarId = ? AND ie.globalTemplateId = ot.globalId", parameterList);
                while (rsInventory.next()) {
                    parameterList.clear();
                    parameterList.add(dbConnector.getLong(rsInventory, "globalTemplateId"));
                    ResultSet rsCAInv = dbConnector.getSQLResults("SELECT * FROM customActions ca, hasCustomAction hca WHERE ca.globalId = hca.customActionId AND hca.templateGlobalId = ?", parameterList);
                    List<CustomAction> customActions = new ArrayList<>();
                    while (rsCAInv.next()) {
                        customActions.add(new CustomAction(dbConnector.getLong(rsCAInv, "globalId"), dbConnector.getInt(rsCAInv, "priority"), dbConnector.getString(rsCAInv, "actionName"),
                                dbConnector.getString(rsCAInv, "command"), dbConnector.getString(rsCAInv, "response")));
                    }
                    inventory.add(new InventoryEntry(new ObjectTemplate(
                            dbConnector.getLong(rsInventory, "globalTemplateId"),
                            dbConnector.getString(rsInventory, "displayName"),
                            dbConnector.getString(rsInventory, "fullDescription"),
                            dbConnector.getBoolean(rsInventory, "pickupable"),
                            customActions),
                            dbConnector.getInt(rsInventory, "count")));
                }

                parameterList.clear();
                parameterList.add(dbConnector.getString(rsPlayerAvatar, "classGlobalId"));
                ResultSet rsAvatarClass = dbConnector.getSQLResults("SELECT * FROM classes WHERE globalId = ?", parameterList);
                if (rsAvatarClass.next()) {
                    avatarClass = new AvatarClass(dbConnector.getLong(rsAvatarClass, "globalId"),
                            dbConnector.getString(rsAvatarClass, "displayName"),
                            dbConnector.getString(rsAvatarClass, "description"),
                            dbConnector.getString(rsAvatarClass, "descriptionModifier"),
                            dbConnector.getInt(rsAvatarClass, "hpModifier"),
                            dbConnector.getBoolean(rsAvatarClass, "enabled"));
                } else {
                    throw new ElementNotFoundException("No avatarclass found for PlayerCharacter ", dungeonName);
                }

                parameterList.clear();
                parameterList.add(dbConnector.getString(rsPlayerAvatar, "raceGlobalId"));
                ResultSet rsAvatarRaces = dbConnector.getSQLResults("SELECT * FROM races WHERE globalId = ?", parameterList);
                if (rsAvatarRaces.next()) {
                    avatarRace = new AvatarRace(dbConnector.getLong(rsAvatarRaces, "globalId"),
                            dbConnector.getString(rsAvatarRaces, "displayName"),
                            dbConnector.getString(rsAvatarRaces, "description"),
                            dbConnector.getString(rsAvatarRaces, "descriptionModifier"),
                            dbConnector.getInt(rsAvatarRaces, "hpModifier"),
                            dbConnector.getBoolean(rsAvatarRaces, "enabled"));
                } else {
                    throw new ElementNotFoundException("No avatarrace found for PlayerCharacter ", dungeonName);
                }

                return new PlayerAvatar(dbConnector.getLong(rsPlayerAvatar, "globalId"),
                        dbConnector.getString(rsPlayerAvatar, "dungeonName"),
                        dbConnector.getString(rsPlayerAvatar, "name"),
                        dbConnector.getString(rsPlayerAvatar, "userDescription"),

                        dbConnector.getInt(rsPlayerAvatar, "currHP"),
                        dbConnector.getInt(rsPlayerAvatar, "maxHP"),

                        dbConnector.getBoolean(rsPlayerAvatar, "isActive"),

                        dbConnector.getString(rsPlayerAvatar, "owner"),
                        dbConnector.getLong(rsPlayerAvatar, "actualDungeonRoomGlobalId"),

                        inventory, avatarRace, avatarClass);

            } else {
                throw new ElementNotFoundException("Player Avatar not Found ", dungeonName);
            }

        } catch (SQLException throwables) {
            ElementNotFoundException elementNotFoundException = new ElementNotFoundException("Dungeon not found", dungeonName + "", throwables);
            elementNotFoundException.setStackTrace(throwables.getStackTrace());
        }

        return null;
    }

    /**
     * @param dungeonName
     * @return List<PlayerAvatar>
     * @Author Moritz Klaiber
     * returns all player avatars
     */
    @Override
    public List<PlayerAvatar> getPlayerAvatars(String dungeonName) throws ElementNotFoundException {

        List<Object> parameterList = new ArrayList<Object>();
        parameterList.add(dungeonName);
        AvatarClass avatarClass;
        AvatarRace avatarRace;
        List<InventoryEntry> inventory = new ArrayList<InventoryEntry>();
        List<PlayerAvatar> playerAvatarList = new ArrayList<>();

        try {
            ResultSet rsPlayerAvatar = dbConnector.getSQLResults("SELECT * FROM playerAvatars WHERE dungeonName = ?", parameterList);
            while (rsPlayerAvatar.next()) {
                parameterList.clear();
                parameterList.add(dbConnector.getLong(rsPlayerAvatar, "globalId"));
                ResultSet rsInventory = dbConnector.getSQLResults("SELECT * FROM inventoryEntries ie, objectTemplates ot WHERE ie.globalPlayerAvatarId = ? AND ie.globalTemplateId = ot.globalId", parameterList);
                while (rsInventory.next()) {
                    parameterList.clear();
                    parameterList.add(dbConnector.getLong(rsInventory, "globalTemplateId"));
                    ResultSet rsCAInv = dbConnector.getSQLResults("SELECT * FROM customActions ca, hasCustomAction hca WHERE ca.globalId = hca.customActionId AND hca.templateGlobalId = ?", parameterList);
                    List<CustomAction> customActions = new ArrayList<>();
                    while (rsCAInv.next()) {
                        customActions.add(new CustomAction(dbConnector.getLong(rsCAInv, "globalId"), dbConnector.getInt(rsCAInv, "priority"), dbConnector.getString(rsCAInv, "actionName"),
                                dbConnector.getString(rsCAInv, "command"), dbConnector.getString(rsCAInv, "response")));
                    }
                    inventory.add(new InventoryEntry(new ObjectTemplate(
                            dbConnector.getLong(rsInventory, "globalTemplateId"),
                            dbConnector.getString(rsInventory, "displayName"),
                            dbConnector.getString(rsInventory, "fullDescription"),
                            dbConnector.getBoolean(rsInventory, "pickupable"),
                            customActions),
                            dbConnector.getInt(rsInventory, "count")));
                }

                parameterList.clear();
                parameterList.add(dbConnector.getString(rsPlayerAvatar, "classGlobalId"));
                ResultSet rsAvatarClass = dbConnector.getSQLResults("SELECT * FROM classes WHERE globalId = ?", parameterList);
                if (rsAvatarClass.next()) {
                    avatarClass = new AvatarClass(dbConnector.getLong(rsAvatarClass, "globalId"),
                            dbConnector.getString(rsAvatarClass, "displayName"),
                            dbConnector.getString(rsAvatarClass, "description"),
                            dbConnector.getString(rsAvatarClass, "descriptionModifier"),
                            dbConnector.getInt(rsAvatarClass, "hpModifier"),
                            dbConnector.getBoolean(rsAvatarClass, "enabled"));
                } else {
                    throw new ElementNotFoundException("No avatarclass found for PlayerCharacter ", dungeonName);
                }

                parameterList.clear();
                parameterList.add(dbConnector.getString(rsPlayerAvatar, "raceGlobalId"));
                ResultSet rsAvatarRaces = dbConnector.getSQLResults("SELECT * FROM races WHERE globalId = ?", parameterList);
                if (rsAvatarRaces.next()) {
                    avatarRace = new AvatarRace(dbConnector.getLong(rsAvatarRaces, "globalId"),
                            dbConnector.getString(rsAvatarRaces, "displayName"),
                            dbConnector.getString(rsAvatarRaces, "description"),
                            dbConnector.getString(rsAvatarRaces, "descriptionModifier"),
                            dbConnector.getInt(rsAvatarRaces, "hpModifier"),
                            dbConnector.getBoolean(rsAvatarRaces, "enabled"));
                } else {
                    throw new ElementNotFoundException("No avatarrace found for PlayerCharacter ", dungeonName);
                }

                playerAvatarList.add(new PlayerAvatar(dbConnector.getLong(rsPlayerAvatar, "globalId"),
                        dbConnector.getString(rsPlayerAvatar, "dungeonName"),
                        dbConnector.getString(rsPlayerAvatar, "name"),
                        dbConnector.getString(rsPlayerAvatar, "userDescription"),

                        dbConnector.getInt(rsPlayerAvatar, "currHP"),
                        dbConnector.getInt(rsPlayerAvatar, "maxHP"),

                        dbConnector.getBoolean(rsPlayerAvatar, "isActive"),

                        dbConnector.getString(rsPlayerAvatar, "owner"),
                        dbConnector.getLong(rsPlayerAvatar, "actualDungeonRoomGlobalId"),

                        inventory, avatarRace, avatarClass));

            }

        } catch (SQLException throwables) {
            ElementNotFoundException elementNotFoundException = new ElementNotFoundException("Dungeon not found", dungeonName + "", throwables);
            elementNotFoundException.setStackTrace(throwables.getStackTrace());
        }

        return playerAvatarList;
    }

    /**
     * returns the id of the actual room from the given user
     *
     * @param dungeonName
     * @param userName
     * @return
     * @throws ElementNotFoundException
     * @author Moritz Klaiber
     */
    public long getActualRoomGlobalId(String dungeonName, String userName) throws ElementNotFoundException {
        List<Object> parameterList = new ArrayList<Object>();
        parameterList.add(dungeonName);
        parameterList.add(userName);

        ResultSet rs = dbConnector.getSQLResults("SELECT actualDungeonRoomGlobalId AS id FROM playerAvatars WHERE dungeonName = ? AND owner = ?", parameterList);

        try {
            if (rs.next()) {
                return dbConnector.getLong(rs, "id");
            } else {
                throw new ElementNotFoundException("Dungeon not found", dungeonName + "");
            }
        } catch (SQLException sqlException) {
            ElementNotFoundException elementNotFoundException = new ElementNotFoundException("Dungeon not found", dungeonName + "");
            elementNotFoundException.setStackTrace(sqlException.getStackTrace());
            throw elementNotFoundException;
        }
    }
}
