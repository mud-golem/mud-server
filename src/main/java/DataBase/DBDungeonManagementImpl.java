package DataBase;

import DatabaseInterfaces.DBConnector;
import DatabaseInterfaces.DBDungeonManagement;
import Exceptions.ElementNotFoundException;
import Model.CommunicationModel.APIDungeon;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@ApplicationScoped
/**
 * This implementation provides a method to Manage the dungeons.
 * It only performs UPDATE SQL Commands
 * @Author Moritz Klaiber
 */
public class DBDungeonManagementImpl implements DBDungeonManagement {

    DBConnector dbConnector;

    @Inject
    public void setDbConnector(DBConnectorImpl dbConnector) {
        this.dbConnector = dbConnector;
    }

    public void setDbConnector(DBConnector dbConnector) {
        this.dbConnector = dbConnector;
    }

    /**
     * This Method overrides the complete dungeon except the name of the dungeon in the Database.
     *
     * @param apiDungeon
     * @throws IllegalArgumentException
     * @throws ElementNotFoundException
     * @author Moritz Klaiber
     */
    @Override
    public void setDungeon(APIDungeon apiDungeon) throws IllegalArgumentException, ElementNotFoundException {
        try {
            List<Object> parameterList = new ArrayList<Object>();
            parameterList.add(apiDungeon.dungeonDescription);
            parameterList.add(apiDungeon.lobbyDescription);
            parameterList.add(apiDungeon.startRoomScopedID);
            parameterList.add(apiDungeon.unknownActionResponse);
            parameterList.add(apiDungeon.dungeonMasterUserName);
            parameterList.add(apiDungeon.isJoinable);
            parameterList.add(apiDungeon.useWhitelistInsteadOfBlacklist);
            parameterList.add(apiDungeon.uniqueName);

            /*newUniqueName = ?, ... WHERE oldUniqueName = ?*/

            dbConnector.executeSQL("UPDATE dungeon SET dungeonDescription = ?, lobbyDescription = ?, startRoomGlobalId = ?, unknownActionResponse = ?, dungeonMasterUserName = ?, " +
                    "isJoinable = ?, useWhitelisteInsteadOfBlacklist = ? WHERE uniqueName = ?", parameterList);

            parameterList.clear();
            parameterList.add(apiDungeon.uniqueName);
            dbConnector.executeSQL("DELETE FROM hasCustomAction WHERE dungeonName = ?", parameterList);

            apiDungeon.actionIDs.forEach(aLong -> {
                try {
                    parameterList.clear();
                    parameterList.add(null);
                    parameterList.add(apiDungeon.uniqueName);
                    parameterList.add(null);
                    parameterList.add(null);
                    parameterList.add(aLong);

                    dbConnector.executeSQL("INSERT INTO hasCustomAction VALUES(?, ?, ?, ?, ?)", parameterList);

                } catch (SQLException sqlException) {
                    ElementNotFoundException elementNotFoundException = new ElementNotFoundException("Dungeon name not found", apiDungeon.uniqueName, sqlException);
                    try {
                        throw elementNotFoundException;
                    } catch (ElementNotFoundException e) {
                        e.printStackTrace();
                    }
                }

            });

        } catch (SQLException e) {
            ElementNotFoundException elementNotFoundException = new ElementNotFoundException("Dungeon name not found", apiDungeon.uniqueName, e);
            throw elementNotFoundException;
        }
    }

    @Override
    public void setActualDM(String dungeonName, String userName) throws ElementNotFoundException {
        try {
            List<Object> parameterList = new ArrayList<Object>();
            parameterList.add(userName);
            parameterList.add(dungeonName);
            dbConnector.executeSQL("UPDATE dungeon SET dungeonMasterUserName = ? WHERE uniqueName = ?", parameterList);

        } catch (SQLException sqlException){
            ElementNotFoundException elementNotFoundException = new ElementNotFoundException("Dungeon name not found", dungeonName, sqlException);
            throw elementNotFoundException;
        }
    }

}
