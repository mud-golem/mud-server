package DataBase;

import DatabaseInterfaces.DBConnector;
import io.quarkus.runtime.Startup;
import org.mariadb.jdbc.MariaDbPoolDataSource;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Singleton;
import java.io.File;
import java.io.FileNotFoundException;
import java.sql.*;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicInteger;

@Singleton
public class DBConnectorTest implements DBConnector {
    private String user = "test";
    private String pwd = giveDBPwd();
    private String poolSize = "100";

    private MariaDbPoolDataSource pool = new MariaDbPoolDataSource("jdbc:mariadb://mud-golem.com:3306/mudgolem-server-test?user=" + user + "&password=" + pwd + "&maxPoolSize=" + poolSize);


    private String giveDBPwd() {
        try {
            File dbpwdFile = new File("dbpass.txt");
            Scanner sc = new Scanner(dbpwdFile);
            return sc.next();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return "";
        }
    }

    @Override
    public long insertSQL(String sequel, List<Object> preparedInjectionValues) throws SQLException {

        Connection connection = null;
        AtomicInteger counter = new AtomicInteger(1);

        try {
            connection = getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(sequel);
            preparedInjectionValues.stream().forEach(value -> {
                try {
                    preparedStatement.setObject(counter.getAndIncrement(), value);
                } catch (SQLException sqlException) {
                    sqlException.printStackTrace();
                }
            });
            PreparedStatement preparedStatement1 = connection.prepareStatement("SELECT LAST_INSERT_ID() as ID;");
            preparedStatement.executeUpdate();
            ResultSet resultSet = preparedStatement1.executeQuery();
            if (resultSet.next()) {
                return getLong(resultSet, "ID");
            } else {
                return -1;
            }

        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public ResultSet getSQLResults(String sequel, List<Object> preparedInjectionValues) {

        Connection connection = null;
        AtomicInteger counter = new AtomicInteger(1);

        try {
            connection = getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(sequel);
            preparedInjectionValues.stream().forEach(value -> {
                try {
                    preparedStatement.setObject(counter.getAndIncrement(), value);
                } catch (SQLException sqlException) {
                    sqlException.printStackTrace();
                }
            });
            return preparedStatement.executeQuery();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return null;
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public void executeSQL(String sequel, List<Object> preparedInjectionValues) {

        Connection connection = null;
        AtomicInteger counter = new AtomicInteger(1);

        try {
            connection = getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(sequel);
            preparedInjectionValues.stream().forEach(value -> {
                try {
                    preparedStatement.setObject(counter.getAndIncrement(), value);
                } catch (SQLException sqlException) {
                    sqlException.printStackTrace();
                }
            });
            preparedStatement.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public ResultSet getSQLResults(String sequel) {

        Connection connection = null;

        try {
            connection = getConnection();
            Statement findUserStmt = connection.createStatement();
            ResultSet resultSet = findUserStmt.executeQuery(sequel);
            return resultSet;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return null;
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }

    public void executeSQL(String sequel) {

        Connection connection = null;

        try {
            connection = getConnection();
            Statement findUserStmt = connection.createStatement();
            findUserStmt.executeUpdate(sequel);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public void closeConn() {
        pool.close();
    }

    private Connection getConnection() {
        try {
            return pool.getConnection();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            pool.close();
            return null;
        }
    }

    public String getString(ResultSet resultSet, String columnName) {
        try {
            if (resultSet.getString(columnName) != null) {
                return resultSet.getString(columnName);
            } else return "n/a";
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
            return "n/a";
        }
    }

    public int getInt(ResultSet resultSet, String columnName) {
        try {
            return resultSet.getInt(columnName);
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
            return 0;
        }
    }

    public long getLong(ResultSet resultSet, String columnName) {
        try {
            return resultSet.getLong(columnName);
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
            return 0;
        }
    }

    public boolean getBoolean(ResultSet resultSet, String columnName) {
        try {
            int intBool = (int) resultSet.getByte(columnName);
            return (intBool == 1 ? true : false);
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
            return false;
        }
    }
}
