package DataBase;

import DatabaseInterfaces.DBConnector;
import DatabaseInterfaces.DBDungeonCreation;
import Exceptions.ElementNotFoundException;
import Model.Base.Dungeon;
import Model.Base.User;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@ApplicationScoped
/**
 * This Class implements all Getter-Functions for a dungeon.
 * It only performs SELECT SQL Commands
 * @Author Moritz Klaiber
 */
public class DBDungeonCreationImpl implements DBDungeonCreation {

    private DBConnector dbConnector;

    @Inject
    public void setDbConnector(DBConnectorImpl dbConnector) {

        this.dbConnector = dbConnector;

    }

    public void setDbConnector(DBConnector dbConnector) {

        this.dbConnector = dbConnector;

    }

    /**
     * This method creats a dungeon
     *
     * @param dungeon
     * @return
     * @throws ElementNotFoundException
     * @Author Moritz Klaiber
     */
    public long createDungeon(Dungeon dungeon) throws ElementNotFoundException {

        Long id = null;

        List<Object> parameterList = new ArrayList<>();
        parameterList.add(dungeon.getUniqueName());
        parameterList.add(dungeon.getDungeonDescription());
        parameterList.add(dungeon.getLobbyDescription());
        parameterList.add(dungeon.getStartRoomGlobalID());
        parameterList.add(dungeon.getUnknownActionResponse());
        parameterList.add(dungeon.getDungeonMasterUserName());
        parameterList.add(dungeon.isJoinable());
        parameterList.add(dungeon.isUseWhitelistInsteadOfBlacklist());

        try {
            dbConnector.executeSQL("INSERT INTO dungeon VALUES(?, ?, ?, ?, ?, ?, ?, ?)", parameterList);
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
            ElementNotFoundException elementNotFoundException = new ElementNotFoundException("Insert Failed", dungeon.getUniqueName(), sqlException);
            elementNotFoundException.setStackTrace(sqlException.getStackTrace());
            throw elementNotFoundException;
        }


        for (String dm : dungeon.getAllowedDMs()) {

            parameterList.clear();
            parameterList.add(dungeon.getUniqueName());
            parameterList.add(dm);

            try {
                dbConnector.executeSQL("INSERT INTO dmList VALUES(?, ?)", parameterList);
            } catch (SQLException sqlException) {
                ElementNotFoundException elementNotFoundException = new ElementNotFoundException("Dungeon name not found", dungeon.getUniqueName(), sqlException);
                elementNotFoundException.setStackTrace(sqlException.getStackTrace());
                throw elementNotFoundException;
            }

        }

        parameterList.clear();
        parameterList.add("startroom");
        parameterList.add(null);
        parameterList.add("This is the initial Startroom");
        parameterList.add(null);
        parameterList.add(null);
        parameterList.add(null);
        parameterList.add(null);
        parameterList.add(dungeon.getUniqueName());
        try {
            id = dbConnector.insertSQL("INSERT INTO dungeonRooms VALUES(?, ?, ?, ?, ?, ?, ?, ?)", parameterList);
        } catch (SQLException sqlException) {
            ElementNotFoundException elementNotFoundException = new ElementNotFoundException("Dungeon name not found", dungeon.getUniqueName(), sqlException);
            elementNotFoundException.setStackTrace(sqlException.getStackTrace());
            throw elementNotFoundException;
        }

        parameterList.clear();
        parameterList.add(id);
        parameterList.add(dungeon.getUniqueName());
        try {
            dbConnector.executeSQL("UPDATE dungeon SET startRoomGlobalId = ? WHERE uniqueName = ?", parameterList);
        } catch (SQLException sqlException) {
            ElementNotFoundException elementNotFoundException = new ElementNotFoundException("Dungeon name not found", dungeon.getUniqueName(), sqlException);
            elementNotFoundException.setStackTrace(sqlException.getStackTrace());
            throw elementNotFoundException;
        }

        return id;

    }

    /**
     * @param uniqueName
     * @return
     * @Autor Motiz Klaiber
     * This method deletes a specific dungeon
     */
    public boolean deleteDungeon(String uniqueName) {
        List<Object> parameterList = new ArrayList<>();
        parameterList.add(uniqueName);

        try {
            dbConnector.executeSQL("DELETE FROM dungeon WHERE uniqueName = ?", parameterList);
        } catch (SQLException sqlException) {
            ElementNotFoundException elementNotFoundException = new ElementNotFoundException("Dungeon name not found", uniqueName, sqlException);
            elementNotFoundException.setStackTrace(sqlException.getStackTrace());
        }

        return true;

    }

}
