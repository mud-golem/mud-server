package DataBase;

import DatabaseInterfaces.DBConnector;
import DatabaseInterfaces.DBUserAccessManagement;
import Exceptions.ElementNotFoundException;
import Exceptions.EntryAlreadyExistsException;
import Exceptions.NoValidUserNameException;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@ApplicationScoped
/**
 * This implementation can be used to get all and set all Information about the access right lists.
 * @author Moritz Klaiber
 */
public class DBUserAccessManagementImpl implements DBUserAccessManagement {

    private DBConnector dbConnector;

    @Inject
    public void setDbConnector(DBConnectorImpl dbConnector) {

        this.dbConnector = dbConnector;
    }

    public void setDbConnector(DBConnector dbConnector) {

        this.dbConnector = dbConnector;
    }

    /**
     * Returns all usernames that are present on the blacklist for the given dungeon.
     *
     * @param dungeonName
     * @return
     * @throws ElementNotFoundException
     * @Author Moritz Klaiber
     */
    @Override
    public List<String> getBlackList(String dungeonName) throws ElementNotFoundException {
        try {
            List<Object> parameterList = new ArrayList<Object>();
            List<String> blackListUserNames = new ArrayList<>();
            parameterList.add(dungeonName);
            ResultSet rsBlackList = dbConnector.getSQLResults("SELECT * FROM blackList WHERE dungeonName = ?", parameterList);

            while (rsBlackList.next()) {
                blackListUserNames.add(dbConnector.getString(rsBlackList, "userName"));
            }

            return blackListUserNames;

        } catch (SQLException sqlException) {
            throw new ElementNotFoundException("dungeon not found: ", dungeonName, sqlException);
        }
    }

    /**
     * Set one player to the blacklist of the given dungeon
     *
     * @param dungeonName
     * @param userName
     * @throws ElementNotFoundException
     * @Author Moritz Klaiber
     */
    @Override
    public void setBlackList(String dungeonName, String userName) throws NoValidUserNameException, EntryAlreadyExistsException {
        try {
            List<Object> parameterList = new ArrayList<Object>();
            parameterList.add(dungeonName);
            parameterList.add(userName);
            dbConnector.executeSQL("INSERT INTO blackList VALUES(?, ?)", parameterList);
        } catch (SQLException sqlException) {
            if(Arrays.stream(sqlException.getStackTrace()).anyMatch(stackTraceElement -> stackTraceElement.toString().contains("Duplicate entry"))){
                throw new EntryAlreadyExistsException(dungeonName+userName, sqlException);
            } else if(Arrays.stream(sqlException.getStackTrace()).anyMatch(stackTraceElement -> stackTraceElement.toString().contains("Cannot add or update a child row"))) {
                throw new NoValidUserNameException(userName, sqlException);
            }
        }
    }

    /**
     * Removes one player from the blacklist of the given dungeon
     *
     * @param dungeonName
     * @param userName
     * @throws ElementNotFoundException
     * @Author Moritz Klaiber
     */
    @Override
    public void removeBlackList(String dungeonName, String userName) throws NoValidUserNameException, EntryAlreadyExistsException {
        try {
            List<Object> parameterList = new ArrayList<Object>();
            parameterList.add(dungeonName);
            parameterList.add(userName);
            dbConnector.executeSQL("DELETE FROM blackList WHERE dungeonName = ? AND userName = ?", parameterList);
        } catch (SQLException sqlException) {
            if(Arrays.stream(sqlException.getStackTrace()).anyMatch(stackTraceElement -> stackTraceElement.toString().contains("Duplicate entry"))){
                throw new EntryAlreadyExistsException(dungeonName+userName, sqlException);
            } else if(Arrays.stream(sqlException.getStackTrace()).anyMatch(stackTraceElement -> stackTraceElement.toString().contains("Cannot add or update a child row"))) {
                throw new NoValidUserNameException(userName, sqlException);
            }
        }
    }

    /**
     * Returns all usernames that are present on the whitelist for the given dungeon.
     *
     * @param dungeonName
     * @return
     * @throws ElementNotFoundException
     * @Author Moritz Klaiber
     */
    @Override
    public List<String> getWhiteList(String dungeonName) throws ElementNotFoundException {
        try {
            List<Object> parameterList = new ArrayList<Object>();
            List<String> whiteListUserNames = new ArrayList<>();
            parameterList.add(dungeonName);
            ResultSet rsBlackList = dbConnector.getSQLResults("SELECT * FROM whiteList WHERE dungeonName = ?", parameterList);

            while (rsBlackList.next()) {
                whiteListUserNames.add(dbConnector.getString(rsBlackList, "userName"));
            }

            return whiteListUserNames;

        } catch (SQLException sqlException) {
            throw new ElementNotFoundException("dungeon not found: ", dungeonName, sqlException);
        }
    }

    /**
     * Set one player to the whitelist of the given dungeon
     *
     * @param dungeonName
     * @param userName
     * @throws ElementNotFoundException
     * @Author Moritz Klaiber
     */
    @Override
    public void setWhiteList(String dungeonName, String userName) throws NoValidUserNameException, EntryAlreadyExistsException {
        try {
            List<Object> parameterList = new ArrayList<Object>();
            parameterList.add(dungeonName);
            parameterList.add(userName);
            dbConnector.executeSQL("INSERT INTO whiteList VALUES(?, ?)", parameterList);
        } catch (SQLException sqlException) {
            if(Arrays.stream(sqlException.getStackTrace()).anyMatch(stackTraceElement -> stackTraceElement.toString().contains("Duplicate entry"))){
                throw new EntryAlreadyExistsException(dungeonName+userName, sqlException);
            } else if(Arrays.stream(sqlException.getStackTrace()).anyMatch(stackTraceElement -> stackTraceElement.toString().contains("Cannot add or update a child row"))) {
                throw new NoValidUserNameException(userName, sqlException);
            }
        }
    }

    /**
     * Removes one player from the whitelist of the given dungeon
     *
     * @param dungeonName
     * @param userName
     * @throws ElementNotFoundException
     * @Author Moritz Klaiber
     */
    @Override
    public void removeWhiteList(String dungeonName, String userName) throws NoValidUserNameException, EntryAlreadyExistsException {
        try {
            List<Object> parameterList = new ArrayList<Object>();
            parameterList.add(dungeonName);
            parameterList.add(userName);
            dbConnector.executeSQL("DELETE FROM whiteList WHERE dungeonName = ? AND userName = ?", parameterList);
        } catch (SQLException sqlException) {
            if(Arrays.stream(sqlException.getStackTrace()).anyMatch(stackTraceElement -> stackTraceElement.toString().contains("Duplicate entry"))){
                throw new EntryAlreadyExistsException(dungeonName+userName, sqlException);
            } else if(Arrays.stream(sqlException.getStackTrace()).anyMatch(stackTraceElement -> stackTraceElement.toString().contains("Cannot add or update a child row"))) {
                throw new NoValidUserNameException(userName, sqlException);
            }
        }
    }

    /**
     * Returns all usernames that are present on the joinrequest list for the given dungeon.
     *
     * @param dungeonName
     * @return
     * @throws ElementNotFoundException
     * @Author Moritz Klaiber
     */
    @Override
    public List<String> getJoinRequestList(String dungeonName) throws ElementNotFoundException {
        try {
            List<Object> parameterList = new ArrayList<Object>();
            List<String> joinRequestUserNames = new ArrayList<>();
            parameterList.add(dungeonName);
            ResultSet rsBlackList = dbConnector.getSQLResults("SELECT * FROM joinRequests WHERE dungeonName = ?", parameterList);

            while (rsBlackList.next()) {
                joinRequestUserNames.add(dbConnector.getString(rsBlackList, "userName"));
            }

            return joinRequestUserNames;

        } catch (SQLException sqlException) {
            throw new ElementNotFoundException("dungeon not found: ", dungeonName, sqlException);
        }
    }

    /**
     * Set one player to the joinrequest list of the given dungeon
     *
     * @param dungeonName
     * @param userName
     * @throws ElementNotFoundException
     * @Author Moritz Klaiber
     */
    @Override
    public void setJoinRequest(String dungeonName, String userName) throws NoValidUserNameException, EntryAlreadyExistsException {
        try {
            List<Object> parameterList = new ArrayList<Object>();
            parameterList.add(dungeonName);
            parameterList.add(userName);
            dbConnector.executeSQL("INSERT INTO joinRequests VALUES(?, ?)", parameterList);
        } catch (SQLException sqlException) {
            if(Arrays.stream(sqlException.getStackTrace()).anyMatch(stackTraceElement -> stackTraceElement.toString().contains("Duplicate entry"))){
                throw new EntryAlreadyExistsException(dungeonName+userName, sqlException);
            } else if(Arrays.stream(sqlException.getStackTrace()).anyMatch(stackTraceElement -> stackTraceElement.toString().contains("Cannot add or update a child row"))) {
                throw new NoValidUserNameException(userName, sqlException);
            }
        }
    }

    /**
     * Removes one player from the joinrequest list of the given dungeon
     *
     * @param dungeonName
     * @param userName
     * @throws ElementNotFoundException
     * @Author Moritz Klaiber
     */
    @Override
    public void removeJoinRequest(String dungeonName, String userName) throws NoValidUserNameException, EntryAlreadyExistsException {
        try {
            List<Object> parameterList = new ArrayList<Object>();
            parameterList.add(dungeonName);
            parameterList.add(userName);
            dbConnector.executeSQL("DELETE FROM joinRequests WHERE dungeonName = ? AND userName = ?", parameterList);
        } catch (SQLException sqlException) {
            if(Arrays.stream(sqlException.getStackTrace()).anyMatch(stackTraceElement -> stackTraceElement.toString().contains("Duplicate entry"))){
                throw new EntryAlreadyExistsException(dungeonName+userName, sqlException);
            } else if(Arrays.stream(sqlException.getStackTrace()).anyMatch(stackTraceElement -> stackTraceElement.toString().contains("Cannot add or update a child row"))) {
                throw new NoValidUserNameException(userName, sqlException);
            }
        }
    }

    /**
     * Returns all usernames that are present on the dmlist for the given dungeon.
     *
     * @param dungeonName
     * @return
     * @throws ElementNotFoundException
     * @Author Moritz Klaiber
     */
    @Override
    public List<String> getDMList(String dungeonName) throws ElementNotFoundException {
        try {
            List<Object> parameterList = new ArrayList<Object>();
            List<String> dmListUserNames = new ArrayList<>();
            parameterList.add(dungeonName);
            ResultSet rsBlackList = dbConnector.getSQLResults("SELECT * FROM dmList WHERE dungeonName = ?", parameterList);

            while (rsBlackList.next()) {
                dmListUserNames.add(dbConnector.getString(rsBlackList, "userName"));
            }

            return dmListUserNames;

        } catch (SQLException sqlException) {
            throw new ElementNotFoundException("dungeon not found: ", dungeonName, sqlException);
        }
    }

    /**
     * Set one player to the dmlist of the given dungeon
     *
     * @param dungeonName
     * @param userName
     * @throws ElementNotFoundException
     * @Author Moritz Klaiber
     */
    @Override
    public void setNewDM(String dungeonName, String userName) throws NoValidUserNameException, EntryAlreadyExistsException, SQLException {
        try {
            List<Object> parameterList = new ArrayList<Object>();
            parameterList.add(dungeonName);
            System.out.println("Want to add" + userName + " to DM List of " +dungeonName);
            parameterList.add(userName);
            dbConnector.executeSQL("INSERT INTO dmList VALUES(?, ?)", parameterList);
        } catch (SQLException sqlException) {
            if(Arrays.stream(sqlException.getStackTrace()).anyMatch(stackTraceElement -> stackTraceElement.toString().contains("Duplicate entry"))){
                throw new EntryAlreadyExistsException(dungeonName+userName, sqlException);
            } else if(Arrays.stream(sqlException.getStackTrace()).anyMatch(stackTraceElement -> stackTraceElement.toString().contains("Cannot add or update a child row"))) {
                throw new NoValidUserNameException(userName, sqlException);
            } else {
                throw new SQLException(sqlException);
            }
        }
    }

    /**
     * Removes one player from the dmlist of the given dungeon
     *
     * @param dungeonName
     * @param userName
     * @throws ElementNotFoundException
     * @Author Moritz Klaiber
     */
    @Override
    public void removeDM(String dungeonName, String userName) throws NoValidUserNameException, EntryAlreadyExistsException {
        try {
            List<Object> parameterList = new ArrayList<Object>();

            parameterList.add(dungeonName);
            parameterList.add(userName);
            dbConnector.executeSQL("DELETE FROM dmList WHERE dungeonName = ? AND userName = ?", parameterList);
        } catch (SQLException sqlException) {
            if(Arrays.stream(sqlException.getStackTrace()).anyMatch(stackTraceElement -> stackTraceElement.toString().contains("Duplicate entry"))){
                throw new EntryAlreadyExistsException(dungeonName+userName, sqlException);
            } else if(Arrays.stream(sqlException.getStackTrace()).anyMatch(stackTraceElement -> stackTraceElement.toString().contains("Cannot add or update a child row"))) {
                throw new NoValidUserNameException(userName, sqlException);
            }
        }
    }
}
