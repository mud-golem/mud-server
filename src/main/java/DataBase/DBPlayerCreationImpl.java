package DataBase;

import DatabaseInterfaces.DBConnector;
import DatabaseInterfaces.DBPlayerCreation;
import Exceptions.ElementNotFoundException;
import Model.Base.PlayerAvatar;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@ApplicationScoped
/**
 * This implementation provides all Methods which are needed to create an own avatar.
 * @author Moritz Klaiber
 */
public class DBPlayerCreationImpl implements DBPlayerCreation {

    private DBConnector dbConnector;

    @Inject
    public void setDbConnector(DBConnectorImpl dbConnector) {

        this.dbConnector = dbConnector;
    }

    public void setDbConnector(DBConnector dbConnector) {

        this.dbConnector = dbConnector;
    }

    /**
     * Creates an avatar in a specific dungeon.
     *
     * @param dungeonName
     * @param playerAvatar
     * @return
     * @throws ElementNotFoundException
     * @author Moritz Klaiber
     */
    @Override
    public long createPlayerCharacter(String dungeonName, PlayerAvatar playerAvatar) throws ElementNotFoundException {
        try {
            List<Object> parameterList = new ArrayList<>();
            parameterList.add(dungeonName);

            ResultSet rs = dbConnector.getSQLResults("SELECT startRoomGlobalId FROM dungeon WHERE uniqueName = ?", parameterList);
            long dungeonRoomGlobalId = -1;

            if (rs.next()) {
                dungeonRoomGlobalId = dbConnector.getLong(rs, "startRoomGlobalId");
            }

            parameterList.clear();
            parameterList.add(null);
            parameterList.add(dungeonName);
            parameterList.add(playerAvatar.getName());
            parameterList.add(playerAvatar.getUserDescription());
            parameterList.add(playerAvatar.getCurrHP());
            parameterList.add(playerAvatar.getMaxHP());
            parameterList.add(playerAvatar.isActive());
            parameterList.add(playerAvatar.getOwnerUserName());
            parameterList.add((dungeonRoomGlobalId == -1 ? null : dungeonRoomGlobalId));
            parameterList.add(playerAvatar.getAvatarRace().getGlobalID());
            parameterList.add(playerAvatar.getAvatarClass().getGlobalID());

            return dbConnector.insertSQL("INSERT INTO playerAvatars VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", parameterList);

        } catch (SQLException sqlException) {
            ElementNotFoundException elementNotFoundException = new ElementNotFoundException("Dunegon was not found", dungeonName, sqlException);
            //elementNotFoundException.setStackTrace(sqlException.getStackTrace());
            throw elementNotFoundException;
        }

    }

    /**
     * Deletes an avatar from a specific dungeon.
     *
     * @param dungeonName
     * @param playerAvatarGlobalID
     * @return
     * @deprecated
     */
    @Override
    public boolean deletePlayerCharacter(String dungeonName, long playerAvatarGlobalID) {
        try {
            List<Object> parameterList = new ArrayList<>();
            parameterList.add(dungeonName);
            parameterList.add(playerAvatarGlobalID);

            dbConnector.executeSQL("DELETE FROM playerAvatars WHERE dungeon = ? AND globalId = ?", parameterList);

            return true;
        } catch (SQLException sqlException) {
            ElementNotFoundException elementNotFoundException = new ElementNotFoundException("Dunegon was not found", dungeonName, sqlException);
            elementNotFoundException.setStackTrace(sqlException.getStackTrace());
            return false;
        }
    }
}
