package DataBase;

import DatabaseInterfaces.DBConnector;
import DatabaseInterfaces.DBPlayerEditing;
import Exceptions.ElementNotFoundException;
import Model.Base.AvatarClass;
import Model.Base.AvatarRace;
import Model.Base.InventoryEntry;
import Model.Base.PlayerAvatar;
import Model.CommunicationModel.APIInventoryEntry;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

@ApplicationScoped
/**
 * @author Moritz Klaiber
 * This implementation can be used to edit an avatar.
 */
public class DBPlayerEditingImpl implements DBPlayerEditing {

    private DBConnector dbConnector;

    @Inject
    public void setDbConnector(DBConnectorImpl dbConnector) {
        this.dbConnector = dbConnector;
    }

    public void setDbConnector(DBConnector dbConnector) {
        this.dbConnector = dbConnector;
    }

    /**
     * @param dungeonName
     * @param playerAvatar
     * @throws ElementNotFoundException
     * @author Moritz Klaiber
     * This method can be used to update all attributes of the Avatar.
     */

    @Override
    public void setPlayer(String dungeonName, PlayerAvatar playerAvatar) throws ElementNotFoundException {
        List<Object> parameterList = new ArrayList<Object>();

        parameterList.add(playerAvatar.getGlobalId());

        ResultSet getAvatar = dbConnector.getSQLResults("SELECT * FROM playerAvatars WHERE globalId = ?", parameterList);
        try {
            if (getAvatar.next()) {
                parameterList.clear();
                parameterList.add(playerAvatar.getDungeonName());
                parameterList.add(playerAvatar.getName());
                parameterList.add(playerAvatar.getUserDescription());
                parameterList.add(playerAvatar.getCurrHP());
                parameterList.add(playerAvatar.getMaxHP());
                parameterList.add(playerAvatar.isActive());
                parameterList.add(playerAvatar.getOwnerUserName());
                parameterList.add(playerAvatar.getActualRoomGlobalId());
                parameterList.add(playerAvatar.getAvatarRace().getGlobalID());
                parameterList.add(playerAvatar.getAvatarClass().getGlobalID());

                parameterList.add(playerAvatar.getGlobalId());

                dbConnector.executeSQL("UPDATE playerAvatars SET dungeonName = ?, name = ?, userDescription = ?, currHP = ?, maxHP = ?, " +
                        "isActive = ?, owner = ?, actualDungeonRoomGlobalId = ?, raceGlobalId = ?, classGlobalId = ? WHERE globalId = ?", parameterList);

                playerAvatar.getInventory().forEach(inventoryEntry -> {
                    try {
                        parameterList.clear();
                        parameterList.add(inventoryEntry.getTemplateGlobalID());
                        parameterList.add(playerAvatar.getGlobalId());
                        ResultSet rs = dbConnector.getSQLResults("SELECT * FROM inventoryEntries WHERE globalTemplateId = ? AND globalPlayerAvatarId = ?", parameterList);

                        if (!rs.next() && inventoryEntry.getCount() > 0) {
                            parameterList.clear();
                            parameterList.add(null);
                            parameterList.add(inventoryEntry.getCount());
                            parameterList.add(inventoryEntry.getTemplateGlobalID());
                            parameterList.add(null);
                            parameterList.add(playerAvatar.getGlobalId());
                            dbConnector.executeSQL("INSERT INTO inventoryEntries VALUES(?, ?, ?, ?, ?)", parameterList);
                        } else {
                            if (inventoryEntry.getCount() == 0) {
                                parameterList.clear();
                                parameterList.add(inventoryEntry.getTemplateGlobalID());
                                parameterList.add(playerAvatar.getGlobalId());
                                dbConnector.executeSQL("DELETE FROM inventoryEntries WHERE globalTemplateId = ? AND globalPlayerAvatarId = ?", parameterList);
                            } else {
                                parameterList.clear();
                                parameterList.add(inventoryEntry.getCount());
                                parameterList.add(inventoryEntry.getTemplateGlobalID());
                                parameterList.add(playerAvatar.getGlobalId());
                                dbConnector.executeSQL("UPDATE inventoryEntries SET count = ? WHERE globalTemplateId = ? AND globalPlayerAvatarId = ?", parameterList);
                            }
                        }
                    } catch (SQLException sqlException) {
                        sqlException.printStackTrace();
                    }
                });

            } else {
                throw new ElementNotFoundException("No Avatar with this attributes found.", dungeonName + "-" + playerAvatar.getOwnerUserName());
            }
        } catch (SQLException sqlException) {
            ElementNotFoundException elementNotFoundException = new ElementNotFoundException("Player Avatar not found", dungeonName + "");
            elementNotFoundException.setStackTrace(sqlException.getStackTrace());
            throw elementNotFoundException;
        }

    }

    /**
     * @param dungeonName
     * @param username
     * @param newValue
     * @author Moritz Klaiber
     * Updates the name of an Avatar.
     */
    @Override
    public void setPlayerName(String dungeonName, String username, String newValue) throws ElementNotFoundException {
        try {
            List<Object> parameterList = new ArrayList<Object>();
            parameterList.add(newValue);
            parameterList.add(dungeonName);
            parameterList.add(username);

            dbConnector.executeSQL("UPDATE playerAvatars SET name = ? WHERE dungeonName = ? AND owner = ?", parameterList);

        } catch (SQLException sqlException) {
            ElementNotFoundException elementNotFoundException = new ElementNotFoundException("Player Avatar not found", dungeonName + "");
            elementNotFoundException.setStackTrace(sqlException.getStackTrace());
            throw elementNotFoundException;
        }

    }

    /**
     * @param dungeonName
     * @param username
     * @param newValue
     * @author Moritz Klaiber
     * Updates the description of an Avatar.
     */
    @Override
    public void setPlayerDescription(String dungeonName, String username, String newValue) throws ElementNotFoundException {
        try {
            List<Object> parameterList = new ArrayList<Object>();
            parameterList.add(newValue);
            parameterList.add(dungeonName);
            parameterList.add(username);

            dbConnector.executeSQL("UPDATE playerAvatars SET userDescription = ? WHERE dungeonName = ? AND owner = ?", parameterList);

        } catch (SQLException sqlException) {
            ElementNotFoundException elementNotFoundException = new ElementNotFoundException("Player Avatar not found", dungeonName + "");
            elementNotFoundException.setStackTrace(sqlException.getStackTrace());
            throw elementNotFoundException;
        }

    }

    /**
     * @param dungeonName
     * @param username
     * @param newValue
     * @author Moritz Klaiber
     * Updates the current Helth Points of an Avatar.
     */
    @Override
    public void setPlayerCurrHP(String dungeonName, String username, int newValue) throws ElementNotFoundException {
        try {
            List<Object> parameterList = new ArrayList<Object>();
            parameterList.add(newValue);
            parameterList.add(dungeonName);
            parameterList.add(username);

            dbConnector.executeSQL("UPDATE playerAvatars SET currHP = ? WHERE dungeonName = ? AND owner = ?", parameterList);
        } catch (SQLException sqlException) {
            ElementNotFoundException elementNotFoundException = new ElementNotFoundException("Player Avatar not found", dungeonName + "");
            elementNotFoundException.setStackTrace(sqlException.getStackTrace());
            throw elementNotFoundException;
        }
    }

    /**
     * @param dungeonName
     * @param username
     * @param newValue
     * @author Moritz Klaiber
     * Updates the maximal Helth Points of an Avatar.
     */
    @Override
    public void setPlayerMaxHP(String dungeonName, String username, int newValue) throws ElementNotFoundException {
        try {
            List<Object> parameterList = new ArrayList<Object>();
            parameterList.add(newValue);
            parameterList.add(dungeonName);
            parameterList.add(username);

            dbConnector.executeSQL("UPDATE playerAvatars SET maxHP = ? WHERE dungeonName = ? AND owner = ?", parameterList);
        } catch (SQLException sqlException) {
            ElementNotFoundException elementNotFoundException = new ElementNotFoundException("Player Avatar not found", dungeonName + "");
            elementNotFoundException.setStackTrace(sqlException.getStackTrace());
            throw elementNotFoundException;
        }
    }

    /**
     * @param dungeonName
     * @param username
     * @param newValue
     * @author Moritz Klaiber
     * Updates the activity status of an Avatar.
     */
    @Override
    public void setPlayerIsActive(String dungeonName, String username, boolean newValue) throws ElementNotFoundException {
        try {
            List<Object> parameterList = new ArrayList<Object>();

            parameterList.add(newValue);
            parameterList.add(dungeonName);
            parameterList.add(username);

            dbConnector.executeSQL("UPDATE playerAvatars SET isActive = ? WHERE dungeonName = ? AND owner = ?", parameterList);
        } catch (SQLException sqlException) {
            ElementNotFoundException elementNotFoundException = new ElementNotFoundException("Dungeon not found", dungeonName);
            elementNotFoundException.setStackTrace(sqlException.getStackTrace());
            throw elementNotFoundException;
        }
    }

    /**
     * @param dungeonName
     * @param username
     * @param newValue
     * @author Moritz Klaiber
     * Updates the actual position of an Avatar in the dungeon.
     */
    @Override
    public void setPlayerActualRoomGlobalID(String dungeonName, String username, long newValue) throws ElementNotFoundException {
        try {
            List<Object> parameterList = new ArrayList<Object>();
            parameterList.add(newValue);
            parameterList.add(dungeonName);
            parameterList.add(username);

            dbConnector.executeSQL("UPDATE playerAvatars SET actualDungeonRoomGlobalId = ? WHERE dungeonName = ? AND owner = ?", parameterList);
        } catch (SQLException sqlException) {
            ElementNotFoundException elementNotFoundException = new ElementNotFoundException("Player Avatar not found", dungeonName);
            elementNotFoundException.setStackTrace(sqlException.getStackTrace());
            throw elementNotFoundException;
        }
    }

    /**
     * @param dungeonName
     * @param username
     * @param newValue
     * @author Moritz Klaiber
     * Updates the race of an Avatar.
     */
    @Override
    public void setPlayerRace(String dungeonName, String username, AvatarRace newValue) throws ElementNotFoundException {
        try {
            List<Object> parameterList = new ArrayList<Object>();
            parameterList.add(newValue.getGlobalID());
            parameterList.add(dungeonName);
            parameterList.add(username);

            dbConnector.executeSQL("UPDATE playerAvatars SET raceGlobalId = ? WHERE dungeonName = ? AND owner = ?", parameterList);
        } catch (SQLException sqlException) {
            ElementNotFoundException elementNotFoundException = new ElementNotFoundException("Player Avatar not found", newValue.getDisplayName());
            elementNotFoundException.setStackTrace(sqlException.getStackTrace());
            throw elementNotFoundException;
        }
    }

    /**
     * @param dungeonName
     * @param username
     * @param newValue
     * @author Moritz Klaiber
     * Updates the class of an Avatar.
     */
    @Override
    public void setPlayerClass(String dungeonName, String username, AvatarClass newValue) throws ElementNotFoundException {
        try {
            List<Object> parameterList = new ArrayList<Object>();
            parameterList.add(newValue.getGlobalID());
            parameterList.add(dungeonName);
            parameterList.add(username);

            dbConnector.executeSQL("UPDATE playerAvatars SET classGlobalId = ? WHERE dungeonName = ? AND owner = ?", parameterList);
        } catch (SQLException sqlException) {
            ElementNotFoundException elementNotFoundException = new ElementNotFoundException("Player Avatar not found", newValue.getDisplayName());
            elementNotFoundException.setStackTrace(sqlException.getStackTrace());
            throw elementNotFoundException;
        }
    }

    /**
     * @param dungeonName
     * @param username
     * @param newValue
     * @author Moritz Klaiber
     * Updates the whole inventory of an Avatar.
     */
    @Override
    public void setInventory(String dungeonName, String username, List<APIInventoryEntry> newValue) throws ElementNotFoundException {
        long globalPlayerAvatarId = 0;
        try {
            List<Object> parameterList = new ArrayList<Object>();

            parameterList.add(dungeonName);
            parameterList.add(username);
            ResultSet resultSet = dbConnector.getSQLResults("SELECT globalId FROM playerAvatars WHERE dungeonName = ? AND owner = ?", parameterList);

            if (resultSet.next()) {
                globalPlayerAvatarId = dbConnector.getLong(resultSet, "globalId");
                System.out.println("PlayerID: " + globalPlayerAvatarId);
            } else {
                throw new ElementNotFoundException("No user was found: ", username);
            }

            parameterList.clear();
            parameterList.add(globalPlayerAvatarId);
            dbConnector.executeSQL("DELETE FROM inventoryEntries WHERE globalPlayerAvatarId = ?", parameterList);


            AtomicLong globalPlayerAvatarIdAL = new AtomicLong(globalPlayerAvatarId);

            newValue.stream().filter(apiInventoryEntry -> apiInventoryEntry.getCount() > 0).forEach(nv -> {
                try {
                    System.out.println("Actual elemend to Add: " + nv.getObjectTemplateGlobalID());
                    System.out.print(" With Count" + nv.getCount());
                    System.out.print(" In dungeon" + dungeonName);
                    System.out.print(" For Player" + globalPlayerAvatarIdAL.get());
                    parameterList.clear();
                    parameterList.add(null);
                    parameterList.add(nv.getCount());
                    parameterList.add(nv.getObjectTemplateGlobalID());
                    parameterList.add(null);
                    parameterList.add(globalPlayerAvatarIdAL.get());
                    dbConnector.executeSQL("INSERT INTO inventoryEntries VALUES(?, ?, ?, ?, ?)", parameterList);
                } catch (SQLException sqlException) {
                    //ElementNotFoundException elementNotFoundException = new ElementNotFoundException("Inventory not found", globalPlayerAvatarIdAL + "");
                    //elementNotFoundException.setStackTrace(sqlException.getStackTrace());
                    StringWriter sw = new StringWriter();
                    sqlException.printStackTrace(new PrintWriter(sw));
                    String exceptionAsString = sw.toString();
                    System.out.println(exceptionAsString);
                }
            });
        } catch (SQLException sqlException) {
            ElementNotFoundException elementNotFoundException = new ElementNotFoundException("Player Avatar not found", globalPlayerAvatarId + "");
            elementNotFoundException.setStackTrace(sqlException.getStackTrace());
            throw elementNotFoundException;
        }
    }

    /**
     * @param playerAvatarGlobalId
     * @param classGlobalId
     * @throws ElementNotFoundException
     * @author Moritz Klaiber
     * Sets a playerclass by the ids
     */
    @Override
    public void setPlayerClassByID(long playerAvatarGlobalId, long classGlobalId) throws ElementNotFoundException {
        try {
            List<Object> parameterList = new ArrayList<Object>();
            parameterList.add(classGlobalId);
            System.out.println("New Class ID = " + classGlobalId);
            parameterList.add(playerAvatarGlobalId);

            dbConnector.executeSQL("UPDATE playerAvatars SET classGlobalId = ? WHERE globalId = ?", parameterList);
        } catch (SQLException sqlException) {
            ElementNotFoundException elementNotFoundException = new ElementNotFoundException("Player Avatar not found", playerAvatarGlobalId + "");
            elementNotFoundException.setStackTrace(sqlException.getStackTrace());
            throw elementNotFoundException;
        }
    }

    /**
     * Sets a playerrace by the ids
     *
     * @param playerAvatarGlobalId
     * @param raceGlobalId
     * @throws ElementNotFoundException
     * @author Moritz Klaiber
     */
    @Override
    public void setPlayerRaceByID(long playerAvatarGlobalId, long raceGlobalId) throws ElementNotFoundException {
        try {
            List<Object> parameterList = new ArrayList<Object>();
            parameterList.add(raceGlobalId);
            System.out.println("New Race ID = " + raceGlobalId);
            parameterList.add(playerAvatarGlobalId);

            dbConnector.executeSQL("UPDATE playerAvatars SET raceGlobalId = ? WHERE globalId = ?", parameterList);
        } catch (SQLException sqlException) {
            ElementNotFoundException elementNotFoundException = new ElementNotFoundException("Player Avatar not found", playerAvatarGlobalId + "");
            elementNotFoundException.setStackTrace(sqlException.getStackTrace());
            throw elementNotFoundException;
        }
    }

    /**
     * Deletes the given player avatar permanently.
     * @author Moritz Klaiber
     * @param globalAvatarId
     * @param dungeonName
     * @throws ElementNotFoundException
     */
    @Override
    public void deletePlayerAvatar(long globalAvatarId, String dungeonName) throws ElementNotFoundException {
        try {
            List<Object> parameterList = new ArrayList<Object>();
            parameterList.add(globalAvatarId);
            parameterList.add(dungeonName);
            dbConnector.executeSQL("DELETE FROM playerAvatars WHERE globalId = ? AND dungeonName = ?", parameterList);
        } catch (SQLException sqlException) {
            ElementNotFoundException elementNotFoundException = new ElementNotFoundException("Player Avatar not found", globalAvatarId+"", sqlException);
            elementNotFoundException.setStackTrace(sqlException.getStackTrace());
            throw elementNotFoundException;
        }
    }
}
