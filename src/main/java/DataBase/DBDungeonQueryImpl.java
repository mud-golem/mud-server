package DataBase;

import DatabaseInterfaces.DBConnector;
import DatabaseInterfaces.DBDungeonQuery;
import Exceptions.ElementNotFoundException;
import Model.Base.*;
import Model.CommunicationModel.APIDungeon;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@ApplicationScoped
/**
 * @Author Moritz Klaiber
 * This Class implements all Getter-Functions for a dungeon.
 * It only performs SELECT SQL Commands
 */
public class DBDungeonQueryImpl implements DBDungeonQuery {

    private DBConnector dbConnector;

    @Inject
    public void setDbConnector(DBConnectorImpl dbConnector) {

        this.dbConnector = dbConnector;
    }

    public void setDbConnector(DBConnector dbConnector) {

        this.dbConnector = dbConnector;
    }

    /**
     * @param dungeonName
     * @return dungeonDescription
     * @Author Moritz Klaiber
     * Returns the long dungeon description
     */
    @Override
    public String getDungeonDescription(String dungeonName) throws ElementNotFoundException {
        List<Object> parameterList = new ArrayList<Object>();
        parameterList.add(dungeonName);

        ResultSet rs = dbConnector.getSQLResults("SELECT dungeonDescription FROM dungeon WHERE uniqueName = ?", parameterList);
        try {
            if (rs.next()) {
                return dbConnector.getString(rs, "dungeonDescription");
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return null;
    }

    /**
     * @param dungeonName
     * @return dungeonDescription
     * @Author Moritz Klaiber
     * Returns the short dungeon description that is shown in the lobby
     */
    @Override
    public String getLobbyDescription(String dungeonName) throws ElementNotFoundException {
        List<Object> parameterList = new ArrayList<Object>();
        parameterList.add(dungeonName);

        ResultSet rs = dbConnector.getSQLResults("SELECT lobbyDescription FROM dungeon WHERE uniqueName = ?", parameterList);
        try {
            if (rs.next()) {
                return dbConnector.getString(rs, "lobbyDescription");
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return null;
    }

    /**
     * @param dungeonName
     * @return boolean
     * @Author Moritz Klaiber
     * Returns the flag, wether a dungeon is joinable by users or not
     */
    @Override
    public boolean getJoinable(String dungeonName) throws ElementNotFoundException {
        List<Object> parameterList = new ArrayList<Object>();
        parameterList.add(dungeonName);

        ResultSet rs = dbConnector.getSQLResults("SELECT isJoinable FROM dungeon WHERE uniqueName = ?", parameterList);
        try {
            if (rs.next()) {
                return dbConnector.getBoolean(rs, "isJoinable");
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return false;
    }

    /**
     * @param dungeonName
     * @return long
     * @Author Moritz Klaiber
     * Returns the global id of the start room
     */
    @Override
    public long getStartRoomGlobalID(String dungeonName) throws ElementNotFoundException {
        List<Object> parameterList = new ArrayList<Object>();
        parameterList.add(dungeonName);

        ResultSet rs = dbConnector.getSQLResults("SELECT startRoomGlobalID FROM dungeon WHERE uniqueName = ?", parameterList);
        try {
            if (rs.next()) {
                return dbConnector.getLong(rs, "startRoomGlobalID");
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return -1;
    }

    /**
     * @param dungeonName
     * @return String
     * @Author Moritz Klaiber
     * returns the unknown action response for the dungeon
     */
    @Override
    public String getUnknownActionResponse(String dungeonName) throws ElementNotFoundException {
        List<Object> parameterList = new ArrayList<Object>();
        parameterList.add(dungeonName);

        ResultSet rs = dbConnector.getSQLResults("SELECT unknownActionResponse FROM dungeon WHERE uniqueName = ?", parameterList);
        try {
            if (rs.next()) {
                return dbConnector.getString(rs, "unknownActionResponse");
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return null;
    }

    /**
     * @param dungeonName
     * @return List<String>
     * @Author Moritz Klaiber
     * returns the blacklist that denies access to specific users
     */
    @Override
    public List<String> getBlacklist(String dungeonName) throws ElementNotFoundException {
        List<Object> parameterList = new ArrayList<Object>();
        parameterList.add(dungeonName);

        List<String> ret = new ArrayList<String>();
        ResultSet rs = dbConnector.getSQLResults("SELECT userName FROM blackList WHERE dungeonName = ?", parameterList);
        try {
            while (rs.next()) {
                ret.add(dbConnector.getString(rs, "userName"));
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return ret;
    }

    /**
     * @param dungeonName
     * @return List<String>
     * @Author Moritz Klaiber
     * returns the blacklist that gives access to specific users
     */
    @Override
    public List<String> getWhitelist(String dungeonName) throws ElementNotFoundException {
        List<Object> parameterList = new ArrayList<Object>();
        parameterList.add(dungeonName);

        List<String> ret = new ArrayList<String>();
        ResultSet rs = dbConnector.getSQLResults("SELECT userName FROM whiteList WHERE dungeonName = ?", parameterList);
        try {
            while (rs.next()) {
                ret.add(dbConnector.getString(rs, "userName"));
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return ret;
    }

    /**
     * @param dungeonName
     * @return List<String>
     * @Author Moritz Klaiber
     * returns the list of all dms
     */
    @Override
    public List<String> getAllowedDms(String dungeonName) throws ElementNotFoundException {
        List<Object> parameterList = new ArrayList<Object>();
        parameterList.add(dungeonName);
        List<String> ret = new ArrayList<String>();
        ResultSet rs = dbConnector.getSQLResults("SELECT * FROM dmList d, user u WHERE d.dungeonName = ? AND d.userName = u.name", parameterList);
        try {
            while (rs.next()) {
                ret.add(dbConnector.getString(rs, "name"));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return ret;
    }

    @Override
    public String getActualLoggedInDM(String dungeonName) throws ElementNotFoundException {
        List<Object> parameterList = new ArrayList<Object>();
        parameterList.add(dungeonName);
        ResultSet rs = dbConnector.getSQLResults("SELECT dungeonMasterUserName FROM dungeon WHERE uniqueName = ?", parameterList);
        try {
            if(rs.next()){
                return dbConnector.getString(rs, "dungeonMasterUserName");
            } else {
                return "-";
            }
        } catch (SQLException throwables) {
            throw new ElementNotFoundException("Dungeon not found: ", dungeonName, throwables);
        }
    }

    /**
     * @param dungeonName
     * @return List<AvatarClass>
     * @Author Moritz Klaiber
     * Returns all Avatar Class for the mentioned dungeon
     */
    @Override
    public List<AvatarClass> getAvatarClasses(String dungeonName) throws ElementNotFoundException {
        List<Object> parameterList = new ArrayList<Object>();
        parameterList.add(dungeonName);

        List<AvatarClass> ret = new ArrayList<AvatarClass>();
        ResultSet rs = dbConnector.getSQLResults("SELECT * FROM classes WHERE dungeonName = ?", parameterList);
        try {
            while (rs.next()) {
                ret.add(new AvatarClass(dbConnector.getLong(rs, "globalId"),
                        dbConnector.getString(rs, "displayName"),
                        dbConnector.getString(rs, "description"),
                        dbConnector.getString(rs, "descriptionModifier"),
                        dbConnector.getInt(rs, "HPModifier"),
                        dbConnector.getBoolean(rs, "enabled")));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return ret;
    }

    /**
     * @param dungeonName
     * @param avatarClassGlobalID
     * @return AvatarClass
     * @throws ElementNotFoundException
     * @Author Moritz Klaiber
     * returns one specific Avatar Class
     */
    @Override
    public AvatarClass getAvatarClass(String dungeonName, long avatarClassGlobalID) throws ElementNotFoundException {
        List<Object> parameterList = new ArrayList<Object>();
        parameterList.add(dungeonName);
        parameterList.add(avatarClassGlobalID);
        ResultSet rs = dbConnector.getSQLResults("SELECT * FROM classes WHERE dungeonName = ? AND globalId = ?", parameterList);
        try {
            if (rs.next()) {
                return new AvatarClass(dbConnector.getLong(rs, "globalId"),
                        dbConnector.getString(rs, "displayName"),
                        dbConnector.getString(rs, "description"),
                        dbConnector.getString(rs, "descriptionModifier"),
                        dbConnector.getInt(rs, "HPModifier"),
                        dbConnector.getBoolean(rs, "enabled"));
            } else {
                return new AvatarClass();
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            throw new ElementNotFoundException("AvatarClass not found", dungeonName + " and " + avatarClassGlobalID);
        }
    }

    /**
     * @param dungeonName
     * @return List<AvatarRace>
     * @Author Moritz Klaiber
     * Returns all Avatar Races for the mentioned dungeon
     */
    @Override
    public List<AvatarRace> getAvatarRaces(String dungeonName) throws ElementNotFoundException {
        List<Object> parameterList = new ArrayList<Object>();
        parameterList.add(dungeonName);

        List<AvatarRace> ret = new ArrayList<AvatarRace>();
        ResultSet rs = dbConnector.getSQLResults("SELECT * FROM races WHERE dungeonName = ?", parameterList);
        try {
            while (rs.next()) {
                ret.add(new AvatarRace(dbConnector.getLong(rs, "globalId"),
                        dbConnector.getString(rs, "displayName"),
                        dbConnector.getString(rs, "description"),
                        dbConnector.getString(rs, "descriptionModifier"),
                        dbConnector.getInt(rs, "HPModifier"),
                        dbConnector.getBoolean(rs, "enabled")));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return ret;
    }

    /**
     * @param dungeonName
     * @param avatarRaceGlobalID
     * @return AvatarRace
     * @throws ElementNotFoundException
     * @Author Moritz Klaiber
     * returns one specific Avatar Race
     */
    @Override
    public AvatarRace getAvatarRace(String dungeonName, long avatarRaceGlobalID) throws ElementNotFoundException {
        List<Object> parameterList = new ArrayList<Object>();
        parameterList.add(dungeonName);
        parameterList.add(avatarRaceGlobalID);
        ResultSet rs = dbConnector.getSQLResults("SELECT * FROM races WHERE dungeonName = ? AND globalId = ?", parameterList);
        try {
            if (rs.next()) {
                return new AvatarRace(dbConnector.getLong(rs, "globalId"),
                        dbConnector.getString(rs, "displayName"),
                        dbConnector.getString(rs, "description"),
                        dbConnector.getString(rs, "descriptionModifier"),
                        dbConnector.getInt(rs, "HPModifier"),
                        dbConnector.getBoolean(rs, "enabled"));
            } else {
                return new AvatarRace();
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            throw new ElementNotFoundException("AvatarRace not found", dungeonName + " and " + avatarRaceGlobalID);
        }
    }

    /**
     * @param dungeonName
     * @return List<ObjectTemplate>
     * @Author Moritz Klaiber
     * Returns all Object Templates for the mentioned dungeon
     */
    @Override
    public List<ObjectTemplate> getObjectTemplates(String dungeonName) throws ElementNotFoundException {
        List<Object> parameterList = new ArrayList<Object>();
        List<CustomAction> customActionList;
        parameterList.add(dungeonName);
        List<ObjectTemplate> ret = new ArrayList<ObjectTemplate>();
        ResultSet rs = dbConnector.getSQLResults("SELECT * FROM objectTemplates WHERE dungeonName = ?", parameterList);
        try {
            while (rs.next()) {
                parameterList.clear();
                parameterList.add(dbConnector.getLong(rs, "globalId"));
                ResultSet rsCusAct = dbConnector.getSQLResults("SELECT * FROM customActions ca, hasCustomAction hca WHERE " +
                        "ca.globalId = hca.customActionId AND hca.templateGlobalId = ?", parameterList);
                customActionList = new ArrayList<>();
                while (rsCusAct.next()) {
                    customActionList.add(new CustomAction(dbConnector.getLong(rsCusAct, "globalId"), dbConnector.getInt(rsCusAct, "priority"), dbConnector.getString(rsCusAct, "actionName"),
                            dbConnector.getString(rsCusAct, "command"), dbConnector.getString(rsCusAct, "response")));
                }
                System.out.println("All Templates: " + dbConnector.getLong(rs, "globalId") + " " + dbConnector.getString(rs, "displayName"));
                ret.add(new ObjectTemplate(dbConnector.getLong(rs, "globalId"),
                        dbConnector.getString(rs, "displayName"),
                        dbConnector.getString(rs, "fullDescription"),
                        dbConnector.getBoolean(rs, "pickupable"),
                        customActionList));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return ret;
    }

    /**
     * @param dungeonName
     * @param objectTemplateGlobalID
     * @return ObjectTemplate
     * @throws ElementNotFoundException
     * @Author Moritz Klaiber
     * Returns a specific Object Template
     */
    @Override
    public ObjectTemplate getObjectTemplate(String dungeonName, long objectTemplateGlobalID) throws ElementNotFoundException {
        List<Object> parameterList = new ArrayList<Object>();
        List<CustomAction> customActionList;
        parameterList.add(dungeonName);
        parameterList.add(objectTemplateGlobalID);
        ResultSet rs = dbConnector.getSQLResults("SELECT * FROM objectTemplates WHERE dungeonName = ? AND globalId = ?", parameterList);
        try {
            if (rs.next()) {
                parameterList.clear();
                parameterList.add(objectTemplateGlobalID);
                ResultSet rsCusAct = dbConnector.getSQLResults("SELECT * FROM customActions ca, hasCustomAction hca WHERE " +
                        "ca.globalId = hca.customActionId AND hca.templateGlobalId = ?", parameterList);
                customActionList = new ArrayList<>();
                while (rsCusAct.next()) {
                    customActionList.add(new CustomAction(dbConnector.getLong(rsCusAct, "globalId"), dbConnector.getInt(rsCusAct, "priority"), dbConnector.getString(rsCusAct, "actionName"),
                            dbConnector.getString(rsCusAct, "command"), dbConnector.getString(rsCusAct, "response")));
                }
                System.out.println(dbConnector.getLong(rs, "globalId") + " " + dbConnector.getString(rs, "displayName"));
                return new ObjectTemplate(dbConnector.getLong(rs, "globalId"),
                        dbConnector.getString(rs, "displayName"),
                        dbConnector.getString(rs, "fullDescription"),
                        dbConnector.getBoolean(rs, "pickupable"),
                        customActionList);
            } else {
                return new ObjectTemplate();
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            throw new ElementNotFoundException("ObjectTemplate not found", dungeonName + " and " + objectTemplateGlobalID);
        }
    }

    /**
     * returns all Custom actions, which are bound to the dungeon
     *
     * @param dungeonName
     * @return
     * @throws ElementNotFoundException
     * @author Moritz Klaiber
     */
    @Override
    public List<CustomAction> getDungeonwideCustomActions(String dungeonName) throws ElementNotFoundException {
        List<Object> parameterList = new ArrayList<Object>();
        List<CustomAction> customActionList = new ArrayList<>();
        parameterList.add(dungeonName);
        ResultSet rs = dbConnector.getSQLResults("SELECT * FROM customActions ca, hasCustomAction hca WHERE ca.globalId = hca.customActionId AND hca.dungeonName = ?", parameterList);
        try {
            while (rs.next()) {
                customActionList.add(new CustomAction(dbConnector.getLong(rs, "globalId"), dbConnector.getInt(rs, "priority"), dbConnector.getString(rs, "actionName"),
                        dbConnector.getString(rs, "command"), dbConnector.getString(rs, "response")));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            throw new ElementNotFoundException("ObjectTemplate not found", dungeonName, throwables);
        }

        return customActionList;
    }

    /**
     * returns all Custom actions
     *
     * @param dungeonName
     * @return List<CustomAction>
     * @throws ElementNotFoundException
     * @author Moritz Klaiber
     */
    @Override
    public List<CustomAction> getAllCustomActions(String dungeonName) throws ElementNotFoundException {
        List<Object> parameterList = new ArrayList<Object>();
        List<CustomAction> customActionList = new ArrayList<>();
        parameterList.add(dungeonName);
        ResultSet rs = dbConnector.getSQLResults("SELECT * FROM customActions ca WHERE dungeonName = ?", parameterList);
        try {
            while (rs.next()) {
                customActionList.add(new CustomAction(dbConnector.getLong(rs, "globalId"), dbConnector.getInt(rs, "priority"), dbConnector.getString(rs, "actionName"),
                        dbConnector.getString(rs, "command"), dbConnector.getString(rs, "response")));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            throw new ElementNotFoundException("ObjectTemplate not found", dungeonName, throwables);
        }

        return customActionList;
    }

    /**
     * @return List<String>
     * @Author Moritz Klaiber
     * returns all dungeon names
     */
    @Override
    public List<String> getAllDungeonNames() throws ElementNotFoundException {
        List<Object> parameterList = new ArrayList<Object>();
        parameterList.add(true);
        List<String> ret = new ArrayList<String>();
        ResultSet rs = dbConnector.getSQLResults("SELECT uniqueName FROM dungeon WHERE isJoinable = ?", parameterList);
        try {
            while (rs.next()) {
                ret.add(dbConnector.getString(rs, "uniqueName"));
            }
        } catch (SQLException e) {
            throw new ElementNotFoundException("ObjectTemplate not found", " dungeons", e);
        }
        return ret;
    }

    /**
     * @return List<APIDungeon>
     * @throws ElementNotFoundException
     * @Author Moritz Klaiber
     * Returns all APIDungeons. This Datatype offers all fields, that are required in the lobby.
     */
    @Override
    public List<APIDungeon> getAllAPIDungeons() throws ElementNotFoundException {
        List<Object> parameterList = new ArrayList<Object>();
        List<APIDungeon> ret = new ArrayList<APIDungeon>();
        ResultSet rs = dbConnector.getSQLResults("SELECT * FROM dungeon", parameterList);
        try {
            while (rs.next()) {
                parameterList.clear();
                parameterList.add(dbConnector.getString(rs, "uniqueName"));
                ResultSet rsDMs = dbConnector.getSQLResults("SELECT * FROM dmList d, user u WHERE d.dungeonName = ? AND d.userName = u.name", parameterList);
                ResultSet rsWhiteList = dbConnector.getSQLResults("SELECT * FROM whiteList w, user u WHERE w.dungeonName = ? AND w.userName = u.name", parameterList);
                ResultSet rsBlackList = dbConnector.getSQLResults("SELECT * FROM blackList b, user u WHERE b.dungeonName = ? AND b.userName = u.name", parameterList);
                ResultSet rsJoinRequestList = dbConnector.getSQLResults("SELECT * FROM joinRequests j, user u WHERE j.dungeonName = ? AND j.userName = u.name", parameterList);
                ResultSet rsActionIds = dbConnector.getSQLResults("SELECT * FROM customActions ca, hasCustomAction hca WHERE ca.globalId = hca.customActionId AND hca.dungeonName = ?", parameterList);

                List<String> whiteListUsernames = new ArrayList<String>();
                List<String> blackListUsernames = new ArrayList<String>();
                List<String> joinRequestUsernames = new ArrayList<String>();
                List<String> allDmUserNames = new ArrayList<String>();
                List<Long> actionIds = new ArrayList<Long>();

                while (rsDMs.next()) {
                    allDmUserNames.add(dbConnector.getString(rsDMs, "name"));
                }
                while (rsWhiteList.next()) {
                    whiteListUsernames.add(dbConnector.getString(rsWhiteList, "userName"));
                }
                while (rsBlackList.next()) {
                    blackListUsernames.add(dbConnector.getString(rsBlackList, "userName"));
                }
                while (rsJoinRequestList.next()) {
                    joinRequestUsernames.add(dbConnector.getString(rsJoinRequestList, "userName"));
                }
                while (rsActionIds.next()) {
                    actionIds.add(dbConnector.getLong(rsActionIds, "globalId"));
                }


                ret.add(new APIDungeon(dbConnector.getString(rs, "uniqueName"),
                        dbConnector.getString(rs, "dungeonDescription"),
                        dbConnector.getString(rs, "lobbyDescription"),
                        dbConnector.getLong(rs, "startRoomGlobalId"),
                        dbConnector.getString(rs, "unknownActionResponse"),
                        dbConnector.getString(rs, "dungeonMasterUserName"),
                        dbConnector.getBoolean(rs, "isJoinable"),
                        dbConnector.getBoolean(rs, "useWhitelisteInsteadOfBlacklist"),
                        whiteListUsernames,
                        blackListUsernames,
                        joinRequestUsernames,
                        allDmUserNames,
                        actionIds));


            }
        } catch (SQLException throwables) {
            throw new ElementNotFoundException("No dungeon Found", "dungeons");
        }
        return ret;
    }

    /**
     * @param dungeonName
     * @return APIDungeon
     * @throws ElementNotFoundException
     * @Author Moritz Klaiber
     * Returns one specific APIDungeons. This Datatype offers all fields, that are required in the lobby.
     */
    @Override
    public APIDungeon getAPIDungeon(String dungeonName) throws ElementNotFoundException {
        try {
            List<Object> parameterList = new ArrayList<Object>();
            parameterList.clear();
            parameterList.add(dungeonName);
            ResultSet rs = dbConnector.getSQLResults("SELECT * FROM dungeon WHERE uniqueName = ?", parameterList);
            ResultSet rsDMs = dbConnector.getSQLResults("SELECT * FROM dmList d, user u WHERE d.dungeonName = ? AND d.userName = u.name", parameterList);
            ResultSet rsWhiteList = dbConnector.getSQLResults("SELECT * FROM whiteList w, user u WHERE w.dungeonName = ? AND w.userName = u.name", parameterList);
            ResultSet rsBlackList = dbConnector.getSQLResults("SELECT * FROM blackList b, user u WHERE b.dungeonName = ? AND b.userName = u.name", parameterList);
            ResultSet rsJoinRequestList = dbConnector.getSQLResults("SELECT * FROM joinRequests j, user u WHERE j.dungeonName = ? AND j.userName = u.name", parameterList);
            ResultSet rsActionIds = dbConnector.getSQLResults("SELECT * FROM customActions ca, hasCustomAction hca WHERE ca.globalId = hca.customActionId AND hca.dungeonName = ?", parameterList);

            List<String> whiteListUsernames = new ArrayList<String>();
            List<String> blackListUsernames = new ArrayList<String>();
            List<String> joinRequestUsernames = new ArrayList<String>();
            List<String> allDmUserNames = new ArrayList<String>();
            List<Long> actionIds = new ArrayList<Long>();

            while (rsDMs.next()) {
                allDmUserNames.add(dbConnector.getString(rsDMs, "name"));
            }
            while (rsWhiteList.next()) {
                whiteListUsernames.add(dbConnector.getString(rsWhiteList, "userName"));
            }
            while (rsBlackList.next()) {
                blackListUsernames.add(dbConnector.getString(rsBlackList, "userName"));
            }
            while (rsJoinRequestList.next()) {
                joinRequestUsernames.add(dbConnector.getString(rsJoinRequestList, "userName"));
            }
            while (rsActionIds.next()) {
                actionIds.add(dbConnector.getLong(rsActionIds, "globalId"));
            }

            if (rs.next()) {
                return new APIDungeon(dbConnector.getString(rs, "uniqueName"),
                        dbConnector.getString(rs, "dungeonDescription"),
                        dbConnector.getString(rs, "lobbyDescription"),
                        dbConnector.getLong(rs, "startRoomGlobalId"),
                        dbConnector.getString(rs, "unknownActionResponse"),
                        dbConnector.getString(rs, "dungeonMasterUserName"),
                        dbConnector.getBoolean(rs, "isJoinable"),
                        dbConnector.getBoolean(rs, "useWhitelisteInsteadOfBlacklist"),
                        whiteListUsernames,
                        blackListUsernames,
                        joinRequestUsernames,
                        allDmUserNames,
                        actionIds);
            } else {
                return new APIDungeon();
            }
        } catch (SQLException sqlException) {
            ElementNotFoundException elementNotFoundException = new ElementNotFoundException("Dungeon name not found ", dungeonName);
            elementNotFoundException.setStackTrace(sqlException.getStackTrace());
            return new APIDungeon();
        }
    }
}
