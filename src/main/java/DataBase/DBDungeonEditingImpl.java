package DataBase;

import DatabaseInterfaces.DBConnector;
import DatabaseInterfaces.DBDungeonEditing;
import Exceptions.ElementNotFoundException;
import Model.Base.*;
import Model.CommunicationModel.APIObjectTemplate;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@ApplicationScoped
/**
 * This implementation provides a few different methods to edit a existing Dungeon.
 * It only performs INSERT and UPDATE SQL Commands
 * @Author Moritz Klaiber
 */
public class DBDungeonEditingImpl implements DBDungeonEditing {

    private DBConnector dbConnector;

    @Inject
    public void setDbConnector(DBConnectorImpl dbConnector) {
        this.dbConnector = dbConnector;
    }

    public void setDbConnector(DBConnector dbConnector) {
        this.dbConnector = dbConnector;
    }

    /**
     * @param dungeonName
     * @param avatarClass
     * @return
     * @throws ElementNotFoundException
     * @Author Moritz Klaiber
     * This method adds a new Avatar Class
     */
    @Override
    public long addNewAvatarClass(String dungeonName, AvatarClass avatarClass) throws ElementNotFoundException {
        try {
            List<Object> parameterList = new ArrayList<Object>();
            parameterList.add(null);
            parameterList.add(avatarClass.getDisplayName());
            parameterList.add(avatarClass.getDescription());
            parameterList.add(avatarClass.getDescriptionModifier());
            parameterList.add(avatarClass.getHpModifier());
            parameterList.add(dungeonName);
            parameterList.add(avatarClass.isEnabled());

            return dbConnector.insertSQL("INSERT INTO classes VALUES(?, ?, ?, ?, ?, ?, ?)", parameterList);
        } catch (SQLException e) {
            ElementNotFoundException elementNotFoundException = new ElementNotFoundException("Dungeon name not found", dungeonName, e);
            throw elementNotFoundException;
        }
    }

    /**
     * @param dungeonName
     * @param avatarClass
     * @return
     * @throws ElementNotFoundException
     * @Author Moritz Klaiber
     * This method updates a existing Avatar Class
     */
    @Override
    public void updateExistingAvatarClass(String dungeonName, AvatarClass avatarClass) throws ElementNotFoundException {
        try {
            List<Object> parameterList = new ArrayList<Object>();
            parameterList.add(avatarClass.getDisplayName());
            parameterList.add(avatarClass.getDescription());
            parameterList.add(avatarClass.getDescriptionModifier());
            parameterList.add(avatarClass.getHpModifier());
            parameterList.add(dungeonName);
            parameterList.add(avatarClass.isEnabled());
            parameterList.add(avatarClass.getGlobalID());

            dbConnector.executeSQL("UPDATE classes SET displayName = ?, description = ?, descriptionModifier = ?, HPModifier = ?, dungeonName = ?, enabled = ? WHERE globalId = ?", parameterList);
        } catch (SQLException e) {
            ElementNotFoundException elementNotFoundException = new ElementNotFoundException("Dungeon name not found", dungeonName, e);
            //elementNotFoundException.setStackTrace(e.getStackTrace());
            throw elementNotFoundException;
        }
    }

    /**
     * @param dungeonName
     * @param avatarRace
     * @return
     * @throws ElementNotFoundException
     * @Author Moritz Klaiber
     * This method adds a new Avatar Race
     */
    @Override
    public long addNewAvatarRace(String dungeonName, AvatarRace avatarRace) throws ElementNotFoundException {
        try {
            List<Object> parameterList = new ArrayList<Object>();
            parameterList.add(null);
            parameterList.add(avatarRace.getDisplayName());
            parameterList.add(avatarRace.getDescription());
            parameterList.add(avatarRace.getDescriptionModifier());
            parameterList.add(avatarRace.getHpModifier());
            parameterList.add(dungeonName);
            parameterList.add(avatarRace.isEnabled());

            return dbConnector.insertSQL("INSERT INTO races VALUES(?, ?, ?, ?, ?, ?, ?)", parameterList);
        } catch (SQLException e) {
            ElementNotFoundException elementNotFoundException = new ElementNotFoundException("Dungeon name not found", dungeonName, e);
            throw elementNotFoundException;
        }
    }

    /**
     * @param dungeonName
     * @param avatarRace
     * @return
     * @throws ElementNotFoundException
     * @Author Moritz Klaiber
     * This method updates a existing Object Template
     */
    @Override
    public void updateExistingAvatarRace(String dungeonName, AvatarRace avatarRace) throws ElementNotFoundException {
        try {
            List<Object> parameterList = new ArrayList<Object>();
            parameterList.add(avatarRace.getDisplayName());
            parameterList.add(avatarRace.getDescription());
            parameterList.add(avatarRace.getDescriptionModifier());
            parameterList.add(avatarRace.getHpModifier());
            parameterList.add(dungeonName);
            parameterList.add(avatarRace.isEnabled());
            parameterList.add(avatarRace.getGlobalID());

            dbConnector.executeSQL("UPDATE races SET displayName = ?, description = ?, descriptionModifier = ?, HPModifier = ?, dungeonName = ?, enabled = ? WHERE globalId = ?", parameterList);
        } catch (SQLException e) {
            ElementNotFoundException elementNotFoundException = new ElementNotFoundException("Dungeon name not found", dungeonName, e);
            throw elementNotFoundException;
        }
    }

    /**
     * @param dungeonName
     * @param objectTemplate
     * @return
     * @throws ElementNotFoundException
     * @Author Moritz Klaiber
     * This method adds a new Object Template
     */
    @Override
    public long addNewObjectTemplate(String dungeonName, ObjectTemplate objectTemplate) throws ElementNotFoundException {
        try {
            List<Object> parameterList = new ArrayList<Object>();
            parameterList.add(null);
            parameterList.add(objectTemplate.getFullDescription());
            parameterList.add(objectTemplate.getDisplayName());
            parameterList.add(objectTemplate.isPickupable());
            parameterList.add(dungeonName);

            return dbConnector.insertSQL("INSERT INTO objectTemplates VALUES(?, ?, ?, ?, ?)", parameterList);
        } catch (SQLException e) {
            ElementNotFoundException elementNotFoundException = new ElementNotFoundException("Dungeon name not found", dungeonName, e);
            throw elementNotFoundException;
        }
    }

    /**
     * @param dungeonName
     * @param objectTemplate
     * @return
     * @throws ElementNotFoundException
     * @Author Moritz Klaiber
     * This method updates a existing Avatar Race
     */
    @Override
    public void updateExistingObjectTemplate(String dungeonName, APIObjectTemplate objectTemplate) throws ElementNotFoundException {
        try {
            List<Object> parameterList = new ArrayList<Object>();
            parameterList.add(objectTemplate.fullDescription);
            parameterList.add(objectTemplate.name);
            parameterList.add(objectTemplate.isPickupable);
            parameterList.add(dungeonName);
            parameterList.add(objectTemplate.globalID);

            dbConnector.executeSQL("UPDATE objectTemplates SET fullDescription = ?, displayName = ?, pickupable = ? WHERE dungeonName = ? AND globalId = ?", parameterList);


            parameterList.clear();
            parameterList.add(objectTemplate.globalID);
            dbConnector.executeSQL("DELETE FROM hasCustomAction WHERE templateGlobalId = ?", parameterList);

            objectTemplate.actionIDs.forEach(aLong -> {

                parameterList.clear();
                parameterList.add(null);
                parameterList.add(null);
                parameterList.add(null);
                parameterList.add(objectTemplate.globalID);
                parameterList.add(aLong);

                try {
                    dbConnector.executeSQL("INSERT INTO hasCustomAction VALUES(?, ?, ?, ?, ?)", parameterList);
                } catch (SQLException sqlException) {
                    sqlException.printStackTrace();
                }
            });

        } catch (SQLException e) {
            ElementNotFoundException elementNotFoundException = new ElementNotFoundException("Dungeon name not found", dungeonName, e);
            throw elementNotFoundException;
        }
    }

    /**
     * @param dungeonName
     * @param customAction
     * @return
     * @throws ElementNotFoundException
     * @Author Moritz Klaiber
     * This method adds a new Custom Action
     */
    @Override
    public long addNewCustomAction(String dungeonName, CustomAction customAction) throws ElementNotFoundException {
        try {
            List<Object> parameterList = new ArrayList<Object>();
            parameterList.add(null);
            parameterList.add(customAction.getPriority());
            parameterList.add(customAction.getActionName());
            parameterList.add(customAction.getCommand());
            parameterList.add(customAction.getResponse());
            parameterList.add(dungeonName);

            return dbConnector.insertSQL("INSERT INTO customActions VALUES(?, ?, ?, ?, ?, ?)", parameterList);
        } catch (SQLException e) {
            ElementNotFoundException elementNotFoundException = new ElementNotFoundException("Dungeon name not found", dungeonName, e);
            throw elementNotFoundException;
        }
    }

    /**
     * @param dungeonName
     * @param customAction
     * @return
     * @throws ElementNotFoundException
     * @Author Moritz Klaiber
     * This method updates a existing Custom Action
     */
    @Override
    public void updateExistingCustomAction(String dungeonName, CustomAction customAction) throws ElementNotFoundException {
        try {
            List<Object> parameterList = new ArrayList<Object>();
            parameterList.add(customAction.getPriority());
            parameterList.add(customAction.getActionName());
            parameterList.add(customAction.getCommand());
            parameterList.add(customAction.getResponse());
            parameterList.add(dungeonName);
            parameterList.add(customAction.getGlobalActionId());

            dbConnector.executeSQL("UPDATE customActions SET priority = ?, actionName = ?,  command = ?, response = ? WHERE dungeonName = ? AND globalId = ?", parameterList);
        } catch (SQLException e) {
            ElementNotFoundException elementNotFoundException = new ElementNotFoundException("Dungeon name not found", dungeonName, e);
            throw elementNotFoundException;
        }
    }

    /**
     * Deletes the given dungeon permanently.
     * @author Moritz Klaiber
     * @param dungeonName
     * @throws ElementNotFoundException
     */
    @Override
    public void deleteDungeon(String dungeonName) throws ElementNotFoundException {
        try{
            List<Object> parameterList = new ArrayList<Object>();
            parameterList.add(dungeonName);
            dbConnector.executeSQL("DELETE FROM dungeon WHERE uniqueName = ?", parameterList);
        }catch (SQLException sqlException){
            ElementNotFoundException elementNotFoundException = new ElementNotFoundException("Dungeon name not found", dungeonName, sqlException);
            throw elementNotFoundException;
        }
    }

}
