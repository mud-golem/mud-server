package DataBase;

import DatabaseInterfaces.DBConnector;
import Exceptions.ElementNotFoundException;
import Exceptions.EmailTakenException;
import Exceptions.UserNameTakenException;
import Model.Base.User;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@ApplicationScoped
/**
 * @author Moritz Klaiber
 * This implementation is used to create and manage our users from the database.
 */
public class DBUserManagerImpl implements DatabaseInterfaces.DBUserManager {


    private DBConnector dbConnector;

    @Inject
    public void setDbConnector(DBConnectorImpl dbConnector) {

        this.dbConnector = dbConnector;
    }

    public void setDbConnector(DBConnector dbConnector) {

        this.dbConnector = dbConnector;
    }

    /**
     * Checks wether the given credentials are valid with the existing data in the database.
     *
     * @param userName
     * @param pwHashed
     * @return credentialsAreCorrect
     * @throws ElementNotFoundException
     * @author Moritz Klaiber
     */
    @Override
    public boolean validateUserCredentials(String userName, String pwHashed) throws ElementNotFoundException {
        try {
            List<Object> parameterList = new ArrayList<>();
            parameterList.add(userName);
            parameterList.add(pwHashed);
            ResultSet credentialsRS = dbConnector.getSQLResults("SELECT * FROM user WHERE name = ? AND password = ?", parameterList);

            return credentialsRS.next();

        } catch (SQLException e) {
            throw new ElementNotFoundException("No user with this Credentials was found!", userName + " and " + pwHashed);
        }
    }

    /**
     * Creates a new user with the given credentials in the database.
     *
     * @param userName
     * @param pwHashed
     * @param mailAddress
     * @return registrationSuccess
     * @throws UserNameTakenException
     * @throws EmailTakenException
     * @author Moritz Klaiber
     */
    @Override
    public boolean registerUser(String userName, String pwHashed, String mailAddress) throws UserNameTakenException, EmailTakenException {
        try {
            List<Object> parameterList = new ArrayList<>();
            parameterList.add(userName);

            ResultSet nameCredentialsRS = dbConnector.getSQLResults("SELECT name FROM user WHERE name = ?", parameterList);

            parameterList.clear();
            parameterList.add(mailAddress);

            ResultSet mailCredentialsRS = dbConnector.getSQLResults("SELECT name FROM user WHERE email = ?", parameterList);

            if (nameCredentialsRS.next()) {
                throw new UserNameTakenException();
            } else if (mailCredentialsRS.next()) {
                throw new EmailTakenException();
            } else {
                parameterList.clear();
                parameterList.add(userName);
                parameterList.add(pwHashed);
                parameterList.add(mailAddress);

                dbConnector.executeSQL("INSERT INTO user VALUES(?, ?, ?)", parameterList);
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Returns the user-object to the given user-name.
     *
     * @param userName
     * @return User
     * @throws ElementNotFoundException
     * @author Moritz Klaiber
     */
    @Override
    public User resolveUser(String userName) throws ElementNotFoundException {
        try {
            List<Object> parameterList = new ArrayList<>();
            parameterList.add(userName);
            ResultSet credentialsRS = dbConnector.getSQLResults("SELECT * FROM user WHERE name = ?", parameterList);

            if (credentialsRS.next()) {
                return new User(dbConnector.getString(credentialsRS, "name"), dbConnector.getString(credentialsRS, "password"),
                        dbConnector.getString(credentialsRS, "email"));
            } else {
                return null;
            }

        } catch (SQLException e) {
            e.printStackTrace();
            throw new ElementNotFoundException("User not available", userName);
        }
    }

    /**
     * Changes the password in the database.
     *
     * @param username
     * @param password
     * @throws ElementNotFoundException
     * @author Moritz Klaiber
     */
    @Override
    public void changeUserPassword(String username, String password) throws ElementNotFoundException {
        try {
            List<Object> parameterList = new ArrayList<>();
            parameterList.add(username);
            ResultSet credentialsRS = dbConnector.getSQLResults("SELECT * FROM user WHERE name = ?", parameterList);

            if (credentialsRS.next()) {
                parameterList.clear();
                parameterList.add(password);
                parameterList.add(username);
                dbConnector.executeSQL("UPDATE user SET password = ? WHERE name = ?", parameterList);
            } else {
                throw new ElementNotFoundException("This user does not exist!", username);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns the email-address from the user.
     *
     * @param username
     * @return emailAddress
     * @throws ElementNotFoundException
     * @author Moritz Klaiber
     */
    @Override
    public String getEmail(String username) throws ElementNotFoundException {
        try {
            List<Object> parameterList = new ArrayList<>();
            parameterList.add(username);
            ResultSet credentialsRS = dbConnector.getSQLResults("SELECT email FROM user WHERE name = ?", parameterList);

            if (credentialsRS.next()) {
                return dbConnector.getString(credentialsRS, "email");
            } else {
                return "n/a";
            }

        } catch (SQLException e) {
            e.printStackTrace();
            throw new ElementNotFoundException("E-Mail not found", username);
        }
    }
}
