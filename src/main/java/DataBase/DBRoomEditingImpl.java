package DataBase;

import DatabaseInterfaces.DBConnector;
import DatabaseInterfaces.DBRoomEditing;
import DatabaseInterfaces.RoomDirection;
import Exceptions.ElementNotFoundException;
import Model.Base.CustomAction;
import Model.Base.InventoryEntry;
import Model.Base.ObjectTemplate;
import Model.CommunicationModel.APIDungeonRoom;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@ApplicationScoped
/**
 * This class is used to editing a room in a dungeon.
 * @author Moritz Klaiber
 */
public class DBRoomEditingImpl implements DBRoomEditing {

    private DBConnector dbConnector;

    @Inject
    public void setDbConnector(DBConnectorImpl dbConnector) {

        this.dbConnector = dbConnector;
    }

    public void setDbConnector(DBConnector dbConnector) {

        this.dbConnector = dbConnector;
    }

    /**
     * This method can be used to connect one Room to another.
     *
     * @param direction
     * @param dungeonRoomId
     * @param newConnectionRoomId
     * @throws ElementNotFoundException
     * @author Moritz Klaiber
     */
    @Override
    public void setRoomsConnection(RoomDirection direction, long dungeonRoomId, long newConnectionRoomId) throws ElementNotFoundException {
        List<Object> parameterList = new ArrayList<Object>();
        parameterList.add((newConnectionRoomId == -1 ? null : newConnectionRoomId));
        parameterList.add(dungeonRoomId);

        try {
            switch (direction) {
                case NORTH:
                    dbConnector.executeSQL("UPDATE dungeonRooms SET roomNorthGlobalId = ? WHERE globalId = ?", parameterList);
                    break;
                case SOUTH:
                    dbConnector.executeSQL("UPDATE dungeonRooms SET roomSouthGlobalId = ? WHERE globalId = ?", parameterList);
                    break;
                case WEST:
                    dbConnector.executeSQL("UPDATE dungeonRooms SET roomWestGlobalId = ? WHERE globalId = ?", parameterList);
                    break;
                case EAST:
                    dbConnector.executeSQL("UPDATE dungeonRooms SET roomEastGlobalId = ? WHERE globalId = ?", parameterList);
                    break;
            }
        } catch (SQLException e) {
            ElementNotFoundException elementNotFoundException = new ElementNotFoundException("Room not found", direction.name() + dungeonRoomId, e);
            throw elementNotFoundException;
        }
    }

    /**
     * This method overrides an existing room in the Database with the incoming values.
     *
     * @param apiRoom
     * @throws ElementNotFoundException
     * @author Moritz Klaiber
     */
    @Override
    public void setAPIRoom(APIDungeonRoom apiRoom) throws ElementNotFoundException {
        try {
            List<Object> parameterList = new ArrayList<Object>();
            parameterList.add(apiRoom.displayName);
            parameterList.add(apiRoom.description);
            parameterList.add(apiRoom.globalID);

            dbConnector.executeSQL("UPDATE dungeonRooms SET displayName = ?, description = ? " +
                    " WHERE globalId = ?", parameterList);

            apiRoom.placedObjects.forEach(apiInventoryEntry -> {
                try {
                    parameterList.clear();
                    parameterList.add(apiInventoryEntry.getObjectTemplateGlobalID());
                    parameterList.add(apiRoom.globalID);
                    ResultSet rs = dbConnector.getSQLResults("SELECT * FROM inventoryEntries WHERE globalTemplateId = ? AND globalDungeonRoomId = ?", parameterList);

                    if (!rs.next() && apiInventoryEntry.getCount() > 0) {
                        parameterList.clear();
                        parameterList.add(null);
                        parameterList.add(apiInventoryEntry.getCount());
                        parameterList.add(apiInventoryEntry.getObjectTemplateGlobalID());
                        parameterList.add(apiRoom.globalID);
                        parameterList.add(null);
                        dbConnector.executeSQL("INSERT INTO inventoryEntries VALUES(?, ?, ?, ?, ?)", parameterList);
                    } else {
                        parameterList.clear();
                        parameterList.add(apiInventoryEntry.getCount());
                        parameterList.add(apiInventoryEntry.getObjectTemplateGlobalID());
                        parameterList.add(apiRoom.globalID);
                        dbConnector.executeSQL("UPDATE inventoryEntries SET count = ? WHERE globalTemplateId = ? AND globalDungeonRoomId = ?", parameterList);
                    }
                } catch (SQLException sqlException) {
                    try {
                        throw new ElementNotFoundException("Room not found", String.valueOf(apiRoom.globalID), sqlException);
                    } catch (ElementNotFoundException e) {
                        e.printStackTrace();
                    }
                }
            });

            parameterList.clear();
            parameterList.add(apiRoom.globalID);
            dbConnector.executeSQL("DELETE FROM hasCustomAction WHERE dungeonRoomGlobalId = ?", parameterList);

            apiRoom.actionIDs.forEach(aLong -> {

                parameterList.clear();
                parameterList.add(null);
                parameterList.add(null);
                parameterList.add(apiRoom.globalID);
                parameterList.add(null);
                parameterList.add(aLong);

                try {
                    dbConnector.executeSQL("INSERT INTO hasCustomAction VALUES(?, ?, ?, ?, ?)", parameterList);
                } catch (SQLException sqlException) {
                    sqlException.printStackTrace();
                }

            });

        } catch (SQLException e) {
            ElementNotFoundException elementNotFoundException = new ElementNotFoundException("Room not found", String.valueOf(apiRoom.globalID), e);
            throw elementNotFoundException;
        }
    }

    /**
     * Adds one Inventory entry to the inventory of an room.
     *
     * @param dungeonName
     * @param roomGlobalId
     * @param inventoryEntry
     * @return
     * @throws ElementNotFoundException
     * @author Moritz Klaiber
     */
    @Override
    public boolean addInventoryEntry(String dungeonName, long roomGlobalId, InventoryEntry inventoryEntry) throws ElementNotFoundException {
        try {
            List<Object> parameterList = new ArrayList<Object>();
            parameterList.add(roomGlobalId);

            ResultSet rsInventory = dbConnector.getSQLResults("SELECT * FROM inventoryEntries ie, objectTemplates ot WHERE ie.globalDungeonRoomId = ? AND ie.globalTemplateId = ot.globalId", parameterList);
            List<InventoryEntry> inventoryEntryList = new ArrayList<InventoryEntry>();
            List<CustomAction> customActionList = new ArrayList<>();
            if (rsInventory.next()) {
                inventoryEntryList.add(new InventoryEntry(new ObjectTemplate(dbConnector.getLong(rsInventory, "globalTemplateId"),
                        dbConnector.getString(rsInventory, "displayName"),
                        dbConnector.getString(rsInventory, "fullDescription"),
                        dbConnector.getBoolean(rsInventory, "pickupable"),
                        customActionList),
                        dbConnector.getInt(rsInventory, "count")));
            }
            if (!inventoryEntryList.stream().anyMatch(ie -> inventoryEntry.equals(ie))) {
                parameterList.clear();
                parameterList.add(inventoryEntry.getCount());
                parameterList.add(inventoryEntry.getTemplateGlobalID());
                parameterList.add(roomGlobalId);
                dbConnector.executeSQL("INSERT INTO inventoryEntries (count, globalTemplateId, globalDungeonRoomId) VALUES(?, ?, ?)", parameterList);
                return false;
            } else {
                return true;
            }

        } catch (SQLException e) {
            ElementNotFoundException elementNotFoundException = new ElementNotFoundException("Dungeon room not found", String.valueOf(roomGlobalId), e);
            throw elementNotFoundException;
        }
    }

    /**
     * Updates an existing inventory entry from an room.
     *
     * @param dungeonName
     * @param roomGlobalId
     * @param inventoryEntry
     * @throws ElementNotFoundException
     * @author Moritz Klaiber
     */
    @Override
    public void updateExistingInventoryEntry(String dungeonName, long roomGlobalId, InventoryEntry inventoryEntry) throws ElementNotFoundException {
        try {
            List<Object> parameterList = new ArrayList<Object>();

            if (inventoryEntry.getCount() == 0) {
                parameterList.clear();
                parameterList.add(roomGlobalId);
                parameterList.add(inventoryEntry.getTemplateGlobalID());
                dbConnector.executeSQL("DELETE FROM inventoryEntries WHERE globalDungeonRoomId = ? AND globalTemplateId = ?", parameterList);
            } else {
                parameterList.clear();
                parameterList.add(inventoryEntry.getCount());
                parameterList.add(roomGlobalId);
                parameterList.add(inventoryEntry.getTemplateGlobalID());
                dbConnector.executeSQL("UPDATE inventoryEntries SET count = ? WHERE globalDungeonRoomId = ? AND globalTemplateId = ?", parameterList);
            }
        } catch (SQLException e) {
            ElementNotFoundException elementNotFoundException = new ElementNotFoundException("Dungeon room not found", String.valueOf(roomGlobalId), e);
            throw elementNotFoundException;
        }
    }

    /**
     * Removes the given room from the database.
     *
     * @param roomGlobalId
     * @throws ElementNotFoundException
     * @author Moritz Klaiber
     */
    public void deleteRoom(long roomGlobalId) throws ElementNotFoundException {
        List<Object> parameterList = new ArrayList<Object>();
        parameterList.add(roomGlobalId);
        try {
            dbConnector.executeSQL("DELETE FROM dungeonRooms WHERE globalId = ?", parameterList);
        } catch (SQLException sqlException) {
            ElementNotFoundException elementNotFoundException = new ElementNotFoundException("Dungeon room not found", String.valueOf(roomGlobalId), sqlException);
            throw elementNotFoundException;
        }
    }
}
