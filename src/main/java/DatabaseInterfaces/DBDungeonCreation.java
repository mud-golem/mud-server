package DatabaseInterfaces;

import Exceptions.ElementNotFoundException;
import Model.Base.Dungeon;

public interface DBDungeonCreation {
    long createDungeon(Dungeon dungeon) throws ElementNotFoundException;
    boolean deleteDungeon(String uniqueDungeonName) throws ElementNotFoundException;
}
