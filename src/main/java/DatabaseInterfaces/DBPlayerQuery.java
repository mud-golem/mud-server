package DatabaseInterfaces;

import Exceptions.ElementNotFoundException;
import Model.Base.PlayerAvatar;

import java.sql.SQLException;
import java.util.List;

public interface DBPlayerQuery {
    PlayerAvatar getPlayerAvatar(String dungeonName, String userName) throws ElementNotFoundException;
    List<PlayerAvatar> getPlayerAvatars(String dungeonName) throws ElementNotFoundException;
    long getActualRoomGlobalId(String dungeonName, String userName) throws ElementNotFoundException;
    PlayerAvatar getPlayerAvatarWithAvatarName(String dungeonName, String avatarName) throws ElementNotFoundException;
    PlayerAvatar getPlayerAvatar(String dungeonName, long globalAvatarId) throws ElementNotFoundException;
}
