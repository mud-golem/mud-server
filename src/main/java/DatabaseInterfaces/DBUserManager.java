package DatabaseInterfaces;

import Exceptions.ElementNotFoundException;
import Exceptions.EmailTakenException;
import Exceptions.UserNameTakenException;
import Model.Base.User;

public interface DBUserManager {
    boolean validateUserCredentials(String UserName, String password) throws ElementNotFoundException;
    boolean registerUser(String userName, String PWHashed, String eMail) throws UserNameTakenException, EmailTakenException;
    User resolveUser(String userName) throws ElementNotFoundException;
    void changeUserPassword(String username, String password) throws ElementNotFoundException;
    String getEmail(String username) throws ElementNotFoundException;
}
