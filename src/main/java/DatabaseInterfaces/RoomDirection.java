package DatabaseInterfaces;

public enum RoomDirection {
    NORTH, SOUTH, WEST, EAST
}
