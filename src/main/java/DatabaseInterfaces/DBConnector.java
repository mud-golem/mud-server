package DatabaseInterfaces;

import Exceptions.ElementNotFoundException;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public interface DBConnector {

    long insertSQL(String sequel, List<Object> preparedInjectionValues) throws SQLException;

    ResultSet getSQLResults(String sequel, List<Object> preparedInjectionValues);

    void executeSQL(String sequel, List<Object> preparedInjectionValues) throws SQLException;

    String getString(ResultSet resultSet, String columnName) throws ElementNotFoundException;

    int getInt(ResultSet resultSet, String columnName) throws ElementNotFoundException;

    long getLong(ResultSet resultSet, String columnName) throws ElementNotFoundException;

    boolean getBoolean(ResultSet resultSet, String columnName) throws ElementNotFoundException;
}
