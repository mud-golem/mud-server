package DatabaseInterfaces;

import Exceptions.ElementNotFoundException;
import Model.Base.PlayerAvatar;

public interface DBPlayerCreation {
    long createPlayerCharacter(String dungeonName, PlayerAvatar playerAvatar) throws ElementNotFoundException;
    boolean deletePlayerCharacter(String dungeonName, long playerAvatarGlobalID) throws ElementNotFoundException;
}
