package DatabaseInterfaces;

import DataBase.DBConnectorImpl;
import Exceptions.ElementNotFoundException;
import Model.Base.AvatarClass;
import Model.Base.AvatarRace;
import Model.Base.InventoryEntry;
import Model.Base.PlayerAvatar;
import Model.CommunicationModel.APIInventoryEntry;

import javax.inject.Inject;
import java.util.List;

public interface DBPlayerEditing {

    void setPlayer(String dungeonName, PlayerAvatar playerAvatar) throws ElementNotFoundException;

    public void setPlayerName(String dungeonName, String username, String newValue) throws ElementNotFoundException;

    void setPlayerDescription(String dungeonName, String username, String newValue) throws ElementNotFoundException;
    void setPlayerCurrHP(String dungeonName, String username, int newValue) throws ElementNotFoundException;
    void setPlayerMaxHP(String dungeonName, String username, int newValue) throws ElementNotFoundException;

    void setPlayerIsActive(String dungeonName, String username, boolean newValue) throws ElementNotFoundException;
    void setPlayerActualRoomGlobalID(String dungeonName, String username, long newValue) throws ElementNotFoundException;
    void setPlayerRace(String dungeonName, String username, AvatarRace newValue) throws ElementNotFoundException;
    void setPlayerClass(String dungeonName, String username, AvatarClass newValue) throws ElementNotFoundException;
    void setInventory(String dungeonName, String username, List<APIInventoryEntry> newValue) throws ElementNotFoundException;

    void setPlayerClassByID(long playerAvatarGlobalId, long classGlobalId) throws ElementNotFoundException;
    void setPlayerRaceByID(long playerAvatarGlobalId, long raceGlobalId) throws ElementNotFoundException;

    void deletePlayerAvatar(long globalAvatarId, String dungeonName) throws ElementNotFoundException;

}
