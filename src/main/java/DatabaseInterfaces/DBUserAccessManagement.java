package DatabaseInterfaces;

import Exceptions.ElementNotFoundException;
import Exceptions.EntryAlreadyExistsException;
import Exceptions.NoValidUserNameException;

import java.sql.SQLException;
import java.util.List;

public interface DBUserAccessManagement {

    List<String> getBlackList(String dungeonName) throws ElementNotFoundException;
    void setBlackList(String dungeonName, String userNames) throws NoValidUserNameException, EntryAlreadyExistsException;
    void removeBlackList(String dungeonName, String userNames) throws NoValidUserNameException, EntryAlreadyExistsException;

    List<String> getWhiteList(String dungeonName) throws ElementNotFoundException;
    void setWhiteList(String dungeonName, String userNames) throws NoValidUserNameException, EntryAlreadyExistsException;
    void removeWhiteList(String dungeonName, String userNames) throws NoValidUserNameException, EntryAlreadyExistsException;

    List<String> getJoinRequestList(String dungeonName) throws ElementNotFoundException;
    void setJoinRequest(String dungeonName, String userNames) throws NoValidUserNameException, EntryAlreadyExistsException;
    void removeJoinRequest(String dungeonName, String userNames) throws NoValidUserNameException, EntryAlreadyExistsException;

    List<String> getDMList(String dungeonName) throws ElementNotFoundException;
    void setNewDM(String dungeonName, String userNames) throws NoValidUserNameException, EntryAlreadyExistsException, SQLException;
    void removeDM(String dungeonName, String userNames) throws NoValidUserNameException, EntryAlreadyExistsException;

}
