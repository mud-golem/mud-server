package DatabaseInterfaces;

import Exceptions.ElementNotFoundException;
import Model.CommunicationModel.APIDungeon;

public interface DBDungeonManagement {
    void setDungeon(APIDungeon apiDungeon) throws IllegalArgumentException, ElementNotFoundException;
    void setActualDM(String dungeonName, String userName) throws ElementNotFoundException;
}
