package DatabaseInterfaces;

import Exceptions.ElementNotFoundException;
import Model.Base.DungeonRoom;

public interface DBRoomCreation {

    long addNewRoom(String dungeonName, DungeonRoom dr) throws ElementNotFoundException;

}
