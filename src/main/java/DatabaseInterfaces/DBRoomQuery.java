package DatabaseInterfaces;

import Exceptions.ElementNotFoundException;
import Model.Base.DungeonRoom;
import Model.CommunicationModel.APIDungeon;

import java.util.List;

public interface DBRoomQuery {

    List<DungeonRoom> getRooms(String dungeonName) throws ElementNotFoundException;

    DungeonRoom getDungeonRoom(String dungeonName, long globalId) throws ElementNotFoundException;

    List<String> getAllAvatarNamesInRoom(String dungeonName, long roomGlobalId) throws ElementNotFoundException;

}