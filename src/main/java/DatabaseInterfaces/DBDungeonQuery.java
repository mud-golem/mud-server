package DatabaseInterfaces;


import Exceptions.ElementNotFoundException;
import Model.Base.*;
import Model.CommunicationModel.APIDungeon;

import java.util.List;

public interface DBDungeonQuery {
    String getDungeonDescription(String dungeonName) throws ElementNotFoundException;
    String getLobbyDescription(String dungeonName) throws ElementNotFoundException;
    boolean getJoinable(String dungeonName) throws ElementNotFoundException;
    long getStartRoomGlobalID(String dungeonName) throws ElementNotFoundException;
    String getUnknownActionResponse(String dungeonName) throws ElementNotFoundException;
    List<String> getBlacklist(String dungeonName) throws ElementNotFoundException;
    List<String> getWhitelist(String dungeonName) throws ElementNotFoundException;
    List<String> getAllowedDms(String dungeonName) throws ElementNotFoundException;
    String getActualLoggedInDM(String dungeonName) throws ElementNotFoundException;
    List<AvatarClass> getAvatarClasses(String dungeonName) throws ElementNotFoundException;
    AvatarClass getAvatarClass(String dungeonName, long avatarClassGlobalID) throws ElementNotFoundException;
    List<AvatarRace> getAvatarRaces(String dungeonName) throws ElementNotFoundException;
    AvatarRace getAvatarRace(String dungeonName, long avatarRaceGlobalID) throws  ElementNotFoundException;
    List<Model.Base.ObjectTemplate> getObjectTemplates(String dungeonName) throws ElementNotFoundException;
    ObjectTemplate getObjectTemplate(String dungeonName, long objectTemplateGlobalID) throws  ElementNotFoundException;
    List<CustomAction> getDungeonwideCustomActions(String dungeonName) throws ElementNotFoundException;
    List<CustomAction> getAllCustomActions(String dungeonName) throws ElementNotFoundException;
    List<String> getAllDungeonNames() throws ElementNotFoundException;
    List<APIDungeon> getAllAPIDungeons() throws ElementNotFoundException;
    APIDungeon getAPIDungeon(String dungeonName) throws ElementNotFoundException;
}
