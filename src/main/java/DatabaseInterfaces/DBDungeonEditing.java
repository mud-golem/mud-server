package DatabaseInterfaces;

import Exceptions.ElementNotFoundException;
import Model.Base.*;
import Model.CommunicationModel.APIObjectTemplate;

public interface DBDungeonEditing {
    long addNewAvatarClass(String dungeonName, AvatarClass avatarClass) throws ElementNotFoundException;

    void updateExistingAvatarClass(String dungeonName, AvatarClass avatarClass) throws ElementNotFoundException;

    long addNewAvatarRace(String dungeonName, AvatarRace avatarRace) throws ElementNotFoundException;

    void updateExistingAvatarRace(String dungeonName, AvatarRace avatarRace) throws ElementNotFoundException;

    long addNewObjectTemplate(String dungeonName, ObjectTemplate objectTemplate) throws ElementNotFoundException;

    void updateExistingObjectTemplate(String dungeonName, APIObjectTemplate objectTemplate) throws ElementNotFoundException;

    long addNewCustomAction(String dungeonName, CustomAction customAction) throws ElementNotFoundException;

    void updateExistingCustomAction(String dungeonName, CustomAction customAction) throws ElementNotFoundException;

    void deleteDungeon(String dungeonName) throws ElementNotFoundException;

}
