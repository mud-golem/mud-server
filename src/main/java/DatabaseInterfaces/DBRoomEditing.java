package DatabaseInterfaces;

import Exceptions.ElementNotFoundException;
import Model.Base.DungeonRoom;
import Model.Base.InventoryEntry;
import Model.CommunicationModel.APIDungeonRoom;

import java.util.List;

public interface DBRoomEditing {

    void setRoomsConnection(RoomDirection direction, long dungeonRoomId, long newConnectionRoomId) throws ElementNotFoundException;

    void setAPIRoom(APIDungeonRoom apiRoom) throws ElementNotFoundException;
    
    boolean addInventoryEntry(String dungeonName, long roomGlobalId, InventoryEntry inventoryEntry) throws ElementNotFoundException;

    void updateExistingInventoryEntry(String dungeonName, long roomGlobalId, InventoryEntry inventoryEntry) throws ElementNotFoundException;

    void deleteRoom(long roomGlobalId) throws ElementNotFoundException;

}
