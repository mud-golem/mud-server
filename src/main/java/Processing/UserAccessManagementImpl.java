package Processing;

import DataBase.DBUserAccessManagementImpl;
import DatabaseInterfaces.DBUserAccessManagement;
import Exceptions.ElementNotFoundException;
import Exceptions.EntryAlreadyExistsException;
import Exceptions.NoValidUserNameException;
import ServerInterfaces.UserAccessManagement;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.sql.SQLException;
import java.util.List;

@ApplicationScoped
public class UserAccessManagementImpl implements UserAccessManagement {
    DBUserAccessManagement dbUserManagement;

    @Inject
    public void setDbUserManagement(DBUserAccessManagementImpl dbUserManagement) {
        this.dbUserManagement = dbUserManagement;
    }

    @Override
    public List<String> getBlackList(String dungeonName) throws ElementNotFoundException {
        return dbUserManagement.getBlackList(dungeonName);
    }

    @Override
    public void setBlackList(String dungeonName, String userNames) throws NoValidUserNameException, EntryAlreadyExistsException {
        dbUserManagement.setBlackList(dungeonName, userNames);
    }

    @Override
    public void removeBlackList(String dungeonName, String userNames) throws NoValidUserNameException, EntryAlreadyExistsException {
        dbUserManagement.removeBlackList(dungeonName, userNames);
    }

    @Override
    public List<String> getWhiteList(String dungeonName) throws ElementNotFoundException {
        return dbUserManagement.getWhiteList(dungeonName);
    }

    @Override
    public void setWhiteList(String dungeonName, String userNames) throws NoValidUserNameException, EntryAlreadyExistsException {
        dbUserManagement.setWhiteList(dungeonName, userNames);
    }

    @Override
    public void removeWhiteList(String dungeonName, String userNames) throws NoValidUserNameException, EntryAlreadyExistsException {
        dbUserManagement.removeWhiteList(dungeonName, userNames);
    }

    @Override
    public List<String> getJoinRequestList(String dungeonName) throws ElementNotFoundException {
        return dbUserManagement.getJoinRequestList(dungeonName);
    }

    @Override
    public void setJoinRequest(String dungeonName, String userNames) throws NoValidUserNameException, EntryAlreadyExistsException {
        dbUserManagement.setJoinRequest(dungeonName, userNames);
    }

    @Override
    public void acceptJoinRequest(String dungeonName, String userNames) throws NoValidUserNameException, EntryAlreadyExistsException {
        dbUserManagement.removeJoinRequest(dungeonName, userNames);
        dbUserManagement.setWhiteList(dungeonName, userNames);
    }

    @Override
    public void declineJoinRequest(String dungeonName, String userNames) throws NoValidUserNameException, EntryAlreadyExistsException {
        dbUserManagement.removeJoinRequest(dungeonName, userNames);
    }

    @Override
    public List<String> getDMList(String dungeonName) throws ElementNotFoundException {
        return dbUserManagement.getDMList(dungeonName);
    }

    @Override
    public void setNewDM(String dungeonName, String userNames) throws NoValidUserNameException, EntryAlreadyExistsException, SQLException {
        dbUserManagement.setNewDM(dungeonName, userNames);
    }

    @Override
    public void removeDM(String dungeonName, String userNames) throws NoValidUserNameException, EntryAlreadyExistsException {
        dbUserManagement.removeDM(dungeonName, userNames);
    }
}
