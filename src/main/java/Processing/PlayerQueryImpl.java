package Processing;

import DataBase.DBDungeonQueryImpl;
import DataBase.DBPlayerQueryImpl;
import DatabaseInterfaces.DBDungeonQuery;
import DatabaseInterfaces.DBPlayerQuery;
import Exceptions.ElementNotFoundException;
import Model.Base.PlayerAvatar;
import Model.CommunicationModel.APIAvatar;
import Model.CommunicationModel.PlayerAvatarListEntry;
import ServerInterfaces.PlayerQuery;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;


@ApplicationScoped
public class PlayerQueryImpl implements PlayerQuery {


    private DBPlayerQuery dbPlayerQuery;

    private DBDungeonQuery dbDungeonQuery;

    @Inject
    public void setDbDungeonQuery(DBDungeonQueryImpl dbDungeonQuery) {
        this.dbDungeonQuery = dbDungeonQuery;
    }

    @Inject
    public void setDbPlayerQuery(DBPlayerQueryImpl dbPlayerQuery){
        this.dbPlayerQuery = dbPlayerQuery;
    }
    /**
     * gets the Avatar assosiated with the user in this particular dungeon
     *
     * @param dungeonName
     * @param userName
     */
    @Override
    public PlayerAvatar getPlayerAvatar(String dungeonName, String userName) throws ElementNotFoundException {
        return dbPlayerQuery.getPlayerAvatar(dungeonName, userName);
    }

    @Override
    public PlayerAvatar getPlayerAvatar(String dungeonName, long globalAvatarId) throws ElementNotFoundException {
        return dbPlayerQuery.getPlayerAvatar(dungeonName, globalAvatarId);
    }

    @Override
    public APIAvatar getAPIAvatar(String dungeonName, String userName) throws ElementNotFoundException {
        return new APIAvatar(getPlayerAvatar(dungeonName, userName));
    }

    @Override
    public APIAvatar getAPIAvatar(String dungeonName, long globalId) throws ElementNotFoundException {
        return new APIAvatar(getPlayerAvatar(dungeonName, globalId));
    }

    @Override
    public List<PlayerAvatar> getPlayerAvatars(String dungeonName) throws ElementNotFoundException {
        return dbPlayerQuery.getPlayerAvatars(dungeonName);
    }

    @Override
    public List<PlayerAvatarListEntry> getPlayerAvatarListEntries(String dungeonName) throws ElementNotFoundException {
        return dbPlayerQuery.getPlayerAvatars(dungeonName).stream().map(
                playerAvatar -> new PlayerAvatarListEntry(playerAvatar)).collect(Collectors.toList());
    }

    public long getActualRoomGlobalId(String dungeonName, String userName) throws ElementNotFoundException {
        return dbPlayerQuery.getActualRoomGlobalId(dungeonName, userName);
    }

    @Override
    public PlayerAvatar getPlayerAvatarWithAvatarName(String dungeonName, String avatarName) throws ElementNotFoundException {
        return dbPlayerQuery.getPlayerAvatarWithAvatarName(dungeonName, avatarName);
    }


}
