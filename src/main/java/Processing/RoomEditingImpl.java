package Processing;

import DataBase.*;
import DatabaseInterfaces.*;
import Exceptions.ElementNotFoundException;
import Exceptions.RoomDirectionException;
import Model.Base.DungeonRoom;
import Model.Base.InventoryEntry;
import Model.CommunicationModel.APIDungeonRoom;
import ServerInterfaces.RoomEditing;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class RoomEditingImpl implements RoomEditing {
    private DBRoomEditing dbRoomEditing;
    private DBRoomQuery dbRoomQuery;
    private DBDungeonQuery dbDungeonQuery;
    private DBPlayerQuery dbPlayerQuery;
    private DBPlayerEditing dbPlayerEditing;

    @Inject
    public void setDbRoomEditing(DBRoomEditingImpl dbRoomEditing) {
        this.dbRoomEditing = dbRoomEditing;
    }

    @Inject
    public void setDbRoomQuery(DBRoomQueryImpl dbRoomQuery) {
        this.dbRoomQuery = dbRoomQuery;
    }

    @Inject
    public void setDbDungeonQuery(DBDungeonQueryImpl dbDungeonQuery) {
        this.dbDungeonQuery = dbDungeonQuery;
    }

    @Inject
    public void setDbPlayerQuery(DBPlayerQueryImpl dbPlayerQuery) {
        this.dbPlayerQuery = dbPlayerQuery;
    }

    @Inject
    public void setDbPlayerEditing(DBPlayerEditingImpl dbPlayerEditing) {
        this.dbPlayerEditing = dbPlayerEditing;
    }

    @Override
    public void setAPIRoom(String dungeonName, APIDungeonRoom apiRoom) throws IllegalArgumentException, ElementNotFoundException, RoomDirectionException {
        if (apiRoom.actionIDs == null || apiRoom.placedObjects == null
                || apiRoom.description == null || apiRoom.displayName == null)
            throw new IllegalArgumentException("one or more fields in apiRoom are null");


        DungeonRoom oldRoom = dbRoomQuery.getDungeonRoom(null, apiRoom.globalID);
        dbRoomEditing.setAPIRoom(apiRoom);

        if (oldRoom.getRoomNorthGlobalID() != apiRoom.roomNorthGlobalID)
            connectRooms(dungeonName, apiRoom.globalID, RoomDirection.NORTH, apiRoom.roomNorthGlobalID);
        if (oldRoom.getRoomSouthGlobalID() != apiRoom.roomSouthGlobalID)
            connectRooms(dungeonName, apiRoom.globalID, RoomDirection.SOUTH, apiRoom.roomSouthGlobalID);
        if (oldRoom.getRoomEastGlobalID() != apiRoom.roomEastGlobalID)
            connectRooms(dungeonName, apiRoom.globalID, RoomDirection.EAST, apiRoom.roomEastGlobalID);
        if (oldRoom.getRoomWestGlobalID() != apiRoom.roomWestGlobalID)
            connectRooms(dungeonName, apiRoom.globalID, RoomDirection.WEST, apiRoom.roomWestGlobalID);
    }

    @Override
    public void connectRooms(String dungeonName, long room1ID, RoomDirection direction, long room2ID) throws RoomDirectionException, ElementNotFoundException {
        DungeonRoom room1 = dbRoomQuery.getDungeonRoom(dungeonName, room1ID);
        DungeonRoom room2 = room2ID != -1 ? dbRoomQuery.getDungeonRoom(dungeonName, room2ID) : null;
        switch (direction) {
            case NORTH:
                if (room2 != null && room2.getRoomSouthGlobalID() != -1)
                    throw new RoomDirectionException("The selected direction is ocupied");
                dbRoomEditing.setRoomsConnection(RoomDirection.NORTH, room1ID, room2ID);
                if (room2ID != -1)
                    dbRoomEditing.setRoomsConnection(RoomDirection.SOUTH, room2ID, room1ID);
                if (room1.getRoomNorthGlobalID() != -1)
                    dbRoomEditing.setRoomsConnection(RoomDirection.SOUTH, room1.getRoomNorthGlobalID(), -1);
                break;
            case SOUTH:
                if (room2 != null && room2.getRoomNorthGlobalID() != -1)
                    throw new RoomDirectionException("The selected direction is ocupied");
                dbRoomEditing.setRoomsConnection(RoomDirection.SOUTH, room1ID, room2ID);
                if (room2ID != -1)
                    dbRoomEditing.setRoomsConnection(RoomDirection.NORTH, room2ID, room1ID);
                if (room1.getRoomSouthGlobalID() != -1)
                    dbRoomEditing.setRoomsConnection(RoomDirection.NORTH, room1.getRoomSouthGlobalID(), -1);
                break;
            case WEST:
                if (room2 != null && room2.getRoomEastGlobalID() != -1)
                    throw new RoomDirectionException("The selected direction is ocupied");
                dbRoomEditing.setRoomsConnection(RoomDirection.WEST, room1ID, room2ID);
                if (room2ID != -1)
                    dbRoomEditing.setRoomsConnection(RoomDirection.EAST, room2ID, room1ID);
                if (room1.getRoomWestGlobalID() != -1)
                    dbRoomEditing.setRoomsConnection(RoomDirection.EAST, room1.getRoomWestGlobalID(), -1);
                break;
            case EAST:
                if (room2 != null && room2.getRoomWestGlobalID() != -1)
                    throw new RoomDirectionException("The selected direction is ocupied");
                dbRoomEditing.setRoomsConnection(RoomDirection.EAST, room1ID, room2ID);
                if (room2ID != -1)
                    dbRoomEditing.setRoomsConnection(RoomDirection.WEST, room2ID, room1ID);
                if(room1.getRoomEastGlobalID() != -1)
                    dbRoomEditing.setRoomsConnection(RoomDirection.WEST, room1.getRoomEastGlobalID(), -1);
                break;
        }
    }

    @Override
    public void updateInventoryEntry(String dungeonName, long roomID, InventoryEntry inventory) throws ElementNotFoundException {
        dbRoomEditing.updateExistingInventoryEntry(dungeonName, roomID, inventory);
    }

    @Override
    public boolean AddInventoryEntry(String dungeonName, long roomID, InventoryEntry inventory) throws ElementNotFoundException {
        return dbRoomEditing.addInventoryEntry(dungeonName, roomID, inventory);
    }

    @Override
    public void deleteRoom(String dungeonName, long roomGlobalId) throws ElementNotFoundException {
        long startRoomGlobalId = dbDungeonQuery.getStartRoomGlobalID(dungeonName);
        if (startRoomGlobalId == roomGlobalId) {
            throw new IllegalArgumentException("Startroom cannot be deleted!");
        } else {
            dbPlayerQuery.getPlayerAvatars(dungeonName).stream()
                    .filter(playerAvatar -> playerAvatar.getActualRoomGlobalId() == roomGlobalId)
                    .forEach(playerAvatar -> {
                        try {
                            dbPlayerEditing.setPlayerActualRoomGlobalID(playerAvatar.getDungeonName(), playerAvatar.getName(), startRoomGlobalId);
                        } catch (ElementNotFoundException e) {
                            e.printStackTrace();
                        }
                    });
            dbRoomEditing.deleteRoom(roomGlobalId);
        }
    }
}
