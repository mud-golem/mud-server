package Processing;

import DataBase.DBDungeonQueryImpl;
import DatabaseInterfaces.DBDungeonQuery;
import DatabaseInterfaces.DBPlayerQuery;
import Exceptions.ElementNotFoundException;
import Model.Base.*;
import Model.CommunicationModel.*;
import ServerInterfaces.DungeonQuery;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;

@ApplicationScoped
public class DungeonQueryImpl implements DungeonQuery {
    private DBDungeonQuery dbDungeonQuery;
    private DBPlayerQuery dbPlayerQuery;

    @Inject
    public void setDbDungeonQuery(DBDungeonQueryImpl dbDungeonQuery) {
        this.dbDungeonQuery = dbDungeonQuery;
    }

    @Inject
    public void setDbPlayerQuery(DBPlayerQuery dbPlayerQuery) {
        this.dbPlayerQuery = dbPlayerQuery;
    }

    @Override
    public String getDungeonDescription(String dungeonName) throws ElementNotFoundException {
        return dbDungeonQuery.getDungeonDescription(dungeonName);
    }

    @Override
    public String getLobbyDescription(String dungeonName) throws ElementNotFoundException {
        return dbDungeonQuery.getLobbyDescription(dungeonName);
    }

    @Override
    public boolean getJoinable(String dungeonName) throws ElementNotFoundException {
        return dbDungeonQuery.getJoinable(dungeonName);
    }

    @Override
    public long getStartRoomGlobalID(String dungeonName) throws ElementNotFoundException {
        return dbDungeonQuery.getStartRoomGlobalID(dungeonName);
    }

    @Override
    public String getUnknownActionResponse(String dungeonName) throws ElementNotFoundException {
        return dbDungeonQuery.getUnknownActionResponse(dungeonName);
    }

    @Override
    public List<String> getBlacklist(String dungeonName) throws ElementNotFoundException {
        return dbDungeonQuery.getBlacklist(dungeonName);
    }

    @Override
    public List<String> getWhitelist(String dungeonName) throws ElementNotFoundException {
        return dbDungeonQuery.getWhitelist(dungeonName);
    }

    @Override
    public List<String> getAllowedDms(String dungeonName) throws ElementNotFoundException {
        return dbDungeonQuery.getAllowedDms(dungeonName);
    }

    @Override
    public List<AvatarClass> getAvatarClasses(String dungeonName) throws ElementNotFoundException {
        return dbDungeonQuery.getAvatarClasses(dungeonName);
    }

    @Override
    public List<APIAvatarClass> getAPIAvatarClasses(String dungeonName) throws ElementNotFoundException {
        return getAvatarClasses(dungeonName).stream().map(d -> new APIAvatarClass(d.getGlobalID(), d.getDisplayName(), d.getDescription(), d.getHpModifier(), d.isEnabled())).collect(Collectors.toList());
    }


    @Override
    public APIAvatarClass getAPIAvatarClass(String dungeonName, long avatarClassGlobalID) throws ElementNotFoundException {
        AvatarClass a = getAvatarClass(dungeonName, avatarClassGlobalID);
        return new APIAvatarClass(a.getGlobalID(), a.getDisplayName(), a.getDescription(), a.getHpModifier(), a.isEnabled());
    }

    @Override
    public String getActualLoggedInDM(String dungeonName) throws ElementNotFoundException {
        return dbDungeonQuery.getActualLoggedInDM(dungeonName);
    }

    @Override
    public AvatarClass getAvatarClass(String dungeonName, long avatarClassGlobalID) throws ElementNotFoundException {
        return dbDungeonQuery.getAvatarClass(dungeonName, avatarClassGlobalID);
    }

    @Override
    public List<AvatarRace> getAvatarRaces(String dungeonName) throws ElementNotFoundException {
        return dbDungeonQuery.getAvatarRaces(dungeonName);
    }

    @Override
    public List<APIAvatarRace> getAPIAvatarRaces(String dungeonName) throws ElementNotFoundException {
        return getAvatarRaces(dungeonName).stream().map(d -> new APIAvatarRace(d.getGlobalID(), d.getDisplayName(), d.getDescription(), d.getHpModifier(), d.isEnabled())).collect(Collectors.toList());
    }


    @Override
    public APIAvatarRace getAPIAvatarRace(String dungeonName, long avatarRaceGlobalID) throws ElementNotFoundException {
        AvatarRace r = getAvatarRace(dungeonName, avatarRaceGlobalID);
        return new APIAvatarRace(r.getGlobalID(), r.getDisplayName(), r.getDescription(), r.getHpModifier(), r.isEnabled());
    }

    @Override
    public AvatarRace getAvatarRace(String dungeonName, long avatarRaceGlobalID) throws ElementNotFoundException {
        return dbDungeonQuery.getAvatarRace(dungeonName, avatarRaceGlobalID);
    }

    @Override
    public List<APIObjectTemplate> getObjectTemplates(String dungeonName) throws ElementNotFoundException {
        List<ObjectTemplate> tempObjTemps = dbDungeonQuery.getObjectTemplates(dungeonName);
        List<APIObjectTemplate> tempRetApi = dbDungeonQuery.getObjectTemplates(dungeonName).stream()
                .map(temp -> new APIObjectTemplate(temp)
                ).collect(Collectors.toList());
        return tempRetApi;
    }

    @Override
    public APIObjectTemplate getObjectTemplate(String dungeonName, long objectTemplateGlobalID) throws ElementNotFoundException {
        ObjectTemplate tempObjTemp = dbDungeonQuery.getObjectTemplate(dungeonName, objectTemplateGlobalID);
        return new APIObjectTemplate(tempObjTemp);
    }

    @Override
    public List<CustomAction> getDungeonwideCustomActions(String dungeonName) throws ElementNotFoundException {
        return dbDungeonQuery.getDungeonwideCustomActions(dungeonName);
    }

    @Override
    public List<APICustomAction> getDungeonwideAPICustomActions(String dungeonName) throws ElementNotFoundException {
        return getDungeonwideCustomActions(dungeonName).stream().map(customAction -> new APICustomAction(customAction)).collect(Collectors.toList());
    }

    @Override
    public List<CustomAction> getAllCustomActions(String dungeonName) throws ElementNotFoundException {
        return dbDungeonQuery.getAllCustomActions(dungeonName);
    }

    @Override
    public List<APICustomAction> getAllAPICustomActions(String dungeonName) throws ElementNotFoundException {
        return getAllCustomActions(dungeonName).stream().map(customAction -> new APICustomAction(customAction)).collect(Collectors.toList());
    }

    @Override
    public List<String> getAllDungeonNames() throws ElementNotFoundException {
        return dbDungeonQuery.getAllDungeonNames();
    }

    @Override
    public List<APIDungeon> getAllAPIDungeons() throws ElementNotFoundException {
        return dbDungeonQuery.getAllAPIDungeons();
    }

    @Override
    public APIDungeon getAPIDungeon(String dungeonName) throws ElementNotFoundException {
        return dbDungeonQuery.getAPIDungeon(dungeonName);
    }


    private LobbyDungeon convertAPIDungeonToLobbyDungeon(APIDungeon apiDungeon, String userNameTryingToJoin) throws ElementNotFoundException {
        int nrActivePlayers = (int) dbPlayerQuery.getPlayerAvatars(apiDungeon.uniqueName).stream().filter(playerAvatar -> playerAvatar.isActive()).count();
        String newLobbyDesc = apiDungeon.lobbyDescription;
        boolean classesAndRacesAreDefined = true;
        boolean allowAsPlayer;
        if (apiDungeon.useWhitelistInsteadOfBlacklist) {
            allowAsPlayer = apiDungeon.whitelistUserNames.contains(userNameTryingToJoin);
        } else {
            allowAsPlayer = !apiDungeon.blacklistUserNames.contains(userNameTryingToJoin);
        }

        if (dbDungeonQuery.getAvatarClasses(apiDungeon.uniqueName).isEmpty() || dbDungeonQuery.getAvatarRaces(apiDungeon.uniqueName).isEmpty()) {
            classesAndRacesAreDefined = false;
            allowAsPlayer = false;
            newLobbyDesc = "[When you can see this Message, please define Races and Classes in your Dungeon. Otherwise, it will not appear in the Lobby for other players.] " + newLobbyDesc;
        }
        System.out.println("User joining: " + apiDungeon.DMUserNames.get(0));
        boolean allowAsDM = apiDungeon.DMUserNames.contains(userNameTryingToJoin) && dbDungeonQuery.getActualLoggedInDM(apiDungeon.uniqueName).equals("no_dm");

        String dungeonMasterUsername = apiDungeon.dungeonMasterUserName;

        if(dbDungeonQuery.getActualLoggedInDM(apiDungeon.uniqueName).equals("no_dm")){
            dungeonMasterUsername = "No DM in here right now!";
        }

        return new LobbyDungeon(apiDungeon.uniqueName,
                newLobbyDesc,
                dungeonMasterUsername,
                nrActivePlayers,
                allowAsPlayer,
                allowAsDM,
                classesAndRacesAreDefined);
    }

    @Override
    public List<LobbyDungeon> getAllLobbyDungeons(String userNameTryingToJoin) throws ElementNotFoundException {
        List<APIDungeon> l = dbDungeonQuery.getAllAPIDungeons();
        List<LobbyDungeon> lobbyDungeons = l.stream()
                .filter(apiDungeon -> apiDungeon.isJoinable || apiDungeon.DMUserNames.contains(userNameTryingToJoin))
                .filter(apiDungeon -> !apiDungeon.blacklistUserNames.contains(userNameTryingToJoin))
                .map(apiDungeon -> {
                    try {
                        return convertAPIDungeonToLobbyDungeon(apiDungeon, userNameTryingToJoin);
                    } catch (ElementNotFoundException e) {
                        e.printStackTrace();
                        return null; //TODO
                    }
                }).collect(Collectors.toList());
        return lobbyDungeons;
    }


}
