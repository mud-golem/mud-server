package Processing;

import DataBase.DBRoomQueryImpl;
import DatabaseInterfaces.DBRoomQuery;
import Exceptions.ElementNotFoundException;
import Model.Base.DungeonRoom;
import Model.CommunicationModel.APIDungeonRoom;
import Model.CommunicationModel.APIInventoryEntry;
import Model.CommunicationModel.DungeonRoomListEntry;
import ServerInterfaces.RoomQuery;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@ApplicationScoped
public class RoomQueryImpl implements RoomQuery {

    private DBRoomQuery dbRoomQuery;

    @Inject
    public void setDbRoomQuery(DBRoomQueryImpl dbRoomQuery) {
        this.dbRoomQuery = dbRoomQuery;
    }

    @Override
    public DungeonRoom getDungeonRoom(String dungeonName, long roomID) throws ElementNotFoundException {
        return dbRoomQuery.getDungeonRoom(dungeonName, roomID);
    }

    @Override
    public List<DungeonRoom> getDungeonRooms(String dungeonName) throws ElementNotFoundException {
        return dbRoomQuery.getRooms(dungeonName);
    }

    @Override
    public APIDungeonRoom getAPIRoom(String dungeonName, long roomID) throws ElementNotFoundException {
        DungeonRoom dungeonRoom = getDungeonRoom(dungeonName, roomID);
        APIDungeonRoom apiDungeonRoom = new APIDungeonRoom();

        apiDungeonRoom.displayName = dungeonRoom.getDisplayName();
        apiDungeonRoom.description = dungeonRoom.getDescription();
        apiDungeonRoom.roomEastGlobalID = dungeonRoom.getRoomEastGlobalID();
        apiDungeonRoom.roomWestGlobalID = dungeonRoom.getRoomWestGlobalID();
        apiDungeonRoom.roomNorthGlobalID = dungeonRoom.getRoomNorthGlobalID();
        apiDungeonRoom.roomSouthGlobalID = dungeonRoom.getRoomSouthGlobalID();
        apiDungeonRoom.globalID = dungeonRoom.getGlobalID();


        apiDungeonRoom.placedObjects = dungeonRoom.getInventory().stream().map(inventoryEntry -> {
            APIInventoryEntry apiInventoryEntry = new APIInventoryEntry(inventoryEntry);

            return apiInventoryEntry;
        }).collect(Collectors.toList());

        apiDungeonRoom.actionIDs = dungeonRoom.getCustomActions().stream()
                .map(customAction -> customAction.getGlobalActionId()).collect(Collectors.toList());
        //TODO ask what in the name of *** he meant with the datatype
        return apiDungeonRoom;
    }

    @Override
    public List<APIDungeonRoom> getAPIRooms(String dungeonName) throws ElementNotFoundException {
        return getDungeonRooms(dungeonName).stream().map(dungeonRoom -> {
            APIDungeonRoom apiDungeonRoom = new APIDungeonRoom();

            apiDungeonRoom.displayName = dungeonRoom.getDisplayName();
            apiDungeonRoom.description = dungeonRoom.getDescription();
            apiDungeonRoom.roomEastGlobalID = dungeonRoom.getRoomEastGlobalID();
            apiDungeonRoom.roomWestGlobalID = dungeonRoom.getRoomWestGlobalID();
            apiDungeonRoom.roomNorthGlobalID = dungeonRoom.getRoomNorthGlobalID();
            apiDungeonRoom.roomSouthGlobalID = dungeonRoom.getRoomSouthGlobalID();
            apiDungeonRoom.globalID = dungeonRoom.getGlobalID();


            apiDungeonRoom.placedObjects = dungeonRoom.getInventory().stream().map(inventoryEntry -> {
                APIInventoryEntry apiInventoryEntry = new APIInventoryEntry(inventoryEntry);

                return apiInventoryEntry;
            }).collect(Collectors.toList());


            apiDungeonRoom.actionIDs = dungeonRoom.getCustomActions().stream()
                    .map(customAction -> customAction.getGlobalActionId()).collect(Collectors.toList());
            //TODO ask what in the name of *** he meant with the datatype
            return apiDungeonRoom;
        }).collect(Collectors.toList());
    }


    @Override
    public DungeonRoomListEntry getDungeonRoomListEntry(String dungeonName, long roomID) throws ElementNotFoundException {
        DungeonRoom dungeonRoom = getDungeonRoom(dungeonName, roomID);
        DungeonRoomListEntry dungeonRoomListEntry = new DungeonRoomListEntry();

        dungeonRoomListEntry.scopedID = dungeonRoom.getGlobalID();
        dungeonRoomListEntry.displayName = dungeonRoom.getDisplayName();
        dungeonRoomListEntry.roomWestScopedID = dungeonRoom.getRoomWestGlobalID();
        dungeonRoomListEntry.roomEastScopedID = dungeonRoom.getRoomEastGlobalID();
        dungeonRoomListEntry.roomNorthScopedID = dungeonRoom.getRoomNorthGlobalID();
        dungeonRoomListEntry.roomSouthScopedID = dungeonRoom.getRoomSouthGlobalID();

        return dungeonRoomListEntry;
    }

    @Override
    public List<DungeonRoomListEntry> getDungeonRoomListEntries(String dungeonName) throws ElementNotFoundException {
        return getDungeonRooms(dungeonName).stream().map(dungeonRoom -> {
            DungeonRoomListEntry dungeonRoomListEntry = new DungeonRoomListEntry();
            System.out.println(dungeonRoom.getDescription());
            dungeonRoomListEntry.scopedID = dungeonRoom.getGlobalID();
            dungeonRoomListEntry.displayName = dungeonRoom.getDisplayName();
            dungeonRoomListEntry.roomWestScopedID = dungeonRoom.getRoomWestGlobalID();
            dungeonRoomListEntry.roomEastScopedID = dungeonRoom.getRoomEastGlobalID();
            dungeonRoomListEntry.roomNorthScopedID = dungeonRoom.getRoomNorthGlobalID();
            dungeonRoomListEntry.roomSouthScopedID = dungeonRoom.getRoomSouthGlobalID();

            return dungeonRoomListEntry;
        }).collect(Collectors.toList());
    }

    @Override
    public List<String> getAllAvatarNamesInRoom(String dungeonName, long roomId) throws ElementNotFoundException {
        return dbRoomQuery.getAllAvatarNamesInRoom(dungeonName, roomId);
    }
}
