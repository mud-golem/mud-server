package Processing;

import DataBase.DBDungeonQueryImpl;
import DatabaseInterfaces.DBDungeonQuery;
import Model.Base.Dungeon;
import Model.Base.PlayerAvatar;
import Model.Base.User;
import Model.CommunicationModel.CreatedAvatar;
import Model.CommunicationModel.LobbyDungeon;
import ServerInterfaces.DungeonQuery;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * The DataConverter class has many static methods to convert Objects from the BackEnd to Objects which are needed in
 * the FrontEnd or the other way round
 * ©Jan Stöffler
 */
public class DataConverter {

    DBDungeonQuery dungeonQuery;

    @Inject
    public void setDungeonQuery(DBDungeonQueryImpl dbDungeonQuery){
        this.dungeonQuery = dbDungeonQuery;
    }

    /**
     * dungeonsToLobbyDungeons converts all dungeons to a List of dungeons for a specific user in the Front-End
     * @param list
     * @param userName
     * @return
     */
    public static List<LobbyDungeon> dungeonsToLobbyDungeons(List<Dungeon> list, String userName, boolean classedAndRacesAreDefined){
        List<LobbyDungeon> resultList = new ArrayList<>();
        list.forEach(dungeon ->{
            String uniqueName = dungeon.getUniqueName();
            String lobbyDescription = dungeon.getLobbyDescription();
            String dungeonMasterUserName = dungeon.getDungeonMasterUserName();
            int playerCount = dungeon.getAvatarsInDungeon().size();
            boolean canJoinAsPlayer = false;
            if (dungeon.isUseWhitelistInsteadOfBlacklist())
            {
                canJoinAsPlayer = dungeon.getWhitelistUserNames().contains(userName);
            }
            else {
                canJoinAsPlayer = !dungeon.getBlacklistUserNames().contains(userName);
            }
            List<String> dmUser = dungeon.getAllowedDMs();
            boolean canJoinAsDM = dmUser.contains(userName);
            LobbyDungeon lobbyDungeon = new LobbyDungeon(uniqueName,lobbyDescription,dungeonMasterUserName,playerCount
                    ,canJoinAsPlayer,canJoinAsDM, classedAndRacesAreDefined);
            resultList.add(lobbyDungeon);
        });
        return resultList;
    }

    /**
     * createdAvatarToPlayerAvatar creates an Avatar for the Back-end with the CreatedAvatar object from the Front-End
     * @param avatar
     * @param dungeonID
     * @param userName
     * @return
     */
    public static PlayerAvatar createdAvatarToPlayerAvatar(CreatedAvatar avatar, String dungeonID, String userName){
        PlayerAvatar player =
                new PlayerAvatar(-1,dungeonID,avatar.uniqueName,"",
                        100,100,true,userName,-1);
        return player;
    }
}
