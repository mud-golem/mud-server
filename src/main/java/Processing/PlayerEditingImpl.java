package Processing;

import DataBase.DBPlayerEditingImpl;
import DatabaseInterfaces.DBPlayerEditing;
import Exceptions.ElementNotFoundException;
import Model.Base.PlayerAvatar;
import Model.CommunicationModel.APIAvatar;
import ServerInterfaces.PlayerEditing;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class PlayerEditingImpl implements PlayerEditing {
    private DBPlayerEditing dbPlayerEditing;

    @Inject
    public void setDbPlayerEditing(DBPlayerEditingImpl dbPlayerEditing) {
        this.dbPlayerEditing = dbPlayerEditing;
    }

    @Override
    public void setPlayer(String dungeonName, PlayerAvatar playerAvatar) throws ElementNotFoundException {
        dbPlayerEditing.setPlayer(dungeonName,playerAvatar);
        //dbPlayerEditing.setPlayer(dungeonName, playerAvatar);
        //String avatarGlobalID = playerAvatar.getGlobalId();
        //dbPlayerEditing.setPlayerDescription(dungeonName, avatarGlobalID, playerAvatar.getUserDescription());
        //dbPlayerEditing.setPlayerCurrHP(dungeonName, avatarGlobalID, playerAvatar.getCurrHP());
        //dbPlayerEditing.setPlayerMaxHP(dungeonName,  avatarGlobalID, playerAvatar.getMaxHP());
        //dbPlayerEditing.setPlayerIsActive(dungeonName,  avatarGlobalID, playerAvatar.isActive());
        //dbPlayerEditing.setPlayerActualRoomGlobalID(dungeonName, avatarGlobalID, playerAvatar.getActualRoomGlobalId());
        //dbPlayerEditing.setPlayerRace(dungeonName, avatarGlobalID, playerAvatar.getAvatarRace());
        //dbPlayerEditing.setPlayerClass(dungeonName, avatarGlobalID, playerAvatar.getAvatarClass());
        //dbPlayerEditing.setInventory(dungeonName, avatarGlobalID, playerAvatar.getInventory());

    }

    @Override
    public void setAPIPlayer(String dungeonName, APIAvatar apiAvatar) throws ElementNotFoundException {

        int maxHP = apiAvatar.maxHP;
        int currHP = apiAvatar.currHP;

        if(maxHP < currHP)
            currHP = maxHP;

        dbPlayerEditing.setPlayerName(dungeonName, apiAvatar.userName, apiAvatar.name);
        dbPlayerEditing.setPlayerDescription(dungeonName, apiAvatar.userName, apiAvatar.userDescription);
        dbPlayerEditing.setPlayerIsActive(dungeonName, apiAvatar.userName, apiAvatar.isActive);
        dbPlayerEditing.setPlayerActualRoomGlobalID(dungeonName, apiAvatar.userName, apiAvatar.roomGlobalID);
        dbPlayerEditing.setPlayerCurrHP(dungeonName, apiAvatar.userName, currHP);
        dbPlayerEditing.setPlayerMaxHP(dungeonName, apiAvatar.userName, maxHP);
        dbPlayerEditing.setInventory(dungeonName, apiAvatar.userName, apiAvatar.inventory);
        dbPlayerEditing.setPlayerClassByID(apiAvatar.globalID, apiAvatar.classGlobalID);
        dbPlayerEditing.setPlayerRaceByID(apiAvatar.globalID, apiAvatar.raceGlobalID);
    }

    @Override
    public void setPlayerIsActive(String dungeonName, String username, boolean newValue) throws ElementNotFoundException {
        dbPlayerEditing.setPlayerIsActive(dungeonName, username, newValue);
    }

    @Override
    public void setCurrRoom(String dungeonName, String username, long newCurrentRoomID) throws ElementNotFoundException {
        dbPlayerEditing.setPlayerActualRoomGlobalID(dungeonName,username, newCurrentRoomID);
    }

    @Override
    public void setPlayerClassByID(long playerAvatarGlobalId, long classGlobalId) throws ElementNotFoundException {
        dbPlayerEditing.setPlayerClassByID(playerAvatarGlobalId, classGlobalId);
    }

    @Override
    public void setPlayerRaceByID(long playerAvatarGlobalId, long raceGlobalId) throws ElementNotFoundException {
        dbPlayerEditing.setPlayerRaceByID(playerAvatarGlobalId, raceGlobalId);
    }

    @Override
    public void deletePlayerAvatar(long globalAvatarId, String dungeonName) throws ElementNotFoundException {
        dbPlayerEditing.deletePlayerAvatar(globalAvatarId, dungeonName);
    }


}
