package Processing;

import ChatManaging.CoreChatManager;
import Communication.ChatClient;
import DatabaseInterfaces.RoomDirection;
import Exceptions.ElementNotFoundException;
import Model.Base.*;
import Model.CommunicationModel.Message;
import Model.CommunicationModel.MessageType;
import ServerInterfaces.*;

import org.apache.commons.lang3.*;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.websocket.Session;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Stream;

@ApplicationScoped
public class ActionHandlerImpl implements ActionHandler {

    private static final String LINE_BREAK_SYMBOL = "\n";
    private static final String TAB_SYMBOL = "\t";

    PlayerEditing playerEditing;

    PlayerQuery playerQuery;

    RoomQuery roomQuery;

    ChatClient chatClient;

    DungeonEditing dungeonEditing;

    DungeonQuery dungeonQuery;

    CoreChatManager coreChatManager;

    RoomEditing roomEditing;

    @Inject
    public void setCoreChatManager(CoreChatManager coreChatManager) {
        this.coreChatManager = coreChatManager;
    }

    @Inject
    public void setRoomEditing(RoomEditing roomEditing) {
        this.roomEditing = roomEditing;
    }

    @Inject
    public void setPlayerEditing(PlayerEditingImpl playerEditing) {
        this.playerEditing = playerEditing;
    }

    @Inject
    public void setPlayerQuery(PlayerQueryImpl playerQuery) {
        this.playerQuery = playerQuery;
    }

    @Inject
    public void setRoomQuery(RoomQueryImpl roomQuery) {
        this.roomQuery = roomQuery;
    }

    @Inject
    public void setChatClient(ChatClient chatClient) {
        this.chatClient = chatClient;
    }

    @Inject
    public void setDungeonEditing(DungeonEditing dungeonEditing) {
        this.dungeonEditing = dungeonEditing;
    }

    @Inject
    public void setDungeonQuery(DungeonQuery dungeonQuery) {
        this.dungeonQuery = dungeonQuery;
    }

    @Override
    public void doAction(String dungeonName, String username, String actionText) throws ElementNotFoundException {
        String[] parts = actionText.trim().split(" ", 2);

        System.out.println("Dungeon Name:" + dungeonName + ", Username:" + username + ", Action:" + actionText);

        PlayerAvatar playerAvatar = playerQuery.getPlayerAvatar(dungeonName, username);
        long roomID = playerAvatar.getActualRoomGlobalId();
        DungeonRoom dungeonRoom = roomQuery.getDungeonRoom(dungeonName, roomID);

        String first = parts[0].toLowerCase().trim();
        String last = parts.length == 2 ? parts[1].trim() : null;
        if (last != null && last.equals(""))
            last = null;
        switch (first) {
            case "look":
                lookAction(dungeonName, username, last);
                break;
            case "north":
                movePlayerAction(dungeonName, username, RoomDirection.NORTH);
                break;
            case "west":
                movePlayerAction(dungeonName, username, RoomDirection.WEST);
                break;
            case "south":
                movePlayerAction(dungeonName, username, RoomDirection.SOUTH);
                break;
            case "east":
                movePlayerAction(dungeonName, username, RoomDirection.EAST);
                break;
            case "inventory":
                inventoryAction(dungeonName, username);
                break;
            case "help":
                help(dungeonName, username);
                break;
            case "take":
                takeAction(dungeonName, username, last);
                break;
            case "drop":
                dropAction(dungeonName, username, last);
                break;
            default:
                customAction(dungeonName, username, first, last);
                return;

        }
        chatClient.sendMessage(
                createMessageToDM(dungeonName, username,
                        first + " " + ((last == null) ? "" : last)
                        , 0, dungeonRoom.getDisplayName()));
    }

    private void customAction(String dungeonName, String username, String first, String last) throws ElementNotFoundException {
        PlayerAvatar playerAvatar = playerQuery.getPlayerAvatar(dungeonName, username);

        DungeonRoom room = roomQuery.getDungeonRoom(dungeonName, playerAvatar.getActualRoomGlobalId());
        Stream<CustomAction> sObjectRoom = Stream.empty();
        Stream<CustomAction> sObjectPlayerInventory = Stream.empty();
        Stream<CustomAction> sRoom = Stream.empty();
        Stream<CustomAction> sDungeon = Stream.empty();
        if (last != null) {
            sObjectRoom = room.getInventory().stream()
                    .filter(inventoryEntry -> inventoryEntry.getName().toLowerCase().equals(last.toLowerCase()))
                    .flatMap(inventoryEntry -> inventoryEntry.getTemplate().getCustomActionList().stream());
            sObjectPlayerInventory = playerAvatar.getInventory().stream()
                    .filter(inventoryEntry -> inventoryEntry.getName().toLowerCase().equals(last.toLowerCase()))
                    .flatMap(inventoryEntry -> inventoryEntry.getTemplate().getCustomActionList().stream());
        } else {
            sRoom = room.getCustomActions().stream();
            sDungeon = dungeonQuery.getDungeonwideCustomActions(dungeonName).stream();
        }

        Stream<CustomAction> s1 = Stream.concat(sObjectRoom, sObjectPlayerInventory);
        Stream<CustomAction> s2 = Stream.concat(s1, sRoom);
        Stream<CustomAction> mergedStream = Stream.concat(s2, sDungeon);

        CustomAction firstMatchingAction = mergedStream
                .filter(customAction -> customAction.getCommand().toLowerCase().equals(first.toLowerCase()))
                .findFirst().orElseGet(() -> null);

        if (firstMatchingAction == null) {
            chatClient.sendMessage(
                    createMessageAsActionResponse(dungeonName, username, dungeonQuery.getUnknownActionResponse(dungeonName))
            );
            chatClient.sendMessage(
                    createMessageToDM(dungeonName, username,
                            "Tried to do >" + first + " " + ((last == null) ? "" : last) + "<"
                            , 1, room.getDisplayName())
            );
            return;
        }
        chatClient.sendMessage(
                createMessageAsActionResponse(dungeonName, username, firstMatchingAction.getResponse())
        );
        chatClient.sendMessage(
                createMessageToDM(dungeonName, username,
                        "CustomAction >"
                                + firstMatchingAction.getActionName() + "< with >" + first + " " + ((last == null) ? "" : last) + "<"
                        , firstMatchingAction.getPriority(), room.getDisplayName())
        );
    }

    private void takeAction(String dungeonName, String username, String last) throws ElementNotFoundException {
        if (last == null || last.equals("")) {
            chatClient.sendMessage(createMessageAsActionResponse(dungeonName, username, "Please select an item first."));
            return;
        }
        PlayerAvatar playerAvatar = playerQuery.getPlayerAvatar(dungeonName, username);
        long roomID = playerAvatar.getActualRoomGlobalId();
        DungeonRoom dungeonRoom = roomQuery.getDungeonRoom(dungeonName, roomID);

        InventoryEntry inventoryEntry = dungeonRoom.getInventory().stream().filter(ie -> ie.getName().toLowerCase().equals(last.toLowerCase())).findFirst().orElseGet(() -> null);
        if (inventoryEntry == null || inventoryEntry.getCount() <= 0) {
            chatClient.sendMessage(createMessageAsActionResponse(dungeonName, username, "An item with this name does not exitst here"));
            return;
        }
        if (!inventoryEntry.getTemplate().isPickupable()) {
            chatClient.sendMessage(createMessageAsActionResponse(dungeonName, username, "This item is not collectable"));
            return;
        }
        inventoryEntry.setCount(inventoryEntry.getCount() - 1);
        roomEditing.updateInventoryEntry(dungeonName, roomID, inventoryEntry);
        playerAvatar.addOrChangeInventoryItemCount(inventoryEntry.getTemplate(), 1);
        playerEditing.setPlayer(dungeonName, playerAvatar);

        chatClient.sendMessage(createMessageAsActionResponse(dungeonName, username, "You take 1 " + inventoryEntry.getName()));

    }

    private void dropAction(String dungeonName, String username, String last) throws ElementNotFoundException {
        if (last == null || last.equals("")) {
            chatClient.sendMessage(createMessageAsActionResponse(dungeonName, username, "Please select an item first."));
            return;
        }
        PlayerAvatar playerAvatar = playerQuery.getPlayerAvatar(dungeonName, username);
        long roomID = playerAvatar.getActualRoomGlobalId();
        DungeonRoom dungeonRoom = roomQuery.getDungeonRoom(dungeonName, roomID);

        InventoryEntry inventoryEntry = playerAvatar.getInventory().stream().filter(ie -> ie.getName().toLowerCase()
                .equals(last.toLowerCase())).findFirst().orElseGet(() -> null);
        if (inventoryEntry == null || inventoryEntry.getCount() <= 0) {
            chatClient.sendMessage(createMessageAsActionResponse(dungeonName, username, "An item with this name does not exitst in your inventory"));
            return;
        }

        InventoryEntry inventoryEntryRoom = dungeonRoom.getInventory().stream().filter(ie -> ie.getName().toLowerCase()
                .equals(last.toLowerCase())).findFirst().orElseGet(() -> null);
        if (inventoryEntryRoom == null)
            roomEditing.AddInventoryEntry(dungeonName, roomID, new InventoryEntry(inventoryEntry.getTemplate(), 1));
        else {
            inventoryEntryRoom.setCount(inventoryEntryRoom.getCount() + 1);
            roomEditing.updateInventoryEntry(dungeonName, roomID, inventoryEntryRoom);
        }
        playerAvatar.addOrChangeInventoryItemCount(inventoryEntry.getTemplate(), -1);
        playerEditing.setPlayer(dungeonName, playerAvatar);

        chatClient.sendMessage(createMessageAsActionResponse(dungeonName, username, "You droped 1 " + inventoryEntry.getName()));


    }

    private void movePlayerAction(String dungeonName, String username, RoomDirection roomDirection) throws ElementNotFoundException {
        long actualRoomId;
        String roomName;
        long newRoomId;
        try {
            actualRoomId = playerQuery.getActualRoomGlobalId(dungeonName, username);
        } catch (ElementNotFoundException e) {
            chatClient.sendMessage(createMessageAsActionResponse(dungeonName, username, "You are in no Room. I have no Idea how you did this..."));
            throw e;
        }
        switch (roomDirection) {
            case NORTH:
                newRoomId = roomQuery.getDungeonRoom(dungeonName, actualRoomId).getRoomNorthGlobalID();
                break;
            case EAST:
                newRoomId = roomQuery.getDungeonRoom(dungeonName, actualRoomId).getRoomEastGlobalID();
                break;
            case WEST:
                newRoomId = roomQuery.getDungeonRoom(dungeonName, actualRoomId).getRoomWestGlobalID();
                break;
            case SOUTH:
                newRoomId = roomQuery.getDungeonRoom(dungeonName, actualRoomId).getRoomSouthGlobalID();
                break;
            default:
                newRoomId = -1;
        }
        if (newRoomId == -1) {
            chatClient.sendMessage(createMessageAsActionResponse(dungeonName, username, "In this way no room exists"));
        } else {
            roomName = roomQuery.getDungeonRoom(dungeonName, newRoomId).getDisplayName();
            coreChatManager.changeRoom(dungeonName, actualRoomId, newRoomId, roomName, username);
            playerEditing.setCurrRoom(dungeonName, username, newRoomId);
            chatClient.sendMessage(createMessageAsActionResponse(dungeonName, username, "You went to " + roomDirection.name()));
            lookAction(dungeonName, username, null);
        }
    }

    private void lookAction(String dungeonName, String username, String last) throws ElementNotFoundException {
        DungeonRoom room = roomQuery.getDungeonRoom(dungeonName, playerQuery.getActualRoomGlobalId(dungeonName, username));
        System.out.println(">" + last + "<");
        if (last == null || last.equals("")) {
            lookRoom(dungeonName, username, room);
        } else {
            boolean found = lookItemInRoom(dungeonName, username, last, room);
            if (!found)
                found = lookItemInOwnInventory(dungeonName, username, last);
            if (!found)
                found = lookPlayers(dungeonName, username, last, room);
            if (!found)
                chatClient.sendMessage(createMessageAsActionResponse(dungeonName, username, "Can't find object to look at."));
        }
    }

    private boolean lookPlayers(String dungeonName, String username, String last, DungeonRoom room) throws ElementNotFoundException {
        String avatarName = roomQuery.getAllAvatarNamesInRoom(dungeonName, room.getGlobalID()).stream()
                .filter(s -> s.toLowerCase().equals(last.toLowerCase())).findFirst().orElseGet(() -> null);

        if (avatarName == null)
            if((last.toLowerCase()).equals("me")){
                avatarName = playerQuery.getPlayerAvatar(dungeonName, username).getName();
            } else {
                return false;
            }

        PlayerAvatar pa = playerQuery.getPlayerAvatarWithAvatarName(dungeonName, avatarName);

        if (pa == null)
            return false;

        chatClient.sendMessage(createMessageAsActionResponse(dungeonName, username, "This is Player: "
                + pa.getName() + LINE_BREAK_SYMBOL + pa.getUserDescription()));

        StringBuilder sb = new StringBuilder();
        sb.append("Their Race: ");
        sb.append(pa.getAvatarRace().getDisplayName());
        sb.append(LINE_BREAK_SYMBOL);
        sb.append("Their Class: ");
        sb.append(pa.getAvatarClass().getDisplayName());
        sb.append(LINE_BREAK_SYMBOL);
        sb.append("HP: (" + pa.getCurrHP() + "/" + pa.getMaxHP() + ")");

        chatClient.sendMessage(createMessageAsActionResponse(dungeonName, username, sb.toString()));
        return true;
    }

    private void lookRoom(String dungeonName, String username, DungeonRoom room) throws ElementNotFoundException {
        chatClient.sendMessage(
                createMessageAsActionResponse(dungeonName, username, "You are in the " + room.getDisplayName() +
                        LINE_BREAK_SYMBOL + room.getDescription() + LINE_BREAK_SYMBOL + "In the room you see:"));

        StringBuilder msgBuilder = new StringBuilder();
        msgBuilder.append("================" + LINE_BREAK_SYMBOL);
        int temp = msgBuilder.length();
        for (String s : roomQuery.getAllAvatarNamesInRoom(dungeonName, room.getGlobalID())) {
            PlayerAvatar pa = playerQuery.getPlayerAvatarWithAvatarName(dungeonName, s);
            if (pa.isActive()) {
                if (pa.getOwnerUserName().equals(username))
                    msgBuilder.append(TAB_SYMBOL + "- Player" + StringUtils.leftPad(s, 25) + "  [YOU]" + LINE_BREAK_SYMBOL);
                else
                    msgBuilder.append(TAB_SYMBOL + "- Player" + StringUtils.leftPad(s, 25) + LINE_BREAK_SYMBOL);
            }
        }
        if (temp != msgBuilder.length()) {
            msgBuilder.append("================" + LINE_BREAK_SYMBOL);
            temp = msgBuilder.length();
        }
        for (InventoryEntry ie : room.getInventory()) {
            msgBuilder.append(TAB_SYMBOL + "- Object" + StringUtils.leftPad(ie.getName(), 25) + StringUtils.leftPad(((Integer) ie.getCount()).toString(), 7) + LINE_BREAK_SYMBOL);
        }
        if (temp != msgBuilder.length()) {
            msgBuilder.append("================" + LINE_BREAK_SYMBOL);
            temp = msgBuilder.length();
        }
        if (room.getRoomNorthGlobalID() != -1)
            msgBuilder.append(TAB_SYMBOL + StringUtils.rightPad("- To the North you see: ", 28) + roomQuery.getDungeonRoom(dungeonName, room.getRoomNorthGlobalID()).getDisplayName() + LINE_BREAK_SYMBOL);
        if (room.getRoomSouthGlobalID() != -1)
            msgBuilder.append(TAB_SYMBOL + StringUtils.rightPad("- To the South you see: ", 28) + roomQuery.getDungeonRoom(dungeonName, room.getRoomSouthGlobalID()).getDisplayName() + LINE_BREAK_SYMBOL);
        if (room.getRoomEastGlobalID() != -1)
            msgBuilder.append(TAB_SYMBOL + StringUtils.rightPad("- To the East you see: ", 28) + roomQuery.getDungeonRoom(dungeonName, room.getRoomEastGlobalID()).getDisplayName() + LINE_BREAK_SYMBOL);
        if (room.getRoomWestGlobalID() != -1)
            msgBuilder.append(TAB_SYMBOL + StringUtils.rightPad("- To the West you see: ", 28) + roomQuery.getDungeonRoom(dungeonName, room.getRoomWestGlobalID()).getDisplayName() + LINE_BREAK_SYMBOL);

        if (temp != msgBuilder.length()) {
            msgBuilder.append("================" + LINE_BREAK_SYMBOL);
            temp = msgBuilder.length();
        }

        if (!room.getCustomActions().isEmpty())
            msgBuilder.append("You can try following in this room:" + LINE_BREAK_SYMBOL);
        for (CustomAction ca : room.getCustomActions()) {
            msgBuilder.append(TAB_SYMBOL + "- " + ca.getCommand() + LINE_BREAK_SYMBOL);
        }
        chatClient.sendMessage(createMessageAsActionResponse(dungeonName, username, msgBuilder.toString()));
    }


    private boolean lookItemInOwnInventory(String dungeonName, String username, String last) throws ElementNotFoundException {
        ObjectTemplate template = playerQuery.getPlayerAvatar(dungeonName, username).getInventory().stream()
                .filter(inventoryEntry -> inventoryEntry.getTemplate().getDisplayName().toLowerCase().equals(last.toLowerCase()))
                .map(inventoryEntry -> inventoryEntry.getTemplate()).findFirst().orElseGet(() -> null);
        if (template == null) {
            return false;
        }
        StringBuilder actions = new StringBuilder();
        if (!template.getCustomActionList().isEmpty())
            actions.append("You can try following on this object:" + LINE_BREAK_SYMBOL);
        for (CustomAction ca : template.getCustomActionList()) {
            actions.append(TAB_SYMBOL + "- " + ca.getCommand() + LINE_BREAK_SYMBOL);
        }
        chatClient.sendMessage(
                createMessageAsActionResponse(dungeonName, username, template.getDisplayName() + ": "
                        + template.getFullDescription() + LINE_BREAK_SYMBOL
                        + actions.toString()));

        return true;
    }

    private boolean lookItemInRoom(String dungeonName, String username, String last, DungeonRoom room) {
        ObjectTemplate template = room.getInventory().stream()
                .filter(inventoryEntry -> inventoryEntry.getTemplate().getDisplayName().toLowerCase().equals(last.toLowerCase()))
                .map(inventoryEntry -> inventoryEntry.getTemplate()).findFirst().orElseGet(() -> null);

        if (template == null) {
            return false;
        }
        StringBuilder actions = new StringBuilder();
        if (!template.getCustomActionList().isEmpty())
            actions.append("You can try following on this object:" + LINE_BREAK_SYMBOL);
        for (CustomAction ca : template.getCustomActionList()) {
            actions.append(TAB_SYMBOL + "- " + ca.getCommand() + LINE_BREAK_SYMBOL);
        }
        chatClient.sendMessage(
                createMessageAsActionResponse(dungeonName, username, template.getDisplayName() + ": "
                        + template.getFullDescription() + LINE_BREAK_SYMBOL
                        + actions.toString()));
        return true;
    }

    private void inventoryAction(String dungeonName, String userName) throws ElementNotFoundException {
        PlayerAvatar pa = playerQuery.getPlayerAvatar(dungeonName, userName);
        StringBuilder sb = new StringBuilder();
        sb.append("Your Race: ");
        sb.append(pa.getAvatarRace().getDisplayName());
        sb.append(LINE_BREAK_SYMBOL);
        sb.append("Your Class: ");
        sb.append(pa.getAvatarClass().getDisplayName());
        sb.append(LINE_BREAK_SYMBOL);
        sb.append("HP: (" + pa.getCurrHP() + "/" + pa.getMaxHP() + ")");

        chatClient.sendMessage(createMessageAsActionResponse(dungeonName, userName, sb.toString()));

        List<InventoryEntry> inventoryEntryList = pa.getInventory();
        if (!inventoryEntryList.isEmpty()) {
            String inventory = "Your Inventory have:" + LINE_BREAK_SYMBOL;
            for (InventoryEntry inventoryEntry : inventoryEntryList) {
                inventory += TAB_SYMBOL + "- " + inventoryEntry.getName() + ": " + inventoryEntry.getCount() + LINE_BREAK_SYMBOL;

            }
            chatClient.sendMessage(createMessageAsActionResponse(dungeonName, userName, inventory));
        } else {
            chatClient.sendMessage(createMessageAsActionResponse(dungeonName, userName, "Your Inventory is empty."));
        }

    }


    public void help(String dungeonName, String userName) throws ElementNotFoundException {
        Map<String, String> allPossibleCommands = new HashMap<>();
        allPossibleCommands.put("look", TAB_SYMBOL + TAB_SYMBOL + "Shows all players and objects in your room and shows you the possible directions.");
        allPossibleCommands.put("look me", TAB_SYMBOL + TAB_SYMBOL + "Shows your own information");
        allPossibleCommands.put("north", TAB_SYMBOL + TAB_SYMBOL + "Moves your character one room to north");
        allPossibleCommands.put("south", TAB_SYMBOL + TAB_SYMBOL + "Moves your character one room to south");
        allPossibleCommands.put("west", TAB_SYMBOL + TAB_SYMBOL + "Moves your character one room to west");
        allPossibleCommands.put("east", TAB_SYMBOL + TAB_SYMBOL + "Moves your character one room to east");
        allPossibleCommands.put("inventory", TAB_SYMBOL + TAB_SYMBOL + "Lists your whole inventory");

        String helpOutput = "All possible commands:" + LINE_BREAK_SYMBOL;
        for (Map.Entry<String, String> entry : allPossibleCommands.entrySet()) {
            helpOutput += TAB_SYMBOL + "- " + entry.getKey() + ": " + entry.getValue() + LINE_BREAK_SYMBOL;
        }

        helpOutput += LINE_BREAK_SYMBOL + "Special Dungeon commands:" + LINE_BREAK_SYMBOL;
        for (CustomAction ca : dungeonQuery.getDungeonwideCustomActions(dungeonName)) {
            helpOutput += TAB_SYMBOL + "- " + ca.getCommand() + ": " + ca.getActionName() + LINE_BREAK_SYMBOL;
        }


        chatClient.sendMessage(createMessageAsActionResponse(dungeonName, userName, helpOutput));
    }

    private Message createMessageAsActionResponse(String dungeonName, String userName, String msgText) {
        Message msg = new Message();
        msg.content = msgText;
        msg.recipient = userName;
        msg.type = MessageType.WhisperSystemMessage;
        msg.dungeonName = dungeonName;
        return msg;
    }

    private Message createMessageToDM(String dungeonName, String userName, String msgText, int prio, String roomName) throws ElementNotFoundException {
        Message msg = new Message();
        msg.content = msgText;
        msg.type = MessageType.ActionAlertMessage;
        msg.priority = (byte) prio;
        msg.dungeonName = dungeonName;
        msg.room = roomName;
        msg.sender = playerQuery.getAPIAvatar(dungeonName, userName).name;

        return msg;
    }
}
