package Processing;

import DataBase.DBDungeonEditingImpl;
import DataBase.DBDungeonManagementImpl;
import DataBase.DBDungeonQueryImpl;
import DatabaseInterfaces.DBDungeonEditing;
import DatabaseInterfaces.DBDungeonManagement;
import DatabaseInterfaces.DBDungeonQuery;
import Exceptions.ElementNotFoundException;
import Model.CommunicationModel.APIDungeon;
import ServerInterfaces.DungeonManagement;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class DungeonManagementImpl implements DungeonManagement {

    private DBDungeonManagement dbDungeonManagement;

    private DBDungeonQuery dungeonQuery;

    @Inject
    public void setDbDungeonManagement(DBDungeonManagementImpl dbDungeonManagement) {
        this.dbDungeonManagement = dbDungeonManagement;
    }

    @Inject
    public void setDbDungeonQuery(DBDungeonQueryImpl dungeonQuery) {
        this.dungeonQuery = dungeonQuery;
    }

    @Override
    public void setComDungeon(APIDungeon APIDungeon) throws IllegalArgumentException, ElementNotFoundException {

        if(APIDungeon.isJoinable) {
            if (dungeonQuery.getAvatarClasses(APIDungeon.uniqueName).isEmpty() || dungeonQuery.getAvatarRaces(APIDungeon.uniqueName).isEmpty()) {
                APIDungeon.isJoinable = false;
            } else if (!dungeonQuery.getAvatarClasses(APIDungeon.uniqueName).stream().anyMatch(avatarClass -> avatarClass.isEnabled())
                    || !dungeonQuery.getAvatarRaces(APIDungeon.uniqueName).stream().anyMatch(avatarRace -> avatarRace.isEnabled())) {
                APIDungeon.isJoinable = false;
            }
        }
        dbDungeonManagement.setDungeon(APIDungeon);
    }

    @Override
    public void setActualDM(String dungeonName, String userName) throws ElementNotFoundException {
        dbDungeonManagement.setActualDM(dungeonName, userName);
    }

    public void removeDM(String dungeonName) throws ElementNotFoundException {
        dbDungeonManagement.setActualDM(dungeonName, "no_dm");
    }
}
