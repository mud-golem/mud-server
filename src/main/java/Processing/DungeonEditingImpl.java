package Processing;

import DataBase.DBDungeonEditingImpl;
import DataBase.DBRoomCreationImpl;
import DatabaseInterfaces.DBDungeonEditing;
import DatabaseInterfaces.DBRoomCreation;
import Exceptions.ElementNotFoundException;
import Model.Base.*;
import Model.CommunicationModel.APICustomAction;
import Model.CommunicationModel.APIObjectTemplate;
import ServerInterfaces.DungeonEditing;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.ArrayList;

@ApplicationScoped
public class DungeonEditingImpl implements DungeonEditing {
    private DBDungeonEditing dbDungeonEditing;
    private DBRoomCreation dbRoomCreation;

    @Inject
    public void setDbDungeonEditing(DBDungeonEditingImpl dbDungeonEditing) {
        this.dbDungeonEditing = dbDungeonEditing;
    }

    @Inject
    public void setDbRoomCreation(DBRoomCreationImpl dbRoomCreation) {
        this.dbRoomCreation = dbRoomCreation;
    }

    @Override
    public void createAvatarClass(String dungeonName, String className, String classDescription, int hpModifier, boolean isEnabled) throws ElementNotFoundException {
        AvatarClass ac = new AvatarClass(-1,className, classDescription, null, hpModifier,isEnabled);
        dbDungeonEditing.addNewAvatarClass(dungeonName,ac);
    }

    @Override
    public void editAvatarClass(String dungeonName, String className, String newClassDescription, int newHPModifier, boolean isEnabled, long globalID) throws ElementNotFoundException {
        AvatarClass ac = new AvatarClass(globalID,className, newClassDescription, null, newHPModifier, isEnabled);
        dbDungeonEditing.updateExistingAvatarClass(dungeonName,ac);
    }

    @Override
    public void createAvatarRace(String dungeonName, String raceName, String raceDescription, int hpModifier, boolean isEnabled) throws ElementNotFoundException {
        AvatarRace ar = new AvatarRace(-1,raceName,raceDescription,null,hpModifier,isEnabled);
        dbDungeonEditing.addNewAvatarRace(dungeonName,ar);
    }

    @Override
    public void editAvatarRace(String dungeonName, String raceName, String newRaceDescription, int newHPModifier, boolean isEnabled, long globalID) throws ElementNotFoundException {
        AvatarRace ar = new AvatarRace(globalID,raceName,newRaceDescription,null,newHPModifier,isEnabled);
        dbDungeonEditing.updateExistingAvatarRace(dungeonName,ar);
    }

    @Override
    public void createRoom(String dungeonName, String displayRoom, String description) throws ElementNotFoundException {
        createRoom(dungeonName,displayRoom,description,-1,-1,-1,-1);
    }

    @Override
    public void createRoom(String dungeonName, String displayRoom, String description, long roomNorthGlobalID, long roomEastGlobalID, long roomSouthGlobalID, long roomWestGlobalID) throws ElementNotFoundException {
        DungeonRoom dr = new DungeonRoom(displayRoom, -1, description,roomNorthGlobalID,roomSouthGlobalID,roomEastGlobalID,roomWestGlobalID);
        dbRoomCreation.addNewRoom(dungeonName,dr);
    }

    @Override
    public long addNewObjectTemplate(String dungeonName, APIObjectTemplate objectTemplate) throws ElementNotFoundException {
        return dbDungeonEditing.addNewObjectTemplate(dungeonName, new ObjectTemplate(-1, objectTemplate.name, objectTemplate.fullDescription, objectTemplate.isPickupable));
    }

    @Override
    public void updateExistingObjectTemplate(String dungeonName, APIObjectTemplate objectTemplate, long templateGlobalId) throws ElementNotFoundException {
        dbDungeonEditing.updateExistingObjectTemplate(dungeonName, objectTemplate);
    }

    @Override
    public long addNewCustomAction(String dungeonName, APICustomAction customAction) throws ElementNotFoundException {
        return dbDungeonEditing.addNewCustomAction(dungeonName, new CustomAction(customAction.globalID, customAction.priority, customAction.name, customAction.command, customAction.response));
    }

    @Override
    public void updateExistingCustomAction(String dungeonName, APICustomAction customAction) throws ElementNotFoundException {
        dbDungeonEditing.updateExistingCustomAction(dungeonName, new CustomAction(customAction.globalID, customAction.priority, customAction.name, customAction.command, customAction.response));
    }

    @Override
    public void deleteDungeon(String dungeonName) throws ElementNotFoundException {
        dbDungeonEditing.deleteDungeon(dungeonName);
    }
}
