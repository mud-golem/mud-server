package Processing;

import DataBase.DBDungeonQueryImpl;
import DataBase.DBPlayerCreationImpl;
import DatabaseInterfaces.DBDungeonQuery;
import Exceptions.ElementNotFoundException;
import Model.Base.AvatarClass;
import Model.Base.AvatarRace;
import Model.Base.InventoryEntry;
import Model.Base.PlayerAvatar;
import ServerInterfaces.PlayerCreation;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.ArrayList;

@ApplicationScoped
public class PlayerCreationImpl implements PlayerCreation {

    @Inject
    DBPlayerCreationImpl dbPlayerCreation;

    @Inject
    DBDungeonQueryImpl dbDungeonQuerry;



    //return dbPlayerCreation.createPlayerCharacter(dungeonName,playerAvatar);

    @Override
    public long createPlayerCharacter(String dungeonName, String ownerUserName, String avatarName, long classGlobalID, long raceGlobalID, String userDescription) throws ElementNotFoundException {
        AvatarClass aclass = dbDungeonQuerry.getAvatarClass(dungeonName, classGlobalID);
        AvatarRace arace= dbDungeonQuerry.getAvatarRace(dungeonName,raceGlobalID);
        int maxHP = aclass.getHpModifier()+arace.getHpModifier();

        PlayerAvatar pa = new PlayerAvatar(-1,dungeonName,avatarName,userDescription,maxHP,maxHP,false,ownerUserName,-1, new ArrayList<InventoryEntry>(), new AvatarRace(raceGlobalID), new AvatarClass(classGlobalID));

        return dbPlayerCreation.createPlayerCharacter(dungeonName,pa);
    }

    @Override
    public boolean deletePlayerCharacter(String dungeonName, long playerAvatarScopedID) throws ElementNotFoundException {
        return dbPlayerCreation.deletePlayerCharacter(dungeonName,playerAvatarScopedID);
    }
}
