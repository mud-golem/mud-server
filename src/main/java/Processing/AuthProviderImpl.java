package Processing;

import DataBase.DBUserManagerImpl;
import DatabaseInterfaces.DBUserManager;
import Exceptions.ElementNotFoundException;
import Exceptions.EmailTakenException;
import Exceptions.UserNameTakenException;
import ServerInterfaces.AuthProvider;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Singleton;


@ApplicationScoped
public class AuthProviderImpl implements AuthProvider {
    private DBUserManager dbAccessor;

    @Inject
    public void setDbAccessor(DBUserManagerImpl dbc){this.dbAccessor = dbc;}

    @Override
    public boolean validateUserCredentials(String UserName, String PWHashed) throws ElementNotFoundException {
        return dbAccessor.validateUserCredentials(UserName,PWHashed);
    }

    @Override
    public boolean registerUser(String UserName, String PWHashed, String eMail)throws UserNameTakenException, EmailTakenException {
        return dbAccessor.registerUser(UserName,PWHashed, eMail);
    }

    @Override
    public void changeUserPassword(String username, String pwHashedNew) throws ElementNotFoundException {
        dbAccessor.changeUserPassword(username,pwHashedNew);
    }

    @Override
    public String getEmail(String username) throws ElementNotFoundException {
        return dbAccessor.getEmail(username);
    }
}
