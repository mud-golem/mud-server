package Processing;

import DataBase.*;
import DatabaseInterfaces.*;
import Exceptions.ElementNotFoundException;
import Model.Base.*;
import Model.CommunicationModel.APIDungeon;
import Model.CommunicationModel.APIDungeonRoom;
import Model.CommunicationModel.APIObjectTemplate;
import ServerInterfaces.DungeonCreation;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

@ApplicationScoped
public class DungeonCreationImpl implements DungeonCreation {
    private DBDungeonCreation dbDungeonCreation;
    private DBDungeonQuery dbDungeonQuery;
    private DBRoomQuery dbRoomQuery;
    private DBUserManager userManager;
    private DBDungeonEditing dbDungeonEditing;
    private DBRoomCreation dbRoomCreation;
    private DBRoomEditing dbRoomEditing;
    private DBDungeonManagement dbDungeonManagement;


    @Inject
    public void setDbDungeonManagement(DBDungeonManagementImpl dbDungeonManagement) {
        this.dbDungeonManagement = dbDungeonManagement;
    }

    @Inject
    public void setDbRoomEditing(DBRoomEditingImpl dbRoomEditing) {
        this.dbRoomEditing = dbRoomEditing;
    }

    @Inject
    public void setDbRoomCreation(DBRoomCreationImpl dbRoomCreation) {
        this.dbRoomCreation = dbRoomCreation;
    }

    @Inject
    public void setDbDungeonEditing(DBDungeonEditingImpl dbDungeonEditing) {
        this.dbDungeonEditing = dbDungeonEditing;
    }

    @Inject
    public void setDbRoomQuery(DBRoomQueryImpl dbRoomQuery) {
        this.dbRoomQuery = dbRoomQuery;
    }

    @Inject
    public void setDbDungeonQuery(DBDungeonQueryImpl dbDungeonQuery) {
        this.dbDungeonQuery = dbDungeonQuery;
    }

    @Inject
    public void setDbDungeonCreation(DBDungeonCreationImpl dbAccessor) {
        this.dbDungeonCreation = dbAccessor;
    }

    @Inject
    public void setUserManager(DBUserManagerImpl dbUserManager) {
        this.userManager = dbUserManager;
    }

    @Override
    public boolean createDungeon(String usernameDM, String dungeonName, String lobbyDescription) throws ElementNotFoundException {
        Dungeon dungeon = new Dungeon();
        dungeon.setUniqueName(dungeonName);

        dungeon.setDungeonDescription("empty Description");
        dungeon.setLobbyDescription(lobbyDescription);

        dungeon.setUnknownActionResponse("empty unknown action response");

        dungeon.addUserToAllowedDMs(usernameDM);

        dungeon.setDungeonMasterUserName(usernameDM);
        dungeon.setJoinable(false);
        //TODO

        DungeonRoom dummyRoom = new DungeonRoom();
        dummyRoom.setDisplayName("Default Room Name");
        dummyRoom.setDescription("Default Room Description");
        dummyRoom.setGlobalID(4312);

        dungeon.addRoomToDungeonRooms(dummyRoom);
        //dungeon.setStartRoomGlobalID(null);

        dbDungeonCreation.createDungeon(dungeon);
        return true;
    }

    @Override
    public boolean copyDungeon(String usernameDM, String dungeonNameToCopy, String dungeonNameNew) throws ElementNotFoundException {
        APIDungeon oldApiDungeon = dbDungeonQuery.getAPIDungeon(dungeonNameToCopy);
        Dungeon dungeon = new Dungeon();

        dungeon.setUniqueName(dungeonNameNew);
        dungeon.setDungeonDescription(oldApiDungeon.dungeonDescription);
        dungeon.setLobbyDescription(oldApiDungeon.lobbyDescription);
        dungeon.setUnknownActionResponse(oldApiDungeon.unknownActionResponse);
        dungeon.setDungeonMasterUserName(usernameDM);
        dungeon.addUserToAllowedDMs(usernameDM);

        dbDungeonCreation.createDungeon(dungeon);


        for (AvatarClass avatarClass : dbDungeonQuery.getAvatarClasses(dungeonNameToCopy)) {
            dbDungeonEditing.addNewAvatarClass(dungeonNameNew, avatarClass);
        }
        for (AvatarRace avatarRace : dbDungeonQuery.getAvatarRaces(dungeonNameToCopy)) {
            dbDungeonEditing.addNewAvatarRace(dungeonNameNew, avatarRace);
        }


        List<DungeonRoom> rooms = dbRoomQuery.getRooms(dungeonNameToCopy);

        HashMap<Long, Long> idTranslationRooms = new HashMap<>();
        for (DungeonRoom r : rooms) {
            long temp = dbRoomCreation.addNewRoom(dungeonNameNew, new DungeonRoom(r.getDisplayName(), r.getDescription()));
            idTranslationRooms.put(r.getGlobalID(), temp);
        }
        for (DungeonRoom r : rooms) {
            if (r.getRoomNorthGlobalID() != -1)
                dbRoomEditing.setRoomsConnection(RoomDirection.NORTH
                        , idTranslationRooms.get(r.getGlobalID())
                        , idTranslationRooms.get(r.getRoomNorthGlobalID()));
            if (r.getRoomSouthGlobalID() != -1)
                dbRoomEditing.setRoomsConnection(RoomDirection.SOUTH
                        , idTranslationRooms.get(r.getGlobalID())
                        , idTranslationRooms.get(r.getRoomSouthGlobalID()));
            if (r.getRoomEastGlobalID() != -1)
                dbRoomEditing.setRoomsConnection(RoomDirection.EAST
                        , idTranslationRooms.get(r.getGlobalID())
                        , idTranslationRooms.get(r.getRoomEastGlobalID()));
            if (r.getRoomWestGlobalID() != -1)
                dbRoomEditing.setRoomsConnection(RoomDirection.WEST
                        , idTranslationRooms.get(r.getGlobalID())
                        , idTranslationRooms.get(r.getRoomWestGlobalID()));
        }


        //Update start room and delete the default startroom in the new dungeon
        long defaultStartRoomIdToDelete = dbDungeonQuery.getStartRoomGlobalID(dungeonNameNew);
        APIDungeon tempDungeon1 = dbDungeonQuery.getAPIDungeon(dungeonNameNew);

        tempDungeon1.startRoomScopedID = idTranslationRooms.get(oldApiDungeon.startRoomScopedID);
        System.out.println("new startroomID : "+tempDungeon1.startRoomScopedID);
        dbDungeonManagement.setDungeon(tempDungeon1);
        dbRoomEditing.deleteRoom(defaultStartRoomIdToDelete);


        List<ObjectTemplate> objects = dbDungeonQuery.getObjectTemplates(dungeonNameToCopy);
        HashMap<Long, Long> idTranslationObjects = new HashMap<>();
        for (ObjectTemplate template : objects) {
            long temp = dbDungeonEditing.addNewObjectTemplate(dungeonNameNew,
                    new ObjectTemplate(-1, template.getDisplayName(), template.getFullDescription(), template.isPickupable()));
            idTranslationObjects.put(template.getGlobalID(), temp);
        }
        for (DungeonRoom room : rooms) {
            for (InventoryEntry ie : room.getInventory()) {
                dbRoomEditing.addInventoryEntry(dungeonNameNew, idTranslationRooms.get(room.getGlobalID()),
                        new InventoryEntry(new ObjectTemplate(idTranslationObjects.get(ie.getTemplateGlobalID())), ie.getCount()));

            }
        }
        List<CustomAction> actions = dbDungeonQuery.getAllCustomActions(dungeonNameToCopy);
        HashMap<Long, Long> idTranslationActions = new HashMap<>();
        for (CustomAction action : actions) {
            long temp = dbDungeonEditing.addNewCustomAction(dungeonNameNew, new CustomAction(-1, action.getPriority(),
                    action.getActionName(), action.getCommand(), action.getResponse()));
            idTranslationActions.put(action.getGlobalActionId(), temp);
        }

        //bind dungeonwide customActions
        APIDungeon tempDungeon = dbDungeonQuery.getAPIDungeon(dungeonNameNew);
        tempDungeon.actionIDs = oldApiDungeon.actionIDs.stream().map(id -> idTranslationActions.get(id)).collect(Collectors.toList());
        dbDungeonManagement.setDungeon(tempDungeon);

        for (DungeonRoom room : rooms) {
            APIDungeonRoom adr = new APIDungeonRoom(dbRoomQuery.getDungeonRoom(dungeonNameNew, idTranslationRooms.get(room.getGlobalID())));
            adr.actionIDs = room.getCustomActions().stream()
                    .map(customAction -> idTranslationActions.get(customAction.getGlobalActionId())).collect(Collectors.toList());
            dbRoomEditing.setAPIRoom(adr);
        }

        for (ObjectTemplate template : objects) {
            ObjectTemplate ot = dbDungeonQuery.getObjectTemplate(dungeonNameNew, idTranslationObjects.get(template.getGlobalID()));
            APIObjectTemplate objectTemplate = new APIObjectTemplate(ot);

            List<CustomAction> newCustomActionList = template.getCustomActionList().stream()
                    .map(customAction -> new CustomAction(idTranslationActions.get(customAction.getGlobalActionId()),
                            customAction.getPriority(), customAction.getActionName(), customAction.getCommand(),
                            customAction.getResponse())).collect(Collectors.toList());

            objectTemplate.actionIDs = (newCustomActionList.stream().map(customAction -> customAction.getGlobalActionId()).collect(Collectors.toList()));
            dbDungeonEditing.updateExistingObjectTemplate(dungeonNameNew, objectTemplate);
        }

        return true;
    }

    @Override
    public boolean deleteDungeon(String usernameDM, String dungeonName) throws ElementNotFoundException {
        dbDungeonCreation.deleteDungeon(dungeonName);
        return true;
    }

    @Override
    public boolean importDungeon(String usernameDM, String dungeonSerializedString) {
        return false;
    }
}
