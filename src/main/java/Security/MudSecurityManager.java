package Security;

import ChatManaging.CoreChatManager;
import Exceptions.AccessDeniedException;
import Exceptions.ElementNotFoundException;
import Model.SecurityModel.MudUser;
import Model.SecurityModel.ResetPasswordObject;
import Model.SecurityModel.UserRegistrationObject;
import Processing.AuthProviderImpl;
import ServerInterfaces.AuthProvider;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.quarkus.runtime.StartupEvent;
import org.jboss.logging.Logger;

import javax.crypto.Cipher;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Optional;

@ApplicationScoped
/**
 * @author Jan Stöffler
 * The MudSecurityManager is needed for the Registration and the Authentication of Users
 */
public class MudSecurityManager {

    private static final Logger LOG = Logger.getLogger(MudSecurityManager.class);

    //needs AuthProvider to validate Data
    private AuthProvider authProvider;

    @Inject
    public void setAuthProvider(AuthProviderImpl authProvider){
        this.authProvider = authProvider;
    }

    @Inject
    CoreChatManager coreChatManager;

    //The following constants are used for encryption and decryption of user data, will be moved to a seperate file soon
    private static final int    GCM_NONCE_LENGTH            = 12;
    private static final int    GCM_TAG_LENGTH              = 16;
    private static final String COOKIE_ENC_KEY              = "w/h2FlPm0H293Gfw";
    private static final String COOKIE_ENC_ALGORITHM        = "AES";
    private static final String COOKIE_ENC_TRANSFORMATION   = "AES/GCM/NoPadding";

    //caches the current User
    private ThreadLocal<MudUser> currentUser;

    public void initialize(@Observes StartupEvent startupEvent) {
        currentUser = new ThreadLocal<>();
    }

    public void initialize(){
        currentUser = new ThreadLocal<>();
    }

    /**
     * @author Jan Stöffler
     * this method return a securityKey if the user can be valoidated, else throw AccesDeniedException
     * @param userName
     * @param sha256
     * @return String
     * @throws AccessDeniedException
     */
    public String verifyUserLoginAndGetSecurityKey(String userName, String sha256) throws AccessDeniedException{
        try{
            return Optional.ofNullable(verifyAndSetCurrentUser(userName,sha256))
                    .map(this::encryptUser).orElseThrow(AccessDeniedException::new);
        } catch(Exception e){
            LOG.info(e.toString(), e);
            throw new AccessDeniedException();
        }
    }

    /**
     * @author Jan Stöffler
     * returns the securityKey
     * @param userName
     * @param sha256
     * @return String
     * @throws AccessDeniedException
     */
    public String getSecurityKey(String userName, String sha256) throws AccessDeniedException{
        try{
            return Optional.ofNullable(MudUser.newUser(userName,sha256)).map(this::encryptUser)
                    .orElseThrow(AccessDeniedException::new);
        } catch(Exception e){
            throw new AccessDeniedException();
        }
    }

    /**
     * @author Jan Stöffler
     * decrypts a securityKey to a MUDUser and sets it as current user
     * @param securityKey
     * @throws AccessDeniedException
     */
    public void setCurrentUserFromSecurityKey(String securityKey) throws AccessDeniedException {
        final MudUser mudUser = decryptUser(securityKey);
        currentUser.set(mudUser);
    }

    public void clearCurrentUser() {
        currentUser.remove();
    }

    public void setCurrentUser(MudUser mudUser){
        currentUser.set(mudUser);
    } 

    /**
     * @author Jan Stöffler
     * verifies a credentials and sets the current user from them
     * @param userName
     * @param sha256
     * @return
     */
    private MudUser verifyAndSetCurrentUser(String userName, String sha256){
        try {
            MudUser mudUser = null;
            boolean temp = authProvider.validateUserCredentials(userName, sha256);
            LOG.info(temp);
            if (temp) {
                mudUser = MudUser.newUser(userName, sha256);
            }
            if (mudUser == null) {
                //No such User
            } else if (!mudUser.getSha256().equals(sha256)) {

            } else {
                currentUser.set(mudUser);
                return mudUser;
            }

            return null;
        }catch(ElementNotFoundException e){
            return null;
        }
    }

    /**
     * @author Jan Stöffler
     * decryopts a mudUser to a securityKey
     * @param mudUser
     * @return key
     */
    private String encryptUser(MudUser mudUser){
        try{
            final ObjectMapper mapper = new ObjectMapper();
            final byte[] bytesToEncrypt = mapper.writeValueAsBytes(mudUser);

            final byte[] keyBits = COOKIE_ENC_KEY.getBytes(StandardCharsets.UTF_8);
            final SecretKeySpec keySpec = new SecretKeySpec(keyBits, COOKIE_ENC_ALGORITHM);
            final Cipher cipher = Cipher.getInstance(COOKIE_ENC_TRANSFORMATION);

            final byte[] nonce = new byte[GCM_NONCE_LENGTH];
            final SecureRandom random = new SecureRandom();
            random.nextBytes(nonce);
            final GCMParameterSpec spec = new GCMParameterSpec(GCM_TAG_LENGTH * 8, nonce);

            cipher.init(Cipher.ENCRYPT_MODE, keySpec, spec);

            final byte[] cipherText = cipher.doFinal(bytesToEncrypt);

            final ByteBuffer byteBuffer = ByteBuffer.allocate(4 + nonce.length + cipherText.length);
            byteBuffer.putInt(nonce.length);
            byteBuffer.put(nonce);
            byteBuffer.put(cipherText);
            final byte[] cipherMessage = byteBuffer.array();

            Arrays.fill(nonce, (byte) 0); // wipe the nonce

            // Apply Base64 and URL encoding
            return URLEncoder.encode(java.util.Base64.getEncoder().encodeToString(cipherMessage), "UTF-8");
        } catch (Exception e) {
            LOG.info(e.toString(), e);
            return null;
        }
    }

    /**
     * @author Jan Stöffler
     * decrypts a security Key to a MudUser
     * @param encrypted
     * @return MudUser
     * @throws AccessDeniedException
     */
    public MudUser decryptUser(String encrypted) throws AccessDeniedException {
        try {
            final String decoded = URLDecoder.decode(encrypted, "UTF-8");
            final ByteBuffer byteBuffer = ByteBuffer.wrap(java.util.Base64.getDecoder().decode(decoded));
            final int nonceLength = byteBuffer.getInt();
            if (nonceLength < 12 || nonceLength >= 16) { // check input parameter
                throw new IllegalArgumentException("invalid nonce length");
            }

            final byte[] nonce = new byte[nonceLength];
            byteBuffer.get(nonce);
            final byte[] cipherText = new byte[byteBuffer.remaining()];
            byteBuffer.get(cipherText);

            final byte[] keyBits = COOKIE_ENC_KEY.getBytes(StandardCharsets.UTF_8);
            final SecretKeySpec keySpec = new SecretKeySpec(keyBits, COOKIE_ENC_ALGORITHM);
            final Cipher cipher = Cipher.getInstance(COOKIE_ENC_TRANSFORMATION);
            cipher.init(Cipher.DECRYPT_MODE, keySpec, new GCMParameterSpec(GCM_TAG_LENGTH * 8, nonce));

            final byte[] decrypted = cipher.doFinal(cipherText);

            return new ObjectMapper().readValue(decrypted, MudUser.class);
        } catch (Exception e) {
            throw new AccessDeniedException();
        }
    }

    /**
     * @author Jan Stöffler
     * decrypts a RegistrationObject to a securityKey
     * @param registrationObject
     * @return key
     */
    public String encryptRegistrationObject(UserRegistrationObject registrationObject){
        try{
            final ObjectMapper mapper = new ObjectMapper();
            final byte[] bytesToEncrypt = mapper.writeValueAsBytes(registrationObject);

            final byte[] keyBits = COOKIE_ENC_KEY.getBytes(StandardCharsets.UTF_8);
            final SecretKeySpec keySpec = new SecretKeySpec(keyBits, COOKIE_ENC_ALGORITHM);
            final Cipher cipher = Cipher.getInstance(COOKIE_ENC_TRANSFORMATION);

            final byte[] nonce = new byte[GCM_NONCE_LENGTH];
            final SecureRandom random = new SecureRandom();
            random.nextBytes(nonce);
            final GCMParameterSpec spec = new GCMParameterSpec(GCM_TAG_LENGTH * 8, nonce);

            cipher.init(Cipher.ENCRYPT_MODE, keySpec, spec);

            final byte[] cipherText = cipher.doFinal(bytesToEncrypt);

            final ByteBuffer byteBuffer = ByteBuffer.allocate(4 + nonce.length + cipherText.length);
            byteBuffer.putInt(nonce.length);
            byteBuffer.put(nonce);
            byteBuffer.put(cipherText);
            final byte[] cipherMessage = byteBuffer.array();

            Arrays.fill(nonce, (byte) 0); // wipe the nonce

            // Apply Base64 and URL encoding
            return URLEncoder.encode(java.util.Base64.getEncoder().encodeToString(cipherMessage), "UTF-8");
        } catch (Exception e) {
            return null;
        }
    }

    public UserRegistrationObject decryptRegistrationObject(String encrypted) throws AccessDeniedException {
        try {
            final String decoded = URLDecoder.decode(encrypted, "UTF-8");
            final ByteBuffer byteBuffer = ByteBuffer.wrap(java.util.Base64.getDecoder().decode(decoded));
            final int nonceLength = byteBuffer.getInt();
            if (nonceLength < 12 || nonceLength >= 16) { // check input parameter
                throw new IllegalArgumentException("invalid nonce length");
            }

            final byte[] nonce = new byte[nonceLength];
            byteBuffer.get(nonce);
            final byte[] cipherText = new byte[byteBuffer.remaining()];
            byteBuffer.get(cipherText);

            final byte[] keyBits = COOKIE_ENC_KEY.getBytes(StandardCharsets.UTF_8);
            final SecretKeySpec keySpec = new SecretKeySpec(keyBits, COOKIE_ENC_ALGORITHM);
            final Cipher cipher = Cipher.getInstance(COOKIE_ENC_TRANSFORMATION);
            cipher.init(Cipher.DECRYPT_MODE, keySpec, new GCMParameterSpec(GCM_TAG_LENGTH * 8, nonce));

            final byte[] decrypted = cipher.doFinal(cipherText);

            return new ObjectMapper().readValue(decrypted, UserRegistrationObject.class);
        } catch (Exception e) {
            LOG.info(e.toString(),e);
            throw new AccessDeniedException();
        }
    }

    /**
     * @author Jan Stöffler
     * decryopts a ResetPasswordObject to a securityKey
     * @param reset
     * @return key
     */
    public String encryptResetPasswordObject(ResetPasswordObject reset){
        try{
            final ObjectMapper mapper = new ObjectMapper();
            final byte[] bytesToEncrypt = mapper.writeValueAsBytes(reset);

            final byte[] keyBits = COOKIE_ENC_KEY.getBytes(StandardCharsets.UTF_8);
            final SecretKeySpec keySpec = new SecretKeySpec(keyBits, COOKIE_ENC_ALGORITHM);
            final Cipher cipher = Cipher.getInstance(COOKIE_ENC_TRANSFORMATION);

            final byte[] nonce = new byte[GCM_NONCE_LENGTH];
            final SecureRandom random = new SecureRandom();
            random.nextBytes(nonce);
            final GCMParameterSpec spec = new GCMParameterSpec(GCM_TAG_LENGTH * 8, nonce);

            cipher.init(Cipher.ENCRYPT_MODE, keySpec, spec);

            final byte[] cipherText = cipher.doFinal(bytesToEncrypt);

            final ByteBuffer byteBuffer = ByteBuffer.allocate(4 + nonce.length + cipherText.length);
            byteBuffer.putInt(nonce.length);
            byteBuffer.put(nonce);
            byteBuffer.put(cipherText);
            final byte[] cipherMessage = byteBuffer.array();

            Arrays.fill(nonce, (byte) 0); // wipe the nonce

            // Apply Base64 and URL encoding
            return URLEncoder.encode(java.util.Base64.getEncoder().encodeToString(cipherMessage), "UTF-8");
        } catch (Exception e) {
            return null;
        }
    }

    public ResetPasswordObject decryptResetPasswordObject(String encrypted) throws AccessDeniedException {
        try {
            final String decoded = URLDecoder.decode(encrypted, "UTF-8");
            final ByteBuffer byteBuffer = ByteBuffer.wrap(java.util.Base64.getDecoder().decode(decoded));
            final int nonceLength = byteBuffer.getInt();
            if (nonceLength < 12 || nonceLength >= 16) { // check input parameter
                throw new IllegalArgumentException("invalid nonce length");
            }

            final byte[] nonce = new byte[nonceLength];
            byteBuffer.get(nonce);
            final byte[] cipherText = new byte[byteBuffer.remaining()];
            byteBuffer.get(cipherText);

            final byte[] keyBits = COOKIE_ENC_KEY.getBytes(StandardCharsets.UTF_8);
            final SecretKeySpec keySpec = new SecretKeySpec(keyBits, COOKIE_ENC_ALGORITHM);
            final Cipher cipher = Cipher.getInstance(COOKIE_ENC_TRANSFORMATION);
            cipher.init(Cipher.DECRYPT_MODE, keySpec, new GCMParameterSpec(GCM_TAG_LENGTH * 8, nonce));

            final byte[] decrypted = cipher.doFinal(cipherText);

            return new ObjectMapper().readValue(decrypted, ResetPasswordObject.class);
        } catch (Exception e) {
            LOG.info(e.toString(),e);
            throw new AccessDeniedException();
        }
    }

    public MudUser getCurrentUser(){
        return currentUser.get();
    }

    /**
     * @author Jan Stöffler
     * checks if user is in the specified dungeon
     * @param dungeonName
     * @return
     */
    public boolean isUserInDungeon(String dungeonName){
        String userName = currentUser.get().getUserName();
        return coreChatManager.getDungeonPlayerMap().get(dungeonName).containsKey(userName);
    }

    /**
     * @author Jan Stöffler
     * checks if user is master in specified in dungeon
     * @param dungeonName
     * @return
     */
    public boolean isUserMasterInDungeon(String dungeonName){
        String userName = currentUser.get().getUserName();
        return coreChatManager.isMasterInDungeon(dungeonName,userName);
    }
}
