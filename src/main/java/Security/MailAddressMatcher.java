package Security;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Jan Stöffler
 * This class provides a method to check if a mailAddress is  has a valid syntax
 */
public class MailAddressMatcher {
    public static final Pattern VALID_MAIL_ADDRESS =
            Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    /**
     * @author Jan Stöffler
     * confirms the syntax of a mail address
     * @param mailAddress
     * @return
     */
    public static boolean validateMailAddress(String mailAddress){
        Matcher matcher = VALID_MAIL_ADDRESS.matcher(mailAddress);
        return matcher.find();
    }
}
