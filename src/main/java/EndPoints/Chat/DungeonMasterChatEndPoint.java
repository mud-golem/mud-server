package EndPoints.Chat;

import ChatManaging.CoreChatManager;
import Exceptions.ElementNotFoundException;
import Model.CommunicationModel.Message;
import Model.CommunicationModel.MessageType;
import Model.SecurityModel.MudUser;
import Exceptions.AccessDeniedException;
import Security.MudSecurityManager;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jboss.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.ArrayList;
import java.util.stream.Collectors;

@ServerEndpoint("/dungeonMaster/chat/{dungeonName}/{securityKey}")
@ApplicationScoped
/**
 * The ChatEndpoint provides Websockets for DMs
 * ©Jan Stöffler
 */
public class DungeonMasterChatEndPoint {

    private static final Logger LOG = Logger.getLogger(DungeonMasterChatEndPoint.class);

    @Inject
    CoreChatManager coreChatManager;

    @Inject
    MudSecurityManager mudSecurityManager;


    @OnOpen
    public void onOpen(Session session,
                       @PathParam("dungeonName") String dungeonName,
                       @PathParam("securityKey") String securityKey) {
        try {
            MudUser mudUser = mudSecurityManager.decryptUser(securityKey);
            coreChatManager.joinAsMaster(session, dungeonName, mudUser.getUserName());
            LOG.info(mudUser.getUserName().concat(" joined the Chat"));
        }catch(AccessDeniedException | ElementNotFoundException e){
            LOG.info(e.toString(),e);
            try {
                session.close(new CloseReason(CloseReason.CloseCodes.CANNOT_ACCEPT, "AccessDenied"));
            }catch(IOException io){
                LOG.info(io.toString(),io);
            }
        }
    }

    @OnClose
    public void onClose(Session session,
                        @PathParam("dungeonName") String dungeonName,
                        @PathParam("securityKey") String securityKey) {
        try{
            MudUser mudUser = mudSecurityManager.decryptUser(securityKey);
            coreChatManager.remove(dungeonName, mudUser.getUserName());
            broadcast(generateLeaveMessage(),dungeonName);
            LOG.info(mudUser.getUserName().concat(" left the Chat"));
        }catch(AccessDeniedException | ElementNotFoundException | IOException e){
            LOG.info(e.toString(),e);
        }
    }

    @OnError
    public void onError(Session session,
                        @PathParam("dungeonName") String dungeonName,
                        @PathParam("securityKey") String securityKey,
                        Throwable throwable) {
        try {
            MudUser mudUser = mudSecurityManager.decryptUser(securityKey);
            coreChatManager.remove(dungeonName, mudUser.getUserName());
            broadcast(generateLeaveMessage(),dungeonName);
            session.close();
            LOG.info(throwable.toString(),throwable);
            LOG.info("User " + mudUser.getUserName() + " left on error: " + throwable);
        }catch(AccessDeniedException | IOException | ElementNotFoundException e){
            LOG.info(e.toString(),e);
        }
    }

    @OnMessage
    public void onMessage(String message,
                          @PathParam("dungeonName") String dungeonName,
                          @PathParam("securityKey") String securityKey) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            MudUser mudUser = mudSecurityManager.decryptUser(securityKey);
            try {
                Message msg = mapper.readValue(message, Message.class);
                msg.sender = "DM";
                if(msg.recipient != null) {
                    String recipient = coreChatManager.getAvatarDungeonMap().get(dungeonName).get(msg.recipient);
                    msg.recipient = recipient;
                }
                switch (msg.type) {
                    case RoomSystemMessage:
                    case RoomChatMessage:
                        roomBroadcast(msg, dungeonName,mudUser.getUserName());
                        break;
                    case WhisperSystemMessage:
                    case WhisperChatMessage:
                        whisper(msg, dungeonName,mudUser.getUserName());
                        break;
                    case DungeonChatMessage:
                    case DungeonSystemMessage:
                        broadcast(msg, dungeonName);
                        break;
                    default:
                        //TODO implement
                }
            } catch (JsonProcessingException j) {
                LOG.info(j.toString(),j);
            }
        }catch(AccessDeniedException e){
            LOG.info(e.toString(),e);
        }
    }

    /**
     * @author Jan Stöffler
     * broadcast() sends the message to all Users, who are in the same dungeon as the Server
     * @param message which was sent
     * @param dungeonName of the Sender's dungeon
     */
    private void broadcast(Message message, String dungeonName) {
        coreChatManager.getDungeonPlayerMap().get(dungeonName)
                .values()
                .forEach(s -> {
            s.getAsyncRemote()
                    .sendObject(message.toJson(), result -> {
                if (result.getException() != null) {
                    System.out.println("Unable to send message: " + result.getException());
                }
            });
        });
    }

    /**
     * @author Jan Stöffler
     * whisper() sends the message to a specified User in the same dungeon as the DM
     * @param message which was sent
     * @param dungeonName of the Sender's dungeon
     */
    private void whisper(Message message, String dungeonName, String username) {
        coreChatManager
                .getDungeonPlayerMap()
                .get(dungeonName)
                .get(message.recipient)
                .getAsyncRemote()
                .sendObject(message.toJson(), result -> {
            if (result.getException() != null) {
                System.out.println("Unable to send message: " + result.getException());
            }
        });
        coreChatManager.getDungeonPlayerMap().get(dungeonName).get(username).getAsyncRemote().sendObject(message.toJson(), result -> {
            if (result.getException() != null) {
                System.out.println("Unable to send message: " + result.getException());
            }
        });
    }

    /**
     * @author Jan Stöffler
     * roomBroadcast() sends the message to all Users in a sepcified room (room is specified in Message Object)
     * @param message which was sent
     * @param dungeonName of the Sender's dungeon
     */
    public void roomBroadcast(Message message, String dungeonName, String username) {
        if(coreChatManager.getDungeonRoomPlayerMap().get(dungeonName)!= null) {
            if (coreChatManager.getDungeonRoomPlayerMap().get(dungeonName).get(Long.parseLong(message.room)) != null) {
                String roomName = "";
                if(coreChatManager.getDungeonRoomPlayerMap().get(dungeonName) != null
                        || coreChatManager.getDungeonRoomPlayerMap().get(dungeonName)
                            .get(Long.parseLong(message.room)) !=null) {
                    String userName = new ArrayList<>(coreChatManager.getDungeonRoomPlayerMap()
                            .get(dungeonName)
                            .get(Long.parseLong(message.room))
                            .keySet())
                            .get(0);
                    roomName = coreChatManager.getUserRoomMap().get(dungeonName).get(userName);
                } else {
                    roomName = message.actionName;
                }
                long remRoom = Long.parseLong(message.room);
                String temp = roomName;
                coreChatManager
                        .getDungeonRoomPlayerMap()
                        .get(dungeonName)
                        .get(remRoom)
                        .values()
                        .forEach(session -> {
                            message.room = temp;
                            session.getAsyncRemote()
                                    .sendObject(message.toJson(), result -> {
                                if (result.getException() != null) {
                                    System.out.println("Unable to send message: " + result.getException());
                                }
                            });
                        });
            }
        }
        message.room = message.actionName;
        coreChatManager.getDungeonPlayerMap().get(dungeonName).get(username).getAsyncRemote().sendObject(message.toJson(), result -> {
            if (result.getException() != null) {
                System.out.println("Unable to send message: " + result.getException());
            }
        });
    }

    /**
     * @author Jan Stöffler
     * generateLeaveMessage generates the message which will be shown if a DM is not available
     * @return Message
     */
    private Message generateLeaveMessage(){
        Message msg = new Message();
        msg.type = MessageType.DungeonSystemMessage;
        msg.content = "A God left your dungeon. Henceforth you will get no further guidance until he returns";
        return msg;
    }
}
