package EndPoints.Chat;

import ChatManaging.CoreChatManager;
import ChatManaging.SessionManager;
import Exceptions.AccessDeniedException;
import Model.CommunicationModel.Message;
import Model.SecurityModel.MudUser;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.ArrayList;

@ServerEndpoint("/server/chat")
@ApplicationScoped
/**
 * @author Jan Stöffler
 * The ServerChatEndPoint is the connection for the backend to the Chatsystem
 */
public class ServerChatEndPoint {
    @Inject
    CoreChatManager coreChatManager;

    @OnOpen
    public void onOpen(Session session) {
    }

    @OnClose
    public void onClose(Session session) {
    }

    @OnError
    public void onError(Session session, Throwable throwable) {
    }

    @OnMessage
    public void onMessage(String message) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            Message msg = mapper.readValue(message,Message.class);
            switch (msg.type) {
                case RoomSystemMessage:
                    roomBroadcast(msg, msg.dungeonName);
                    break;
                case WhisperSystemMessage:
                    whisper(msg, msg.dungeonName);
                    break;
                case DungeonSystemMessage:
                    broadcast(msg, msg.dungeonName);
                    break;
                case ActionAlertMessage:
                    whisperDM(msg,msg.dungeonName);
                    break;
                default:

            }
        }catch(Exception e){
            //TODO implement
        }
    }

    /**
     * @author Jan Stöffler
     * whisperDM sends a message only to the DM
     * @param message
     * @param dungeonName
     */
    private void whisperDM(Message message, String dungeonName){
        if (coreChatManager.getAvatarDungeonMap().get(dungeonName) != null
                && coreChatManager.getMasterMap().get(dungeonName) != null) {
            String masterName = coreChatManager.getMasterMap().get(dungeonName);
            if (coreChatManager.getDungeonPlayerMap().get(dungeonName).get(masterName) != null) {
                coreChatManager.getDungeonPlayerMap().get(dungeonName).get(masterName)
                        .getAsyncRemote()
                        .sendObject(message.toJson(), result -> {
                    if (result.getException() != null) {
                        System.out.println("Unable to send message: " + result.getException());
                    }
                });
            }
        }
    }

    /**
     * @author Jan Stöffler
     * broadcast sends a message to all users in a dungeon
     * @param message
     * @param dungeonName
     */
    private void broadcast(Message message, String dungeonName) {
        coreChatManager.getDungeonPlayerMap().get(dungeonName)
                .values()
                .forEach(s -> {
            s.getAsyncRemote().sendObject(message.toJson(), result -> {
                if (result.getException() != null) {
                    System.out.println("Unable to send message: " + result.getException());
                }
            });
        });
    }

    /**
     * @author Jan Stöffler
     * whisper sends a message to one specific person in the dungeon
     * @param message
     * @param dungeonName
     */
    private void whisper(Message message, String dungeonName) {
        coreChatManager.getDungeonPlayerMap().get(dungeonName).get(message.recipient)
                .getAsyncRemote()
                .sendObject(message.toJson(), result -> {
            if (result.getException() != null) {
                System.out.println("Unable to send message: " + result.getException());
            }
        });
    }

    /**
     * @author Jan Stöffler
     * roomBroadcast sends a message to all users in a specified room (specified in msg-object)
     * @param message
     * @param dungeonName
     */
    public void roomBroadcast(Message message, String dungeonName) {
        String userName = new ArrayList<>(coreChatManager.getDungeonRoomPlayerMap()
                .get(dungeonName)
                .get(Long.parseLong(message.room))
                .keySet()).get(0);
        String roomName = coreChatManager.getUserRoomMap().get(dungeonName).get(userName);
        long remRoom = Long.parseLong(message.room);
        coreChatManager.getDungeonRoomPlayerMap().get(dungeonName)
                .get(remRoom)
                .values()
                .forEach((session) -> {
            message.room = roomName;
            session.getAsyncRemote().sendObject(message.toJson(), result -> {
                if (result.getException() != null) {
                    System.out.println("Unable to send message: " + result.getException());
                }
            });
        });
    }
}
