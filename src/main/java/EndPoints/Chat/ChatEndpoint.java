package EndPoints.Chat;

import ChatManaging.CoreChatManager;
import DataBase.DBDungeonQueryImpl;
import DatabaseInterfaces.DBPlayerQuery;
import Exceptions.ElementNotFoundException;
import Model.Base.PlayerAvatar;
import Model.CommunicationModel.Message;
import Model.CommunicationModel.MessageType;
import Model.SecurityModel.MudUser;
import Exceptions.AccessDeniedException;
import Processing.DungeonQueryImpl;
import Processing.RoomQueryImpl;
import Security.MudSecurityManager;
import ServerInterfaces.DungeonQuery;
import ServerInterfaces.RoomQuery;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jboss.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.List;

@ServerEndpoint("/chat/{dungeonName}/{securityKey}")
@ApplicationScoped
/**
 * The ChatEndpoint provides Websockets for normal Players
 * ©Jan Stöffler
 */
public class ChatEndpoint {

    private RoomQuery roomQuery;
    private DungeonQuery dungeonQuery;

    private static final Logger LOG = Logger.getLogger(ChatEndpoint.class);

    @Inject
    public void setDungeonQuery(DungeonQueryImpl dungeonQuery) {
        this.dungeonQuery = dungeonQuery;
    }

    @Inject
    public void setRoomQuery(RoomQueryImpl roomQuery) {
        this.roomQuery = roomQuery;
    }

    @Inject
    CoreChatManager coreChatManager;

    @Inject
    MudSecurityManager mudSecurityManager;

    @Inject
    DBPlayerQuery playerQuery;

    @OnOpen
    public void onOpen(Session session, @PathParam("dungeonName") String dungeonName, @PathParam("securityKey") String securityKey) {
        LOG.info(dungeonName);
        LOG.info(securityKey);
        try {
            MudUser user = mudSecurityManager.decryptUser(securityKey);
            List<PlayerAvatar> list = playerQuery.getPlayerAvatars(dungeonName);
            String avatarName = "";
            for (PlayerAvatar avatar : list) {
                if (avatar.getOwnerUserName().equals(user.getUserName())) {
                    avatarName = avatar.getName();
                }
            }
            long id = playerQuery.getActualRoomGlobalId(dungeonName, user.getUserName());
            String name = roomQuery.getAPIRoom(dungeonName, id).displayName;
            coreChatManager.joinChat(session, dungeonName, user.getUserName(), avatarName, id, name);
            LOG.info(user.getUserName() + "joined the Chat");
            Message roomDes = new Message();
            String description = dungeonQuery.getDungeonDescription(dungeonName);
            if (description == null) {
                description = "You wake up in a dungeon, with no knowledge where you are and why you are here.";
            }
            Message joinMsg = new Message();
            joinMsg.type = MessageType.DungeonSystemMessage;
            joinMsg.content = description;
            roomDes.content = roomQuery
                    .getDungeonRoom(dungeonName, dungeonQuery.getStartRoomGlobalID(dungeonName)).getDescription();
            roomDes.type = MessageType.DungeonSystemMessage;
            session.getAsyncRemote().sendObject(joinMsg.toJson());
            session.getAsyncRemote().sendObject(roomDes.toJson());
            if (!coreChatManager.getMasterMap().containsKey(dungeonName)) {
                Message noDmMessage = new Message();
                noDmMessage.content = "There is no God present in this world. Your prayers won't be answered";
                noDmMessage.type = MessageType.DungeonSystemMessage;
                session.getAsyncRemote().sendObject(noDmMessage.toJson());
            }
        } catch (Exception e) {
            try {
                LOG.info(e.toString(), e);
                session.close(new CloseReason(CloseReason.CloseCodes.CANNOT_ACCEPT, "AccessDenied"));
            } catch (IOException io) {
                LOG.info(io.toString(), io);
            }
        }
    }

    @OnClose
    public void onClose(Session session, @PathParam("dungeonName") String dungeonName, @PathParam("securityKey") String securityKey) {
        try {
            MudUser mudUser = mudSecurityManager.decryptUser(securityKey);
            coreChatManager.remove(dungeonName, mudUser.getUserName());
            LOG.info(mudUser.getUserName() + " left the Chat");
        } catch (AccessDeniedException | ElementNotFoundException | IOException e) {
            LOG.info(e.toString(), e);
        }
    }

    @OnError
    public void onError(Session session, @PathParam("dungeonName") String dungeonName, @PathParam("securityKey") String securityKey, Throwable throwable) {
        try {
            MudUser mudUser = mudSecurityManager.decryptUser(securityKey);
            coreChatManager.getDungeonPlayerMap().get(dungeonName).get(mudUser.getUserName()).close();
            coreChatManager.remove(dungeonName, mudUser.getUserName());
            LOG.info("User " + mudUser.getUserName() + " left on error: " + throwable);
            LOG.info(throwable.toString(), throwable);
        } catch (AccessDeniedException | ElementNotFoundException | IOException e) {
            LOG.info(e.toString(), e);
        }
    }

    @OnMessage
    public void onMessage(String message, @PathParam("dungeonName") String dungeonName, @PathParam("securityKey") String securityKey) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            MudUser mudUser = mudSecurityManager.decryptUser(securityKey);
            String avatarName = coreChatManager.getUserDungeonMap().get(dungeonName).get(mudUser.getUserName());
            String roomName = coreChatManager.getUserRoomMap().get(dungeonName).get(mudUser.getUserName());
            try {
                Message msg = mapper.readValue(message, Message.class);
                msg.sender = avatarName;
                msg.room = roomName;
                switch (msg.type) {
                    case RoomChatMessage:
                        roomBroadcast(msg, dungeonName, mudUser.getUserName());
                        break;
                    case WhisperChatMessage:
                        whisper(msg, dungeonName, mudUser.getUserName());
                        break;
                    default:
                        //TODO implement
                }
            } catch (JsonProcessingException j) {
                LOG.info(j.toString(), j);
            }
        } catch (AccessDeniedException e) {
            LOG.info(e.toString(), e);
        }
    }

    /**
     * @param message     which was sent
     * @param dungeonName of the Sender's dungeon
     * @author Jan Stöffler
     * broadcast() sends the message to all Users, who are in the same dungeon as the Server
     */
    private void broadcast(String message, String dungeonName) {
        coreChatManager.getDungeonPlayerMap().get(dungeonName).values().forEach(s -> {
            s.getAsyncRemote().sendObject(message, result -> {
                if (result.getException() != null) {
                    System.out.println("Unable to send message: " + result.getException());
                }
            });
        });
    }

    /**
     * @param message     which was sent
     * @param dungeonName of the Sender's dungeon
     * @author Jan Stöffler
     * whisper() sends the message to a specified USer in the same room as the Sender
     */
    public void whisper(Message message, String dungeonName, String sender) {
        if (!message.recipient.equals("DM")) {
            String recipient = coreChatManager.getAvatarDungeonMap().get(dungeonName).get(message.recipient);
            coreChatManager.getDungeonRoomPlayerMap().get(dungeonName)
                    .values()
                    .forEach(map -> {
                        if (map.containsKey(sender) && map.containsKey(recipient)) {
                            map.get(recipient)
                                    .getAsyncRemote()
                                    .sendObject(message.toJson(), result -> {
                                        if (result.getException() != null) {
                                            System.out.println("Unable to send message: " + result.getException());
                                        }
                                    });
                            map.get(sender).getAsyncRemote().sendObject(message.toJson(), result -> {
                                if (result.getException() != null) {
                                    System.out.println("Unable to send message: " + result.getException());
                                }
                            });
                        }
                    });
        } else if (coreChatManager.getAvatarDungeonMap().get(dungeonName) != null
                && coreChatManager.getMasterMap().get(dungeonName) != null) {
            String masterName = coreChatManager.getMasterMap().get(dungeonName);
            if (coreChatManager.getDungeonPlayerMap().get(dungeonName).get(masterName) != null) {
                coreChatManager.getDungeonPlayerMap().get(dungeonName).get(masterName)
                        .getAsyncRemote()
                        .sendObject(message.toJson(), result -> {
                            if (result.getException() != null) {
                                System.out.println("Unable to send message: " + result.getException());
                            }
                        });
                coreChatManager.getDungeonRoomPlayerMap().get(dungeonName)
                        .values()
                        .forEach(map -> {
                            if (map.containsKey(sender))
                                map.get(sender)
                                        .getAsyncRemote()
                                        .sendObject(message.toJson(), result -> {
                                            if (result.getException() != null) {
                                                System.out.println("Unable to send message: " + result.getException());
                                            }
                                        });
                        });
            }
        }
    }

    /**
     * @param message     which was sent
     * @param dungeonName of the Sender's dungeon
     * @author Jan Stöffler
     * roomBroadcast() sends the message to all Users which are in the same room as the Sender
     */
    public void roomBroadcast(Message message, String dungeonName, String sender) {
        coreChatManager.getDungeonRoomPlayerMap().get(dungeonName).values().forEach(map -> {
            if (map.containsKey(sender)) {
                map.values().forEach(session -> {
                    session.getAsyncRemote().sendObject(message.toJson(), result -> {
                        if (result.getException() != null) {
                            System.out.println("Unable to send message: " + result.getException());
                        }
                    });
                });
            }
        });
        if (coreChatManager.getAvatarDungeonMap().get(dungeonName) != null
                && coreChatManager.getMasterMap().get(dungeonName) != null) {
            String masterName = coreChatManager.getMasterMap().get(dungeonName);
            if (coreChatManager.getDungeonPlayerMap().get(dungeonName).get(masterName) != null) {
                coreChatManager.getDungeonPlayerMap().get(dungeonName).get(masterName)
                        .getAsyncRemote()
                        .sendObject(message.toJson(), result -> {
                            if (result.getException() != null) {
                                System.out.println("Unable to send message: " + result.getException());
                            }
                        });
            }
        }
    }
}
