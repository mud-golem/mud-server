package EndPoints.RESTful;

import Annotations.Authenticated;
import ChatManaging.CoreChatManager;
import DataBase.DBDungeonQueryImpl;
import Exceptions.ElementNotFoundException;
import Model.CommunicationModel.*;
import Model.Base.Dungeon;
import Model.Base.PlayerAvatar;
import Processing.*;
import Security.MudSecurityManager;
import ServerInterfaces.*;
import org.jboss.logging.Logger;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Path("/dungeons")
/**
 * The DungeonEndpoint is a RESTful EndPoint which is adressed by the FronteEnd when a dungeon should be
 * created or deleted
 * ©Jan Stöffler
 */
public class DungeonEndPoint {

    private DungeonCreation dungeonCreation;
    private PlayerCreation playerCreation;
    private DungeonEditing dungeonEditing;
    private DungeonQuery dungeonQuery;
    private PlayerQuery playerQuery;
    private DungeonManagement dungeonManagement;

    @Inject
    public void setPlayerCreation(PlayerCreationImpl playerCreation) {
        this.playerCreation = playerCreation;
    }

    @Inject
    public void setDungeonCreation(DungeonCreationImpl dungeonCreation) {
        this.dungeonCreation = dungeonCreation;
    }

    @Inject
    public void setDungeonEditing(DungeonEditingImpl dungeonEditing) {
        this.dungeonEditing = dungeonEditing;
    }

    @Inject
    public void setDungeonQuery(DungeonQueryImpl dungeonQuery) {
        this.dungeonQuery = dungeonQuery;
    }

    @Inject
    public void setPlayerQuery(PlayerQueryImpl playerQuery) {
        this.playerQuery = playerQuery;
    }

    @Inject
    public void setDungeonManagement(DungeonManagementImpl dungeonManagement) {
        this.dungeonManagement = dungeonManagement;
    }

    @Inject
    MudSecurityManager mudSecurityManager;

    @Inject
    DBDungeonQueryImpl dungeonQuerry;

    @Inject
    CoreChatManager coreChatManager;

    public static final Logger LOG = Logger.getLogger(DungeonEndPoint.class);

    public static final String LOCATION = "https://alpha.mud-golem.com:1996/dungeons";

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Authenticated
    /**
     * createDungeon() is a method to create a dungeon, it is a post method
     * it can just be called by authenticated Users
     * to trigger it the user has to post a CreatedDungeonObject
     * @param CreatedDungeon
     * @return Response with http Codes
     */
    public Response createDungeon(CreatedDungeon createdDungeon) {
        try {
            String username = mudSecurityManager.getCurrentUser().getUserName();
            LOG.info(username);
            dungeonCreation.createDungeon(username, createdDungeon.uniqueName, createdDungeon.lobbyDescription);
            return Response
                    .status(201)
                    .build();
        } catch (ElementNotFoundException el) {
            LOG.info(el.toString(), el);
            return Response
                    .status(404)
                    .entity("dungeon not existing")
                    .build();
        }
    }

    @GET
    @Path("/{dungeonID}")
    @Produces(MediaType.APPLICATION_JSON)
    @Authenticated
    /**
     * getDungeon returns the specified dungeon
     * @param String dungeonID
     * @return Response
     */
    public Response getDungeon(@PathParam("dungeonID") String dungeonID) {
        LOG.info(dungeonID);
        try {
            APIDungeon result = dungeonQuery.getAPIDungeon(dungeonID);
            return Response
                    .ok(result)
                    .build();
        } catch (ElementNotFoundException el) {
            LOG.info(el.toString(), el);
            return Response
                    .status(404)
                    .entity("dungeon not found")
                    .build();
        } catch (Exception e) {
            LOG.info(e.toString(), e);
            return Response
                    .status(500, "Unexpected Error")
                    .entity("Unexpected Error")
                    .build();
        }
    }

    @PUT
    @Path("/{dungeonID}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    @Authenticated
    /**
     * editDungeon can edit a dungeon, by replacing values
     * @param String dungeonID
     * @param Dungeon with changes from Front-End
     * @return Response
     */
    public Response editDungeon(@PathParam("dungeonID") String dungeonID,
                                APIDungeon APIDungeon) {
        try {
            if (mudSecurityManager.isUserMasterInDungeon(dungeonID)) {
                dungeonManagement.setComDungeon(APIDungeon);
                return Response
                        .status(202, "edited dungeon")
                        .entity("edited dungeon")
                        .build();
            } else {
                return Response
                        .status(401, "No master")
                        .entity("No master")
                        .build();
            }
        } catch (Exception e) {
            LOG.info(e.toString(), e);
            return Response
                    .status(500, "Unexpected Error")
                    .entity("Unexpected Error")
                    .build();
        }
    }

    @GET
    @Path("/{dungeonID}/allAvatarNames")
    @Produces(MediaType.APPLICATION_JSON)
    @Authenticated
    public Response getAllAvatarNames(@PathParam("dungeonID") String dungeonName) {
        try {
            if (mudSecurityManager.isUserInDungeon(dungeonName)) {
                return Response
                        .ok(coreChatManager.getAllAvatarNamesInDungeon(dungeonName))
                        .build();
            } else {
                return Response
                        .status(401, "No master")
                        .entity("No master")
                        .build();
            }
        } catch (Exception e) {
            LOG.info(e.toString(), e);
            return Response
                    .status(500, "Unexpected Error")
                    .entity("Unexpected Error")
                    .build();
        }
    }

    @GET
    @Path("/{dungeonID}/allAvatarNamesSameRoom")
    @Produces(MediaType.APPLICATION_JSON)
    @Authenticated
    public Response getAllAvatarNamesSameRoom(@PathParam("dungeonID") String dungeonName) {
        try {
            if (mudSecurityManager.isUserInDungeon(dungeonName)) {
                String userName = mudSecurityManager.getCurrentUser().getUserName();
                return Response
                        .ok(coreChatManager.getAvatarNamesInSameRoom(dungeonName, userName))
                        .build();
            } else {
                return Response
                        .status(401, "Wrong dungeon")
                        .entity("Wrong dungeon")
                        .build();
            }
        } catch (Exception e) {
            LOG.info(e.toString(), e);
            return Response
                    .status(500, "Unexpected Error")
                    .entity("Unexpected Error")
                    .build();
        }
    }

    @GET
    @Path("/{dungeonID}/myAvatar")
    @Produces(MediaType.APPLICATION_JSON)
    @Authenticated
    /**
     * getMyAvatar() returns the name of the avatar of the user, if he has one in the dungeon
     * otherwise the method returns a 404 error
     * @param dungeonID
     * @return Response
     */
    public Response getMyAvatar(@PathParam("dungeonID") String dungeonID) {
        String username = mudSecurityManager.getCurrentUser().getUserName();
        try {
            APIAvatar avatar = playerQuery.getAPIAvatar(dungeonID, username);

            return Response
                    .ok(avatar)
                    .build();

        } catch (ElementNotFoundException e) {
            LOG.info("Cant find avatar for user ".concat(username).concat(" in dungeon ").concat(dungeonID));
            return Response
                    .status(404, "No avatar for that User")
                    .entity("No avatar found for that User")
                    .build();
        }


    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Authenticated
    /**
     * getAllDungeons() returns a List of Objects that can be visualized in the Front-End as dungeons/lobbies
     * every user gets slightly different results because of joining rights etc.
     * @return Response
     */
    public Response getAllDungeons() {
        try {
            List<LobbyDungeon> resultList =
                    dungeonQuery.getAllLobbyDungeons(mudSecurityManager.getCurrentUser().getUserName());
            return Response
                    .ok(resultList)
                    .build();
        } catch (ElementNotFoundException el) {
            LOG.info(el.toString(), el);
            return Response
                    .status(404, "user not found")
                    .entity("user not found")
                    .build();
        }
    }

    @GET
    @Path("/{dungeonID}/classes")
    @Produces(MediaType.APPLICATION_JSON)
    @Authenticated
    /**
     * getDungeonRaces returns a list of all Classes of a specified dungeon
     * @param String dungeonName
     * @return Response
     */
    public Response getDungeonClasses(@PathParam("dungeonID") String dungeonName) {
        try {
            List<APIAvatarClass> result = dungeonQuery.getAPIAvatarClasses(dungeonName);
            return Response
                    .ok(result)
                    .build();
        } catch (Exception e) {
            LOG.info(e.toString(), e);
            return Response
                    .status(500, "Unexpected Error")
                    .entity("Unexpected Error")
                    .build();
        }
    }

    @POST
    @Path("/{dungeonID}/classes")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    @Authenticated
    /**
     * createClassInDungeon create a new class in the specified dungeon
     * @param String dungeonName
     * @param AvatarClass that should be created
     * @return Response
     */
    public Response createClassInDungeon(@PathParam("dungeonID") String dungeonName,
                                         APIAvatarClass avatarClass) {
        try {
            if (mudSecurityManager.isUserMasterInDungeon(dungeonName)) {
                dungeonEditing.createAvatarClass(dungeonName, avatarClass.displayName, avatarClass.description, (int) avatarClass.HPModifier, true);
                return Response
                        .status(201, "created Class")
                        .entity("created Class")
                        .build();
            } else {
                return Response
                        .status(401, "No master")
                        .entity("No master")
                        .build();
            }
        } catch (Exception e) {
            LOG.info(e.toString(), e);
            return Response
                    .status(500, "Unexpected Error")
                    .entity("Unexpected Error")
                    .build();
        }
    }

    @GET
    @Path("/{dungeonID}/classes/{classID}")
    @Produces(MediaType.APPLICATION_JSON)
    @Authenticated
    /**
     * getDungeonRace gets a specified class of a specified dungeon
     * @param String dungeonName
     * @param String raceID
     * @return Response
     */
    public Response getDungeonClass(@PathParam("dungeonID") String dungeonName,
                                    @PathParam("classID") String classID) {
        try {
            APIAvatarClass result = dungeonQuery.getAPIAvatarClass(dungeonName, Long.parseLong(classID));
            return Response
                    .ok(result)
                    .build();
        } catch (Exception e) {
            LOG.info(e.toString(), e);
            return Response
                    .status(500, "Unexpected Error")
                    .entity("Unexpected Error")
                    .build();
        }
    }

    @PUT
    @Path("/{dungeonID}/classes/{classID}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Authenticated
    /**
     * @author Jan Stöffler
     * editDungeonClass can edit a class in a specified dungeon by replacing it.
     * @param dungeonID
     * @param raceID
     * @param APIAvatarClass
     * @return Response
     */
    public Response editDungeonClass(@PathParam("dungeonID") String dungeonName,
                                     @PathParam("classID") String raceID,
                                     APIAvatarClass avatarClass) {
        try {
            if (mudSecurityManager.isUserMasterInDungeon(dungeonName)) {
                dungeonEditing.editAvatarClass(dungeonName,
                        avatarClass.displayName,
                        avatarClass.description,
                        (int) avatarClass.HPModifier,
                        avatarClass.isEnabled,
                        avatarClass.globalID);
                return Response
                        .status(202, "edited class")
                        .entity("edited class")
                        .build();
            } else {
                return Response
                        .status(401, "No master")
                        .entity("No master")
                        .build();
            }
        } catch (Exception e) {
            LOG.info(e.toString(), e);
            return Response
                    .status(500, "Unexpected Error")
                    .entity("Unexpected Error")
                    .build();
        }
    }

    @GET
    @Path("/{dungeonID}/races")
    @Produces(MediaType.APPLICATION_JSON)
    @Authenticated
    /**
     * @author Jan Stöffler
     * getDungeonRaces returns a list of all Races of a specified dungeon
     * @param String dungeonID
     * @return Response
     */
    public Response getDungeonRaces(@PathParam("dungeonID") String dungeonName) {
        try {
            List<APIAvatarRace> result = dungeonQuery.getAPIAvatarRaces(dungeonName);
            return Response
                    .ok(result)
                    .build();
        } catch (Exception e) {
            LOG.info(e.toString(), e);
            return Response
                    .status(500, "Unexpected Error")
                    .entity("Unexpected Error")
                    .build();
        }
    }

    @POST
    @Path("/{dungeonID}/races")
    @Consumes(MediaType.APPLICATION_JSON)
    @Authenticated
    /**
     * @author Jan Stöffler
     * createRaceInDungeon create a new Race in the specified dungeon
     * @param String dungeonID
     * @param AvatarRace that should be created
     * @return Response
     */
    public Response createRaceInDungeon(@PathParam("dungeonID") String dungeonName,
                                        APIAvatarRace race) {
        try {
            if (mudSecurityManager.isUserMasterInDungeon(dungeonName)) {
                dungeonEditing.createAvatarRace(dungeonName, race.displayName, race.description, (int) race.HPModifier, true);
                return Response
                        .status(201, "created Race")
                        .entity("created Race")
                        .build();
            } else {
                return Response
                        .status(401, "No master")
                        .entity("No master")
                        .build();
            }
        } catch (Exception e) {
            LOG.info(e.toString(), e);
            return Response
                    .status(500, "Unexpected Error")
                    .entity("Unexpected Error")
                    .build();
        }
    }

    @GET
    @Path("/{dungeonID}/races/{raceID}")
    @Produces(MediaType.APPLICATION_JSON)
    @Authenticated
    /**
     * @author Jan Stöffler
     * getDungeonRace gets a specified Race of a specified dungeon
     * @param String dungeonName
     * @param String raceID
     * @return Response
     */
    public Response getDungeonRace(@PathParam("dungeonID") String dungeonName,
                                   @PathParam("raceID") String raceID) {
        try {
            APIAvatarRace result = dungeonQuery.getAPIAvatarRace(dungeonName, Long.parseLong(raceID));
            return Response
                    .ok(result)
                    .build();
        } catch (Exception e) {
            LOG.info(e.toString(), e);
            return Response
                    .status(500, "Unexpected Error")
                    .entity("Unexpected Error")
                    .build();
        }
    }

    @PUT
    @Path("/{dungeonID}/races/{raceID}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Authenticated
    /**
     * @author Jan Stöffler
     * editDungeonRace can edit a race in a specified dungeon by replacing it.
     * @param dungeonName
     * @param raceID
     * @param APIAvatarRace
     * @return Response
     */
    public Response editDungeonRace(@PathParam("dungeonID") String dungeonName,
                                    @PathParam("raceID") String raceID,
                                    APIAvatarRace race) {
        try {
            if (mudSecurityManager.isUserMasterInDungeon(dungeonName)) {
                dungeonEditing.editAvatarRace(dungeonName, race.displayName, race.description, (int) race.HPModifier, race.isEnabled, race.globalID);
                //processData
                return Response
                        .status(202, "edited Race")
                        .entity("edited Race")
                        .build();
            } else {
                return Response
                        .status(401, "No master")
                        .entity("No master")
                        .build();
            }
        } catch (Exception e) {
            LOG.info(e.toString(), e);
            return Response
                    .status(500, "Unexpected Error")
                    .entity("Unexpected Error")
                    .build();
        }
    }

    @GET
    @Path("/{dungeonID}/activeAvatars")
    @Produces(MediaType.APPLICATION_JSON)
    @Authenticated
    /**
     * @author Jan Stöffler
     * getAllActiveAvatars returns all active avatars in a dungeon
     * only performable if caller is DM
     * @param dungeonName
     * @return Response
     */
    public Response getAllActiveAvatars(@PathParam("dungeonID") String dungeonName) {
        try {
            if (mudSecurityManager.isUserMasterInDungeon(dungeonName)) {
                List<PlayerAvatarListEntry> result = new ArrayList<>();
                playerQuery.getPlayerAvatarListEntries(dungeonName).stream().forEach(avatar -> {
                    if (avatar.isActive) {
                        result.add(avatar);
                    }
                });
                return Response
                        .ok(result)
                        .build();
            } else {
                return Response
                        .status(401, "No master")
                        .entity("No master")
                        .build();
            }
        } catch (Exception e) {
            LOG.info(e.toString(), e);
            return Response
                    .status(500, "Unexpected Error")
                    .entity("Unexpected Error")
                    .build();
        }
    }

    @POST
    @Path("/copyDungeon")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    @Authenticated
    /**
     * @author Jan Stöffler
     * this method makes it possible to copy dungeons (specified in copyDungeon)
     * @param CopyRequest
     * @return Response
     */
    public Response copyDungeon(CopyRequest copyRequest) {
        try {
            String userName = mudSecurityManager.getCurrentUser().getUserName();
            dungeonCreation.copyDungeon(userName, copyRequest.originalDungeonName, copyRequest.newDungeonName);
            return Response
                    .status(201)
                    .build();
        } catch (Exception e) {
            LOG.info(e.toString(), e);
            return Response
                    .status(500, "Unexpected Error")
                    .entity("Unexpected Error")
                    .build();
        }
    }

    @DELETE
    @Path("/{dungeonID}")
    @Produces(MediaType.TEXT_PLAIN)
    @Authenticated
    /**
     * @author Jan Stöffler
     * this method makes it possible to delete dungeons
     * @param dungeonName
     * @return Response
     */
    public Response deleteDungeon(@PathParam("dungeonID") String dungeonName) {
        try {
            String userName = mudSecurityManager.getCurrentUser().getUserName();
            if (dungeonQuery.getAllowedDms(dungeonName).contains(userName)) {
                dungeonCreation.deleteDungeon(userName, dungeonName);
                return Response
                        .status(204)
                        .entity("deleted dungeon")
                        .build();
            }
            return Response
                    .status(401)
                    .entity("No permission")
                    .build();
        } catch (Exception e) {
            LOG.info(e.toString(), e);
            return Response
                    .status(500, "Unexpected Error")
                    .entity("Unexpected Error")
                    .build();
        }
    }

    @POST
    @Path("/{dungeonID}/requestJoin")
    @Produces(MediaType.TEXT_PLAIN)
    @Authenticated
    public Response requestJoin(@PathParam("dungeonID") String dungeonName) {
        try {
            String userName = mudSecurityManager.getCurrentUser().getUserName();
            //processData here
            return Response
                    .status(202)
                    .entity("requested join")
                    .build();
        } catch (Exception e) {
            LOG.info(e.toString(), e);
            return Response
                    .status(500, "Unexpected Error")
                    .entity("Unexpected Error")
                    .build();
        }
    }
}