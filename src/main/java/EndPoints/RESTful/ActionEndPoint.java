package EndPoints.RESTful;

import Annotations.Authenticated;
import DatabaseInterfaces.DBRoomQuery;
import Model.CommunicationModel.APIAction;
import Model.CommunicationModel.APICustomAction;
import Processing.ActionHandlerImpl;
import Processing.DungeonEditingImpl;
import Processing.DungeonQueryImpl;
import Security.MudSecurityManager;
import ServerInterfaces.ActionHandler;
import ServerInterfaces.DungeonEditing;
import ServerInterfaces.DungeonQuery;
import org.jboss.logging.Logger;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/dungeons/{dungeonName}/action")
/**
 * @author Jan Stöffler
 * The ActionEndPoint is the EndPoint where all Actions of players are handeld
 */
public class ActionEndPoint {

    private ActionHandler actionHandler;

    private DungeonEditing dungeonEditing;

    private DungeonQuery dungeonQuery;

    public static final Logger LOG = Logger.getLogger(ActionEndPoint.class);

    @Inject
    MudSecurityManager mudSecurityManager;

    @Inject
    public void setActionHandler(ActionHandlerImpl actionHandler) {
        this.actionHandler = actionHandler;
    }

    @Inject
    public void setDungeonEditing(DungeonEditingImpl dungeonEditing) {
        this.dungeonEditing = dungeonEditing;
    }

    @Inject
    public void setDungeonQuery(DungeonQueryImpl dungeonQuery) {
        this.dungeonQuery = dungeonQuery;
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    @Authenticated
    /**
     * @author Jan Stöffler
     * sendAction sends an action-Request to the backend where it is handeld
     * @param dungeonName
     * @param APIAction
     * @return Response
     */
    public Response sendAction(@PathParam("dungeonName") String dungeonName,
                               APIAction action) {
        try {
            if (mudSecurityManager.isUserInDungeon(dungeonName)) {
                String userName = mudSecurityManager.getCurrentUser().getUserName();
                actionHandler.doAction(dungeonName, userName, action.action);
                return Response
                        .status(202)
                        .entity("accepted Action")
                        .build();
            } else {
                return Response
                        .status(401, "Wrong dungeon")
                        .entity("Wrong dungeon")
                        .build();
            }
        } catch (Exception e) {
            LOG.info(e.toString(), e);
            return Response
                    .status(500, "Unexpected Error")
                    .entity("Unexpected Error")
                    .build();
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Authenticated
    /**
     * @author Jan Stöffler
     * getAllCustomActions returns a List of all available customActions in a dungeon
     * @param dungeonName
     * @rteturn Response
     */
    public Response getAllCustomActions(@PathParam("dungeonName") String dungeonName) {
        try {
            List<APICustomAction> allApiCustomActions = dungeonQuery.getAllAPICustomActions(dungeonName);
            return Response
                    .ok(allApiCustomActions)//objekt rein
                    .build();
        } catch (Exception e) {
            LOG.info(e.toString(), e);
            return Response
                    .status(500, "Unexpected Error")
                    .entity("Unexpected Error")
                    .build();
        }
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    @Authenticated
    /**
     * @author Jan Stöffler
     * addCustomAction adds a new Custom Action to a dungeon
     * @param dungeonName
     * @param APIAction
     * @return Response
     */
    public Response addCustomAction(@PathParam("dungeonName") String dungeonName,
                                    APICustomAction apiAction) {
        try {
            if (apiAction.name.equals("") || apiAction.command.equals("") || apiAction.response.equals("")) {
                return Response
                        .status(406)
                        .entity("The strings are not allowed to be empty here!")
                        .build();
            } else {
                dungeonEditing.addNewCustomAction(dungeonName, apiAction);
                return Response
                        .status(201)
                        .entity("")
                        .build();
            }
        } catch (Exception e) {
            LOG.info(e.toString(), e);
            return Response
                    .status(500, "Unexpected Error")
                    .entity("Unexpected Error")
                    .build();
        }
    }

    @PUT
    @Path("/{actionID}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    @Authenticated
    /**
     * @author Jan Stöffler
     * editCustomAction can edit a specified Custom action
     * @param dungeonName
     * @param actionID
     * @param APIAction
     * @return Response
     */
    public Response editCustomAction(@PathParam("dungeonName") String dungeonName,
                                     @PathParam("actionID") String actionID,
                                     APICustomAction apiAction) {
        try {
            dungeonEditing.updateExistingCustomAction(dungeonName, apiAction);
            return Response
                    .status(202)
                    .entity("")
                    .build();
        } catch (Exception e) {
            LOG.info(e.toString(), e);
            return Response
                    .status(500, "Unexpected Error")
                    .entity("Unexpected Error")
                    .build();
        }
    }

    @GET
    @Path("/{actionID}")
    @Produces(MediaType.APPLICATION_JSON)
    @Authenticated
    /**
     * @author Jan Stöffler
     * getCustomAction gets a sepcified custom Action with all its attributes
     * @param dungeonName
     * @param actionID
     * @param APIAction
     * @return Response
     */
    public Response getCustomAction(@PathParam("dungeonName") String dungeonName,
                                    @PathParam("actionID") String actionID,
                                    APIAction apiAction) {
        try {
            //getData()
            return Response
                    .ok()
                    .build();
        } catch (Exception e) {
            LOG.info(e.toString(), e);
            return Response
                    .status(500, "Unexpected Error")
                    .entity("Unexpected Error")
                    .build();
        }
    }
}
