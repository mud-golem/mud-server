package EndPoints.RESTful;

import Annotations.Authenticated;
import Model.Base.ObjectTemplate;
import Model.CommunicationModel.APIObjectTemplate;
import Processing.DungeonEditingImpl;
import Processing.DungeonQueryImpl;
import Security.MudSecurityManager;
import ServerInterfaces.DungeonEditing;
import ServerInterfaces.DungeonQuery;
import org.jboss.logging.Logger;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/dungeons/{dungeonName}/objects")
/**
 * @author Jan Stöffler
 * This EP can access all Objects and makes it possible to manage them
 */
public class ObjectEndPoint {

    private DungeonQuery dungeonQuery;
    private DungeonEditing dungeonEditing;

    public static final Logger LOG = Logger.getLogger(ObjectEndPoint.class);

    public static final String LOCATION = "https://alpha.mud-golem.com:1996/dungeons";

    @Inject
    MudSecurityManager mudSecurityManager;

    @Inject
    public void setDungeonQuery(DungeonQueryImpl dungeonQuery) {
        this.dungeonQuery = dungeonQuery;
    }

    @Inject
    public void setDungeonEditing(DungeonEditingImpl dungeonEditing) {
        this.dungeonEditing = dungeonEditing;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    @Authenticated
    /**
     * @author Jan Stöffler
     * creates a new Object, specified in the objectTemplate
     * @param dungeonName
     * @param APIObjectTemplate
     * @return Response
     */
    public Response createNewObjects(@PathParam("dungeonName") String dungeonName,
                                     APIObjectTemplate objectTemplate) {
        try {
            if(mudSecurityManager.isUserInDungeon(dungeonName)) {
                dungeonEditing.addNewObjectTemplate(dungeonName, objectTemplate);
                return Response
                        .status(201)
                        .build();
            } else {
                return Response
                        .status(401,"No master")
                        .entity("No master")
                        .build();
            }
        } catch (Exception e) {
            LOG.info(e.toString(), e);
            return Response
                    .status(500, "Unexpected Error")
                    .entity("Unexpected Error")
                    .build();
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Authenticated
    /**
     * @author Jan Stöffler
     * returns a list of all objectTemplates in a dungeon
     * @param dungeonName
     * @return Response
     */
    public Response getAllObjects(@PathParam("dungeonName") String dungeonName) {
        LOG.info(dungeonName);
        try {
            List<APIObjectTemplate> result = dungeonQuery.getObjectTemplates(dungeonName);
            return Response
                    .ok(result)
                    .build();
        } catch (Exception e) {
            LOG.info(e.toString(), e);
            return Response
                    .status(500, "Unexpected Error")
                    .entity("Unexpected Error")
                    .build();
        }
    }

    @GET
    @Path("/{objectID}")
    @Produces(MediaType.APPLICATION_JSON)
    @Authenticated
    /**
     * @author Jan Stöffler
     * gets a single object specified by its ID
     * @param dungeonName
     * @param objectID
     * @return Response
     */
    public Response getObject(@PathParam("dungeonName") String dungeonName,
                              @PathParam("objectID") String objectID) {
        try {
            APIObjectTemplate result = dungeonQuery.getObjectTemplate(dungeonName,Long.parseLong(objectID));
            return Response
                    .ok(result)
                    .build();
        } catch (Exception e) {
            LOG.info(e.toString(), e);
            return Response
                    .status(500, "Unexpected Error")
                    .entity("Unexpected Error")
                    .build();
        }
    }

    @PUT
    @Path("/{objectID}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    @Authenticated
    /**
     * @author Jan Stöffler
     * edits a object specified by its id and overwrites it
     * @param dungeonName
     * @param objectID
     * @param APIObjectTemplate
     * @return Response
     */
    public Response editObject(@PathParam("dungeonName") String dungeonName,
                               @PathParam("objectID") String objectID,
                               APIObjectTemplate objectTemplate) {
        try {
            if(mudSecurityManager.isUserInDungeon(dungeonName)) {
                dungeonEditing.updateExistingObjectTemplate(dungeonName, objectTemplate, Long.parseLong(objectID));
                return Response
                        .status(202)
                        .entity("")
                        .build();
            } else {
                return Response
                        .status(401,"No master")
                        .entity("No master")
                        .build();
            }
        } catch (Exception e) {
            LOG.info(e.toString(), e);
            return Response
                    .status(500, "Unexpected Error")
                    .entity("Unexpected Error")
                    .build();
        }
    }
}
