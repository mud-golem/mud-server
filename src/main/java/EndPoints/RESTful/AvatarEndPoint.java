package EndPoints.RESTful;

import Annotations.Authenticated;
import ChatManaging.CoreChatManager;
import Exceptions.ElementNotFoundException;
import Model.CommunicationModel.APIAvatar;
import Model.CommunicationModel.CreatedAvatar;
import Processing.PlayerCreationImpl;
import Processing.PlayerEditingImpl;
import Security.MudSecurityManager;
import ServerInterfaces.PlayerCreation;
import ServerInterfaces.PlayerEditing;
import Processing.PlayerQueryImpl;
import ServerInterfaces.PlayerQuery;
import org.jboss.logging.Logger;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

@Path("/dungeons/{dungeonID}/avatars")
/**
 * @author Jan Stöffler
 * This EP manages all Avatars in the System
 */
public class AvatarEndPoint {

    private PlayerQuery playerQuery;
    private PlayerCreation playerCreation;
    private PlayerEditing playerEditing;

    public static final Logger LOG = Logger.getLogger(ObjectEndPoint.class);

    public static final String LOCATION = "https://alpha.mud-golem.com:1996/dungeons";

    @Inject
    public void setPlayerEditing(PlayerEditingImpl playerEditing) {
        this.playerEditing = playerEditing;
    }

    @Inject
    MudSecurityManager mudSecurityManager;

    @Inject
    CoreChatManager coreChatManager;

    @Inject
    public void setPlayerQuery(PlayerQueryImpl playerQuery) {
        this.playerQuery = playerQuery;
    }

    @Inject
    public void setPlayerCreation(PlayerCreationImpl playerCreation) {
        this.playerCreation = playerCreation;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Authenticated
    /**
     * @author Jan Stöffler
     * createAvatar() creates an avatar with data from the Front-End and registers it in the Back-End
     * @param String dungeonID
     * @param CreatedAvatar avatar
     * @return Response
     */
    public Response createAvatar(@PathParam("dungeonID") String dungeonID,
                                 CreatedAvatar avatar) {
        try {
            String userName = mudSecurityManager.getCurrentUser().getUserName();
            playerCreation.createPlayerCharacter(dungeonID, userName, avatar.uniqueName, avatar.classID, avatar.raceID, avatar.userDescription);
            long ID =
                    playerQuery.getPlayerAvatar(dungeonID, mudSecurityManager.getCurrentUser().getUserName()).getGlobalId();
            String uri = LOCATION + dungeonID + "/avatars/" + ID;
            try {
                return Response
                        .status(201)
                        //.location(new URI(uri))
                        .build();
            } catch (Exception e) {
                LOG.info(e.toString(), e);
                return Response
                        .status(500, "Couldn't create Object at location " + uri)
                        .entity("Couldn't create Object at location " + uri)
                        .build();
            }
        } catch (ElementNotFoundException el) {
            LOG.info(el.toString(), el);
            return Response
                    .status(404)
                    .entity("dungeon not existing")
                    .build();
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Authenticated
    /**
     * @author Jan Stöffler
     * getAllAvatars returns a list of all avatars as APIAvatars
     * @param dungeonID
     * @return Response
     */
    public Response getAllAvatars(@PathParam("dungeonID") String dungeonID) {
        try {
            if (mudSecurityManager.isUserMasterInDungeon(dungeonID)) {
                List<APIAvatar> result =
                        playerQuery.getPlayerAvatars(dungeonID).stream().map(APIAvatar::new).collect(Collectors.toList());
                return Response
                        .ok(result)
                        .build();
            } else {
                return Response
                        .status(401, "No master")
                        .entity("No master")
                        .build();
            }
        } catch (Exception e) {
            LOG.info(e.toString(), e);
            return Response
                    .status(500, "Unexpected Error")
                    .entity("Unexpected Error")
                    .build();
        }
    }

    @GET
    @Path("/{avatarID}")
    @Produces(MediaType.APPLICATION_JSON)
    @Authenticated
    /**
     * @author Jan Stöffler
     * getAvatar returns a single APIAvatar object, which was requested
     * @param dungeonID
     * @param avatarID
     * @return Response
     */
    public Response getAvatar(@PathParam("dungeonID") String dungeonID,
                              @PathParam("avatarID") long avatarID) {
        try {
            APIAvatar result = playerQuery.getAPIAvatar(dungeonID, avatarID);
            return Response
                    .ok(result)
                    .build();
        } catch (Exception e) {
            LOG.info(e.toString(), e);
            return Response
                    .status(500, "Unexpected Error")
                    .entity("Unexpected Error")
                    .build();
        }
    }

    @PUT
    @Path("/{avatarID}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    @Authenticated
    /**
     * @author Jan Stöffler
     * editAvatar can edit a vatar which is specified by the avatarID
     * @param dungeonID
     * @param avatarID
     * @param APIAvatar
     * @return Response
     */
    public Response editAvatar(@PathParam("dungeonID") String dungeonID,
                               @PathParam("avatarID") long avatarID,
                               APIAvatar avatar) {
        try {
            if (mudSecurityManager.isUserMasterInDungeon(dungeonID)) {
                playerEditing.setAPIPlayer(dungeonID, avatar);
                return Response
                        .status(202)
                        .build();
            } else {
                return Response
                        .status(401, "No master")
                        .entity("No master")
                        .build();
            }
        } catch (Exception e) {
            LOG.info(e.toString(), e);
            return Response
                    .status(500, "Unexpected Error")
                    .entity("Unexpected Error")
                    .build();
        }
    }

    @DELETE
    @Path("/{avatarID}")
    @Produces(MediaType.TEXT_PLAIN)
    @Authenticated
    /**
     * @author Jan Stöffler
     * deleteCharacter makes it possible to delete an avatar specified by the avatarID
     * @paran dungeonName
     * @param avatarID
     * @return Response
     */
    public Response deleteCharacter(@PathParam("dungeonID") String dungeonName,
                                    @PathParam("avatarID") String avatarID) {
        try {
            String userName = mudSecurityManager.getCurrentUser().getUserName();
            long avatarIDtemp = Long.parseLong(avatarID);
            if(playerQuery.getPlayerAvatar(dungeonName,userName).getGlobalId() == Long.parseLong(avatarID)) {
                playerEditing.deletePlayerAvatar(avatarIDtemp, dungeonName);
                return Response
                        .status(204)
                        .build();
            }
            if (mudSecurityManager.isUserMasterInDungeon(dungeonName)) {
                playerEditing.deletePlayerAvatar(avatarIDtemp, dungeonName);
                return Response
                        .status(204)
                        .build();
            } else {
                return Response
                        .status(401)
                        .entity("no permission")
                        .build();
            }
        } catch (Exception e) {
            LOG.info(e.toString(), e);
            return Response
                    .status(500, "Unexpected Error")
                    .entity("Unexpected Error")
                    .build();
        }
    }
}
