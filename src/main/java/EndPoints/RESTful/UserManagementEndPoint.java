package EndPoints.RESTful;

import Annotations.Authenticated;
import Exceptions.NoValidUserNameException;
import Model.CommunicationModel.APIAction;
import Model.CommunicationModel.APIJoinRequestAnswer;
import Model.CommunicationModel.APINameObject;
import Processing.UserAccessManagementImpl;
import Security.MudSecurityManager;
import ServerInterfaces.UserAccessManagement;
import org.jboss.logging.Logger;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/dungeons")
public class UserManagementEndPoint {

    public static final Logger LOG = Logger.getLogger(UserManagementEndPoint.class);

    private UserAccessManagement userManagement;

    @Inject
    public void setUserManagement(UserAccessManagementImpl userManagement) {
        this.userManagement = userManagement;
    }

    @Inject
    MudSecurityManager mudSecurityManager;

    @POST
    @Path("/{dungeonID}/blacklist")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    @Authenticated
    public Response addToBlackList(@PathParam("dungeonID") String dungeonName,
                                   APINameObject obj) {
        try {
            if (mudSecurityManager.isUserMasterInDungeon(dungeonName)) {
                userManagement.setBlackList(dungeonName, obj.username);
                return Response
                        .status(201)
                        .entity("added user to bl")
                        .build();
            }
            return Response
                    .status(401)
                    .entity("You are no master")
                    .build();
        } catch (NoValidUserNameException n) {
            LOG.info(n.toString(), n);
            return Response
                    .status(406)
                    .entity("Invalid username")
                    .build();
        } catch (Exception e) {
            LOG.info(e.toString(), e);
            return Response
                    .status(500, "Unexpected Error")
                    .entity("Unexpected Error")
                    .build();
        }
    }

    @POST
    @Path("/{dungeonID}/whitelist")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    @Authenticated
    public Response addToWhiteList(@PathParam("dungeonID") String dungeonName,
                                   APINameObject obj) {
        try {
            if (mudSecurityManager.isUserMasterInDungeon(dungeonName)) {
                userManagement.setWhiteList(dungeonName, obj.username);
                return Response
                        .status(201)
                        .entity("added user to wl")
                        .build();
            }
            return Response
                    .status(401)
                    .entity("You are no master")
                    .build();
        } catch (NoValidUserNameException n) {
            LOG.info(n.toString(), n);
            return Response
                    .status(406)
                    .entity("Invalid username")
                    .build();
        } catch (Exception e) {
            LOG.info(e.toString(), e);
            return Response
                    .status(500, "Unexpected Error")
                    .entity("Unexpected Error")
                    .build();
        }
    }

    @POST
    @Path("/{dungeonID}/joinRequests")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    @Authenticated
    public Response addToJoinRequests(@PathParam("dungeonID") String dungeonName,
                                      APIAction apiAction) {
        try {
            String userName = mudSecurityManager.getCurrentUser().getUserName();
            userManagement.setJoinRequest(dungeonName, userName);
            return Response
                    .status(201)
                    .entity("added user to JR")
                    .build();
        } catch (Exception e) {
            LOG.info(e.toString(), e);
            return Response
                    .status(500, "Unexpected Error")
                    .entity("Unexpected Error")
                    .build();
        }
    }

    @POST
    @Path("/{dungeonID}/dmlist")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    @Authenticated
    public Response addToDmlist(@PathParam("dungeonID") String dungeonName,
                                APINameObject apiNameObject) {
        try {
            if (mudSecurityManager.isUserMasterInDungeon(dungeonName)) {
                userManagement.setNewDM(dungeonName, apiNameObject.username);
                return Response
                        .status(201)
                        .entity("added user to dmlist")
                        .build();
            }
            return Response
                    .status(401)
                    .entity("You are no master")
                    .build();
        } catch (Exception e) {
            LOG.info(e.toString(), e);
            return Response
                    .status(500, "Unexpected Error")
                    .entity("Unexpected Error")
                    .build();
        }
    }

    @GET
    @Path("/{dungeonID}/blacklist")
    @Produces(MediaType.APPLICATION_JSON)
    @Authenticated
    public Response getBlackList(@PathParam("dungeonID") String dungeonName,
                                 String userName) {
        try {
            if (mudSecurityManager.isUserMasterInDungeon(dungeonName)) {
                List<String> blacklist =
                        userManagement.getBlackList(dungeonName);
                return Response
                        .ok(blacklist)
                        .build();
            }
            return Response
                    .status(401)
                    .entity("You are no master")
                    .build();
        } catch (Exception e) {
            LOG.info(e.toString(), e);
            return Response
                    .status(500, "Unexpected Error")
                    .entity("Unexpected Error")
                    .build();
        }
    }

    @GET
    @Path("/{dungeonID}/whitelist")
    @Produces(MediaType.APPLICATION_JSON)
    @Authenticated
    public Response getWhiteList(@PathParam("dungeonID") String dungeonName,
                                 String userName) {
        try {
            if (mudSecurityManager.isUserMasterInDungeon(dungeonName)) {
                List<String> whitelist =
                        userManagement.getWhiteList(dungeonName);
                return Response
                        .ok(whitelist)
                        .build();
            }
            return Response
                    .status(401)
                    .entity("You are no master")
                    .build();
        } catch (Exception e) {
            LOG.info(e.toString(), e);
            return Response
                    .status(500, "Unexpected Error")
                    .entity("Unexpected Error")
                    .build();
        }
    }

    @GET
    @Path("/{dungeonID}/joinRequests")
    @Produces(MediaType.APPLICATION_JSON)
    @Authenticated
    public Response getJoinRequests(@PathParam("dungeonID") String dungeonName,
                                    String userName) {
        try {
            if (mudSecurityManager.isUserMasterInDungeon(dungeonName)) {
                List<String> joinRequests =
                        userManagement.getJoinRequestList(dungeonName);
                return Response
                        .ok(joinRequests)
                        .build();
            }
            return Response
                    .status(401)
                    .entity("You are no master")
                    .build();
        } catch (Exception e) {
            LOG.info(e.toString(), e);
            return Response
                    .status(500, "Unexpected Error")
                    .entity("Unexpected Error")
                    .build();
        }
    }

    @GET
    @Path("/{dungeonID}/dmlist")
    @Produces(MediaType.APPLICATION_JSON)
    @Authenticated
    public Response getDmlist(@PathParam("dungeonID") String dungeonName) {
        try {
            if (mudSecurityManager.isUserMasterInDungeon(dungeonName)) {
                List<String> dmlist =
                        userManagement.getDMList(dungeonName);
                return Response
                        .ok(dmlist)
                        .build();
            }
            return Response
                    .status(401)
                    .entity("You are no master")
                    .build();
        } catch (Exception e) {
            LOG.info(e.toString(), e);
            return Response
                    .status(500, "Unexpected Error")
                    .entity("Unexpected Error")
                    .build();
        }
    }

    @PUT
    @Path("/{dungeonID}/joinRequests/{userName}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    @Authenticated
    public Response processJoinRequest(@PathParam("dungeonID") String dungeonName,
                                       @PathParam("userName") String userName,
                                       APIJoinRequestAnswer answer) {
        try {
            if (mudSecurityManager.isUserMasterInDungeon(dungeonName)) {
                if (answer.isAccepted) {
                    userManagement.acceptJoinRequest(dungeonName, userName);
                    return Response
                            .status(202)
                            .entity("accepted user")
                            .build();
                } else {
                    userManagement.declineJoinRequest(dungeonName, userName);
                    return Response
                            .status(202)
                            .entity("declined user")
                            .build();
                }
            }
            return Response
                    .status(401)
                    .entity("You are no master")
                    .build();
        } catch (Exception e) {
            LOG.info(e.toString(), e);
            return Response
                    .status(500, "Unexpected Error")
                    .entity("Unexpected Error")
                    .build();
        }
    }

    @DELETE
    @Path("/{dungeonID}/blacklist/{userName}")
    @Produces(MediaType.TEXT_PLAIN)
    @Authenticated
    public Response removeFromBlackList(@PathParam("dungeonID") String dungeonName,
                                        @PathParam("userName") String userName) {
        try {
            if (mudSecurityManager.isUserMasterInDungeon(dungeonName)) {
                userManagement.removeBlackList(dungeonName, userName);
                return Response
                        .status(204)
                        .entity("removed user from bl")
                        .build();
            }
            return Response
                    .status(401)
                    .entity("You are no master")
                    .build();
        } catch (Exception e) {
            LOG.info(e.toString(), e);
            return Response
                    .status(500, "Unexpected Error")
                    .entity("Unexpected Error")
                    .build();
        }
    }

    @DELETE
    @Path("/{dungeonID}/whitelist/{userName}")
    @Produces(MediaType.TEXT_PLAIN)
    @Authenticated
    public Response removeFromWhiteList(@PathParam("dungeonID") String dungeonName,
                                        @PathParam("userName") String userName) {
        try {
            if (mudSecurityManager.isUserMasterInDungeon(dungeonName)) {
                userManagement.removeWhiteList(dungeonName, userName);
                return Response
                        .status(204)
                        .entity("removed user from wl")
                        .build();
            }
            return Response
                    .status(401)
                    .entity("You are no master")
                    .build();
        } catch (Exception e) {
            LOG.info(e.toString(), e);
            return Response
                    .status(500, "Unexpected Error")
                    .entity("Unexpected Error")
                    .build();
        }
    }

    @DELETE
    @Path("/{dungeonID}/dmlist/{userName}")
    @Produces(MediaType.TEXT_PLAIN)
    @Authenticated
    public Response removeFromDmlist(@PathParam("dungeonID") String dungeonName,
                                     @PathParam("userName") String userName) {
        try {
            String userNameCurrentDM = mudSecurityManager.getCurrentUser().getUserName();
            if(userNameCurrentDM.equals(userName)){
                return Response
                        .status(403)
                        .entity("You cannot remove yourself")
                        .build();
            } else if (mudSecurityManager.isUserMasterInDungeon(dungeonName)) {
                userManagement.removeDM(dungeonName, userName);
                return Response
                        .status(204)
                        .entity("removed user from dmlist")
                        .build();
            }
            return Response
                    .status(401)
                    .entity("You are no master")
                    .build();
        } catch (Exception e) {
            LOG.info(e.toString(), e);
            return Response
                    .status(500, "Unexpected Error")
                    .entity("Unexpected Error")
                    .build();
        }
    }
}
