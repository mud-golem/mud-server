package EndPoints.RESTful;

import Exceptions.MailErrorException;
import Communication.MailSender;
import Model.CommunicationModel.Key;
import Model.CommunicationModel.RegistrationForm;
import Model.SecurityModel.UserRegistrationObject;
import Processing.AuthProviderImpl;
import Exceptions.AccessDeniedException;
import Security.MudSecurityManager;
import ServerInterfaces.AuthProvider;
import org.apache.commons.codec.digest.DigestUtils;
import org.jboss.logging.Logger;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Date;
import java.util.concurrent.TimeUnit;

@Path("/register")
/**
 * @author Jan Stöffler
 * This EP has all REST-methods concerning the registration process
 */
public class RegistrationEndPoint {

    private static final Logger LOG = Logger.getLogger(RegistrationEndPoint.class);

    private AuthProvider authProvider;
    @Inject
    public void setAuthProvider(AuthProviderImpl authProvider){
        this.authProvider = authProvider;
    }

    @Inject
    MudSecurityManager mudSecurityManager;

    @Inject
    MailSender mailSender;

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    /**
     * @author Jan Stöffler
     *  request a registration, will send mail to the specified e-mail address
     *  specified in RgistrationForm
     * @param RegistrationForm
     * @return Response
     */
    public Response register(RegistrationForm form){
        try{
            UserRegistrationObject userRegistrationObject = new UserRegistrationObject(form.username, DigestUtils.sha256Hex(form.password),form.mailAddress);
            String key = mudSecurityManager.encryptRegistrationObject(userRegistrationObject);
            mailSender.sendRegistrationMail(key,form.mailAddress);
            LOG.info("Sent registration mail to " + form.mailAddress);
            return Response
                    .status(200,"sent Registration Mail")
                    .build();
        } catch (MailErrorException e) {
            LOG.info(e.toString(),e);
            return Response
                    .status(401)
                    .entity("Error with Mail")
                    .build();
        }
    }

    @POST
    @Path("/confirm")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    /**
     * @author Jan Stöffler
     * confirms thge registration of a user with the key sent to him via mail
     * @param Key
     * @return Response
     */
    public Response registerUserWithKey(Key key){
        try {
            LOG.info(key.key);
            UserRegistrationObject registrationObject = mudSecurityManager.decryptRegistrationObject(key.key);
            Date date = new Date();
            long timeStamp = date.getTime();
            long test = TimeUnit.MILLISECONDS.toHours(timeStamp - registrationObject.getTimeStamp());
            if(test <= 24) {
                if (authProvider.registerUser(registrationObject.getUserName(),registrationObject.getPassword(),registrationObject.getMailAddress())){
                    String securityKey = mudSecurityManager
                            .getSecurityKey(registrationObject.getUserName(), registrationObject.getPassword());
                    LOG.info("User " + registrationObject.getUserName() + " was registered successfully");
                    return Response
                            .ok(securityKey)
                            .build();
                }
            }
            LOG.info("A User tried to register with an outdated Link");
            return Response
                    .status(401,"Link is outdated")
                    .entity("Link is outdated")
                    .build();
        }catch(AccessDeniedException e){
            LOG.info(e.toString(),e);
            return Response
                    .status(402,"Access denied")
                    .entity("Access denied")
                    .build();
        } catch(Exception e){
            LOG.info(e.toString(),e);
            return Response
                    .status(404,"Other")
                    .entity("An unexpected failure occured")
                    .build();
        }
    }
}
