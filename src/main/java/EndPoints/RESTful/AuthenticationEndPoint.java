package EndPoints.RESTful;

import Exceptions.MailErrorException;
import Communication.MailSender;
import Exceptions.ElementNotFoundException;
import Model.CommunicationModel.LogInForm;
import Model.CommunicationModel.NewPasswordForm;
import Model.CommunicationModel.ResetPasswordForm;
import Model.SecurityModel.ResetPasswordObject;
import Processing.AuthProviderImpl;
import Exceptions.AccessDeniedException;
import ServerInterfaces.AuthProvider;
import Security.MudSecurityManager;
import org.apache.commons.codec.digest.DigestUtils;
import org.jboss.logging.Logger;


import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Date;
import java.util.concurrent.TimeUnit;

@Path("/login")
/**
 * @author Jan Stöffler
 * AuthenticationEndPoint is used to authenticate/login Players to the Game(the API)
 */
public class AuthenticationEndPoint {

    public static final Logger LOG = Logger.getLogger(AuthenticationEndPoint.class);

    @Inject
    MudSecurityManager mudSecurityManager;

    private AuthProvider authProvider;

    @Inject
    public void setAuthProvider(AuthProviderImpl authProvider) {
        this.authProvider = authProvider;
    }

    @Inject
    MailSender mailSender;

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    /**
     * @author Jan Stöffler
     * login() let users login in to the API-System so they can participate in a game
     * @param LogInForm
     * @return Response which includes the securityKey for the local storage
     */
    public Response login(LogInForm login) {
        try {
            //validate credentials
            if (authProvider.validateUserCredentials(login.username, DigestUtils.sha256Hex(login.password))) {
                final String securityKey = mudSecurityManager.verifyUserLoginAndGetSecurityKey(login.username, DigestUtils.sha256Hex(login.password));
                //return the security key, so it can be stored on client side
                return Response
                        .ok(securityKey)
                        .build();
            } else return Response
                    .status(401,"Wrong credentials")
                    .entity("Wrong credentials")
                    .build();
        } catch (AccessDeniedException e) {
            LOG.info(e.toString(),e);
            return Response
                    .status(401,"Wrong Credentials")
                    .entity("Wrong credentials")
                    .build();
        } catch (ElementNotFoundException el){
            LOG.info(el.toString(),el);
            return Response
                    .status(404)
                    .entity("no such user")
                    .build();
        }
    }

    @POST
    @Path("/changePassword")
    @Consumes(MediaType.APPLICATION_JSON)
    /**
     * @author Jan Stöffler
     * this method requests a password change for the specified user (specified in form)
     * @param ResetPasswordForm
     * @return Response
     */
    public Response requestPasswordChange(ResetPasswordForm form){
        //confirm mailAddress
        try{
            if(authProvider.getEmail(form.username).equals(form.mailAddress)) {
                ResetPasswordObject keyObject =
                        new ResetPasswordObject(form.mailAddress, form.username);
                String key = mudSecurityManager.encryptResetPasswordObject(keyObject);
                mailSender.sendChangePasswordMail(key, form.mailAddress);
                LOG.info("Sent registration mail to " + form.mailAddress + " with key " + key);
                return Response
                        .status(200, "sent Registration Mail")
                        .build();
            } else {
                return Response
                        .status(400)
                        .entity("Wrong mailAddress")
                        .build();
            }
        } catch (MailErrorException e) {
            LOG.info(e.toString(),e);
            return Response
                    .status(401)
                    .entity("Error with Mail")
                    .build();
        } catch (ElementNotFoundException e){
            LOG.info(e.toString(),e);
            return Response
                    .status(404)
                    .entity("The user has no MailAddress in the Database")
                    .build();
        }
    }

    @POST
    @Path("/changePassword/confirm")
    @Consumes(MediaType.APPLICATION_JSON)
    /**
     * @author Jan Stöffler
     * changes the password for the specified user (specified in form)
     * @param NewPasswordForm
     * @return Response
     */
    public Response changePassword(NewPasswordForm form){
        try {
            LOG.info(form.key);
            ResetPasswordObject reset = mudSecurityManager.decryptResetPasswordObject(form.key);
            Date date = new Date();
            long timeStamp = date.getTime();
            long test = TimeUnit.MILLISECONDS.toHours(timeStamp - reset.timeStamp);
            if(test <= 24){
                authProvider.changeUserPassword(reset.username,DigestUtils.sha256Hex(form.password));
                return Response
                        .ok()
                        .build();
            }
            return Response
                    .status(401,"Link is outdated")
                    .entity("Link is outdated")
                    .build();
        }catch(Exception e){
            LOG.info(e.toString(),e);
            return Response
                    .status(500,"Unexpected Error")
                    .entity("Unexpected Error")
                    .build();
        }
    }
}
