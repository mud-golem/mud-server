package EndPoints.RESTful;

import Annotations.Authenticated;
import Model.CommunicationModel.APIDungeonRoom;
import Model.CommunicationModel.DungeonRoomListEntry;
import Processing.DungeonEditingImpl;
import Processing.DungeonQueryImpl;
import Processing.RoomEditingImpl;
import Security.MudSecurityManager;
import ServerInterfaces.DungeonEditing;
import ServerInterfaces.DungeonQuery;
import ServerInterfaces.RoomEditing;
import ServerInterfaces.RoomQuery;
import org.jboss.logging.Logger;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Path("/dungeons/{dungeonName}/rooms")
/**
 * @author Jan Stöffler
 * The RoomEndPoint is responsible for all API-calls concerning rooms
 */
public class RoomEndPoint {

    private DungeonEditing dungeonEditing;
    private DungeonQuery dungeonQuery;
    private RoomQuery roomQuery;
    private RoomEditing roomEditing;

    @Inject
    public void setDungeonEditing(DungeonEditingImpl dungeonEditing) {
        this.dungeonEditing = dungeonEditing;
    }

    @Inject
    public void setDungeonQuery(DungeonQueryImpl dungeonQuery) {
        this.dungeonQuery = dungeonQuery;
    }

    @Inject
    public void setRoomQuery(RoomQuery roomQuery) {
        this.roomQuery = roomQuery;
    }

    @Inject
    public void setDungeonEditing(RoomEditingImpl roomEditing) {
        this.roomEditing = roomEditing;
    }

    @Inject
    MudSecurityManager mudSecurityManager;

    public static final Logger LOG = Logger.getLogger(RoomEndPoint.class);

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Authenticated
    /**
     * @author Jan Stöffler
     * getDungeonRooms gets all rooms of a dungeons as DungeonRoomListEntry
     * @param String dungeonName
     * @return Response
     */
    public Response getDungeonRooms(@PathParam("dungeonName") String dungeonName) {
        try {
            List<DungeonRoomListEntry> result = roomQuery.getDungeonRoomListEntries(dungeonName);
            return Response
                    .ok(result)
                    .build();
        } catch (Exception e) {
            LOG.info(e.toString(), e);
            return Response
                    .status(500, "Unexpected Error")
                    .entity("Unexpected Error")
                    .build();
        }
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Authenticated
    /**
     * @author Jan Stöffler
     * createRoomInDungeon creates a new room in the specified dungeon
     * @param String dungeonName
     * @param DungeonRoom (the room that should be created)
     * @return Response
     */
    public Response createRoomInDungeon(@PathParam("dungeonName") String dungeonName,
                                        APIDungeonRoom dungeonRoom) {
        try {
            if (mudSecurityManager.isUserMasterInDungeon(dungeonName) && !(dungeonRoom.displayName.equals("") || dungeonRoom.displayName == null)) {
                dungeonEditing.createRoom(dungeonName, dungeonRoom.displayName, dungeonRoom.description);
                return Response
                        .status(201)
                        .build();
            } else if(dungeonRoom.displayName.equals("") || dungeonRoom.displayName == null){
                return Response
                        .status(400)
                        .build();
            } else {
                return Response
                        .status(401, "No master")
                        .entity("No master")
                        .build();
            }
        } catch (Exception e) {
            LOG.info(e.toString(), e);
            return Response
                    .status(500, "Unexpected Error")
                    .entity("Unexpected Error")
                    .build();
        }
    }

    @PUT
    @Path("/{roomName}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    @Authenticated
    /**
     * @author Jan Stöffler
     * editRoomInDungeon edits the specified ressource
     * @param String dungeonName
     * @param String roomName
     * @param DungeonRoom with the new changes
     */
    public Response editRoomInDungeon(@PathParam("dungeonName") String dungeonName,
                                      @PathParam("roomName") String roomName,
                                      APIDungeonRoom apiRoom) {
        try {
            if (mudSecurityManager.isUserMasterInDungeon(dungeonName)) {
                roomEditing.setAPIRoom(dungeonName, apiRoom);
                return Response
                        .status(202, "edited room")
                        .entity("edited room")
                        .build();
            } else {
                return Response
                        .status(401, "No master")
                        .entity("No master")
                        .build();
            }
        } catch (Exception e) {
            LOG.info(e.toString(), e);
            return Response
                    .status(500, "Unexpected Error")
                    .entity("Unexpected Error")
                    .build();
        }
    }

    @GET
    @Path("/{roomName}")
    @Produces(MediaType.APPLICATION_JSON)
    @Authenticated
    /**
     * @author Jan Stöffler
     * getDungeonRoom returns a specified DungeonRoom
     * @param String dungeonName
     * @param String roomName
     * @return Response
     */
    public Response getDungeonRoom(@PathParam("dungeonName") String dungeonName,
                                   @PathParam("roomName") String roomName) {
        try {
            APIDungeonRoom result =
                    roomQuery.getAPIRoom(dungeonName, Long.parseLong(roomName));
            return Response
                    .ok(result)
                    .build();
        } catch (Exception e) {
            LOG.info(e.toString(), e);
            return Response
                    .status(500, "Unexpected Error")
                    .entity("Unexpected Error")
                    .build();
        }
    }

    @DELETE
    @Path("/{roomID}")
    @Produces(MediaType.TEXT_PLAIN)
    @Authenticated
    /**
     * @author Jan Stöffler
     * this method deltes a room specified by its ID
     * @param dungeonName
     * @param roomID
     * @return Response
     */
    public Response deleteRoom(@PathParam("dungeonName") String dungeonName,
                               @PathParam("roomID") String roomID) {
        try {
            if (mudSecurityManager.isUserMasterInDungeon(dungeonName)) {
                roomEditing.deleteRoom(dungeonName, Long.parseLong(roomID));
                return Response
                        .status(204)
                        .entity("deleted room")
                        .build();
            }
            return Response
                    .status(401)
                    .entity("No permission")
                    .build();
        } catch (Exception e) {
            LOG.info(e.toString(), e);
            return Response
                    .status(500, "Unexpected Error")
                    .entity("Unexpected Error")
                    .build();
        }
    }
}
