package Exceptions;

public class NoValidUserNameException extends Exception {
    public NoValidUserNameException(String username, Throwable innerException){
        super("Invalid username: " + username + " " + innerException);
    }

}
