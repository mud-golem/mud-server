package Exceptions;

public class RoomDirectionException extends Exception {
    public RoomDirectionException(String msg) {
        super(msg);
    }
}
