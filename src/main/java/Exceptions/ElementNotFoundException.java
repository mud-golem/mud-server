package Exceptions;

/**
 * Indicates one of the searched elements could not be found
 */
public class ElementNotFoundException extends Exception {
    public ElementNotFoundException(String elementName, String usedSearchKey){
        super(elementName+" used key"+ usedSearchKey);
    }
    public ElementNotFoundException(String elementName, String usedSearchKey, Throwable innerException){
        super(elementName+" used key"+ usedSearchKey, innerException);
    }

}
