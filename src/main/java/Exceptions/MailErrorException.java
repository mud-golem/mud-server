package Exceptions;

/**
 * The MailErrorException is thrown when there is a Problem with the Mailflow
 */
public class MailErrorException extends Exception {
}
