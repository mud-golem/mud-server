package Exceptions;

/**
 * The requested username is already taken
 */
public class UserNameTakenException extends Exception {
}
