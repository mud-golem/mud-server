package Exceptions;

public class EntryAlreadyExistsException extends Exception {
    public EntryAlreadyExistsException(String entry, Throwable innerStackTrace){
        super("Entry Already Exists: " + entry + " " + innerStackTrace);
    }
}
