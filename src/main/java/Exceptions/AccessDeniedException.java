package Exceptions;

/**
 *The AcccessDeniedException is thrown when a USer tries to login or register with invalid Credentials
 */
public class AccessDeniedException extends Exception {
}
