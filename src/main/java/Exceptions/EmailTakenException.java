package Exceptions;

/**
 * The requested Email is already taken
 */
public class EmailTakenException extends Exception {
}
