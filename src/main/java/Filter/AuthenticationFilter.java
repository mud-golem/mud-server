package Filter;

import Annotations.Authenticated;
import Security.MudSecurityManager;
import io.vertx.ext.web.handler.impl.HttpStatusException;

import javax.inject.Inject;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.RedirectionException;
import javax.ws.rs.client.ResponseProcessingException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import java.net.URI;
import java.net.URISyntaxException;

@Provider
@Authenticated
/**
 * @author Jan Stöffler
 * This class is a filter which is binded to the @Authenticated Annotation, it makes sure that a method just can be
 * called if a user is authenticated
 */
public class AuthenticationFilter implements ContainerRequestFilter, ContainerResponseFilter {

    @Inject
    MudSecurityManager mudSecurityManager;

    @Override
    public void filter(ContainerRequestContext requestContext) {
        try{
            mudSecurityManager.setCurrentUserFromSecurityKey(requestContext.getHeaderString("SecurityKey"));
        }catch(Exception e){
            throw new NotAuthorizedException(Response.status(401,"wrong credentials")
                    .entity("wrong credentials").build());
        }
    }

    @Override
    public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext) {
        //cleanup
        mudSecurityManager.clearCurrentUser();
    }

    private URI buildURI(String s){
        try{
            return new URI(s);
        }catch(URISyntaxException us){
            throw new IllegalArgumentException(us);
        }
    }
}
