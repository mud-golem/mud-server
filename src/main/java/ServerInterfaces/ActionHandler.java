package ServerInterfaces;

import Exceptions.ElementNotFoundException;

public interface ActionHandler {
    void doAction(String dungeonName, String username, String actionText) throws ElementNotFoundException;
}
