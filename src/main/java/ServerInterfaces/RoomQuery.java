package ServerInterfaces;

import Exceptions.ElementNotFoundException;
import Model.Base.DungeonRoom;
import Model.CommunicationModel.APIDungeonRoom;
import Model.CommunicationModel.DungeonRoomListEntry;

import java.util.List;

public interface RoomQuery {
    DungeonRoom getDungeonRoom(String dungeonName, long roomID) throws ElementNotFoundException;

    List<DungeonRoom> getDungeonRooms(String dungeonName) throws ElementNotFoundException;

    APIDungeonRoom getAPIRoom(String dungeonName, long roomID) throws  ElementNotFoundException;

    List<APIDungeonRoom> getAPIRooms(String dungeonName) throws ElementNotFoundException;

    DungeonRoomListEntry getDungeonRoomListEntry(String dungeonName, long roomID) throws ElementNotFoundException;

    List<DungeonRoomListEntry> getDungeonRoomListEntries(String dungeonName)throws ElementNotFoundException;

    List<String> getAllAvatarNamesInRoom(String dungeonName, long roomId) throws ElementNotFoundException;
}
