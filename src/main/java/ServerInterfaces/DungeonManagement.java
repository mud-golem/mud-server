package ServerInterfaces;

import Exceptions.ElementNotFoundException;
import Model.CommunicationModel.APIDungeon;

public interface DungeonManagement {
    void setComDungeon(APIDungeon APIDungeon) throws IllegalArgumentException, ElementNotFoundException;
    void setActualDM(String dungeonName, String userName) throws ElementNotFoundException;
    void removeDM(String dungeonName) throws ElementNotFoundException;
}
