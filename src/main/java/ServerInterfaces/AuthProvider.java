package ServerInterfaces;

import Exceptions.ElementNotFoundException;
import Exceptions.EmailTakenException;
import Exceptions.UserNameTakenException;

public interface AuthProvider {
    /**
     * verifies that a user with these credentials exists in the system
     */
    boolean validateUserCredentials(String UserName, String PWHashed) throws ElementNotFoundException;

    /**
     * registers user in the system
     * @throws UserNameTakenException
     * @throws EmailTakenException
     */
    boolean registerUser(String UserName, String PWHashed, String eMail) throws UserNameTakenException, EmailTakenException;

    /**
     * upadates the user password
     * @param username
     * @param pwHashedNew
     */
    void changeUserPassword(String username, String pwHashedNew) throws ElementNotFoundException;

    String getEmail(String username) throws ElementNotFoundException;
}
