package ServerInterfaces;

import DatabaseInterfaces.RoomDirection;
import Exceptions.ElementNotFoundException;
import Exceptions.RoomDirectionException;
import Model.CommunicationModel.APIDungeon;
import Model.CommunicationModel.APIDungeonRoom;
import Model.Base.InventoryEntry;

import java.util.List;

public interface RoomEditing {
    void setAPIRoom(String dungeonName, APIDungeonRoom apiDungeon) throws IllegalArgumentException, ElementNotFoundException, RoomDirectionException;

    void connectRooms(String dungeonName, long room1ID, RoomDirection direction, long room2ID) throws RoomDirectionException, ElementNotFoundException;
    void updateInventoryEntry(String dungeonName,long roomID, InventoryEntry inventory) throws ElementNotFoundException;
    boolean AddInventoryEntry(String dungeonName,long roomID, InventoryEntry inventory) throws ElementNotFoundException;
    void deleteRoom(String dungeonName, long roomGlobalId) throws ElementNotFoundException;
}
