package ServerInterfaces;

import Exceptions.ElementNotFoundException;
import Model.Base.PlayerAvatar;
import Model.CommunicationModel.APIAvatar;

public interface PlayerEditing {
    void setPlayer(String dungeonName, PlayerAvatar playerAvatar) throws ElementNotFoundException;

    void setAPIPlayer(String dungeonName, APIAvatar apiAvatar) throws  ElementNotFoundException;
    
    void setPlayerIsActive(String dungeonName, String username, boolean newValue) throws ElementNotFoundException;

    void setCurrRoom(String dungeonName, String username, long newCurrentRoomID) throws ElementNotFoundException;

    void setPlayerClassByID(long playerAvatarGlobalId, long classGlobalId) throws ElementNotFoundException;

    void setPlayerRaceByID(long playerAvatarGlobalId, long raceGlobalId) throws ElementNotFoundException;

    void deletePlayerAvatar(long globalAvatarId, String dungeonName) throws ElementNotFoundException;

}
