package ServerInterfaces;

import Exceptions.ElementNotFoundException;

public interface DungeonCreation {
    boolean createDungeon(String usernameDM, String dungeonName, String lobbyDescription) throws ElementNotFoundException;
    boolean copyDungeon(String usernameDM, String dungeonNameToCopy, String dungeonNameNew) throws ElementNotFoundException;
    boolean deleteDungeon(String usernameDM, String dungeonName) throws ElementNotFoundException;
    boolean importDungeon(String usernameDM, String dungeonSerializedString);

}
