package ServerInterfaces;

import Exceptions.ElementNotFoundException;
import Model.Base.CustomAction;
import Model.Base.ObjectTemplate;
import Model.CommunicationModel.APICustomAction;
import Model.CommunicationModel.APIObjectTemplate;

public interface DungeonEditing {
    void createAvatarClass(String dungeonName, String className, String classDescription, int hpModifier, boolean isEnabled) throws ElementNotFoundException;

    void editAvatarClass(String dungeonName, String className, String newClassDescription, int newHPModifier, boolean isEnabled, long globalID) throws ElementNotFoundException;

    void createAvatarRace(String dungeonName, String raceName, String raceDescription, int hpModifier, boolean isEnabled) throws ElementNotFoundException;

    void editAvatarRace(String dungeonName, String raceName, String newRaceDescription, int newHPModifier, boolean isEnabled, long globalID)throws ElementNotFoundException;

    void createRoom(String dungeonName, String displayRoom, String description) throws ElementNotFoundException;

    void createRoom(String dungeonName, String displayRoom, String description, long roomNorthGlobalID,
                    long roomEastGlobalID,long roomSouthGlobalID,long roomWestGlobalID) throws ElementNotFoundException;

    long addNewObjectTemplate(String dungeonName, APIObjectTemplate objectTemplate) throws ElementNotFoundException;

    void updateExistingObjectTemplate(String dungeonName, APIObjectTemplate objectTemplate, long templateGlobalId) throws ElementNotFoundException;

    long addNewCustomAction(String dungeonName, APICustomAction customAction) throws ElementNotFoundException;

    void updateExistingCustomAction(String dungeonName, APICustomAction customAction) throws ElementNotFoundException;

    void deleteDungeon(String dungeonName) throws ElementNotFoundException;

}
