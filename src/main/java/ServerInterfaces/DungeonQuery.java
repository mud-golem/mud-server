package ServerInterfaces;

import Exceptions.ElementNotFoundException;
import Model.Base.*;
import Model.Base.ObjectTemplate;
import Model.CommunicationModel.*;

import java.util.List;

public interface DungeonQuery {
    String getDungeonDescription(String dungeonName) throws ElementNotFoundException;
    String getLobbyDescription(String dungeonName) throws ElementNotFoundException;
    boolean getJoinable(String dungeonName) throws ElementNotFoundException;
    long getStartRoomGlobalID(String dungeonName) throws ElementNotFoundException;
    String getUnknownActionResponse(String dungeonName) throws ElementNotFoundException;
    List<String> getBlacklist(String dungeonName) throws ElementNotFoundException;
    List<String> getWhitelist(String dungeonName) throws ElementNotFoundException;
    List<String> getAllowedDms(String dungeonName) throws ElementNotFoundException;

    List<AvatarClass> getAvatarClasses(String dungeonName) throws ElementNotFoundException;
    List<APIAvatarClass> getAPIAvatarClasses(String dungeonName) throws ElementNotFoundException;
    AvatarClass getAvatarClass(String dungeonName, long avatarClassGlobalID) throws ElementNotFoundException;
    APIAvatarClass getAPIAvatarClass(String dungeonName, long avatarClassGlobalID) throws ElementNotFoundException;
    String getActualLoggedInDM(String dungeonName) throws ElementNotFoundException;

    List<AvatarRace> getAvatarRaces(String dungeonName) throws ElementNotFoundException;
    List<APIAvatarRace> getAPIAvatarRaces(String dungeonName) throws ElementNotFoundException;
    AvatarRace getAvatarRace(String dungeonName, long avatarRaceGlobalID) throws  ElementNotFoundException;
    APIAvatarRace getAPIAvatarRace(String dungeonName, long avatarRaceGlobalID) throws ElementNotFoundException;

    List<APIObjectTemplate> getObjectTemplates(String dungeonName) throws ElementNotFoundException;
    APIObjectTemplate getObjectTemplate(String dungeonName, long objectTemplateGlobalID) throws  ElementNotFoundException;
    List<CustomAction> getDungeonwideCustomActions(String dungeonName) throws ElementNotFoundException;
    List<APICustomAction> getDungeonwideAPICustomActions(String dungeonName) throws ElementNotFoundException;
    List<CustomAction> getAllCustomActions(String dungeonName) throws ElementNotFoundException;
    List<APICustomAction> getAllAPICustomActions(String dungeonName) throws ElementNotFoundException;
    List<String> getAllDungeonNames() throws ElementNotFoundException;
    List<APIDungeon> getAllAPIDungeons() throws ElementNotFoundException;
    APIDungeon getAPIDungeon(String dungeonName) throws ElementNotFoundException;
    List<LobbyDungeon> getAllLobbyDungeons(String userNameTryingToJoin) throws ElementNotFoundException;

}
