package ServerInterfaces;

import Exceptions.ElementNotFoundException;

public interface PlayerCreation {
    long createPlayerCharacter(String dungeonName, String ownerUserName, String avatarName, long classGlobalID, long raceGlobalID, String userDescription) throws ElementNotFoundException;
    boolean deletePlayerCharacter(String dungeonName, long playerAvatarScopedID) throws ElementNotFoundException;
}
