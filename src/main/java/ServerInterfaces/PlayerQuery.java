package ServerInterfaces;

import Exceptions.ElementNotFoundException;
import Model.Base.PlayerAvatar;
import Model.CommunicationModel.APIAvatar;
import Model.CommunicationModel.PlayerAvatarListEntry;

import java.sql.SQLException;
import java.util.List;

public interface PlayerQuery {
    /**
     * gets the Avatar assosiated with the user in this particular dungeon
     * @param dungeonName
     * @param userName
     */
    PlayerAvatar getPlayerAvatar(String dungeonName, String userName) throws ElementNotFoundException;

    APIAvatar getAPIAvatar(String dungeonName, String userName) throws  ElementNotFoundException;

    APIAvatar getAPIAvatar(String dungeonName, long globalId) throws ElementNotFoundException;

    List<PlayerAvatarListEntry> getPlayerAvatarListEntries(String dungeonName) throws  ElementNotFoundException;

    List<PlayerAvatar> getPlayerAvatars(String dungeonName) throws ElementNotFoundException;

    long getActualRoomGlobalId(String dungeonName, String userName) throws ElementNotFoundException;

    PlayerAvatar getPlayerAvatarWithAvatarName(String dungeonName, String avatarName) throws ElementNotFoundException;

    PlayerAvatar getPlayerAvatar(String dungeonName, long globalAvatarId) throws ElementNotFoundException;
}
